﻿Your Game Title
===============

CONTACT:

Homepage: 
Name: 
Team: 
Members: 


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

Move the cursor around the screen with the mouse.

Press the left mouse button to fire the ducks.



LICENSE:

This game skellington has same license as pyknic.


========= remove from here ===================================================

FOR DEVELOPERS:

- put your code into the gamelib/main.py
- put your data (images, sound, etc.) into data/
- the working directory is set to the directory where the run_game.py is
- fill out this readme.txt so it describes your game
- remove the unwanted run_game* files (run_game_debug.py should be removed also the py2exe script)
- use the make_exe.py (uses py2exe) script at own risk
  - fill in the fields in the make_exe.py
  - run it (it will include the pygame *.dll files and default font file)


