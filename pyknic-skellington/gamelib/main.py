# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

logger = logging.getLogger(__name__)


def main(log_level):  # todo maybe pass in more by arguments?
    # put here your code

    # import pyknic
    # pyknic.logs.log_environment()
    # pyknic.logs.print_logging_hierarchy()

    raise NotImplementedError("Implement your code here!")


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main(logging.DEBUG)
