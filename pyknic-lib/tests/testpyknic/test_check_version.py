# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.info module.
"""
from __future__ import print_function, division

import unittest
import warnings

import pyknic.info as mut  # module under test

warnings.simplefilter('always')  # make warnings visible for developers


__version__ = '6.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class CheckVersionTests(unittest.TestCase):

    # noinspection PyMethodMayBeStatic
    def test_valid_version(self):
        # act / verify
        mut.check_version("test_valid_version", (1, 0), (2, 0), (3, 0))

    # noinspection PyMethodMayBeStatic
    def test_same_upper_version(self):
        # act / verify
        # mut.check_version("test_same_upper_version", (1, 0), (3, 0), (3, 0))
        # arrange
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            # act
            mut.check_version("test_too_upper_version", (1, 0), (3, 0), (3, 0))

            # verify
            self.assertEqual(1, len(w))

    # noinspection PyMethodMayBeStatic
    def test_same_lower_version(self):
        # act / verify
        mut.check_version("test_same_lower_version", (1, 0), (1, 0), (3, 0))

    def test_too_lower_version(self):
        # act / verify
        self.assertRaises(mut.VersionMismatchError, mut.check_version, "test_too_lower_version", (1, 0), (0, 5), (3, 0))

    def test_too_upper_version(self):
        # arrange
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            # act
            mut.check_version("test_too_upper_version", (1, 0), (4, 5), (3, 0))

            # verify
            self.assertEqual(1, len(w))

    def test_that_exact_upper_version_with_minor_raises(self):
        # arrange
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            # act
            mut.check_version("test_too_upper_version", (1, 0), (3, 5), (3, 0))

            # verify
            self.assertEqual(1, len(w))

    def test_that_excluded_version_raise_an_version_mismatch_exception(self):
        # act / verify
        self.assertRaises(mut.VersionMismatchError, mut.check_version, "lib", 1, 3, 6, [3])

    def test_that_excluded_versions_raise_an_version_mismatch_exception(self):
        # act / verify
        self.assertRaises(mut.VersionMismatchError, mut.check_version, "lib", (1, 0), (4, 0), (6, 0), [(2,1), (3,0), (4,0)])

    def test_that_lower_excluded_version_is_in_range_of_lower_and_upper_versions(self):
        # act / verify
        self.assertRaises(ValueError, mut.check_version, "lib", 2, 3, 6, [1])

    def test_that_upper_excluded_version_is_in_range_of_lower_and_upper_versions(self):
        # act / verify
        self.assertRaises(ValueError, mut.check_version, "lib", 2, 3, 6, [9])

    def test_that_all_versions_provided_are_of_the_same_type(self):
        # act / verify
        self.assertRaises(ValueError, mut.check_version, "lib", 2, 3, 6, [9.0])


if __name__ == '__main__':
    unittest.main()
