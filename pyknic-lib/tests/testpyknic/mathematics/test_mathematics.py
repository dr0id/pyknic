#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function, division

import sys
import unittest
import warnings
from math import sqrt

from pyknic import mathematics
from pyknic.mathematics import Vec2, Vec3, sign, is_other_in_front, Point2, Point3, is_in_view_field, distance_from_line

warnings.simplefilter('error')  # make warnings visible for developers


class TestVector2(unittest.TestCase):

    def test_vector2D(self):
        Vec = Vec2
        v = Vec(2, 2)
        w = Vec(3, 5)
        self.assertTrue(v == Vec(2 + sys.float_info.epsilon, 2 + sys.float_info.epsilon), u'equal failed')
        self.assertTrue(v + w == Vec(5, 7), u'add failed')
        self.assertTrue(w - v == Vec(1, 3), u'sub failed')
        self.assertTrue(2 * v == Vec(4, 4), u'rmul failed')
        self.assertTrue(v * 3 == Vec(6, 6), u'mul failed')
        self.assertTrue(v.dot(w) == 16, u'dot failed')

        z = Vec(0,0)
        z.copy_values(v)
        z *= 4
        self.assertTrue(z == Vec(8, 8) , u'imul failed')
        self.assertTrue(z / 2 == Vec(4, 4), u'div failed')
        z /= 2
        self.assertTrue(z == Vec(4, 4), u'idiv failed')
        self.assertTrue(Vec(3, 4).length == 5, u'length failed')
        self.assertTrue(Vec(4, 3).length == 5, u'length failed')
        self.assertTrue(Vec(0, 0).length == 0, u'length failed')
        self.assertTrue(Vec(5, 2).length_sq == 29, u'lengsq failed')
        self.assertTrue(Vec(2, 5).length_sq == 29, u'lengsq failed')
        v += w
        self.assertTrue(v == Vec(5, 7), u'iadd failed')
        v -= w
        self.assertTrue(v == Vec(2, 2), u'isub failed')

        self.assertTrue(v.normal_left == Vec(-2, 2), u'normal_L failed')
        self.assertTrue(v.normal_right == Vec(2, -2), u'normal_R failed')
        self.assertTrue(v.normalized == v / v.length, u'normalized failed')
        n = v.normalized
        v.normalize()
        self.assertTrue(v == n, u'normalize failed')

        self.assertTrue(Vec(1, 1).project_onto(Vec(1, 0)) == Vec(1, 0), u'project_onto failed')
        self.assertTrue(Vec(-1, 1).project_onto(Vec(0, 1)) == Vec(0, 1), u'project_onto failed')

        self.assertTrue(Vec(1, -1).reflect(Vec(0, 1)) == Vec(1, 1), u'reflect failed')
        self.assertTrue(Vec(1, -1).reflect_tangent(Vec(1, 0)) == Vec(1, 1), u'reflect_tangent failed')

        v = Vec(1, 1)
        w = Vec(3, 3)
        v.copy_values(w)
        w.x = 100
        w.y = 100
        self.assertTrue(v.x == 3, u'value for x failed')
        self.assertTrue(v.y == 3, u'value for y failed')
        v = Vec(2, 2)
        w = Vec(0, 2)
        self.assertTrue(v.cross(w).z == 4)
        self.assertTrue(w.cross(v).z == -4)

        v = Vec(1, 0)
        v.rotate(90)
        v.round(round, 4)
        self.assertTrue(v == Vec(0.0, 1.0), u'rotate angle is wrong')
        v = Vec(1, 0)
        v.rotate(-90)
        v.round(round, 5)
        self.assertTrue(v == Vec(0, -1), u'rotate -angle is wrong')
        self.assertTrue(Vec(1, 0).rotated(90).rounded(round, 5) == Vec(0, 1), u'rotated angel is wrong')
        self.assertTrue(Vec(1, 0).rotated(-90).rounded(round, 5) == Vec(0, -1), u'rotated -angel is wrong')
        self.assertTrue(Vec(1, 0).rotated(90).angle == 90, u'wrong angle')
        self.assertTrue(Vec(1, 0).rotated(-90).angle == 360-90, u'wrong -angle')
        v = Vec(1, 0)
        v.rotate_to(90)
        v.round(round, 5)
        self.assertTrue(v == Vec(0, 1), u'wrong rotate_to angle')

        v = Vec(1,0)
        w = Vec(0,1)
        self.assertTrue(v.get_angle_between(w) == 90)
        self.assertTrue(w.get_angle_between(v) == 90)
        self.assertTrue(round(w.get_full_angle_between(v)) == -90)
        self.assertTrue(round(v.get_full_angle_between(w)) == 90)
        w = Vec(1,1)
        self.assertTrue(round(w.get_angle_between(v)) == 45.0)
        self.assertTrue(round(v.get_angle_between(w)) == 45.0)

        self.assertTrue(Vec2.zero() == Vec2(0,0))
        self.assertTrue(Vec2.unit_x() == Vec2(1,0))
        self.assertTrue(Vec2.unit_y() == Vec2(0,1))
        # self.assertTrue(Vec2.unit_z() == Vec3(0,0))

        v = Vec(1,2)
        v.set_from_iterable([3, 2, 1, 0, -1])
        self.assertTrue(v.x==3 and v.y==2)
        v = Vec(1,2)
        v.set_from_iterable([3])
        self.assertTrue(v.x==3 and v.y==2)
        self.assertTrue(len(v) == 2)
        v = Vec(-1, -2)
        self.assertTrue(+v == Vec(1, 2))
        v = Vec(1,2)
        w = Vec(2,1)
        self.assertTrue(not (v == w))
        self.assertTrue(v != w)
        self.assertTrue(v[0] == 1 and v[1] == 2)
        it = iter(v)
        self.assertTrue(next(it) == 1 and next(it) == 2)
        self.assertTrue(v.as_tuple() == (1, 2))
        w = Vec(1.2, 1.8)
        self.assertTrue(w.as_tuple(round, 0) == (1, 2))

        w = v.scaled(2)
        v.length = 2
        self.assertTrue(w == v)

        v = Vec.zero()
        w = v.scaled(2)
        v.length = 0
        self.assertTrue(w == v)

        v = Vec(0, 0)
        w = Vec(10, 10)
        self.assertTrue(round(v.get_distance(w), 3) == round(10 * (2**0.5), 3))
        self.assertTrue(round(v.get_distance_sq(w), 3) == 200)
        v = Vec(1.11191, 1.11191)
        v.round(round, 3)
        self.assertTrue(v == Vec(1.112, 1.112))

        self.assertTrue(Vec(0, 0).normalized == Vec(0, 0))
        self.assertTrue(-Vec(1,2) == Vec(-1, -2))
        self.assertTrue(Vec(0, 0).get_angle_between(Vec(0, 0)) == 0)
        self.assertTrue(Vec(0, 0).get_full_angle_between(Vec(0, 0)) == 0)

        self.assertTrue(Vec(1, 1).normal_right == Vec(1, -1))
        self.assertTrue(Vec(1, 1).normal_left == Vec(-1, 1))
        self.assertTrue(round(Vec(1, 1).angle)==45)
        self.assertTrue(Vec(4, 5).as_tuple() == (4, 5))
        self.assertTrue(not Vec(1,1) == 1)
        self.assertTrue(len(str(Vec(1,1))) > 0)

        v = Vec(1.0, 2.0)
        w = Vec(1.0, 2.0)
        self.assertFalse(hash(v) == hash(w), "hash values should not be the same")

        v = Vec(1.0, 1.0)
        w = Vec(1.0, 0.0)
        v.face_forward(w)
        self.assertTrue(v==Vec(1.0, 1.0), "face_forward didn't work")
        w = Vec(-1.0, 0.0)
        v.face_forward(w)
        self.assertTrue(v==Vec(-1.0, -1.0), "face_forward didn't work")

        v = Vec(1.0, 1.0)
        n = Vec(0.0, -1.0)
        r = v.face_forwarding(n)
        v.face_forward(n)
        self.assertTrue(r == v, "face_forwarding din't work")


    def test_refraction2D(self):
        Vec = Vec2
        # import decimal
        # # emulate python 2.7 'from_float'
        # setattr(decimal.Decimal, 'from_float', classmethod(lambda cls, x: decimal.Decimal(str(x))))

        # normal = Vec(decimal.Decimal.from_float(0.0), decimal.Decimal.from_float(-1.0))
        # light = Vec(decimal.Decimal.from_float(1.0), decimal.Decimal.from_float(0.0)).rotated(decimal.Decimal.from_float(45.237740459293))
        # ratio = decimal.Decimal.from_float().from_float(0.75) # 1.0 / 1.33  ~ air / water


        normal = Vec(0.0, -1.0)
        light = Vec(1.0, 0.0).rotated(45.237740459293)
        ratio = 0.75 # 1.0 / 1.33  ~ air / water

        ref = light.refract(normal, ratio)

        ref.round(round, 2)
        expected = Vec(1.0, 0.0).rotated(90.0 - 32.2345838693056)
        expected.round(round, 2)
        # expected.rotate_to(90-32)
        self.assertTrue(ref == expected, "refracted ray is not as expected: {0} expected: {1}".format(ref, expected))

        light2 = Vec(1.0, 0.0).rotated(41.0)
        # print('??? light2', str(light2))
        result = light2.refract(normal, 1.33)

        self.assertTrue(result.length == 0, "should be totally reflected, result: {0}".format(result))

    def test_rotation2D(self):
        Vec = Vec2
        v = Vec(1, 1)
        n = Vec(-1, -1)
        r = v.rotated(180)
        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(n.y, 5), u'rotated failed')

        v = Vec(1, 1)
        n = Vec(-1, -1)
        r = v.rotated(-180)
        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(n.y, 5), u'rotated failed')

        v = Vec(1, 0)
        n = Vec(0, -1)
        r = v.rotated(-90)
        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(n.y, 5), u'rotated failed')

        v = Vec(1, 0)
        n = Vec(0, 1)
        r = v.rotated(90)
        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(n.y, 5), u'rotated failed')

        v = Vec(1, 0)
        n = Vec(1, 0)
        r = v.rotated(0)
        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(n.y, 5), u'rotated failed')

        v = Vec(1, 0)
        n = Vec(1, 1).normalized
        r = v.rotated(45).normalized
        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(n.y, 5), u'rotated failed')
        r = Vec(1, 0)
        r.rotate(-45)

        self.assertTrue(round(r.x, 5) == round(n.x, 5) and round(r.y, 5) == round(-n.y, 5), u'rotate failed')

        v = Vec(1.0, 0.0).rotated(45)
        v.rotate_to(90)
        self.assertTrue(round(v.x, 5) == 0.0 and round(v.y, 5) == 1.0, u'rotate failed')


class TestPoint2(unittest.TestCase):

    def setUp(self):
        self.point = mathematics.Point2(1.1, 2.2)
        self.point_6x = mathematics.Point2(6 * 1.1, 6 * 2.2)
        self.point_div_6 = mathematics.Point2(1.1 / 6, 2.2 / 6)

    def test_point(self):
        self.assertEqual(self.point.w, mathematics.TYPE_POINT, "point has should have a point w coordinate")

    def test_imul(self):
        # act
        self.point *= 6

        # verify
        self.assertEqual(self.point_6x, self.point)
        self.assertEqual(self.point.w, mathematics.TYPE_POINT)

    def test_mul(self):
        # act
        actual = 6 * self.point

        # verify
        self.assertEqual(self.point_6x, actual)
        self.assertEqual(actual.w, mathematics.TYPE_POINT)

    def test_rmul(self):
        # act
        actual = self.point * 6

        # verify
        self.assertEqual(self.point_6x, actual)
        self.assertEqual(actual.w, mathematics.TYPE_POINT)

    def test_idiv(self):
        # act
        self.point /= 6

        # verify
        self.assertEqual(self.point_div_6, self.point)
        self.assertEqual(self.point.w, mathematics.TYPE_POINT)

    def test_div(self):
        # act
        actual = self.point / 6

        # verify
        self.assertEqual(self.point_div_6, actual)
        self.assertEqual(actual.w, mathematics.TYPE_POINT)

    def test_truediv(self):
        # act
        actual = self.point / 6

        # verify
        self.assertEqual(self.point_div_6, actual)
        self.assertEqual(actual.w, mathematics.TYPE_POINT)


class TestPoint3(TestPoint2):

    def setUp(self):
        self.point = mathematics.Point3(1.1, 2.2, 3.3)
        self.point_6x = mathematics.Point3(6 * 1.1, 6 * 2.2, 6 * 3.3)
        self.point_div_6 = mathematics.Point3(1.1 / 6, 2.2 / 6, 3.3 / 6)


class TestFunctions2D(unittest.TestCase):

    def setUp(self):
        self.p = Point2(1.0, 1.0)
        self.n = Vec2(1.0, 1.0).normalized
        self.v = Vec2(3, 3)
        self.o_front = Point2(3.0, 2.0)
        self.o_back = Point2(0.5, 0.75)
        self.orthogonal_n = self.n.normal_left
        self.v_class = Vec2

    def test_other_is_in_front(self):
        # arrange

        # act
        actual = is_other_in_front(self.p, self.n, self.o_front)

        # verify
        self.assertTrue(actual)

    def test_other_is_in_back(self):
        # arrange

        # act
        actual = is_other_in_front(self.p, self.n, self.o_back)

        # verify
        self.assertFalse(actual)

    def test_other_is_in_viewfield(self):
        # arrange

        # look = Vec2(1.0, 0.0)
        # rot = look.clone()
        # for i in range(360):
        #     r = rot.rotated(i)
        #     r.normalize()
        #     print(i, r.angle, r.dot(look), r.get_angle_between(look))

        # act
        actual = is_in_view_field(self.p, self.n, self.o_front, 45.0)

        # verify
        self.assertTrue(actual)

    def test_other_is_not_in_viewfield(self):
        # arrange

        # act
        actual = is_in_view_field(self.p, self.n, self.o_front, 1.0)

        # verify
        self.assertFalse(actual)


    def test_other_is_straight_in_front(self):
        # arrange
        straight = self.p + self.n * 5

        # act
        actual = is_in_view_field(self.p, self.n, straight, 0.0)

        # verify
        self.assertTrue(actual)


    def test_other_is_straight_in_back(self):
        # arrange
        straight_back = self.p - self.n * 5

        # act
        actual = is_in_view_field(self.p, self.n, straight_back , 0.0)

        # verify
        self.assertFalse(actual)

    def test_other_is_straight_in_front_too_far_away(self):
        # arrange
        straight = self.p + self.n * 5

        # act
        actual = is_in_view_field(self.p, self.n, straight, 0.0, 2.0)

        # verify
        self.assertFalse(actual)

    def test_other_is_straight_in_front_within_radius(self):
        # arrange
        straight = self.p + self.n * 5

        # act
        actual = is_in_view_field(self.p, self.n, straight, 2.0, 20.0)

        # verify
        self.assertTrue(actual)

    def test_distance_from_line_point_on_line(self):
        # arrange
        point_on_line = self.p + self.n * 5
        expected = 0.0

        # act
        dist = distance_from_line(self.p, self.n, point_on_line)

        # verify
        self.assertEqual(expected, dist, "distance should be {0} but is {1}".format(expected, dist))

    def test_distance_from_line(self):
        # arrange
        # rotated_normal = self.n.cross(self.o_front - self.p).normalized
        point = self.p + self.orthogonal_n * 5
        expected = 5.0

        # act
        dist = distance_from_line(self.p, self.n, point)

        # verify
        # 10 * mathematics.EPSILON should still be small enough
        self.assertEqual(0 <= abs(expected - dist) < mathematics.EPSILON, True,
                         "distance should be {0} but is {1} (rot: {2})".format(expected, dist, self.orthogonal_n, ))


    def test_componentwise_multiplication(self):
        # arrange

        # act
        actual = self.n.mult_comp(self.v)

        # verify
        expected = self.n.clone() * 3
        self.assertEqual(expected, actual)

    def test_componentwise_multiplication_values(self):
        # arrange

        # act
        actual = self.n.mult_comp_values(self.v.x, self.v.y)

        # verify
        expected = self.n.clone() * 3
        self.assertEqual(expected, actual)

    def test_rotate_90(self):
        # arrange
        Vec = self.v_class
        v = Vec(1, 0)

        # act
        v.rotate(90)

        # verify
        expected = Vec(0, 1, 0)
        self.assertEqual(expected, v.rounded(round, 5))

    def test_rotate_45(self):
        # arrange
        Vec = self.v_class
        v = Vec(1, 0)

        # act
        v.rotate(45)

        # verify
        expected = 45
        self.assertEqual(expected, v.angle)



class TestFunctions3D(TestFunctions2D):

    def setUp(self):
        self.p = Point3(1.0, 1.0, 1.0)
        self.n = Vec3(1.0, 1.0, 1.0).normalized
        self.v = Vec3(3, 3, 3)
        self.o_front = Point3(3.0, 2.4, 2.2)
        self.o_back = Point3(0.5, 0.75, 0.3)
        self.orthogonal_n = self.n.rotated(90, Vec3(1.0, -1.0, 0.0)).cross(self.n).normalized
        self.v_class = Vec3

    def test_componentwise_multiplication_values(self):
        # arrange

        # act
        actual = self.n.mult_comp_values(self.v.x, self.v.y, self.v.z)

        # verify
        expected = self.n.clone() * 3
        self.assertEqual(expected, actual)


class TestVector3(unittest.TestCase):

    def test_vector3D(self):
        Vec = Vec3
        v = Vec(2, 2, 2)
        w = Vec(3, 5, 7)
        self.assertTrue(v == Vec(2, 2, 2), u'equal failed')
        self.assertTrue(v + w == Vec(5, 7, 9), u'add failed')
        self.assertTrue(w - v == Vec(1, 3, 5), u'sub failed')
        self.assertTrue(2 * v == Vec(4, 4, 4), u'rmul failed')
        self.assertTrue(v * 3 == Vec(6, 6, 6), u'mul failed')
        self.assertTrue(v.dot(w) == 30, u'dot failed')

        z = Vec(0, 0, 0)
        z.copy_values(v)
        z *= 4
        self.assertTrue(z == Vec(8, 8, 8) , u'imul failed')
        self.assertTrue(z / 2 == Vec(4, 4, 4), u'div failed')
        z /= 2
        self.assertTrue(z == Vec(4, 4, 4), u'idiv failed')
        self.assertTrue(Vec(1, 1, 1).length == sqrt(3), u'length failed')
        self.assertTrue(Vec(0, 0, 0).length == 0, u'length failed')
        self.assertTrue(Vec(1, 1, 1).length_sq == 3, u'lengsq failed')
        self.assertTrue(Vec(3, 4, 5).length_sq == 50, u'lengsq failed')
        self.assertTrue(Vec(4, 3, 5).length_sq == 50, u'lengsq failed')
        self.assertTrue(Vec(5, 4, 3).length_sq == 50, u'lengsq failed')
        v += w
        self.assertTrue(v == Vec(5, 7, 9), u'iadd failed')
        v -= w
        self.assertTrue(v == Vec(2, 2, 2), u'isub failed')

        self.assertTrue(v.normal_left == Vec(-2, 2, 2), u'normal_L failed')
        self.assertTrue(v.normal_right == Vec(2, -2, 2), u'normal_R failed')
        self.assertTrue(v.normalized == v / v.length, u'normalized failed')
        n = v.normalized
        v.normalize()
        self.assertTrue(v == n, u'normalize failed')

        self.assertTrue(Vec(1, 1, 0).project_onto(Vec(1, 0, 0)) == Vec(1, 0, 0), u'project_onto failed')
        self.assertTrue(Vec(-1, 1, 0).project_onto(Vec(0, 1, 0)) == Vec(0, 1, 0), u'project_onto failed')

        self.assertTrue(Vec(1, -1, 1).reflect(Vec(0, 1, 0)) == Vec(1, 1, 1), u'reflect failed')
        self.assertTrue(Vec(1, -1, 1).reflect_tangent(Vec(1, 0, 0)) == Vec(1, 1, -1), u'reflect_tangent failed')

        v = Vec(1, 1, 1)
        w = Vec(3, 3, 3)
        v.copy_values(w)
        w.x = 100
        w.y = 100
        self.assertTrue(v.x == 3, u'value for x failed')
        self.assertTrue(v.y == 3, u'value for y failed')
        self.assertTrue(v.z == 3, u'value for z failed')
        v = Vec(2, 2, 2)
        w = Vec(0, 2, 1)
        self.assertTrue(v.cross(w) == Vec(-2, -2, 4))
        self.assertTrue(w.cross(v) == -1 * Vec(-2, -2, 4))

        v = Vec(1,0,0)
        w = Vec(0,1,0)
        self.assertTrue(v.get_angle_between(w) == 90)
        self.assertTrue(w.get_angle_between(v) == 90)
        self.assertTrue(round(w.get_full_angle_between(v)) == -90)
        self.assertTrue(round(v.get_full_angle_between(w)) == 90)
        w = Vec(1,1,0)
        self.assertTrue(round(w.get_angle_between(v)) == 45.0)
        self.assertTrue(round(v.get_angle_between(w)) == 45.0)
        v = Vec(1,0,0)
        w = Vec(1,0,1)
        self.assertTrue(round(w.get_angle_between(v)) == 45.0)
        self.assertTrue(round(v.get_angle_between(w)) == 45.0)
        v = Vec(0,1,0)
        w = Vec(0,1,1)
        self.assertTrue(round(w.get_angle_between(v)) == 45.0)
        self.assertTrue(round(v.get_angle_between(w)) == 45.0)

        v = Vec3(10, 0, 0)
        v.length = 5
        self.assertTrue(v == Vec3(5, 0, 0))

        self.assertTrue(Vec3.zero() == Vec3(0,0,0))
        self.assertTrue(Vec3.unit_x() == Vec3(1,0,0))
        self.assertTrue(Vec3.unit_y() == Vec3(0,1,0))
        self.assertTrue(Vec3.unit_z() == Vec3(0,0,1))

        v = Vec3(1,2)
        v.set_from_iterable([3, 2, 1])
        self.assertTrue(v.x==3 and v.y==2 and v.z==1)
        v = Vec3(1,2, 1)
        v.set_from_iterable([9])
        self.assertTrue(v.x==9 and v.y==2 and v.z==1)
        self.assertTrue(len(v) == 3)
        v = Vec3(-1, -2, -3)
        self.assertTrue(+v == Vec3(1, 2, 3))
        v = Vec3(1, 2, 3)
        w = Vec3(7, 8, 9)
        self.assertTrue(not (v == w))
        self.assertTrue(v != w)
        self.assertTrue(v[0] == 1 and v[1] == 2 and v[2] == 3)
        it = iter(v)
        self.assertTrue(next(it) == 1 and next(it) == 2 and next(it) == 3)
        self.assertTrue(v.as_tuple() == (1, 2, 3))
        w = Vec(1.2, 1.8, 3.3)
        self.assertTrue(w.as_tuple(round, 0) == (1, 2, 3))

        w = v.scaled(2)
        v.length = 2
        self.assertTrue(w == v)

        v = Vec.zero()
        w = v.scaled(2)
        v.length = 0
        self.assertTrue(w == v)

        v = Vec3(0, 0, 0)
        w = Vec3(10, 10, 10)
        self.assertTrue(round(v.get_distance(w), 3) == round(10 * (3**0.5), 3))
        self.assertTrue(round(v.get_distance_sq(w), 3) == 300)
        v = Vec3(1.11191, 1.11191, 1.111911)
        v.round(round, 3)
        self.assertTrue(v == Vec3(1.112, 1.112, 1.112))

        self.assertTrue(Vec(0, 0, 0).normalized == Vec3(0, 0, 0))
        self.assertTrue(-Vec(1,2, 3) == Vec3(-1, -2, -3))
        self.assertTrue(Vec(0, 0).get_angle_between(Vec3(0, 0)) == 0)
        self.assertTrue(Vec(0, 0).get_full_angle_between(Vec(0, 0)) == 0)

        self.assertTrue(Vec(1, 1).normal_right == Vec3(1, -1))
        self.assertTrue(Vec(1, 1).normal_left == Vec3(-1, 1))
        self.assertTrue(round(Vec3(1, 1).angle)==45)
        self.assertTrue(Vec3(4, 5, 6).as_xy_tuple() == (4, 5))
        self.assertTrue(Vec3(4.4, 5.4, 6.5).as_xy_tuple(int) == (4, 5))
        self.assertTrue(not Vec3(1,1) == 1)
        self.assertTrue(len(str(Vec3(1,1))) > 0)

        v = Vec(1.0, 2.0, 3.0)
        w = Vec(1.0, 2.0, 3.0)
        self.assertFalse(hash(v) == hash(w), "hash values should not be the same")

        v = Vec(1.0, 1.0, 1.0)
        w = Vec(1.0, 0.0, 0.0)
        v.face_forward(w)
        self.assertTrue(v==Vec(1.0, 1.0, 1.0), "faceforward didnt work")
        w = Vec(-1.0, 0.0, 0.0)
        v.face_forward(w)
        self.assertTrue(v==Vec(-1.0, -1.0, -1.0), "faceforward didnt work")

    def test_refraction(self):
        Vec = Vec3
        # import decimal
        # # emulate python 2.7 'from_float'
        # setattr(decimal.Decimal, 'from_float', classmethod(lambda cls, x: decimal.Decimal(str(x))))

        # normal = Vec(decimal.Decimal.from_float(0.0), decimal.Decimal.from_float(-1.0))
        # light = Vec(decimal.Decimal.from_float(1.0), decimal.Decimal.from_float(0.0)).rotated(decimal.Decimal.from_float(45.237740459293))
        # ratio = decimal.Decimal.from_float().from_float(0.75) # 1.0 / 1.33  ~ air / water


        normal = Vec(0.0, -1.0, 0.0)
        light = Vec(1.0, 0.0, 0.0).rotated(45.237740459293, Vec.unit_z())
        ratio = 0.75 # 1.0 / 1.33  ~ air / water

        ref = light.refract(normal, ratio)

        ref.round(round, 2)
        expected = Vec(1.0, 0.0, 0.0).rotated(90.0 - 32.2345838693056, Vec.unit_z())
        expected.round(round, 2)
        # expected.rotate_to(90-32)
        self.assertTrue(ref == expected, "refracted ray is not as expected: {0} expected: {1}".format(ref, expected))

        light2 = Vec(1.0, 0.0, 0.0).rotated(41.0, Vec.unit_z())
        result = light2.refract(normal, 1.33)

        self.assertTrue(result.length == 0, "should be totally reflected, result: {0}".format(result))

    def test_rotation(self):
        Vec = Vec3
        v = Vec(1, 0, 0)
    #    for i in range(0, 300, 30):
    #        print v.rotate(i, Vec(1, 1, 1)).rounded(5)
        self.assertTrue(v.rotated(90, Vec(0, 0, 1)).rounded(round, 5) == Vec(0, 1, 0))
        self.assertTrue(v.rotated(90, Vec(0, 1, 0)).rounded(round, 5) == Vec(0, 0, -1))
        self.assertTrue(v.rotated(240, Vec(1, 1, 1)).rounded(round, 5) == Vec(0, 0, 1))
        self.assertTrue(v.rotated(-120, Vec(1, 1, 1)).rounded(round, 5) == Vec(0, 0, 1))

        # try:
            # v.rotate_to(45, Vec.unit_z())
        # except NotImplementedError as e:
            # pass
        # except Exception as ex:
            # self.fail("should have raised an NotImplementedError, not " + str(ex))


class TestSign(unittest.TestCase):

    def test_sign(self):
        self.assertTrue(sign(34)==1, u"sign for positive number failed")
        self.assertTrue(sign(-34)==-1, u"sign for negative number failed")
        self.assertTrue(sign(0)==0, u"sign for 0 number failed")


if __name__ == '__main__':
    unittest.main()

