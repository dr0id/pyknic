# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_clipping.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.mathematics.clipping module.
"""
from __future__ import print_function, division
import unittest
import warnings
import math
import time

import pyknic.mathematics.clipping as mut  # module under test
from pyknic.mathematics import EPSILON

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


def clip_segment_against_aabb_sat(px, py, ex, ey, x_min, y_min, x_max, y_max):
    r"""

    SAT::

                                                    X
            +---'----'--'---------------'------------>
            |   '    '  '               '
            -. .'. . x  '               '
            |   '   /   '               '
            |   '  /    '               '
            -. .'./. . .+---------------+
            |   '/     ´|              ´|
            -. .x     ´ |             ´ |
            |  ´     ´  |            ´  |
          \ -.´. . .´. .+-----------´---+
           \|´     ´   ´           ´   ´
            ´     ´   ´           ´   ´
            |\   ´   ´           ´   ´
            | \ ´   ´           ´   ´
            |  ´   ´           ´   ´
            |   \ ´           ´   ´
            |    ´           ´   ´
            |     \         ´   ´
         Y  v      \       ´   ´
                    \     ´   ´
                     \   ´   ´
                      \ ´   ´
                       ´   ´
                        \ ´
                         ´
                          \
                           \ normal axis to the segment

    :param px:
    :param py:
    :param ex:
    :param ey:
    :param x_min:
    :param y_min:
    :param x_max:
    :param y_max:
    """
    # can't handle inf values
    assert not math.isinf(px), "can't handle 'inf' for px"
    assert not math.isinf(py), "can't handle 'inf' for py"
    assert not math.isinf(ex), "can't handle 'inf' for ex"
    assert not math.isinf(ey), "can't handle 'inf' for ey"
    assert not math.isinf(x_min), "can't handle 'inf' for x_min"
    assert not math.isinf(y_min), "can't handle 'inf' for y_min"
    assert not math.isinf(x_max), "can't handle 'inf' for x_max"
    assert not math.isinf(y_max), "can't handle 'inf' for y_max"

    assert not math.isnan(px), "can't handle 'nan' for px"
    assert not math.isnan(py), "can't handle 'nan' for py"
    assert not math.isnan(ex), "can't handle 'nan' for ex"
    assert not math.isnan(ey), "can't handle 'nan' for ey"
    assert not math.isnan(x_min), "can't handle 'nan' for x_min"
    assert not math.isnan(y_min), "can't handle 'nan' for y_min"
    assert not math.isnan(x_max), "can't handle 'nan' for x_max"
    assert not math.isnan(y_max), "can't handle 'nan' for y_max"

    # x axis
    if ex > px:
        if ex < x_min:
            return False, px, py, ex, ey
        elif px > x_max:
            return False, px, py, ex, ey
    else:
        if px < x_min:
            return False, px, py, ex, ey
        elif ex > x_max:
            return False, px, py, ex, ey

    # y axis
    if ey > py:
        if ey < y_min:
            return False, px, py, ex, ey
        elif py > y_max:
            return False, px, py, ex, ey
    else:
        if py < y_min:
            return False, px, py, ex, ey
        elif ey > y_max:
            return False, px, py, ex, ey

    # normal axis
    nx = float(ey - py)
    ny = float(px - ex)

    l_sq = nx * nx + ny * ny

    # if l_sq == 0:
    if l_sq < EPSILON:
        # point is inside aabb
        return True, px, py, ex, ey

    # project aabb onto normal axis
    # return self.dot(other) / other.length_sq * other
    # p1 = (nx * vx + ny + vy) / l_sq

    # terms for the projection to project all 4 corners of the aabb
    x_min_px = x_min - px
    x_max_px = x_max - px
    y_min_py = y_min - py
    y_max_py = y_max - py

    x_min_px_nx = x_min_px * nx
    x_max_px_nx = x_max_px * nx
    y_min_py_ny = y_min_py * ny
    y_max_py_ny = y_max_py * ny

    # actually the projections would need a division by l_sq
    # but this is only a scaling and does not change the
    # sign nor the order so we can save these cpu cycles
    # do also determine the max and min points
    p1 = x_min_px_nx + y_min_py_ny
    p4 = x_max_px_nx + y_max_py_ny
    if p1 > p4:
        p1, p4 = p4, p1

    p = x_min_px_nx + y_max_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    p = x_max_px_nx + y_min_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    if p1 > 0:
        return False, px, py, ex, ey
    if p4 < 0:
        return False, px, py, ex, ey

    # print('!!', p1, p4)

    # here we have intersection!!
    # either it is within the aabb
    # or it intersects one side of the aabb
    # or it intersects both sides
    #
    #    using a line in parametric form:
    #
    #        P + t * V      where    P start point (px, py)
    #                                t parameter in range [0, 1]
    #                                V as direction vector (vx, vy) = (ex-px,ey-py)
    #
    #
    #    intersection of two parametric lines:
    #
    #        P + t * V = Q + s * U
    #
    #            =>   s = (V cross (P - Q)) / (V cross U) = (vx * (py - qy) - vy * (px - qx)) / (vx * uy - vy * ux)
    #
    #            =>   t = (qy - py + s * uy) / vy
    #
    #
    # Q = px, py
    # U = ex-px, ey-py  = -ny, nx # reusing the variables and their value
    # P = ox, oy
    # V = vx, vy

    # initial values
    s2 = 0.0
    s3 = 1.0

    # ox = x_min, oy = y_min, vx = x_max - x_min
    # x_max_x_min = x_max - x_min
    # denominator = x_max_x_min * nx
    if nx != 0.0:
        s2 = y_min_py / nx
        # ox = x_max, oy = y_max, vx = x_min - x_max, vy = 0.0
        # denominator = (x_min - x_max) * nx
        # if denominator != 0.0:
        s3 = y_max_py / nx
    if s2 > s3:
        s2, s3 = s3, s2
        # ox = x_min, oy = y_min, vx = 0.0, vy = y_max - y_min
    # y_max_y_min = y_max - y_min
    # denominator = y_max_y_min * ny
    if ny != 0.0:
        s1 = -x_min_px / ny
        if s1 > s3:
            s1, s2, s3 = s2, s3, s1
        elif s1 > s2:
            s1, s2 = s2, s1

            # ox = x_max, oy = y_max, vx = 0.0, vy = y_min - y_max
            # denominator = (y_min - y_max) * ny
            # if denominator != 0.0:
        s4 = -x_max_px / ny
        if s4 < s1:
            s3 = s2
            s2 = s1
        elif s4 < s2:
            s3 = s2
            s2 = s4
        elif s4 < s3:
            s3 = s4
            # print('!', s2, s3)

    # clamp to range [0, 1], outside of range is not in the segment
    if s2 < 0.0:
        s2 = 0.0

    if s3 > 1.0:
        s3 = 1.0

    # print('!', s2, s3, px, ny)

    # calculate the points to return
    return True, px - s2 * ny, py + s2 * nx, px - s3 * ny, py + s3 * nx


# from:
# https://bitbucket.org/marcusva/py-sdl2/src/fce4c7cd5bf8349b929ddc01e9c2f4e744dacbd4/sdl2/ext/algorithms.py?at=default
def cohensutherland(left, top, right, bottom, x1, y1, x2, y2):
    """Clips a line to a rectangular area.

    This implements the Cohen-Sutherland line clipping algorithm.  left,
    top, right and bottom denote the clipping area, into which the line
    defined by x1, y1 (start point) and x2, y2 (end point) will be
    clipped.

    If the line does not intersect with the rectangular clipping area,
    four None values will be returned as tuple. Otherwise a tuple of the
    clipped line points will be returned in the form (cx1, cy1, cx2, cy2).
    """
    LEFT, RIGHT, LOWER, UPPER = 1, 2, 4, 8

    def _getclip(xa, ya):
        p = 0
        if xa < left:
            p = LEFT
        elif xa > right:
            p = RIGHT
        if ya < top:
            p |= LOWER
        elif ya > bottom:
            p |= UPPER
        return p

    k1 = _getclip(x1, y1)
    k2 = _getclip(x2, y2)
    while (k1 | k2) != 0:
        if (k1 & k2) != 0:
            return None, None, None, None

        opt = k1 or k2
        if opt & UPPER:
            x = x1 + (x2 - x1) * (1.0 * (bottom - y1)) / (y2 - y1)
            y = bottom
        elif opt & LOWER:
            x = x1 + (x2 - x1) * (1.0 * (top - y1)) / (y2 - y1)
            y = top
        elif opt & RIGHT:
            y = y1 + (y2 - y1) * (1.0 * (right - x1)) / (x2 - x1)
            x = right
        elif opt & LEFT:
            y = y1 + (y2 - y1) * (1.0 * (left - x1)) / (x2 - x1)
            x = left
        else:
            raise Exception()

        if opt == k1:
            # x1, y1 = int(x), int(y)
            x1, y1 = x, y
            k1 = _getclip(x1, y1)
        else:
            # x2, y2 = int(x), int(y)
            x2, y2 = x, y
            k2 = _getclip(x2, y2)

    return x1, y1, x2, y2


# from:
# https://bitbucket.org/marcusva/py-sdl2/src/fce4c7cd5bf8349b929ddc01e9c2f4e744dacbd4/sdl2/ext/algorithms.py?at=default
def liangbarsky(left, top, right, bottom, x1, y1, x2, y2):
    """Clips a line to a rectangular area.

    This implements the Liang-Barsky line clipping algorithm.  left,
    top, right and bottom denote the clipping area, into which the line
    defined by x1, y1 (start point) and x2, y2 (end point) will be
    clipped.

    If the line does not intersect with the rectangular clipping area,
    four None values will be returned as tuple. Otherwise a tuple of the
    clipped line points will be returned in the form (cx1, cy1, cx2, cy2).
    """
    dx = x2 - x1 * 1.0
    dy = y2 - y1 * 1.0
    dt0, dt1 = 0.0, 1.0
    xx1 = x1
    yy1 = y1

    checks = ((-dx, x1 - left),
              (dx, right - x1),
              (-dy, y1 - top),
              (dy, bottom - y1))

    for p, q in checks:
        if p == 0 and q < 0:
            return None, None, None, None
        if p != 0:
            dt = q / (p * 1.0)
            if p < 0:
                if dt > dt1:
                    return None, None, None, None
                dt0 = max(dt0, dt)
            else:
                if dt < dt0:
                    return None, None, None, None
                dt1 = min(dt1, dt)
    if dt0 > 0:
        x1 += dt0 * dx
        y1 += dt0 * dy
    if dt1 < 1:
        x2 = xx1 + dt1 * dx
        y2 = yy1 + dt1 * dy
    return x1, y1, x2, y2


class ClippingTests(unittest.TestCase):
    """Test data and methods for clipping functions."""

    x_min = 10
    y_min = 10
    x_max = 20
    y_max = 20

    inf = float("inf")

    # values = [(args, expected),...]
    values = [
        # parallel to x axis
        ((5, 5, 15, 5, x_min, y_min, x_max, y_max), (False, 5, 5, 15, 5)),  # s1
        ((5, 15, 15, 15, x_min, y_min, x_max, y_max), (True, 10, 15, 15, 15)),  # s2
        ((15, 15, 25, 15, x_min, y_min, x_max, y_max), (True, 15, 15, 20, 15)),  # s3
        ((5, 15, 25, 15, x_min, y_min, x_max, y_max), (True, 10, 15, 20, 15)),  # s4
        ((5, 15, 9, 15, x_min, y_min, x_max, y_max), (False, 5, 15, 9, 15)),  # s5
        ((21, 15, 29, 15, x_min, y_min, x_max, y_max), (False, 21, 15, 29, 15)),  # s6
        ((11, 15, 19, 15, x_min, y_min, x_max, y_max), (True, 11, 15, 19, 15)),  # s7
        ((11, 25, 19, 25, x_min, y_min, x_max, y_max), (False, 11, 25, 19, 25)),  # s8

        # parallel to y axis
        ((5, 5, 5, 15, x_min, y_min, x_max, y_max), (False, 5, 5, 5, 15)),  # s11
        ((15, 5, 15, 15, x_min, y_min, x_max, y_max), (True, 15, 10, 15, 15)),  # s12
        ((15, 15, 15, 25, x_min, y_min, x_max, y_max), (True, 15, 15, 15, 20)),  # s13
        ((15, 5, 15, 25, x_min, y_min, x_max, y_max), (True, 15, 10, 15, 20)),  # s14
        ((15, 5, 15, 9, x_min, y_min, x_max, y_max), (False, 15, 5, 15, 9)),  # s15
        ((15, 21, 15, 29, x_min, y_min, x_max, y_max), (False, 15, 21, 15, 29)),  # s16
        ((15, 11, 15, 19, x_min, y_min, x_max, y_max), (True, 15, 11, 15, 19)),  # s17
        ((25, 5, 25, 25, x_min, y_min, x_max, y_max), (False, 25, 5, 25, 25)),  # s18

        # other line segments
        # crossing aabb line segments in 45 degrees angle
        ((5, 20, 20, 5, x_min, y_min, x_max, y_max), (True, 10, 15, 15, 10)),
        ((20, 5, 5, 20, x_min, y_min, x_max, y_max), (True, 15, 10, 10, 15)),
        ((10, 5, 25, 20, x_min, y_min, x_max, y_max), (True, 15, 10, 20, 15)),
        ((25, 20, 10, 5, x_min, y_min, x_max, y_max), (True, 20, 15, 15, 10)),
        ((5, 10, 20, 25, x_min, y_min, x_max, y_max), (True, 10, 15, 15, 20)),
        ((20, 25, 5, 10, x_min, y_min, x_max, y_max), (True, 15, 20, 10, 15)),
        ((10, 25, 25, 10, x_min, y_min, x_max, y_max), (True, 15, 20, 20, 15)),
        ((25, 10, 10, 25, x_min, y_min, x_max, y_max), (True, 20, 15, 15, 20)),

        ((5, 20, 25, 10, x_min, y_min, x_max, y_max), (True, 10.0, 17.5, 20.0, 12.5)),
        ((5, 10, 25, 20, x_min, y_min, x_max, y_max), (True, 10.0, 12.5, 20.0, 17.5)),
        ((10, 5, 20, 25, x_min, y_min, x_max, y_max), (True, 12.5, 10.0, 17.5, 20.0)),
        ((20, 5, 10, 25, x_min, y_min, x_max, y_max), (True, 17.5, 10.0, 12.5, 20.0)),

        ((25, 10, 5, 20, x_min, y_min, x_max, y_max), (True, 20.0, 12.5, 10.0, 17.5)),
        ((25, 20, 5, 10, x_min, y_min, x_max, y_max), (True, 20.0, 17.5, 10.0, 12.5)),
        ((20, 25, 10, 5, x_min, y_min, x_max, y_max), (True, 17.5, 20.0, 12.5, 10.0)),
        ((10, 25, 20, 5, x_min, y_min, x_max, y_max), (True, 12.5, 20.0, 17.5, 10.0)),

        # line segments starting within and ending outside aabb
        ((15, 15, 20, 5, x_min, y_min, x_max, y_max), (True, 15, 15, 17.5, 10)),
        ((15, 15, 10, 5, x_min, y_min, x_max, y_max), (True, 15, 15, 12.5, 10)),
        ((15, 15, 25, 10, x_min, y_min, x_max, y_max), (True, 15, 15, 20, 12.5)),
        ((15, 15, 25, 20, x_min, y_min, x_max, y_max), (True, 15, 15, 20, 17.5)),
        ((15, 15, 20, 25, x_min, y_min, x_max, y_max), (True, 15, 15, 17.5, 20)),
        ((15, 15, 10, 25, x_min, y_min, x_max, y_max), (True, 15, 15, 12.5, 20)),
        ((15, 15, 5, 20, x_min, y_min, x_max, y_max), (True, 15, 15, 10, 17.5)),
        ((15, 15, 5, 10, x_min, y_min, x_max, y_max), (True, 15, 15, 10, 12.5)),

        # line segments starting outside and ending within aabb
        ((20, 5, 15, 15, x_min, y_min, x_max, y_max), (True, 17.5, 10, 15, 15)),
        ((10, 5, 15, 15, x_min, y_min, x_max, y_max), (True, 12.5, 10, 15, 15)),
        ((25, 10, 15, 15, x_min, y_min, x_max, y_max), (True, 20, 12.5, 15, 15)),
        ((25, 20, 15, 15, x_min, y_min, x_max, y_max), (True, 20, 17.5, 15, 15)),
        ((20, 25, 15, 15, x_min, y_min, x_max, y_max), (True, 17.5, 20, 15, 15)),
        ((10, 25, 15, 15, x_min, y_min, x_max, y_max), (True, 12.5, 20, 15, 15)),
        ((5, 20, 15, 15, x_min, y_min, x_max, y_max), (True, 10, 17.5, 15, 15)),
        ((5, 10, 15, 15, x_min, y_min, x_max, y_max), (True, 10, 12.5, 15, 15)),

        # only corner is crossed
        ((5, 15, 15, 5, x_min, y_min, x_max, y_max), (True, 10, 10, 10, 10)),
        ((15, 5, 25, 15, x_min, y_min, x_max, y_max), (True, 20, 10, 20, 10)),
        ((25, 15, 15, 25, x_min, y_min, x_max, y_max), (True, 20, 20, 20, 20)),
        ((15, 25, 5, 15, x_min, y_min, x_max, y_max), (True, 10, 20, 10, 20)),

        ((15, 5, 5, 15, x_min, y_min, x_max, y_max), (True, 10, 10, 10, 10)),
        ((25, 15, 15, 5, x_min, y_min, x_max, y_max), (True, 20, 10, 20, 10)),
        ((15, 25, 25, 15, x_min, y_min, x_max, y_max), (True, 20, 20, 20, 20)),
        ((5, 15, 15, 25, x_min, y_min, x_max, y_max), (True, 10, 20, 10, 20)),

        # line segments completely within aabb
        ((12.5, 12.5, 17.5, 17.5, x_min, y_min, x_max, y_max), (True, 12.5, 12.5, 17.5, 17.5)),
        ((12.5, 15.0, 17.5, 17.5, x_min, y_min, x_max, y_max), (True, 12.5, 15.0, 17.5, 17.5)),
        ((15.0, 17.5, 17.5, 15.0, x_min, y_min, x_max, y_max), (True, 15.0, 17.5, 17.5, 15.0)),

        ((17.5, 17.5, 12.5, 12.5, x_min, y_min, x_max, y_max), (True, 17.5, 17.5, 12.5, 12.5)),
        ((17.5, 17.5, 12.5, 15.0, x_min, y_min, x_max, y_max), (True, 17.5, 17.5, 12.5, 15.0)),
        ((17.5, 15.0, 15.0, 17.5, x_min, y_min, x_max, y_max), (True, 17.5, 15.0, 15.0, 17.5)),

        # line segments completely outside aabb
        ((5, 5, 10, 5, x_min, y_min, x_max, y_max), (False, 5, 5, 10, 5)),
        ((5, 5, 5, 20, x_min, y_min, x_max, y_max), (False, 5, 5, 5, 20)),
        ((15, 22, 15, 25, x_min, y_min, x_max, y_max), (False, 15, 22, 15, 25)),

        ((5, 10, 10, 5, x_min, y_min, x_max, y_max), (False, 5, 10, 10, 5)),
        ((20, 5, 25, 10, x_min, y_min, x_max, y_max), (False, 20, 5, 25, 10)),
        ((25, 20, 20, 25, x_min, y_min, x_max, y_max), (False, 25, 20, 20, 25)),
        ((10, 25, 5, 20, x_min, y_min, x_max, y_max), (False, 10, 25, 5, 20)),

        ((10, 5, 5, 10, x_min, y_min, x_max, y_max), (False, 10, 5, 5, 10)),
        ((25, 10, 20, 5, x_min, y_min, x_max, y_max), (False, 25, 10, 20, 5)),
        ((20, 25, 25, 20, x_min, y_min, x_max, y_max), (False, 20, 25, 25, 20)),
        ((5, 20, 10, 25, x_min, y_min, x_max, y_max), (False, 5, 20, 10, 25)),

        # points, inside
        ((15, 15, 15, 15, x_min, y_min, x_max, y_max), (True, 15, 15, 15, 15)),

        # points, outside
        ((5, 5, 5, 5, x_min, y_min, x_max, y_max), (False, 5, 5, 5, 5)),

    ]

    values_other_args_order = [
        ((val[0][4], val[0][5], val[0][6], val[0][7], val[0][0], val[0][1], val[0][2], val[0][3]),
         (val[1][1], val[1][2], val[1][3], val[1][4],) if val[1][0] else (None, None, None, None)) for val in values]

    # print(values_other_args_order)

    # # cant satisfy both methods because of rounding issues, still the values are the same until
    # # the 5th digit after the dot, probably precise enough
    # ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365, 10, 10, 20, 20),
    #        (True, 13.353899999999999, 11.81231, 13.35389, 10)),

    # (AssertionError('using function: clip_segment_against_aabb_sat(...) \n '
    #    'segment ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365)) clipped at aabb (10, 10, 20, 20)? '
    #    '\n\t expected: (True, 13.353897290260468, 11.812311612950818, 13.353894247932462, 10) '
    #    '\n\t      got: (True, 13.353897290260468, 11.812311612950818, 13.353896821566268, 10.0)',), '\n')

    # (AssertionError('using function: clip_segment_against_aabb_sat(...) '
    #                '\n segment ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365)) clipped at aabb (10, 10, 20, 20)? '
    #                '\n\t expected: (True, 13.35389, 11.81231, 13.35389, 10) '
    #                '\n\t      got: (True, 13.353899999999999, 11.81231, 13.353899999999999, 10.0)'

    # (AssertionError('using function: clip_segment_against_aabb(...) '
    #                '\n segment ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365)) clipped at aabb (10, 10, 20, 20)? '
    #                '\n\t expected: (True, 13.353899999999999, 11.81231, 13.353899999999999, 10) '
    #                '\n\t      got: (True, 13.353899999999999, 11.81231, 13.35389, 10.0)'

    inf_values = [
        # inf
        # one point inside, diagonal
        ((15, 15, inf, inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  20,  20)),
        ((15, 15, -inf, inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  10,  20)),
        ((15, 15, inf, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  20,  10)),
        ((15, 15, -inf, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  10,  10)),

        ((inf, inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True, 20, 20, 15, 15)),
        ((-inf, inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True, 10, 20, 15, 15)),
        ((inf, -inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True, 20, 10, 15, 15)),
        ((-inf, -inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True, 10, 10, 15, 15)),

        # one point inside, parallel to axis
        ((15, 15, 15, inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  15,  20)),
        ((15, 15, 15, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  15,  10)),
        ((15, 15, inf, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  20,  15)),
        ((15, 15, -inf, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  15,  10,  15)),

        ((15, inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  20,  15,  15)),
        ((15, -inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  10,  15,  15)),
        ((inf, 15, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   20,  15,  15,  15)),
        ((-inf, 15, 15, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   10,  15,  15,  15)),


        # intersecting, points outside, parallel to axis
        ((inf, 15, -inf, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   20,  15,  10,  15)),
        ((-inf, 15, inf, 15, x_min, y_min, x_max, y_max), AssertionError),  # (True,   10,  15,  20,  15)),
        ((15, -inf, 15, inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  10,  15,  20)),
        ((15, inf, 15, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   15,  20,  15,  10)),

        # intersecting, points outside, diagonal
        ((inf, inf, 1, 1, x_min, y_min, x_max, y_max), AssertionError),  # (True,   20,  20,  10,  10)),
        ((-inf, -inf, 21, 21, x_min, y_min, x_max, y_max), AssertionError),  # (True,   10,  10,  20,  20)),
        ((1, 1, inf, inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   10,  10,  20,  20)),
        ((1, 1, -inf, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (True,   20,  20,  10,  10)),


        # not intersecting, points outside, parallel to axis
        ((inf, 25, -inf, 25, x_min, y_min, x_max, y_max), AssertionError),  # (False,  inf,   25, -inf, 25)),
        ((-inf, 5, inf, 5, x_min, y_min, x_max, y_max), AssertionError),  # (False, -inf,    5,  inf,  5)),
        ((25, -inf, 25, inf, x_min, y_min, x_max, y_max), AssertionError),  # (False,   25, -inf,   25,  inf)),
        ((5, inf, 5, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (False,    5,  inf,    5, -inf)),

        # not intersecting, points outside, diagonal
        ((inf, inf, 21, 21, x_min, y_min, x_max, y_max), AssertionError),  # (False,  inf,  inf,   21,   21)),
        ((-inf, -inf, 1, 1, x_min, y_min, x_max, y_max), AssertionError),  # (False, -inf, -inf,    1,    1)),
        ((21, 21, inf, inf, x_min, y_min, x_max, y_max), AssertionError),  # (False,   21,   21,  inf,  inf)),
        ((1, 1, -inf, -inf, x_min, y_min, x_max, y_max), AssertionError),  # (False,    1,    1, -inf, -inf)),

        # (( , , , , x_min, y_min, x_max, y_max), (True,  , , , )),
    ]

    def _test_func(self, the_values, func):
        err_cnt = 0
        count = 0
        digits = 5
        for args, exp in the_values:
            try:
                count += 1
                res = func(*args)
                # print('?  ', res)
                if len(res) == 5:
                    res = (
                        res[0], round(res[1], digits), round(res[2], digits), round(res[3], digits),
                        round(res[4], digits))
                    assert res[0] is exp[0] and res[1] == exp[1] and res[2] == exp[2] and res[3] == exp[3] \
                           and res[4] == exp[4], "using function: {4}(...) \n segment ({0}) clipped at " \
                                                 "aabb {1}? \n\t expected: {2} \n\t      got: {3}".format(
                        args[:4], args[4:], exp, res, func.__name__)
                    assert mut.is_segment_intersecting_aabb(*args) is exp[0], "is_intersecting failed"
                else:
                    if res[0] is not None:
                        res = (
                            round(res[0], digits), round(res[1], digits), round(res[2], digits), round(res[3], digits))
                    # print(res, exp)
                    assert res[0] == exp[0] and res[1] == exp[1] and res[2] == exp[2] and res[3] == exp[3], \
                        "using function: {4}(...) \n segment ({1}) clipped at " \
                        "aabb {0}? \n\t expected: {2} \n\t      got: {3}".format(
                            args[:4], args[4:], exp, res, func.__name__)

            except Exception as ex:
                print(ex, "\n")
                err_cnt += 1
        print("\nerrors {0}/{1}    {2}\n\n".format(err_cnt, count, func.__name__))

    def _test_func_throws_error(self, the_values, func):
        err_cnt = 0
        count = 0
        last = None
        for args, exp in the_values:
            try:
                count += 1
                func(*args)
                raise Exception("should have raised '{0}'".format(exp))
            except Exception as ex:
                try:
                    assert isinstance(ex, exp), "function '{3}' threw exception '{0}' but expected " \
                                                "exception '{1}' for args '{2}'".format(
                        ex.__class__, str(exp), args, func.__name__)
                except Exception as ex2:
                    print(ex2, "\n")
                    err_cnt += 1
        print("\nerrors {0}/{1}    {2}\n\n".format(err_cnt, count, func.__name__))

    def _test_random_values(self, iterations):
        import random

        def _r(r_min, r_max):
            return (random.random() * (r_max - r_min)) + r_min

        digits = 5
        for i in range(iterations):

            aabb_x_min = _r(-10000, 10000)
            aabb_y_min = _r(-10000, 10000)
            aabb_x_max = _r(-10000, 10000)
            aabb_y_max = _r(-10000, 10000)

            # normalize
            if aabb_x_min > aabb_x_max:
                aabb_x_min, aabb_x_max = aabb_x_max, aabb_x_min

            if aabb_y_min > aabb_y_max:
                aabb_y_min, aabb_y_max = aabb_y_max, aabb_y_min

            interval = (_r(min(aabb_x_min, aabb_y_min) - 10000, max(aabb_x_max, aabb_y_max) + 10000),
                        _r(min(aabb_x_min, aabb_y_min) - 10000, max(aabb_x_max, aabb_y_max) + 10000))
            args = (
                _r(*interval), _r(*interval), _r(*interval), _r(*interval), aabb_x_min, aabb_y_min, aabb_x_max,
                aabb_y_max)
            # print(args)
            try:
                r0, r1, r2, r3, r4 = mut.clip_segment_against_aabb(*args)
                s0, s1, s2, s3, s4 = clip_segment_against_aabb_sat(*args)
                r1 = round(r1, digits)
                r2 = round(r2, digits)
                r3 = round(r3, digits)
                r4 = round(r4, digits)
                s1 = round(s1, digits)
                s2 = round(s2, digits)
                s3 = round(s3, digits)
                s4 = round(s4, digits)
                assert r0 == s0 and r1 == s1 and r2 == s2 and r3 == s3 and r4 == s4, \
                    "error for args: {0}\n clip: {1}\n  SAT: {2}".format(
                        args, mut.clip_segment_against_aabb(*args),
                        clip_segment_against_aabb_sat(
                            *args))
                assert mut.is_segment_intersecting_aabb(*args) is r0, "is_intersecting failed"
            except Exception as ex:
                print(str(ex))
        print("random test values executed successfully")

    def _test_performance(self, iterations, func, *args):
        time_time = time.time
        i = 0
        start = time_time()
        while i < iterations:
            i += 1
            func(*args)
        end = time_time()
        return end - start

    def _test_performance_special(self, func, the_values):
        """
        Calls a function with the arguments fro  the values list.
        :param func:
            the function to test
        :param the_values:
            list of tuple, each tuple containing the function args and the expected result as tuple, e.g.:
                [((arg1, arg2,...), (result1, result2,...)), (...), ...]
        """
        for argus, exp in the_values:
            func(*argus)

    def test_clipping_values(self):
        # arrange

        # act
        # test that correct values are calculated
        self._test_func(self.values, mut.clip_segment_against_aabb)

    def test_clipping_values_for_other_algorithms(self):

        self._test_func(self.values, clip_segment_against_aabb_sat)

        self._test_func(self.values_other_args_order, cohensutherland)
        self._test_func(self.values_other_args_order, liangbarsky)

    def test_handle_inf_and_Nan_values_for_clipping(self):
        # test handling of inf and NaN
        self._test_func_throws_error(self.inf_values, mut.clip_segment_against_aabb)

    def test_handle_inf_and_Nan_values_for_other_clipping_algorithms(self):
        self._test_func_throws_error(self.inf_values, clip_segment_against_aabb_sat)

    def test_random_value_for_clipping(self):

        # test random values
        num_iterations = 10000
        self._test_random_values(num_iterations)

    def manually_test_performance_comparing(self):
        num_iterations = 10000

        # test performance
        print("\ntesting {0} times:".format(num_iterations))
        print("performance clip_segment_against_aabb_SAT() :", self._test_performance(num_iterations,
                                                                                      self._test_performance_special,
                                                                                      clip_segment_against_aabb_sat,
                                                                                      self.values))
        print("performance clip_segment_against_aabb() :    ", self._test_performance(num_iterations,
                                                                                      self._test_performance_special,
                                                                                      mut.clip_segment_against_aabb,
                                                                                      self.values))

        print("performance cohensutherland() :    ", self._test_performance(num_iterations,
                                                                            self._test_performance_special,
                                                                            cohensutherland,
                                                                            self.values_other_args_order))

        print("performance liangbarsky() :    ", self._test_performance(num_iterations,
                                                                        self._test_performance_special, liangbarsky,
                                                                        self.values_other_args_order))

        print("performance is_segment_intersecting_aabb() :", self._test_performance(num_iterations,
                                                                                     self._test_performance_special,
                                                                                     mut.is_segment_intersecting_aabb,
                                                                                     self.values))

        # print(_test_performance(n, _test_performance_mut.clip_segment_against_aabb_SAT, values))
        # print(_test_performance(n, _test_performance_clip_segment_against_aabb, values))
        # print(_test_performance(values, n, _test_performance_clip_segment_against_aabb))


if __name__ == '__main__':
    unittest.main()

    t = ClippingTests()
    t.manually_test_performance_comparing()
