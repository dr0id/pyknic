# -*- coding: utf-8 -*-
from __future__ import print_function, division

import unittest
import warnings
from threading import RLock

import pyknic.context as context
import pyknic.context.transitions as transitions

warnings.simplefilter('error')  # make warnings visible for developers


class EMethodsType(object):
    UPDATE = "update"
    SUSPEND = "suspend"
    RESUME = "resume"
    ENTER = "enter"
    EXIT = "exit"
    DRAW = "draw"


# make it read only
EMethodsType.__setattr__ = None

captured = []  # [EMethodsType]
current_context = None
lock = RLock()


def remove_processing_context(cont):
    global current_context
    assert cont == current_context
    current_context = None


def set_processing_context(cont):
    global current_context
    if current_context is not None:
        raise AssertionError(
            "context stack manipulation during method current '{0}' new '{1}'".format(current_context, cont))
    assert current_context is None
    current_context = cont


class SpyEffect(transitions.TransitionEffect):
    def __init__(self, context_id):
        self.id = context_id
        self.update_return = False
        transitions.TransitionEffect.__init__(self)

    def enter(self):
        captured.append((self.id, EMethodsType.ENTER))
        transitions.TransitionEffect.enter(self)

    def exit(self):
        captured.append((self.id, EMethodsType.EXIT))
        transitions.TransitionEffect.exit(self)

    def suspend(self):
        captured.append((self.id, EMethodsType.SUSPEND))
        transitions.TransitionEffect.suspend(self)

    def resume(self):
        captured.append((self.id, EMethodsType.RESUME))
        transitions.TransitionEffect.resume(self)

    def update(self, dt, sim_time):
        captured.append((self.id, EMethodsType.UPDATE))
        try:
            transitions.TransitionEffect.update(self, dt, sim_time)
            assert False, "should have raise NotImplementedError"
        except NotImplementedError:
            pass
        return self.update_return

    def draw(self, screen, old_context, new_context):
        assert new_context is not None, "new_context should not be None"
        captured.append((self.id, EMethodsType.DRAW))
        try:
            transitions.TransitionEffect.draw(self, screen, old_context, new_context)
            assert False, "should have raise NotImplementedError"
        except NotImplementedError:
            pass
        old_context.draw(screen)
        new_context.draw(screen)


class SpyContext(context.Context):
    def __init__(self, context_id, update_callback=None, resume_callback=None):
        self.id = context_id
        self.update_cb = update_callback
        self.resume_callback = resume_callback
        context.Context.__init__(self)

    def update(self, dt, sim_time):
        set_processing_context(self)
        captured.append((self.id, EMethodsType.UPDATE))
        context.Context.update(self, dt, sim_time)
        if self.update_cb:
            self.update_cb(self)
        remove_processing_context(self)

    def suspend(self):
        set_processing_context(self)
        captured.append((self.id, EMethodsType.SUSPEND))
        context.Context.suspend(self)
        remove_processing_context(self)

    def resume(self):
        set_processing_context(self)
        captured.append((self.id, EMethodsType.RESUME))
        context.Context.resume(self)
        if self.resume_callback:
            self.resume_callback(self)
        remove_processing_context(self)

    def enter(self):
        set_processing_context(self)
        captured.append((self.id, EMethodsType.ENTER))
        context.Context.enter(self)
        remove_processing_context(self)

    def exit(self):
        set_processing_context(self)
        captured.append((self.id, EMethodsType.EXIT))
        context.Context.exit(self)
        remove_processing_context(self)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        captured.append((self.id, EMethodsType.DRAW))
        context.Context.draw(self, screen, do_flip, interpolation_factor)


class SpyTransition(transitions.Transition):
    def __init__(self, context_id, new_context, update_old=False, update_new=False, skip_cb=None):
        effect = SpyEffect("effect_" + str(context_id))
        transitions.Transition.__init__(self, new_context, effect, update_old, update_new, skip_cb)
        self.id = context_id

    def update(self, dt, sim_time):
        captured.append((self.id, EMethodsType.UPDATE))
        transitions.Transition.update(self, dt, sim_time)

    def suspend(self):
        captured.append((self.id, EMethodsType.SUSPEND))
        transitions.Transition.suspend(self)

    def resume(self):
        captured.append((self.id, EMethodsType.RESUME))
        transitions.Transition.resume(self)

    def enter(self):
        captured.append((self.id, EMethodsType.ENTER))
        transitions.Transition.enter(self)

    def exit(self):
        captured.append((self.id, EMethodsType.EXIT))
        transitions.Transition.exit(self)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        captured.append((self.id, EMethodsType.DRAW))
        transitions.Transition.draw(self, screen, do_flip, interpolation_factor)


class SpyPopTransition(transitions.PopTransition):
    def __init__(self, context_id, skip_cb=None):
        effect = SpyEffect("effect_" + str(context_id))
        transitions.PopTransition.__init__(self, effect, skip_cb=skip_cb)
        self.id = context_id

    def update(self, dt, sim_time):
        captured.append((self.id, EMethodsType.UPDATE))
        transitions.PopTransition.update(self, dt, sim_time)

    def suspend(self):
        captured.append((self.id, EMethodsType.SUSPEND))
        transitions.PopTransition.suspend(self)

    def resume(self):
        captured.append((self.id, EMethodsType.RESUME))
        transitions.PopTransition.resume(self)

    def enter(self):
        captured.append((self.id, EMethodsType.ENTER))
        transitions.PopTransition.enter(self)

    def exit(self):
        captured.append((self.id, EMethodsType.EXIT))
        transitions.PopTransition.exit(self)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        captured.append((self.id, EMethodsType.DRAW))
        transitions.PopTransition.draw(self, screen, do_flip, interpolation_factor)


class SpyPushTransition(transitions.PushTransition):
    def __init__(self, context_id, new_context, skip_cb=None):
        effect = SpyEffect("effect_" + str(context_id))
        transitions.PushTransition.__init__(self, new_context, effect, skip_cb=skip_cb)
        self.id = context_id

    def update(self, dt, sim_time):
        captured.append((self.id, EMethodsType.UPDATE))
        transitions.PushTransition.update(self, dt, sim_time)

    def suspend(self):
        captured.append((self.id, EMethodsType.SUSPEND))
        transitions.PushTransition.suspend(self)

    def resume(self):
        captured.append((self.id, EMethodsType.RESUME))
        transitions.PushTransition.resume(self)

    def enter(self):
        captured.append((self.id, EMethodsType.ENTER))
        transitions.PushTransition.enter(self)

    def exit(self):
        captured.append((self.id, EMethodsType.EXIT))
        transitions.PushTransition.exit(self)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        captured.append((self.id, EMethodsType.DRAW))
        transitions.PushTransition.draw(self, screen, do_flip, interpolation_factor)


class CommonBaseTestMethods(unittest.TestCase):
    def compare(self, expected, actual, element_name="action"):
        """
        Helper method to compare to lists.
        """
        if len(expected) != len(actual):
            self.fail(str.format(
                "Not same number of expected and captured {0}s! \n expected: {1} \n captured: {2}", element_name,
                ", ".join(map(str, expected)),
                ", ".join(map(str, actual))))
        for idx, expected_action in enumerate(expected):
            action = actual[idx]
            if action != expected_action:
                self.fail(str.format(
                    "captured {0} does not match with expected {0}! \n expected: {1} \n captured: {2}",
                    element_name,
                    ", ".join(map(str, expected)),
                    ", ".join(map(str, actual))))

    def compare_stack(self, expected_stack):
        # if len(expected_stack) != context.length():
        # self.fail("expected stack differs from ")

        stack = []
        for idx in range(context.length()):
            stack.append(context.top(idx))
        stack.reverse()
        self.compare(expected_stack, stack, "context stack")

    def setUp(self):
        lock.acquire()
        self.con = SpyContext(1)
        global current_context
        current_context = None
        context.set_deferred_mode(False)
        context.pop(context.length() + 1, do_resume=False)  # remove all previous contexts
        global captured
        captured = []  # reset

        self.assertEqual(context.length(), 0,
                         "initial context stack length should be 0 but is " + str(context.length()) + str(
                             context.top()))
        self.assertEqual(len(captured), 0, "captured should be empty but is " + str(captured))

    def tearDown(self):
        lock.release()


class TestContext(CommonBaseTestMethods):
    def test_push_calls_enter(self):
        context.push(self.con)
        expected = [(1, EMethodsType.ENTER)]
        self.compare(expected, captured)
        self.compare_stack([self.con])

    def test_push_calls_suspend_and_enter_on_second_context(self):
        con2 = SpyContext(2)
        context.push(self.con)
        context.push(con2)
        expected = [(1, EMethodsType.ENTER), (1, EMethodsType.SUSPEND), (2, EMethodsType.ENTER)]
        self.compare(expected, captured)
        self.compare_stack([self.con, con2])

    def test_top_returns_None_without_context(self):
        self.assertTrue(context.top() is None, "top should returns None if no context is pushed")

    def test_top_returns_topmost_context(self):
        context.push(self.con)
        self.assertTrue(context.top() is self.con, "top should return context")
        con2 = SpyContext(2)
        context.push(con2)
        self.assertTrue(context.top() is con2, "top should return context2 after pushing context2")
        context.pop()
        self.assertTrue(context.top() is self.con, "top should return context after pop")
        self.compare_stack([self.con])

    def test_pop(self):
        context.push(self.con)
        context.pop()
        expected = [(1, EMethodsType.ENTER), (1, EMethodsType.EXIT)]
        self.compare(expected, captured)
        self.compare_stack([])

    def test_pop_top(self):
        con2 = SpyContext(2)
        context.push(self.con)
        context.push(con2)
        context.pop()
        expected = [(1, EMethodsType.ENTER),
                    (1, EMethodsType.SUSPEND),
                    (2, EMethodsType.ENTER),
                    (2, EMethodsType.EXIT),
                    (1, EMethodsType.RESUME),
                    ]
        self.compare(expected, captured)
        self.compare_stack([self.con])

    def test_pop_do_no_resume(self):

        contexts = []
        for i in range(10):
            cont = SpyContext(i)
            contexts.append(cont)
            context.push(cont)

        context.pop(5, False)

        expected = [
            (0, EMethodsType.ENTER), (0, EMethodsType.SUSPEND),
            (1, EMethodsType.ENTER), (1, EMethodsType.SUSPEND),
            (2, EMethodsType.ENTER), (2, EMethodsType.SUSPEND),
            (3, EMethodsType.ENTER), (3, EMethodsType.SUSPEND),
            (4, EMethodsType.ENTER), (4, EMethodsType.SUSPEND),
            (5, EMethodsType.ENTER), (5, EMethodsType.SUSPEND),
            (6, EMethodsType.ENTER), (6, EMethodsType.SUSPEND),
            (7, EMethodsType.ENTER), (7, EMethodsType.SUSPEND),
            (8, EMethodsType.ENTER), (8, EMethodsType.SUSPEND),
            (9, EMethodsType.ENTER),
            (9, EMethodsType.EXIT),
            (8, EMethodsType.EXIT),
            (7, EMethodsType.EXIT),
            (6, EMethodsType.EXIT),
            (5, EMethodsType.EXIT),
            (4, EMethodsType.RESUME),
        ]
        self.compare(expected, captured)
        self.compare_stack(contexts[0:5])

    def test_pop_empty_stack(self):
        try:
            context.pop()
        except Exception as e:
            self.fail("Calling pop() on empty stack should not give exception: " + str(e))

    def test_top_n(self):
        context.push(self.con)
        context_new = SpyContext("context_new")
        context.push(context_new)

        self.assertTrue(context.top() is context_new, "should be context_new")
        self.assertTrue(context.top(0) is context_new, "should be context_new using 1")
        self.assertTrue(context.top(1) is self.con, "should be self.con")
        self.assertTrue(context.top(2) is None, "should be None")
        self.compare_stack([self.con, context_new])

    def test_length(self):
        self.assertTrue(context.length() == 0, "should be 0")
        context.push(self.con)
        self.assertTrue(context.length() == 1, "should be 1 after a single push")
        self.compare_stack([self.con])

    def test_length_from_context_instance(self):
        self.assertTrue(context.length() == 0, "should be 0")
        context.push(self.con)
        self.assertTrue(self.con.get_stack_length() == 1, "should be 1 after a single push")
        self.compare_stack([self.con])

    def test_exchange(self):
        # arrange
        expected = [(1, EMethodsType.ENTER),
                    (1, EMethodsType.EXIT),
                    (2, EMethodsType.ENTER),
                    ]
        con2 = SpyContext(2)

        # act
        context.push(self.con)
        context.exchange(con2)

        # verify
        self.compare(expected, captured)
        self.compare_stack([con2])


class TestDeferredModeContextStack(CommonBaseTestMethods):
    def setUp(self):
        CommonBaseTestMethods.setUp(self)
        context.set_deferred_mode(True)

    def test_should_not_call_pop_multiple_times_after_effect_is_done(self):
        context_new0 = SpyContext(0)
        context.push(context_new0)
        context_new = SpyContext("context_new")
        trans = SpyTransition(2, context_new)
        trans.effect.update_return = True  # effect is done
        context.push(self.con)
        context.push(trans)
        context.update()
        context.top().update(1, 1)
        context.top().update(1, 2)
        context.update()
        context.top().update(1, 3)
        context.top().update(1, 4)
        context.update()
        expected = [
            (0, 'enter'),
            (0, 'suspend'),
            (1, EMethodsType.ENTER),
            (2, EMethodsType.ENTER),
            ("context_new", EMethodsType.ENTER),
            ("effect_2", EMethodsType.ENTER),
            (2, EMethodsType.UPDATE),
            ("effect_2", EMethodsType.UPDATE),
            (2, EMethodsType.UPDATE),
            ("effect_2", EMethodsType.UPDATE),
            (2, EMethodsType.EXIT),
            ("effect_2", EMethodsType.EXIT),
            (1, EMethodsType.EXIT),
            ("context_new", EMethodsType.UPDATE),
            ("context_new", EMethodsType.UPDATE),
        ]
        self.compare(expected, captured)
        self.compare_stack([context_new0, context_new])

    def test_disable_deferred_mode_executes_remaining_commands(self):
        # arrange
        context.push(self.con)
        context.pop()

        # act
        self.assertEqual(len(captured), 0, "captured should be empty but is " + str(captured))
        context.set_deferred_mode(False)

        # verify
        self.compare([(1, EMethodsType.ENTER), (1, EMethodsType.EXIT)], captured)
        self.compare_stack([])

    def test_multiple_calls_to_update_do_nothing(self):
        # arrange
        context.push(self.con)
        context.pop()

        # act
        context.update()
        context.update()
        context.update()
        context.update()

        # verify
        self.compare([(1, EMethodsType.ENTER), (1, EMethodsType.EXIT)], captured)
        self.compare_stack([])

    def test_call_to_update_in_non_deferred_mode_does_nothing(self):
        # arrange
        context.push(self.con)
        context.pop()
        context.set_deferred_mode(False)

        # act
        context.update()
        context.update()
        context.update()

        # verify
        self.compare([(1, EMethodsType.ENTER), (1, EMethodsType.EXIT)], captured)
        self.compare_stack([])

    def test_pop_top_from_within_update(self):
        # arrange
        con2 = SpyContext(2, lambda con: con.pop())
        context.push(self.con)
        context.push(con2)
        context.update()

        # act
        con2.update(1.0, 1.0)
        context.update()

        # verify
        expected = [(1, EMethodsType.ENTER),
                    (1, EMethodsType.SUSPEND),
                    (2, EMethodsType.ENTER),
                    (2, EMethodsType.UPDATE),
                    (2, EMethodsType.EXIT),
                    (1, EMethodsType.RESUME),
                    ]
        self.compare(expected, captured)
        self.compare_stack([self.con])

    def test_pop_top_resume_push_pop(self):
        # arrange
        con2 = SpyContext(2, update_callback=lambda con: con.pop())
        con4 = SpyContext(4)
        con3 = SpyContext(3, resume_callback=lambda con: con.push(con4))
        context.push(self.con)
        context.push(con3)
        context.push(con2)
        context.update()

        # act
        con2.update(1.0, 1.0)
        context.update()

        # verify
        expected = [
            (1, EMethodsType.ENTER),
            (1, EMethodsType.SUSPEND),
            (3, EMethodsType.ENTER),
            (3, EMethodsType.SUSPEND),
            (2, EMethodsType.ENTER),
            (2, EMethodsType.UPDATE),
            (2, EMethodsType.EXIT),
            (3, EMethodsType.RESUME),
            (3, EMethodsType.SUSPEND),
            (4, EMethodsType.ENTER),
        ]
        self.compare(expected, captured)
        self.compare_stack([self.con, con3, con4])

    def test_exchange(self):
        # arrange
        con1 = SpyContext(1)
        context.push(con1)
        con2 = SpyContext(2)
        context.exchange(con2)
        self.assertEqual(0, len(captured), "in deferred mode nothing should have happened until now")

        expected = [
            (1, EMethodsType.ENTER),
            (1, EMethodsType.EXIT),
            (2, EMethodsType.ENTER),
        ]

        # act
        context.update()
        actual = captured

        # verify
        self.assertEqual(expected, actual)


class TestTransition(CommonBaseTestMethods):
    def test_transition(self):
        context_old = SpyContext("context_old")
        context_new = SpyContext("context_new")

        context.push(context_old)

        context_trans = SpyTransition("context_trans", context_new)

        context.push(context_trans)
        context.pop()

        expected = [
            ("context_old", EMethodsType.ENTER),

            ("context_trans", EMethodsType.ENTER),
            ("context_new", EMethodsType.ENTER),
            ("effect_context_trans", EMethodsType.ENTER),

            ("context_trans", EMethodsType.EXIT),
            ("effect_context_trans", EMethodsType.EXIT),
            ("context_old", EMethodsType.EXIT),
        ]
        self.compare(expected, captured)
        self.assertTrue(context.top() is context_new, "top should be context_new after transition  has been removed")
        self.compare_stack([context_new])
        context.pop()
        self.assertTrue(context.top() is None, "no context should be present on stack")
        self.compare_stack([])

    def test_transition_with_effect_update(self):
        context_old = SpyContext("context_old")
        context_new = SpyContext("context_new")

        context.push(context_old)

        context_trans = SpyTransition("context_trans", context_new)

        context.push(context_trans)
        context.top().update(1, 1)
        context_trans.effect.update_return = True
        context.top().update(1, 2)

        expected = [
            ("context_old", EMethodsType.ENTER),

            ("context_trans", EMethodsType.ENTER),
            ("context_new", EMethodsType.ENTER),
            ("effect_context_trans", EMethodsType.ENTER),

            ("context_trans", EMethodsType.UPDATE),
            ("effect_context_trans", EMethodsType.UPDATE),
            ("context_trans", EMethodsType.UPDATE),
            ("effect_context_trans", EMethodsType.UPDATE),

            ("context_trans", EMethodsType.EXIT),
            ("effect_context_trans", EMethodsType.EXIT),
            ("context_old", EMethodsType.EXIT),
        ]
        self.compare(expected, captured)
        self.assertTrue(context.top() is context_new, "top should be context_new after transition  has been removed")
        self.compare_stack([context_new])
        context.pop()
        self.assertTrue(context.top() is None, "no context should be present on stack")
        self.compare_stack([])

    def test_transition_skip_effect(self):
        # arrange
        context_old = SpyContext("context_old")
        context_new = SpyContext("context_new")

        context.push(context_old)

        self.n = 0

        def cb():
            self.n += 1
            if self.n == 2:
                return True
            return False

        context_trans = SpyTransition("context_trans", context_new, skip_cb=cb)

        # act
        context.push(context_trans)
        context.top().update(1, 1)
        context.top().update(1, 2)
        context.top().update(1, 3)

        # verify
        expected = [
            ("context_old", EMethodsType.ENTER),

            ("context_trans", EMethodsType.ENTER),
            ("context_new", EMethodsType.ENTER),
            ("effect_context_trans", EMethodsType.ENTER),

            ("context_trans", EMethodsType.UPDATE),
            ("effect_context_trans", EMethodsType.UPDATE),
            ("context_trans", EMethodsType.UPDATE),
            ("effect_context_trans", EMethodsType.UPDATE),

            ("context_trans", EMethodsType.EXIT),
            ("effect_context_trans", EMethodsType.EXIT),
            ("context_old", EMethodsType.EXIT),
            ("context_new", EMethodsType.UPDATE),
        ]
        self.compare(expected, captured)
        self.assertTrue(context.top() is context_new, "top should be context_new after transition  has been removed")
        self.compare_stack([context_new])
        context.pop()
        self.assertTrue(context.top() is None, "no context should be present on stack")
        self.compare_stack([])

    def test_transition_updating_context(self):
        context_old = SpyContext("context_old")
        context_new = SpyContext("context_new")

        context.push(context_old)

        context_trans = SpyTransition("context_trans", context_new, True, True)

        context.push(context_trans)

        context_trans.update(1.12, 1.12)

        context.pop()

        expected = [
            ("context_old", EMethodsType.ENTER),

            ("context_trans", EMethodsType.ENTER),
            ("context_new", EMethodsType.ENTER),
            ("effect_context_trans", EMethodsType.ENTER),

            ("context_trans", EMethodsType.UPDATE),
            ("context_old", EMethodsType.UPDATE),
            ("context_new", EMethodsType.UPDATE),
            ("effect_context_trans", EMethodsType.UPDATE),

            ("context_trans", EMethodsType.EXIT),
            ("effect_context_trans", EMethodsType.EXIT),
            ("context_old", EMethodsType.EXIT),
        ]
        self.compare(expected, captured)
        self.assertTrue(context.top() is context_new, "top should be context_new after transition  has been removed")
        self.compare_stack([context_new])
        context.pop()
        self.assertTrue(context.top() is None, "no context should be present on stack")
        self.compare_stack([])

    def test_transition_effect_enter_exit_suspend_resume(self):
        con2 = SpyContext(2)
        trans = SpyTransition("trans", con2)
        con3 = SpyContext(3)

        context.push(self.con)
        context.push(trans)

        context.push(con3)
        context.pop()  # con3

        context.pop()  # transition

        expected = [
            (1, EMethodsType.ENTER),
            ("trans", EMethodsType.ENTER),
            (2, EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.SUSPEND),
            (1, EMethodsType.SUSPEND),
            (2, EMethodsType.SUSPEND),
            ("effect_trans", EMethodsType.SUSPEND),

            (3, EMethodsType.ENTER),
            (3, EMethodsType.EXIT),

            ("trans", EMethodsType.RESUME),
            (1, EMethodsType.RESUME),
            (2, EMethodsType.RESUME),
            ("effect_trans", EMethodsType.RESUME),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (1, EMethodsType.EXIT),
        ]
        self.compare(expected, captured)
        self.compare_stack([con2])


class TestPopTransition(CommonBaseTestMethods):
    def test_pop_transition(self):
        context.push(self.con)
        con2 = SpyContext(2)
        context.push(con2)

        trans = SpyPopTransition("trans")

        context.push(trans)
        context.pop()

        expected = [
            (1, EMethodsType.ENTER),
            (1, EMethodsType.SUSPEND),
            (2, EMethodsType.ENTER),

            (1, EMethodsType.RESUME),
            ("trans", EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (2, EMethodsType.EXIT),
        ]

        self.compare(expected, captured)
        self.assertTrue(context.top() is self.con, "top should be self.con after transition has been removed")
        self.compare_stack([self.con])

    def test_pop_single_context_with_pop_transition_and_effect(self):
        context.push(self.con)

        trans = SpyPopTransition("trans")
        trans.update_old = True
        trans.update_new = True

        context.push(trans)
        context.top().update(1, 1)
        context.top().draw(1)
        context.pop()

        expected = [
            (1, EMethodsType.ENTER),
            ("trans", EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.UPDATE),
            (1, EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),

            ("trans", EMethodsType.DRAW),
            ("effect_trans", EMethodsType.DRAW),
            (1, EMethodsType.DRAW),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (1, EMethodsType.EXIT),
        ]

        self.compare(expected, captured)
        # self.assertTrue(context.top() is self.con, "top should be self.con after transition has been removed")
        self.assertIsNone(context.top(), "there should no context be left on the stack")
        self.compare_stack([])

    def test_pop_transition_with_effect(self):
        context.push(self.con)
        con2 = SpyContext(2)
        context.push(con2)

        trans = SpyPopTransition("trans")

        context.push(trans)
        context.top().update(1, 1)
        context.top().update(1, 2)
        trans.effect.update_return = True
        context.top().update(1, 3)

        expected = [
            (1, EMethodsType.ENTER),
            (1, EMethodsType.SUSPEND),
            (2, EMethodsType.ENTER),

            (1, EMethodsType.RESUME),
            ("trans", EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),
            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),
            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (2, EMethodsType.EXIT),
        ]

        self.compare(expected, captured)
        self.assertTrue(context.top() is self.con, "top should be self.con after transition has been removed")
        self.compare_stack([self.con])

    def test_pop_transition_using_skip(self):
        # arrange
        context.push(self.con)
        con2 = SpyContext(2)
        context.push(con2)

        self.n = 0

        def skip_cb():
            self.n += 1
            if self.n == 2:
                return True
            return False

        trans = SpyPopTransition("trans", skip_cb)

        # act
        context.push(trans)
        context.top().update(1, 1)
        context.top().update(1, 2)
        context.top().update(1, 3)

        # verify
        expected = [
            (1, EMethodsType.ENTER),
            (1, EMethodsType.SUSPEND),
            (2, EMethodsType.ENTER),

            (1, EMethodsType.RESUME),
            ("trans", EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),

            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (2, EMethodsType.EXIT),

            (1, EMethodsType.UPDATE),
        ]

        self.compare(expected, captured)
        self.assertTrue(context.top() is self.con, "top should be self.con after transition has been removed")
        self.compare_stack([self.con])

    def test_pop_transition_effect_enter_exit_suspend_resume(self):
        trans = SpyPopTransition("trans")
        con2 = SpyContext(2)
        con3 = SpyContext(3)

        context.push(self.con)
        context.push(con2)
        context.push(trans)

        context.push(con3)
        context.pop()  # con3

        context.pop()  # transition

        expected = [
            (1, EMethodsType.ENTER),
            (1, EMethodsType.SUSPEND),
            (2, EMethodsType.ENTER),
            (1, EMethodsType.RESUME),
            ("trans", EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.SUSPEND),
            (2, EMethodsType.SUSPEND),
            (1, EMethodsType.SUSPEND),
            ("effect_trans", EMethodsType.SUSPEND),

            (3, EMethodsType.ENTER),
            (3, EMethodsType.EXIT),

            ("trans", EMethodsType.RESUME),
            (2, EMethodsType.RESUME),
            (1, EMethodsType.RESUME),
            ("effect_trans", EMethodsType.RESUME),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (2, EMethodsType.EXIT),
        ]
        self.compare(expected, captured)
        self.compare_stack([self.con])


class TestPushTransition(CommonBaseTestMethods):
    def test_push_transition(self):
        context.push(self.con)
        con2 = SpyContext(2)

        trans = SpyPushTransition("trans", con2)

        context.push(trans)
        context.pop()

        expected = [
            (1, EMethodsType.ENTER),

            ("trans", EMethodsType.ENTER),
            (2, EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (1, EMethodsType.SUSPEND),
        ]

        self.compare(expected, captured)
        self.assertTrue(context.top() is con2, "top should be self.con after transition has been removed")
        self.compare_stack([self.con, con2])

    def test_push_transition_with_effect_update(self):
        context.push(self.con)
        con2 = SpyContext(2)

        trans = SpyPushTransition("trans", con2)

        context.push(trans)
        context.top().update(1, 1)
        context.top().update(1, 2)
        trans.effect.update_return = True
        context.top().update(1, 3)

        expected = [
            (1, EMethodsType.ENTER),

            ("trans", EMethodsType.ENTER),
            (2, EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),
            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),
            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (1, EMethodsType.SUSPEND),
        ]

        self.compare(expected, captured)
        self.assertTrue(context.top() is con2, "top should be self.con after transition has been removed")
        self.compare_stack([self.con, con2])

    def test_push_transition_with_skip_effect(self):
        # arrange
        context.push(self.con)
        con2 = SpyContext(2)

        self.n = 0

        def skip_cb():
            self.n += 1
            if self.n == 2:
                return True
            return False

        trans = SpyPushTransition("trans", con2, skip_cb=skip_cb)

        context.push(trans)

        # act
        context.top().update(1, 1)
        context.top().update(1, 2)
        context.top().update(1, 3)

        # verify
        expected = [
            (1, EMethodsType.ENTER),

            ("trans", EMethodsType.ENTER),
            (2, EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),
            ("trans", EMethodsType.UPDATE),
            ("effect_trans", EMethodsType.UPDATE),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (1, EMethodsType.SUSPEND),

            (2, EMethodsType.UPDATE),
        ]

        self.compare(expected, captured)
        self.assertTrue(context.top() is con2, "top should be self.con after transition has been removed")
        self.compare_stack([self.con, con2])

    def test_push_transition_effect_enter_exit_suspend_resume(self):
        con2 = SpyContext(2)
        trans = SpyPushTransition("trans", con2)
        con3 = SpyContext(3)

        context.push(self.con)
        context.push(trans)
        context.push(con3)
        context.pop()  # con3
        context.pop()  # transition

        expected = [
            (1, EMethodsType.ENTER),
            ("trans", EMethodsType.ENTER),
            (2, EMethodsType.ENTER),
            ("effect_trans", EMethodsType.ENTER),

            ("trans", EMethodsType.SUSPEND),
            (1, EMethodsType.SUSPEND),
            (2, EMethodsType.SUSPEND),
            ("effect_trans", EMethodsType.SUSPEND),

            (3, EMethodsType.ENTER),
            (3, EMethodsType.EXIT),

            ("trans", EMethodsType.RESUME),
            (1, EMethodsType.RESUME),
            (2, EMethodsType.RESUME),
            ("effect_trans", EMethodsType.RESUME),

            ("trans", EMethodsType.EXIT),
            ("effect_trans", EMethodsType.EXIT),
            (1, EMethodsType.SUSPEND),
        ]
        self.compare(expected, captured)
        self.compare_stack([self.con, con2])


class TestEffect(CommonBaseTestMethods):
    def test_effect_update(self):
        context_new = SpyContext("context_new")
        trans = SpyTransition(2, context_new)
        context.push(self.con)
        context.push(trans)
        context.top().update(1, 1)
        expected = [(1, EMethodsType.ENTER),
                    (2, EMethodsType.ENTER),
                    ("context_new", EMethodsType.ENTER),
                    ("effect_2", EMethodsType.ENTER),
                    (2, EMethodsType.UPDATE),
                    ("effect_2", EMethodsType.UPDATE),
                    ]
        self.compare(expected, captured)
        self.compare_stack([self.con, trans])

    def test_effect_update_done(self):
        context_new = SpyContext("context_new")
        trans = SpyTransition(2, context_new)
        trans.effect.update_return = True  # effect is done
        context.push(self.con)
        context.push(trans)
        context.top().update(1, 1)
        expected = [(1, EMethodsType.ENTER),
                    (2, EMethodsType.ENTER),
                    ("context_new", EMethodsType.ENTER),
                    ("effect_2", EMethodsType.ENTER),
                    (2, EMethodsType.UPDATE),
                    ("effect_2", EMethodsType.UPDATE),
                    (2, EMethodsType.EXIT),
                    ("effect_2", EMethodsType.EXIT),
                    (1, EMethodsType.EXIT),
                    ]
        self.compare(expected, captured)
        self.compare_stack([context_new])

    def test_effect_draw(self):
        context_new = SpyContext("context_new")
        trans = SpyTransition(2, context_new)
        context.push(self.con)
        context.push(trans)
        context.top().draw(None)
        expected = [(1, EMethodsType.ENTER),
                    (2, EMethodsType.ENTER),
                    ("context_new", EMethodsType.ENTER),
                    ("effect_2", EMethodsType.ENTER),
                    (2, EMethodsType.DRAW),
                    ("effect_2", EMethodsType.DRAW),
                    (1, EMethodsType.DRAW),
                    ("context_new", EMethodsType.DRAW),
                    ]
        self.compare(expected, captured)
        self.compare_stack([self.con, trans])

    def test_draw_pop_transition(self):
        context_new = SpyContext("context_new")
        trans = SpyPopTransition("trans")
        context.push(self.con)
        context.print_stack()
        context.push(context_new)
        # context.print_stack()
        context.push(trans)
        # context.print_stack()
        context.top().draw(None)
        # context.print_stack()
        expected = [(1, EMethodsType.ENTER),
                    (1, EMethodsType.SUSPEND),
                    ("context_new", EMethodsType.ENTER),
                    (1, EMethodsType.RESUME),
                    ("trans", EMethodsType.ENTER),
                    ("effect_trans", EMethodsType.ENTER),

                    ("trans", EMethodsType.DRAW),
                    ("effect_trans", EMethodsType.DRAW),
                    ("context_new", EMethodsType.DRAW),
                    (1, EMethodsType.DRAW),
                    ]
        self.compare(expected, captured)
        self.compare_stack([self.con, context_new, trans])

    def test_push_using_effect(self):
        # arrange
        old_context = SpyContext(0)
        new_context = SpyContext(1)
        effect = SpyEffect("effect")

        expected = [(0, 'enter'), (1, 'enter'), ('effect', 'enter'), ('effect', 'update'), ('effect', 'update'),
                    ('effect', 'exit'), (0, 'suspend')]

        context.push(old_context)

        # act
        context.push(new_context, effect)
        context.top().update(10, 10)
        effect.update_return = True  # end effect
        context.top().update(10, 20)

        actual = captured

        # verify
        self.compare(expected, captured)
        self.compare_stack([old_context, new_context])
        self.assertEqual(expected, actual)

    def test_push_using_effect_update_context(self):
        # arrange
        old_context = SpyContext(0)
        new_context = SpyContext(1)
        effect = SpyEffect("effect")

        expected = [(0, 'enter'), (1, 'enter'), ('effect', 'enter'),
                    (0, 'update'), (1, 'update'), ('effect', 'update'),
                    (0, 'update'), (1, 'update'), ('effect', 'update'),
                    ('effect', 'exit'), (0, 'suspend')]

        context.push(old_context)

        # act
        context.push(new_context, effect, True, True)
        # run the effect through update
        context.top().update(10, 10)
        effect.update_return = True  # end effect
        context.top().update(10, 20)

        actual = captured

        # verify
        self.compare(expected, captured)
        self.compare_stack([old_context, new_context])
        self.assertEqual(expected, actual)

    def test_pop_using_effect_update_context(self):
        # arrange
        old_context = SpyContext(0)
        new_context = SpyContext(1)
        effect = SpyEffect("effect")

        expected = [(0, 'enter'), (0, 'suspend'), (1, 'enter'), (0, 'resume'), ('effect', 'enter'),
                    (1, 'update'), (0, 'update'), ('effect', 'update'),
                    (1, 'update'), (0, 'update'), ('effect', 'update'),
                    ('effect', 'exit'), (1, 'exit')]

        context.push(old_context)
        context.push(new_context)

        # act
        context.pop(effect=effect, update_old=True, update_new=True)
        # run the effect through update
        context.top().update(10, 10)
        effect.update_return = True  # end effect
        context.top().update(10, 20)

        actual = captured

        # verify
        self.compare(expected, captured)
        self.compare_stack([old_context])
        self.assertEqual(expected, actual)

    def test_pop_using_effect(self):
        # arrange
        old_context = SpyContext(0)
        new_context = SpyContext(1)
        effect = SpyEffect("effect")

        expected = [(0, 'enter'), (0, 'suspend'), (1, 'enter'), (0, 'resume'), ('effect', 'enter'),
                    ('effect', 'update'),
                    ('effect', 'update'),
                    ('effect', 'exit'), (1, 'exit')]

        context.push(old_context)
        context.push(new_context)

        # act
        context.pop(effect=effect)
        # run the effect through update
        context.top().update(10, 10)
        effect.update_return = True  # end effect
        context.top().update(10, 20)

        actual = captured

        # verify
        self.compare(expected, captured)
        self.compare_stack([old_context])
        self.assertEqual(expected, actual)

    def test_exchange_using_effect_update_context(self):
        # arrange
        old_context = SpyContext(0)
        new_context = SpyContext(1)
        effect = SpyEffect("effect")

        expected = [(0, 'enter'), (1, 'enter'), ('effect', 'enter'),
                    (0, 'update'), (1, 'update'), ('effect', 'update'),
                    (0, 'update'), (1, 'update'), ('effect', 'update'),
                    ('effect', 'exit'), (0, 'exit')]

        context.push(old_context)

        # act
        context.exchange(new_context, effect, True, True)
        # run the effect through update
        context.top().update(10, 10)
        effect.update_return = True  # end effect
        context.top().update(10, 20)

        actual = captured

        # verify
        self.compare(expected, captured)
        self.compare_stack([new_context])
        self.assertEqual(expected, actual)

    def test_exchange_using_effect(self):
        # arrange
        old_context = SpyContext(0)
        new_context = SpyContext(1)
        effect = SpyEffect("effect")

        expected = [(0, 'enter'), (1, 'enter'), ('effect', 'enter'),
                    ('effect', 'update'),
                    ('effect', 'update'),
                    ('effect', 'exit'), (0, 'exit')]

        # context.push(old_context)
        old_context.push(old_context)

        # act
        context.exchange(new_context, effect)
        # run the effect through update
        context.top().update(10, 10)
        effect.update_return = True  # end effect
        context.top().update(10, 20)

        actual = captured

        # verify
        self.compare(expected, captured)
        self.compare_stack([new_context])
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
