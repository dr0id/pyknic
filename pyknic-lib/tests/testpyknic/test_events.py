# -*- coding: utf-8 -*-
from __future__ import print_function, division

import unittest
import warnings

from pyknic import events
from pyknic.events import WeakMethodRef

warnings.simplefilter('error')  # make warnings visible for developers


class EventTests(unittest.TestCase):

    def add_handler(self, sender, *args, **kwargs):
        self.event += self.handler
        self.event += self.handler2
        self.call_order.append('add_handler')

    def self_remove(self, sender, *args, **kwargs):
        self.event -= self.self_remove
        self.call_order.append('self_remove')

    def handler(self, sender, *args, **kwargs):
        self.handler_data.append((args, kwargs))
        self.call_order.append('handler')

    def handler2(self, sender, *args, **kwargs):
        self.handler2_data.append((args, kwargs))
        self.call_order.append('handler2')

    def handled(self, sender, *args, **kwargs):
        self.handler_data.append((args, kwargs))
        self.call_order.append('handled')
        return events.HANDLED

    def handle_source(self, sender, *args, **kwargs):
        self.handler_data.append((args, kwargs))
        source, data = sender, args
        source -= self.handle_source
        self.call_order.append('handle_source')

    def setUp(self):
        self.event = events.Signal()
        self.handler_data = []
        self.handler2_data = []
        self.call_order = []

    def test_after_clear_no_handler_is_executed(self):
        self.event += self.handler
        self.event += self.handler2
        self.event += self.handled
        self.assertEqual(len(self.event), 3)
        self.event.clear()
        self.assertEqual(len(self.event), 0)
        self.event.fire()
        self.assertEqual(0, len(self.call_order))

    def test_should_not_call_handlers_if_disabled(self):
        # arrange
        self.event.enabled = False

        # act
        self.event.fire()

        # verify
        self.assertEqual(0, len(self.call_order))

    def test_multiple_commands_are_synchronized(self):
        # arrange
        self.event += self.handler
        self.event.clear()
        self.event += self.handler2
        self.event -= self.handler

        # act
        self.event.fire("sender")

        # verify
        self.assertEqual(1, len(self.call_order))
        self.assertEqual("handler2", self.call_order[0])

    def test_add_counts_has_observers_up(self):
        # arrange
        self.event.add(self.handler)
        expected = 2

        # act
        self.event.add(self.handler2)
        actual = self.event.has_observers

        # verify
        self.assertEqual(expected, actual)

    def test_remove_counts_has_observers_down(self):
        # arrange
        self.event.add(self.handler)
        self.event.add(self.handler2)
        expected = 1

        # act
        self.event.remove(self.handler)
        actual = self.event.has_observers

        # verify
        self.assertEqual(expected, actual)

    def test_clear_resets_has_observers(self):
        # arrange
        self.event.add(self.handler)
        self.event.add(self.handler2)
        self.assertEqual(2, self.event.has_observers)
        expected = 0

        # act
        self.event.clear()
        actual = self.event.has_observers

        # verify
        self.assertEqual(expected, actual)

    def test_add(self):
        self.event += self.handler
        self.event.fire(self, '1')
        self.assertEqual(self.handler_data, [(('1',), {})])

    def test_fire_by_calling(self):
        # arrange
        self.event += self.handler
        # act
        self.event(self, '1')
        # verify
        self.assertEqual(self.handler_data, [(('1',), {})])

    def test_remove(self):
        self.event += self.handler
        self.event.fire(self, '1')
        self.event -= self.handler
        self.assertEqual(self.handler_data, [(('1',), {})])

    def test_add_remove(self):
        self.event += self.handler
        self.event -= self.handler
        self.event.fire('3')
        self.assertEqual(self.handler_data, [])

    def test_add_remove2(self):
        self.event += self.handler
        self.event += self.handler2
        self.event -= self.handler
        self.event.fire(self, '4')
        self.assertEqual(self.handler2_data, [(('4',), {})])
        self.assertEqual(self.handler_data, [])

    def test_len(self):
        warnings.simplefilter('error')
        try:
            self.assertEqual(len(self.event), 0)
            self.event += self.handler
            len(self.event)
            self.assertEqual(len(self.event), 1)
            self.event -= self.handler
            self.assertEqual(len(self.event), 0)
            # print len(self.event._to_add), len(self.event._to_remove), len(self.event._callbacks), '\n'
            self.event += self.handler
            self.event += self.handler2
            self.assertEqual(len(self.event), 2)
            self.event -= self.handler
            self.assertEqual(len(self.event), 1)
        except events.EventTypeAlreadyRegisteredError:
            self.fail('Warning was raised')
        finally:
            warnings.resetwarnings()

    def test_fire(self):
        self.event += self.handler
        self.event += self.handler2
        self.event.fire(self, '8', name='bla')
        self.assertEqual(self.handler_data, [(('8',), {'name': 'bla'})])
        self.assertEqual(self.handler2_data, [(('8',), {'name': 'bla'})])

    def test_fire_chaning(self):
        self.event += self.handler
        self.event += self.handler2
        another = self.event.__class__()
        another += self.event.fire
        another.fire(another, '8', name='bla')
        self.assertEqual(self.handler_data, [(('8',), {'name': 'bla'})])
        self.assertEqual(self.handler2_data, [(('8',), {'name': 'bla'})])

    def test_self_remove(self):
        self.event += self.self_remove
        self.assertEqual(len(self.event), 1)
        self.event.fire(self)
        self.assertEqual(len(self.event), 0)

    def test_self_remove2(self):
        self.event += self.handle_source
        self.assertEqual(len(self.event), 1)
        self.event.fire(self.event, '6a', name='bla')
        self.assertEqual(len(self.event), 0)
        self.assertEqual(self.handler_data, [(('6a',), {'name': 'bla'})], 'handler_data: ' + str(self.handler_data))

    def test_adder(self):
        self.event += self.add_handler
        self.assertEqual(len(self.event), 1)
        self.event.fire(self)
        self.assertEqual(len(self.event), 3)

    # def test_warnings(self):
    # try:
    # warnings.simplefilter('error')
    # self.event += self.handler
    # self.assertRaises(Exception, self.event.add, self.handler)
    # self.event -= self.handler
    # self.assertRaises(events.RemoveWarning, self.event.remove, self.handler)
    # finally:
    # warnings.resetwarnings()

    def test_remove_non_existent(self):
        try:
            warnings.simplefilter("ignore")
            self.event -= self.handler
            self.event.fire(self)
        except:
            self.fail('removing unkown handler should not raise any exception')
        finally:
            warnings.resetwarnings()

    def test_return_HANDELD_order(self):
        self.event += self.handled
        self.event += self.handler2
        self.event.fire(self.event)
        # print '\n', self.call_order
        self.assertTrue(self.call_order == ['handler2', 'handled'], 'wrong callorder')
        self.assertTrue(len(self.handler_data) == 1, 'no influence, wrong order')
        self.assertTrue(len(self.handler2_data) == 1, 'no influence, wrong order 2')

    def test_return_HANDELD(self):
        self.event += self.handler2
        self.event += self.handled
        self.event.fire(self.event)
        # print '\n', self.call_order
        self.assertTrue(self.call_order == ['handled'], 'wrong callorder')
        self.assertTrue(len(self.handler_data) == 1, 'order, break processing')
        self.assertTrue(len(self.handler2_data) == 0, 'order, break processing 2')

    def test_clear(self):
        self.event += self.handler
        self.event += self.handler2
        self.event += self.handled
        self.assertEqual(len(self.event), 3)
        self.event.clear()
        self.assertEqual(len(self.event), 0)

    def test_str(self):
        s = str(self.event)
        # compare = "<%s('%s') at %s>" % (type(self.event).__name__, self.event.name, hex(id(self.event)))
        self.assertTrue(len(s) > 0, 'wrong __str__: ' + s)

    def test_add_same_handler_many_times(self):
        self.event += self.handler
        self.event += self.handler
        self.event += self.handler

        self.event.fire(self, 1)
        self.assertEqual(self.handler_data, [((1,), {})])


class ReversedEventTests(EventTests):

    def setUp(self):
        self.event = events.Signal("name", events.NEW_LAST)
        self.handler_data = []
        self.handler2_data = []
        self.call_order = []

    def test_return_HANDELD_order(self):
        self.event += self.handled
        self.event += self.handler2
        self.event.fire(self)
        # print '\n', self.call_order
        self.assertTrue(self.call_order == ['handled'], 'wrong callorder')
        self.assertTrue(len(self.handler_data) == 1, 'no influence, wrong order')
        self.assertTrue(len(self.handler2_data) == 0, 'no influence, wrong order 2')

    def test_return_HANDELD(self):
        self.event += self.handler2
        self.event += self.handled
        self.event.fire(self)
        # print '\n', self.call_order
        self.assertTrue(self.call_order == ['handler2', 'handled'], 'wrong callorder')
        self.assertTrue(len(self.handler_data) == 1, 'order, break processing')
        self.assertTrue(len(self.handler2_data) == 1, 'order, break processing 2')


class SignalProviderTests(unittest.TestCase):

    def handler(self, sender, *args, **kwargs):
        self.data = sender, args, kwargs

    def setUp(self):
        self.provider = events.SignalProvider()
        self.data = None

    def test_same(self):
        sig = self.provider.test
        sig2 = self.provider['test']
        self.assertTrue(sig is sig2, '\nsig =' + str(sig) + ' \nsig2=' + str(sig2) + ' \n\n' + str(self.provider))
        self.assertTrue(sig == sig2, '\nsig =' + str(sig) + ' \nsig2=' + str(sig2) + ' \n\n' + str(self.provider))

    def test_same2(self):
        sig2 = self.provider['test2']
        sig = self.provider.test2
        self.assertTrue(sig is sig2, 'sig=\n' + str(sig) + ' sig2=\n' + str(sig2) + ' \n\n' + str(self.provider))
        self.assertTrue(sig == sig2, 'sig=\n' + str(sig) + ' sig2=\n' + str(sig2) + ' \n\n' + str(self.provider))

    def test_fire(self):
        sig = self.provider['test']
        sig += self.handler
        self.provider['test'].fire(1000, 99)
        self.assertTrue(self.data == (1000, (99,), {}), 'fire through SignalProvider failed')

    def test_fire_by_attribute(self):
        sig = self.provider['test']
        sig += self.handler
        self.provider.test.fire(1000, 99)
        self.assertTrue(self.data == (1000, (99,), {}), 'fire through SignalProvider attribute failed')

    def test_fire_by_attribute_and_direct_call(self):
        sig = self.provider['test']
        sig += self.handler
        self.provider.test(1000, 99)
        self.assertTrue(self.data == (1000, (99,), {}), 'fire through SignalProvider attribute and direct call failed')

    def test_fire_disabled(self):
        sig = self.provider['test']
        sig += self.handler
        self.provider.enabled = False
        self.provider['test'].fire(self, 77)
        self.assertTrue(self.data is None)


# global test helper variable
wobj = False


class W(object):
    """ Test helper class for the TestEventsysSignalWeakMethodRef"""

    def __init__(self):
        global wobj
        wobj = True

    def m(self):
        """bound method for testing"""

        return 1

    def __del__(self):
        """register to the global variable then you know the instance is dead"""

        global wobj
        wobj = False


class EventsSignalWeakMethodRefTests(unittest.TestCase):
    """Test cases for the WeakMethodRef class"""

    def test_wobj(self):
        w = W()
        global wobj
        self.assertTrue(True == wobj)
        self.assertTrue(1 == w.m())
        del w
        self.assertFalse(wobj)

    def test_bound_instance(self):
        obj = W()
        wm_ref1 = WeakMethodRef(obj.m)
        try:
            res1 = wm_ref1()
        except Exception as e:
            self.fail(e)

    def test_bound_return_value(self):
        obj2 = W()
        wm_ref = WeakMethodRef(obj2.m)
        res = wm_ref()
        # there is still a strong ref: obj
        # so it should return a strong ref
        self.assertFalse(res is None, '1')

        del obj2
        del res  # need to deleted this because it holds a strong ref
        # wm_ref() should return None
        res2 = wm_ref()
        self.assertTrue(res2 is None, '3')

        # verify that the wobj died
        global wobj
        self.assertFalse(wobj, '2')

    def test_bound_equal(self):
        w = W()
        a = WeakMethodRef(w.m)
        b = WeakMethodRef(w.m)
        self.assertTrue(a.__eq__(b), '__eq__() explicit')
        self.assertTrue(a == b, '== ->__eq__() implicit')

    def test_bound_not_equal(self):
        w1 = W()
        w2 = W()
        a = WeakMethodRef(w1.m)
        b = WeakMethodRef(w2.m)
        self.assertTrue(a.__ne__(b), '__eq__() explicit')
        self.assertTrue(a != b, '== ->__eq__() implicit')

    def test_bound_call_method(self):
        w = W()
        # print dir(w.m)
        wref = WeakMethodRef(w.m)
        self.assertTrue(1 == wref()())

    def test_repr(self):
        w = W()
        wref2 = WeakMethodRef(w.m)
        try:
            repr2 = str(wref2)
        except Exception as e:
            self.fail(e)
        try:
            del w
            repr3 = str(wref2)
        except Exception as e:
            self.fail(e)


data = None


def unbound(*args, **kwargs):
    global data
    data = (args, kwargs)


class WeakEventTests(EventTests):

    def setUp(self):
        self.event = events.WeakSignal("name", events.NEW_FIRST)
        self.handler_data = []
        self.handler2_data = []
        self.call_order = []

    def test_weakref_dead_while_fire(self):
        global wobj
        w = W()
        self.event += w.m  # the handler that should die
        self.event += self.handler  # to control if events are fired
        del w
        self.event.fire(self, 1, 2)
        self.assertTrue(len(self.handler_data) == 1 and len(self.call_order) == 1,
                        "deleted handler called since not dead, handlers count: " + str(self.handler_data))

    def test_weakref_unbound_method(self):
        self.event += unbound
        self.event.fire(self.event, '6a', name='bla')
        global data
        self.assertTrue(data == ((self.event, '6a',), {'name': 'bla'}), "unbound call failed")


class WeakEventReversedTests(WeakEventTests):

    def setUp(self):
        self.event = events.WeakSignal("name", events.NEW_LAST)
        self.handler_data = []
        self.handler2_data = []
        self.call_order = []

    def test_del_weak_ref_counts_has_observers_down(self):
        # arrange
        w = W()
        self.event.add(w.m)
        self.assertEqual(1, self.event.has_observers)
        expected = 0

        # act
        del w
        actual = self.event.has_observers

        # verify
        self.assertEqual(expected, actual)


EVT_TYPE1 = 0
EVT_TYPE2 = 1
EVT_TYPE3 = 2

CB1 = 1
CB2 = 2
CB3 = 8
UNREG_CB = 3
FIREING_CB_SINGLE = 6
ENQUEU_CB_SINGLE = 7
ENQUEU_CB = 4
RET_HANDLED_CB = 5


class EventDispatcherTests(unittest.TestCase):

    def callback1(self, *args, **kwargs):
        self.cb1.append((args, kwargs))
        self.order.append(CB1)

    def callback2(self, *args, **kwargs):
        self.cb2.append((args, kwargs))
        self.order.append(CB2)

    def callback3(self, *args, **kwargs):
        self.cb3.append((args, kwargs))
        self.order.append(CB3)

    def unregister_cb(self, *args, **kwargs):
        self.unreg_cb.append((args, kwargs))
        self.dispatcher.remove_listener(self.unregister_cb)
        self.order.append(UNREG_CB)

    def firing_cb_single(self, *args, **kwargs):
        self.order.append(FIREING_CB_SINGLE)
        self.dispatcher.fire(EVT_TYPE2, "sender: enqueueing_cb_fire_single", "args")
        self.update_dispatcher()

    def return_handled_cb(self, *args, **kwargs):
        self.return_handled_cb_args.append((args, kwargs))
        self.order.append(RET_HANDLED_CB)
        return events.HANDLED

    class Observer(object):
        order = []

        def __init__(self):
            self.event1_args = []
            self.event2_args = []
            self.event3_args = []

        def on_event1(self, *args):
            self.event1_args.append(args)
            self.order.append((self, 1))

        def on_event2(self, *args):
            self.event2_args.append(args)
            self.order.append((self, 2))

        def on_event3(self, *args):
            self.event3_args.append(args)
            self.order.append((self, 3))

    class EventAttributes(object):
        def __init__(self):
            self.args = []
            self.kwargs = []
            self.order = []
            self.senders = []

    def setUp(self):
        self.dispatcher = events.EventDispatcher()
        events.EventDispatcher._event_types.clear()  # clears all known event types
        assert len(events.EventDispatcher._event_types) == 0
        assert len(self.dispatcher._event_types) == 0
        self.cb1 = []
        self.cb2 = []
        self.cb3 = []
        self.unreg_cb = []
        self.order = []
        self.return_handled_cb_args = []
        self.enqueueing_cb_count = 0
        self.Observer.order = []
        self.observers = [self.Observer(), self.Observer(), self.Observer()]

    def update_dispatcher(self):
        pass

    def tearDown(self):
        events.EventDispatcher._event_types.clear()  # clears all known event types

    def test_valid_eventtype(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE1), "event type should be valid")
        self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE2) == False, "eventtype2 should not be valid")

    def test_dispatch_fire(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.register_event_type(EVT_TYPE2)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE2, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(len(self.cb1) == 1, "callback1 not called")
        self.assertTrue(len(self.cb2) == 0, "callback2 called, should not be")

    def test_fire_should_not_do_anything_without_listeners(self):
        # arrange
        self.dispatcher.register_event_type(EVT_TYPE1)

        # act
        self.dispatcher.fire(EVT_TYPE1)

        # verify
        self.assertEqual(0, len(self.cb1), "no callback should have been called")

    def test_dispatch_fire_self_remove(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.unregister_cb)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(len(self.unreg_cb) == 1, "unregister_cb not called")
        self.assertTrue(len(self.cb1) == 1, "callback1 not called")
        self.assertTrue(len(self.cb2) == 1, "callback2 not called")

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(len(self.unreg_cb) == 1, "unregister_cb should not be called twice")
        self.assertTrue(len(self.cb1) == 2, "callback1 not called")
        self.assertTrue(len(self.cb2) == 2, "callback2 not called")

    def test_dispatch_fire_self_remove_wrong(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.remove_listener(self.unregister_cb)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(len(self.cb1) == 1, "unregister_cb not called")

    def test_dispatch_fire_multiple(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(len(self.cb1) == 1, "callback1 not called")
        self.assertTrue(len(self.cb2) == 1, "callback2 not called")

    def test_dispatch_should_not_add_new_event_types(self):
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_FIRST)

        if __debug__:
            self.assertRaises(AssertionError, self.dispatcher.fire, EVT_TYPE2, "sender")
        else:
            res = self.dispatcher.fire(EVT_TYPE2, "sender", "args")

            self.assertTrue(res == False, "fire should not return True if enqueueing invalid event type")
            self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE2) == False,
                            "unregistered but fired event type should not be valid")

    def test_cb_call_order_default(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB2, CB1], "wrong call order")

    def test_cb_call_order_reversed(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.NEW_LAST)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB1, CB2], "wrong call order")

    def test_fire_HANDLED_cb(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)
        self.dispatcher.add_listener(EVT_TYPE1, self.return_handled_cb)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [RET_HANDLED_CB], "no other handler should be called after returning HANDLED")

    def test_add_listener_twice_warning(self):
        if __debug__:
            with warnings.catch_warnings(record=True) as caught_warning:
                # Cause all warnings to always be triggered.
                warnings.simplefilter("always")
                # Trigger a warning.
                self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_FIRST)
                self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_FIRST)  # triggers warning

                self.assertTrue(len(caught_warning) == 1, "only one warning should have been raised")
                self.assertTrue(caught_warning[-1].category is UserWarning, "should be a userwarning")
        else:
            warnings.warn("warnings are only raised if run in __debug__ mode")

    # def test_register_event_type_twice_with_different_sort_order_warning(self):
    # if __debug__:
    # with warnings.catch_warnings(record=True) as caught_warning:
    # # Cause all warnings to always be triggered.
    # warnings.simplefilter("always")
    # # Trigger a warning.
    # self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_FIRST)
    # self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_LAST)

    # if __debug__:
    # self.assertTrue(len(caught_warning) == 1, "it should raise a warning if the same event type is registered twice with different sort order")
    # self.assertTrue(caught_warning[-1].category is UserWarning, "should be a userwarning")
    # else:
    # warnings.warn("warnings are only raised if run in __debug__ mode")

    def test_register_event_type_twice_raises_exception(self):
        self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_FIRST)
        try:
            self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_FIRST)
            self.fail("should have raised EventTypeAlreadyRegisteredError")
        except events.EventTypeAlreadyRegisteredError as err:
            pass
        except Exception as ex:
            self.fail(str(ex))

    def test_register_event_type(self):
        self.dispatcher.register_event_type(EVT_TYPE1)

        self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE1), "event type should be known")
        self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE2) == False, "event type should not be known")
        self.assertTrue(EVT_TYPE1 in self.dispatcher.event_types, "event type should have been added")

    # def test_add_listener_adds_event_type(self):
    # self.dispatcher.add_listener(EVT_TYPE1, self.callback1)

    # self.assertTrue(EVT_TYPE1 in self.dispatcher.event_types, "add listeners did not register event type")
    # self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE1), "event should be valid after adding a listener for it")

    def test_add_listener_sort_order_default(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB2, CB1], "order of callbacks is wrong")

    def test_add_listener_sort_order_specified(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.NEW_LAST)

        self.dispatcher.fire(EVT_TYPE1, "sener", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB1, CB2], "order of callbacks is wrong")

    def test_add_listener_sort_order_specified_already_registered_event_type_first(self):
        self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_FIRST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.NEW_LAST)

        self.dispatcher.fire(EVT_TYPE1, "sener", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB1, CB2], "order of callbacks is wrong")

    def test_add_listener_sort_order_specified_already_registered_event_type_last(self):
        self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_FIRST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.NEW_FIRST)

        self.dispatcher.fire(EVT_TYPE1, "sener", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB2, CB1], "order of callbacks is wrong")

    def test_add_listener_sort_order_default_already_registered_event_type_last(self):
        self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.NEW_LAST)

        self.dispatcher.fire(EVT_TYPE1, "sener", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB1, CB2], "order of callbacks is wrong")

    def test_add_listener_sort_order_default_specified_order_1(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_FIRST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback3, events.NEW_FIRST)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB3, CB1, CB2], "order of callbacks is wrong")

    def test_add_listener_sort_order_default_specified_order_2(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.NEW_LAST)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback3)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB3, CB2, CB1], "order of callbacks is wrong")

    def test_fire_order_of_callback_triggered_event(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.register_event_type(EVT_TYPE2)
        self.dispatcher.register_event_type(EVT_TYPE3)

        self.dispatcher.add_listener(EVT_TYPE1, self.firing_cb_single)
        self.dispatcher.add_listener(EVT_TYPE3, self.callback3)
        self.dispatcher.add_listener(EVT_TYPE2, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "bla bla args")
        self.dispatcher.fire(EVT_TYPE3, "bla bla args")
        self.update_dispatcher()

        self.assertTrue(self.order == [FIREING_CB_SINGLE, CB2, CB3], "events have been fire in wrong order")

    def test_push_instance(self):
        self.dispatcher.register_event_type('on_event1')
        self.dispatcher.register_event_type('on_event2')

        self.dispatcher.push_listeners(self.observers[0])

        self.dispatcher.fire('on_event1', "sender", 1)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event1_args) == 1,
                        "callback not called, probably not registered through push")

        self.dispatcher.fire('on_event2', "sender", 2)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event2_args) == 1,
                        "callback not called, probably not registered through push")

        if __debug__:
            self.assertRaises(AssertionError, self.dispatcher.fire, 'on_event3', "sender", 3)
        else:
            self.dispatcher.fire('on_event3', "sender", 3)
            self.assertTrue(len(self.observers[0].event3_args) == 0, "callback called should not since not registered")

    def test_pop_instance(self):
        self.dispatcher.register_event_type('on_event1')
        self.dispatcher.register_event_type('on_event2')

        self.dispatcher.push_listeners(self.observers[0])
        self.dispatcher.pop_listeners(self.observers[0])

        self.dispatcher.fire('on_event1', "sender", 1)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event1_args) == 0, "callback1 should not have been called")

        self.dispatcher.fire('on_event2', "sender", 2)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event2_args) == 0, "callback2 should not have been called")

    def test_push_multiple_instances(self):
        self.dispatcher.register_event_type('on_event1')
        self.dispatcher.register_event_type('on_event2')
        self.dispatcher.register_event_type('on_event3')

        self.dispatcher.push_listeners(self.observers[0])
        self.dispatcher.push_listeners(self.observers[1])
        self.dispatcher.push_listeners(self.observers[2])

        self.dispatcher.fire('on_event1', "sender", 1)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event1_args) == 1,
                        "obs0, callback1 not called, probably not registered through push")
        self.assertTrue(len(self.observers[1].event1_args) == 1,
                        "obs1, callback1 not called, probably not registered through push")
        self.assertTrue(len(self.observers[2].event1_args) == 1,
                        "obs2, callback1 not called, probably not registered through push")

        self.dispatcher.fire('on_event2', "sender", 2)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event2_args) == 1,
                        "obs0, callback2 not called, probably not registered through push")
        self.assertTrue(len(self.observers[1].event2_args) == 1,
                        "obs1, callback2 not called, probably not registered through push")
        self.assertTrue(len(self.observers[2].event2_args) == 1,
                        "obs2, callback2 not called, probably not registered through push")

        self.dispatcher.fire('on_event3', "sender", 3)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event2_args) == 1,
                        "obs0, callback not3 called, probably not registered through push")
        self.assertTrue(len(self.observers[1].event2_args) == 1,
                        "obs0, callback not3 called, probably not registered through push")
        self.assertTrue(len(self.observers[2].event2_args) == 1,
                        "obs0, callback not3 called, probably not registered through push")

        call_order = [(self.observers[2], 1),
                      (self.observers[1], 1),
                      (self.observers[0], 1),
                      (self.observers[2], 2),
                      (self.observers[1], 2),
                      (self.observers[0], 2),
                      (self.observers[2], 3),
                      (self.observers[1], 3),
                      (self.observers[0], 3)]
        self.assertTrue(self.observers[0].order == call_order, "wrong callorder")

    # def test_push_multiple_instances_reverse_order(self):
    # self.dispatcher.register_event_type('on_event1')
    # self.dispatcher.register_event_type('on_event2')
    # self.dispatcher.register_event_type('on_event3')

    # self.dispatcher.push_new_last(self.observers[0])
    # self.dispatcher.push_new_last(self.observers[1])
    # self.dispatcher.push_new_last(self.observers[2])

    # self.dispatcher.fire('on_event1', "sender", 1)
    # self.assertTrue(len(self.observers[0].event1_args) == 1, "obs0, callback1 not called, probably not registered through push")
    # self.assertTrue(len(self.observers[1].event1_args) == 1, "obs1, callback1 not called, probably not registered through push")
    # self.assertTrue(len(self.observers[2].event1_args) == 1, "obs2, callback1 not called, probably not registered through push")

    # self.dispatcher.fire('on_event2', "sender", 2)
    # self.assertTrue(len(self.observers[0].event2_args) == 1, "obs0, callback2 not called, probably not registered through push")
    # self.assertTrue(len(self.observers[1].event2_args) == 1, "obs1, callback2 not called, probably not registered through push")
    # self.assertTrue(len(self.observers[2].event2_args) == 1, "obs2, callback2 not called, probably not registered through push")

    # self.dispatcher.fire('on_event3', "sender", 3)
    # self.assertTrue(len(self.observers[0].event3_args) == 1, "obs0, callback3 not called, probably not registered through push")
    # self.assertTrue(len(self.observers[1].event3_args) == 1, "obs0, callback3 not called, probably not registered through push")
    # self.assertTrue(len(self.observers[2].event3_args) == 1, "obs0, callback3 not called, probably not registered through push")

    # call_order = [(self.observers[0], 1),
    # (self.observers[1], 1),
    # (self.observers[2], 1),
    # (self.observers[0], 2),
    # (self.observers[1], 2),
    # (self.observers[2], 2),
    # (self.observers[0], 3),
    # (self.observers[1], 3),
    # (self.observers[2], 3)]
    # self.assertTrue(self.observers[0].order == call_order, "wrong callorder")

    def test_push_routine(self):
        self.dispatcher.register_event_type('on_event1')
        self.dispatcher.register_event_type('on_event2')

        self.dispatcher.push_listeners(on_event1)  # see below for the function

        eventattr = self.EventAttributes()

        self.dispatcher.fire('on_event1', "sender1", eventattr, 1, 2, 3)
        self.update_dispatcher()
        self.assertTrue(len(eventattr.args) == 1, "routine callback not called, probably not registered through push")

        self.dispatcher.fire('on_event2', "sender2", eventattr, 9, 8, 7)
        self.update_dispatcher()
        self.assertTrue(len(eventattr.args) == 1, "callback should not have been called again")

    def test_pop_instances(self):
        self.dispatcher.register_event_type('on_event1')
        self.dispatcher.register_event_type('on_event2')
        self.dispatcher.register_event_type('on_event3')

        self.dispatcher.push_listeners(self.observers[0])
        self.dispatcher.push_listeners(self.observers[1])
        self.dispatcher.push_listeners(self.observers[2])

        self.dispatcher.fire('on_event1', "sender", 1)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event1_args) == 1,
                        "obs0, callback1 not called, probably not registered through push")
        self.assertTrue(len(self.observers[1].event1_args) == 1,
                        "obs1, callback1 not called, probably not registered through push")
        self.assertTrue(len(self.observers[2].event1_args) == 1,
                        "obs2, callback1 not called, probably not registered through push")

        self.dispatcher.pop_listeners(self.observers[0])

        self.dispatcher.fire('on_event2', "sender", 2)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event2_args) == 0,
                        "obs0, callback2 should not be called, probably not unregistered through pop")
        self.assertTrue(len(self.observers[1].event2_args) == 1,
                        "obs1, callback2 not called, probably not registered through push")
        self.assertTrue(len(self.observers[2].event2_args) == 1,
                        "obs2, callback2 not called, probably not registered through push")

        self.dispatcher.pop_listeners(self.observers[1])

        self.dispatcher.fire('on_event3', "sender", 3)
        self.update_dispatcher()
        self.assertTrue(len(self.observers[0].event3_args) == 0,
                        "obs0, callback3 should not be called, probably not unregistered through pop")
        self.assertTrue(len(self.observers[1].event3_args) == 0,
                        "obs0, callback3 should not be called, probably not unregistered through pop")
        self.assertTrue(len(self.observers[2].event3_args) == 1,
                        "obs0, callback3 not called, probably not registered through push")

        call_order = [(self.observers[2], 1),
                      (self.observers[1], 1),
                      (self.observers[0], 1),
                      (self.observers[2], 2),
                      (self.observers[1], 2),
                      (self.observers[2], 3), ]
        self.assertTrue(self.observers[0].order == call_order, "wrong callorder")

    def test_register_event_sort_order(self):
        self.dispatcher.register_event_type(EVT_TYPE2, events.NEW_FIRST)
        self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_LAST)

        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)

        self.dispatcher.add_listener(EVT_TYPE2, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE2, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "sender1")
        self.dispatcher.fire(EVT_TYPE2, "sender2")
        self.update_dispatcher()

        expected_order = [CB1, CB2, CB2, CB1]
        self.assertEqual(self.order, expected_order,
                         "callbacks called in wrong order, expected: " + str(expected_order) + " got: " + str(
                             self.order))

    def test_push_routine_with_unkown_eventtype(self):
        self.assertRaises(events.EventTypeUnknownException, self.dispatcher.push_listeners, on_event1)

    def test_call_order_set_by_register_event_used_by_push(self):
        self.dispatcher.register_event_type('on_event2', events.NEW_LAST)
        self.dispatcher.register_event_type('on_event1')

        self.dispatcher.push_listeners(*self.observers)

        self.dispatcher.fire('on_event1', "sender", "args")
        self.update_dispatcher()

        call_order = [(self.observers[2], 1),
                      (self.observers[1], 1),
                      (self.observers[0], 1), ]
        self.assertTrue(self.observers[0].order == call_order, "wrong callorder for on_event1")

        self.dispatcher.fire('on_event2', "sender", "args")
        self.update_dispatcher()

        call_order.append((self.observers[0], 2))
        call_order.append((self.observers[1], 2))
        call_order.append((self.observers[2], 2))

        self.assertTrue(self.observers[0].order == call_order, "wrong callorder for on_event2")

    def test_call_order_add_listener_with_sort_order_None(self):
        self.dispatcher.register_event_type(EVT_TYPE1, events.NEW_LAST)

        self.dispatcher.add_listener(EVT_TYPE1, self.callback1, events.USE_EVENT_TYPE_ORDER)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2, events.USE_EVENT_TYPE_ORDER)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback3, events.USE_EVENT_TYPE_ORDER)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.update_dispatcher()

        self.assertTrue(self.order == [CB1, CB2, CB3], "wrong call order")

    def test_add_listener_raises_EventTypeUnkownException_with_sort_order_None(self):
        self.assertRaises(events.EventTypeUnknownException, self.dispatcher.add_listener, 'event_type????',
                          self.callback1, events.USE_EVENT_TYPE_ORDER)

    def test_warning_if_no_event_listener_could_be_added_through_push(self):
        if __debug__:
            with warnings.catch_warnings(record=True) as caught_warning:
                # Cause all warnings to always be triggered.
                warnings.simplefilter("always")
                # Trigger a warning.
                self.dispatcher.push_listeners(object())  # triggers warning no listeners added

                self.assertTrue(len(caught_warning) == 1, "only one warning should have been raised")
                self.assertTrue(caught_warning[-1].category is UserWarning, "should be a userwarning")
        else:
            warnings.warn("warnings are only raised if run in __debug__ mode")


def on_event1(sender, eventattr, *args, **kwargs):
    eventattr.args.append(args)
    eventattr.kwargs.append(kwargs)
    eventattr.order.append('on_event1')
    eventattr.senders.append(sender)


class TestEventQueueDispatcherFireInstant(EventDispatcherTests):

    def setUp(self):
        EventDispatcherTests.setUp(self)
        self.dispatcher = events.EventQueueDispatcher()
        self.dispatcher.fire = self.dispatcher.fire_instant


class TestEventQueueDispatcher(EventDispatcherTests):

    def setUp(self):
        EventDispatcherTests.setUp(self)
        self.dispatcher = events.EventQueueDispatcher()

    def update_dispatcher(self):
        self.dispatcher.update_all()

    def test_dispatch_update_all_self_remove(self):
        self.dispatcher.register_event_type(EVT_TYPE1)

        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.unregister_cb)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.dispatcher.update_all()

        self.assertTrue(len(self.unreg_cb) == 1, "unregister_cb not called")
        self.assertTrue(len(self.cb1) == 1, "callback1 not called")
        self.assertTrue(len(self.cb2) == 1, "callback2 not called")

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.dispatcher.update_all()

        self.assertTrue(len(self.unreg_cb) == 1, "unregister_cb should not be called twice")
        self.assertTrue(len(self.cb1) == 2, "callback1 not called")
        self.assertTrue(len(self.cb2) == 2, "callback2 not called")

    def test_instance_queues(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        other = events.EventQueueDispatcher()
        other.add_listener(EVT_TYPE1, self.callback2)
        # EVT_TYPE1 is already registered
        # other.register_event_type(EVT_TYPE1)

        self.dispatcher.fire(EVT_TYPE1, "sender")
        self.assertTrue(len(other) == 0, "event has been added to both dispatchers!")

        other.fire(EVT_TYPE1, "sender")

        other.update_all()
        self.assertTrue(len(self.cb1) == 0, "callback from wrong dispatcher instance has been called")

        self.dispatcher.update_all()
        self.assertTrue(len(self.cb2) == 1, "callback from wrong dispatcher instance has been called")

    def enqueueing_cb_single(self, *args, **kwargs):
        self.order.append(ENQUEU_CB_SINGLE)
        self.dispatcher.fire(EVT_TYPE2, "sender: enqueueing_cb_fire_single", "args")

    def enqueueing_cb(self, *args, **kwargs):
        self.order.append(ENQUEU_CB)
        self.enqueueing_cb_count += 1
        if self.enqueueing_cb_count < 100:
            self.dispatcher.fire(EVT_TYPE1, "enqueueing_cb", self.enqueueing_cb_count)
            self.dispatcher.fire(EVT_TYPE1, "enqueueing_cb", self.enqueueing_cb_count)
        else:
            self.fail("enqueueing_cb called to many times")

    def test_enqueue_event_no_dispatch_until_update_all(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.register_event_type(EVT_TYPE2)

        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE2, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "sender1", "args1")
        self.dispatcher.fire(EVT_TYPE2, "sender2", "args2")
        self.dispatcher.fire(EVT_TYPE2, "sender2", "args2")
        self.dispatcher.fire(EVT_TYPE1, "sender1", "args1")
        self.dispatcher.fire(EVT_TYPE1, "sender1", "args1")

        self.assertTrue(len(self.cb1) == 0, "callback1 called, should not")
        self.assertTrue(len(self.cb2) == 0, "callback1 called, should not")

        self.dispatcher.update_all()

        self.assertTrue(len(self.cb1) == 3, "callback1 not called correct number of times %s" % len(self.cb1))
        self.assertTrue(len(self.cb2) == 2, "callback1 not called correct number of times")
        self.assertTrue(self.order == [CB1, CB2, CB2, CB1, CB1], "callbacks called in wrong order")

    # def test_enqueue_event_update_all_should_not_add_new_event_types(self):
    # self.dispatcher.add_listener(EVT_TYPE1, self.callback1)

    # if __debug__:
    # self.assertRaises(AssertionError, self.dispatcher.enqueue_event, EVT_TYPE2, "sender2", "args2")
    # else:
    # res = self.dispatcher.fire(EVT_TYPE2, "sender2", "args2")
    # self.dispatcher.update_all()

    # self.assertTrue(res == False, "post should not return True if enqueueing invalid event type")
    # self.assertTrue(self.dispatcher.is_event_type_valid(EVT_TYPE2) == False, "unregistered event type should not be added by post(), update_all()")

    def test_enqueue_event_update_all_callback_enqueueing_event_limit_recursion(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.enqueueing_cb)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.dispatcher.update_all()

        self.assertTrue(self.enqueueing_cb_count == 1, "enqueueing callback called to many times in a update_all")
        self.assertTrue(len(self.dispatcher) == 2,
                        "enqueueing callback posted wrong number of events, wrong number of events in event queue")

    def test_enqueue_event_update_all_empty_queue_no_error(self):
        self.dispatcher.update_all()

    def test_update_all_event_processing_average(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)

        for i in range(100):
            self.dispatcher.fire(EVT_TYPE1, "sender", i)

        res = self.dispatcher.update_all()

        self.assertTrue(len(self.cb1) == 100, "callback should have been called 100 times")

        for i in range(200):
            self.dispatcher.fire(EVT_TYPE1, "sender", i)

        res = self.dispatcher.update_all()

        self.assertTrue(len(self.cb1) == 300,
                        "callback should have been called 300 times by now, but were: " + str(len(self.cb1)))

        res = self.dispatcher.update_all()

        self.assertTrue(len(self.cb1) == 300,
                        "callback should have been called 300 times by now, but where: " + str(len(self.cb1)))

    def test_enqueue_event_HANDLED_cb(self):
        self.dispatcher.register_event_type(EVT_TYPE1)

        self.dispatcher.add_listener(EVT_TYPE1, self.callback1)
        self.dispatcher.add_listener(EVT_TYPE1, self.callback2)
        self.dispatcher.add_listener(EVT_TYPE1, self.return_handled_cb)

        self.dispatcher.fire(EVT_TYPE1, "sender", "args")
        self.dispatcher.update_all()

        self.assertTrue(self.order == [RET_HANDLED_CB], "no other handler should be called after returning HANDLED")

    def test_fire_order_of_enqueued_callback_triggered_event(self):
        self.dispatcher.register_event_type(EVT_TYPE1)
        self.dispatcher.register_event_type(EVT_TYPE2)
        self.dispatcher.register_event_type(EVT_TYPE3)
        self.dispatcher.add_listener(EVT_TYPE1, self.enqueueing_cb_single)
        self.dispatcher.add_listener(EVT_TYPE3, self.callback3)
        self.dispatcher.add_listener(EVT_TYPE2, self.callback2)

        self.dispatcher.fire(EVT_TYPE1, "bla bla args")
        self.dispatcher.fire(EVT_TYPE3, "bla bla args")
        self.dispatcher.update_all()
        # the second update_all is needed because the event triggered by a callback
        # is processed on the next update_all
        self.dispatcher.update_all()

        self.assertTrue(self.order == [ENQUEU_CB_SINGLE, CB3, CB2], "events have been fire in wrong order")

    def test_enqueue_unregistered_event_type_raises(self):
        self.assertRaises(AssertionError, self.dispatcher.fire, EVT_TYPE1)


if __name__ == '__main__':
    unittest.main()
