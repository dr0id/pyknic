# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_statemachines.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.ai.statemachines module.
"""
from __future__ import print_function, division

import unittest
import warnings

from pyknic.ai.statemachines import SimpleStateMachine, StateDrivenAgentBase, SwitchingStateException

warnings.simplefilter('error')  # make warnings visible for developers


class NullState(object):
    @staticmethod
    def enter(state_machine):
        pass

    @staticmethod
    def exit(state_machine):
        pass

    @staticmethod
    def activate(state_machine):
        pass

    @staticmethod
    def deactivate(state_machine):
        pass

    @staticmethod
    def update(state_machine, dt, sim_t, *args, **kwargs):
        pass


class RecordingState(object):
    @staticmethod
    def enter(owner):
        owner.record_call("enter", owner)

    @staticmethod
    def exit(owner):
        owner.record_call("exit", owner)

    # noinspection PyUnusedLocal
    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.record_call("update", owner, RecordingState)


class RecordingState2(RecordingState):
    pass


class StateSwitchInEnter(NullState):

    @staticmethod
    def enter(sm):
        sm.switch_state(RecordingState)


class StateSwitchInActivate(NullState):

    @staticmethod
    def activate(sm):
        sm.switch_state(NullState)


class RecordingStateMachine(SimpleStateMachine):
    call_order = []

    def record_call(self, msg, owner, *args):
        owner.call_order.append((msg, id(self.current_state), id(owner), args))


class StateMachineTests(unittest.TestCase):
    def setUp(self):
        self.longMessage = True
        self.maxDiff = None
        self.state_machine = RecordingStateMachine(NullState, "MyStateMachine")
        self.state_machine.call_order = []
        RecordingStateMachine.call_order = []
        self.recorded = []

    def test_first_state_set(self):
        # arrange
        new_state = RecordingState()

        # act
        self.state_machine.switch_state(new_state)

        # verify
        self.assertTrue(self.state_machine.current_state == new_state, "first state not set to current")

    def test_first_state_enter_called(self):
        # arrange
        new_state = RecordingState()

        # act
        self.state_machine.switch_state(new_state)

        # verify
        self.assertEqual(self.state_machine.call_order, [("enter", id(new_state), id(self.state_machine), ())],
                         "first state enter method not called")

    def test_call_exit_at_switch(self):
        # arrange
        state1 = RecordingState()
        state2 = RecordingState()

        # act
        self.state_machine.switch_state(state1)
        self.state_machine.switch_state(state2)

        # verify
        calls = [
            ("enter", id(state1), id(self.state_machine), ()),
            ("exit", id(state1), id(self.state_machine), ()),
            ("enter", id(state2), id(self.state_machine), ())
        ]
        self.assertTrue(len(self.state_machine.call_order) == len(calls), "not enough calls made")
        self.assertEqual(self.state_machine.call_order[0], calls[0], "missed an enter of state1")
        self.assertEqual(self.state_machine.call_order[1], calls[1], "missed an exit of state1")
        self.assertEqual(self.state_machine.call_order[2], calls[2], "missed an enter of state2")

    def test_sm_should_throw_exception_if_switch_state_is_called_recursively(self):
        # arrange
        new_state = StateSwitchInEnter()

        # act / verify
        self.assertRaises(SwitchingStateException, self.state_machine.switch_state, new_state)

    # noinspection PyUnusedLocal
    def record_event_switch(self, state_machine, old_state, sender):
        self.recorded.append((state_machine.name, old_state.__name__))

    def test_should_fire_event_switched_state(self):
        # arrange
        self.state_machine.event_switched_state += self.record_event_switch

        # act
        self.state_machine.switch_state(RecordingState)

        # verify
        expected = [("MyStateMachine", "NullState")]
        self.assertEqual(expected, self.recorded, "event switched arguments are wrong")
        self.assertEqual(RecordingState, self.state_machine.current_state, "current state should be new state")

    # noinspection PyUnusedLocal
    def record_event_switching(self, state_machine, old_state, new_state, sender):
        self.recorded.append((state_machine.name, old_state.__name__, new_state.__name__))

    def test_should_fire_event_switching_states(self):
        # arrange
        self.state_machine.event_switching_state += self.record_event_switching

        # act
        self.state_machine.switch_state(RecordingState)

        # verify
        expected = [("MyStateMachine", "NullState", "RecordingState")]
        self.assertEqual(expected, self.recorded, "should have fired switching state with old and new state")

    def _transition_action(self, sm):
        self.recorded.append(sm.name)

    def test_switch_state_should_call_transition_action_if_provided(self):
        # act
        self.state_machine.switch_state(NullState, self._transition_action)
        actual = self.recorded

        # verify
        expected = [self.state_machine.name]
        self.assertEqual(expected, actual, "Switch state should call the provided transition action")

    def test_revert_to_previous_state(self):
        # arrange
        self.state_machine.switch_state(RecordingState)
        self.state_machine.switch_state(RecordingState2)

        # act
        self.state_machine.revert_to_previous_state()

        # verify
        self.assertEqual(RecordingState, self.state_machine.current_state, "Current state should be RecordingState")
        self.assertEqual(RecordingState2, self.state_machine.previous_state, "Previous state should be RecordingState2")
        expected = [
            ('enter', id(RecordingState)),
            ('exit', id(RecordingState)),
            ('enter', id(RecordingState2)),
            ('exit', id(RecordingState2)),
            ('enter', id(RecordingState)),
        ]
        actual = [_v[:2] for _v in self.state_machine.call_order]
        self.assertEqual(expected, actual, "State switch enter exit call order wrong.")

    def test_global_state_is_updated(self):
        # arrange
        RecordingStateMachine.call_order = []
        self.state_machine = RecordingStateMachine(NullState, "RecStateMachine", global_state=RecordingState)

        # act
        self.state_machine.update(1.0, 3.0)

        # verify
        self.assertEqual(NullState, self.state_machine.current_state, "Current state should be NullState")
        expected = [("update", id(self.state_machine.current_state), id(self.state_machine), (RecordingState, ))]
        self.assertEqual(expected, self.state_machine.call_order, "Update call on global state should be called.")


class AgentBaseState(object):

    @staticmethod
    def enter(agent):
        agent.record_call("enter", agent, agent.state_machine.current_state)

    @staticmethod
    def exit(agent):
        agent.record_call("exit", agent, agent.state_machine.current_state)

    @staticmethod
    def handle_event(agent, event):
        agent.record_call("handle_event", agent, agent.state_machine.current_state, event)

    @staticmethod
    def activate(agent):
        agent.record_call("activate", agent, agent.state_machine.current_state)

    @staticmethod
    def deactivate(agent):
        agent.record_call("deactivate", agent, agent.state_machine.current_state)

    @staticmethod
    def update(agent, dt, sim_t, *args, **kwargs):
        agent.record_call("update", agent, agent.state_machine.current_state, dt, sim_t, args, kwargs)


class AgentGlobalState(object):

    @staticmethod
    def update(agent, dt, sim_t, *args, **kwargs):
        agent.record_call("update", agent, AgentGlobalState, dt, sim_t, args, kwargs)


class AgentState0(AgentBaseState):

    @staticmethod
    def handle_event(agent, event):
        AgentBaseState.handle_event(agent, event)
        if event == 1:
            agent.state_machine.switch_state(AgentState1)
        elif event == 2:
            agent.state_machine.switch_state(AgentState1, action=AgentState0.transition2_action)
        elif event == 9:
            agent.state_machine.push_state(AgentState1)

    @staticmethod
    def transition2_action(agent):
        agent.record_call("action", agent, AgentState0)


class AgentState1(AgentBaseState):

    @staticmethod
    def handle_event(agent, event):
        if event == 0:
            agent.state_machine.switch_state(AgentState0)
        elif event == 9:
            agent.state_machine.pop_state()


class RecordingAgent(StateDrivenAgentBase):
    def __init__(self):
        self.recorded_calls = []
        super(RecordingAgent, self).__init__(AgentState0, self)
        AgentState0.enter(self)  # this line is only important if you need initial state to be entered

    def record_call(self, *args):
        self.recorded_calls.append(args)


class AgentTests(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        self.longMessage = True
        self.agent = RecordingAgent()

    def test_agent_should_change_state_on_event_1(self):
        # act
        self.agent.handle_event(1)

        # verify
        expected = [
            ("enter", self.agent, AgentState0),
            ("handle_event", self.agent, AgentState0, 1),
            ("exit", self.agent, AgentState0),
            ("enter", self.agent, AgentState1),
        ]
        self.assertEqual(expected, self.agent.recorded_calls, "Method calls order should be identical.")

    def test_agent_should_not_change_state_on_event_0(self):
        # act
        self.agent.handle_event(0)

        # verify
        expected = [
            ("enter", self.agent, AgentState0),
            ("handle_event", self.agent, AgentState0, 0),
        ]
        self.assertEqual(expected, self.agent.recorded_calls, "Should ignore event 0")

    def test_agent_should_pass_owner_when_update_is_called(self):
        # arrange
        self.agent.recorded_calls = []

        # act
        self.agent.update(1.0, 3)

        # verify
        expected = [
            ("update", self.agent, AgentState0, 1.0, 3, (), {})
        ]
        self.assertEqual(expected, self.agent.recorded_calls, "Should pass owner in update method.")

    def test_agent_should_pass_owner_to_global_state_too(self):
        # arrange
        self.agent.state_machine.global_state = AgentGlobalState
        self.agent.recorded_calls = []

        # act
        self.agent.update(2, 3)

        # verify
        expected = [
            ("update", self.agent, AgentGlobalState, 2, 3, (), {}),
            ("update", self.agent, AgentState0, 2, 3, (), {}),
        ]
        self.assertEqual(expected, self.agent.recorded_calls, "Should have updated global state before current state.")

    def test_agent_pass_owner_to_events(self):
        # arrange
        self.agent.state_machine.event_switching_state += self.record_switching_state
        self.agent.state_machine.event_switched_state += self.record_switched_state

        # act
        self.agent.handle_event(1)

        # verify
        expected = [
            ("enter", self.agent, AgentState0),
            ("handle_event", self.agent, AgentState0, 1),
            ("exit", self.agent, AgentState0),
            ("event_switching", (self.agent, AgentState0, AgentState1, self.agent.state_machine)),
            # ("action", self.agent, AgentState0),
            ("enter", self.agent, AgentState1),
            ("event_switched", (self.agent, AgentState0, self.agent.state_machine)),
        ]
        self.assertEqual(expected, self.agent.recorded_calls, "order or arguments of state switch is wrong for events")

    def record_switching_state(self, *args):
        self.agent.record_call("event_switching", args)

    def record_switched_state(self, *args):
        self.agent.record_call("event_switched", args)

    def test_agent_should_call_transition_action_with_owner(self):
        # act
        self.agent.handle_event(2)

        # verify
        expected = [
            ("enter", self.agent, AgentState0),
            ("handle_event", self.agent, AgentState0, 2),
            ("exit", self.agent, AgentState0),
            ("action", self.agent, AgentState0),
            ("enter", self.agent, AgentState1),
        ]

        self.assertEqual(expected, self.agent.recorded_calls, "order or arguments of state switch is wrong for action")


if __name__ == '__main__':
    unittest.main()
