# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_registry.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.registry module.
"""
from __future__ import print_function, division
import unittest
import warnings

import pyknic.registry as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class _Entity(object):

    def __init__(self, unique_id, kind):
        self.id = unique_id
        self.kind = kind


class _EntityWithoutKind(object):

    def __init__(self, unique_id):
        self.id = unique_id


class RegistryTests(unittest.TestCase):

    def setUp(self):
        self.sut = mut.Registry()
        ent1_kind1 = _Entity(5, 1)
        self.sut.register(ent1_kind1, ent1_kind1.id, ent1_kind1.kind)
        ent2_kind1 = _Entity(6, 1)
        self.sut.register(ent2_kind1, ent2_kind1.id, ent2_kind1.kind)
        ent3_kind2 = _Entity(7, 2)
        self.sut.register(ent3_kind2, ent3_kind2.id, ent3_kind2.kind)

        self.entities = [ent1_kind1, ent2_kind1, ent3_kind2]

    def test_register_entity_without_kind(self):
        # arrange
        entity = _EntityWithoutKind(33)

        # act
        self.sut.register_entity(entity)

        # verify
        self.assertTrue(entity in self.sut.get_all())

    def test_register_entity_to_multiple_kinds_at_once(self):
        # arrange
        kind1 = 1 << 0
        kind2 = 1 << 2
        kind3 = 1 << 4
        kinds = kind1 | kind2 | kind3
        ent = _Entity(99, kinds)

        # act
        self.sut.register_entity(ent)

        # verify
        for k in self.sut.by_kind.keys():
            if k & ent.kind:
                self.assertTrue(ent in self.sut.by_kind[k], "should be in " + str(k))
            else:
                self.assertFalse(ent in self.sut.by_kind[k], "should not be in " + str(k))

    def test_remove_entity_from_multiple_kinds_at_once(self):
        # arrange
        kind1 = 1 << 0
        kind2 = 1 << 2
        kind3 = 1 << 4
        kinds = kind1 | kind2 | kind3
        ent = _Entity(99, kinds)
        self.sut.register_entity(ent)

        # act
        self.sut.remove_entity(ent)

        # verify
        for k in self.sut.by_kind.keys():
            if k & ent.kind:
                self.assertFalse(ent in self.sut.by_kind[k], "should not be in " + str(k))

    def test_only_single_bits_kinds_are_generated(self):
        # arrange
        kind1 = 1 << 0
        kind2 = 1 << 2
        kind3 = 1 << 4
        kinds = kind1 | kind2 | kind3
        ent = _Entity(99, kinds)

        # act
        self.sut.register_entity(ent)

        # verify
        for k in self.sut.by_kind.keys():
            b = 1
            while b <= k:
                # 00101
                # 00100
                if b & k:
                    if b != k:
                        self.fail("ist not single bit kind " + str(k))
                b <<= 1

    def test_zero_kind_raises_error(self):
        # arrange
        ent = _Entity(99, 0)

        # act / verify
        self.assertRaises(ValueError, self.sut.register_entity, ent)

    def test_registry_get_by_id_method(self):
        # arrange
        ent = _Entity(4, None)

        # act
        self.sut.register(ent, ent.id)

        # verify
        self.assertEqual(ent, self.sut.get_by_id(4))

    def test_registry_get_by_id(self):
        # arrange
        ent = _Entity(4, None)

        # act
        self.sut.register(ent, ent.id)

        # verify
        self.assertEqual(ent, self.sut.by_id[4])

    def test_registry_get_by_id_not_existent_id(self):
        # act
        actual = self.sut.get_by_id(4)

        # verify
        self.assertEqual(None, actual)

    def test_registry_get_by_id_wth_default_value_for_not_existent_id(self):
        # act
        default_value = "1234"
        actual = self.sut.get_by_id(4, default_value)

        # verify
        self.assertEqual(default_value, actual)

    def test_registry_get_by_kind_method(self):
        # arrange
        kind = self.entities[0].kind

        # act
        actual = self.sut.get_by_kind(kind)

        # verify
        expected = set(self.entities[:2])
        self.assertEqual(expected, actual)

    def test_registry_get_by_kind(self):
        # arrange

        # act
        actual = self.sut.by_kind[self.entities[0].kind]

        # verify
        expected = set(self.entities[:2])
        self.assertEqual(expected, actual)

    def test_registry_get_all(self):
        # arrange
        # act
        actual = self.sut.get_all()

        # verify
        expected = set(self.entities)
        self.assertEqual(expected, actual)

    def test_registry_clear(self):
        # arrange
        # act
        self.sut.clear()

        # verify
        actual = self.sut.get_all()
        actual_kind1 = self.sut.get_by_kind(self.entities[0].kind)
        actual_kind2 = self.sut.get_by_kind(1)
        expected = set()
        self.assertEqual(expected, actual, "should have cleared the entries by id")
        self.assertEqual(expected, actual_kind1, "should have cleared the entries by kind")
        self.assertEqual(expected, actual_kind2, "should have cleared the entries by kind")

    def test_remove_entity(self):
        # arrange
        ent1, ent2, ent3 = self.entities

        # act
        self.sut.remove_by_id(ent2.id)

        # verify
        self.assertEqual({ent1, ent3}, self.sut.get_all(), "should have removed entity 2")
        self.assertEqual({ent1}, self.sut.by_kind[ent1.kind], "should leave only one entity for kind1")
        self.assertEqual({ent3}, self.sut.by_kind[ent3.kind], "should leave only one entity for kind2")

    def test_save_remove_unknown_entity(self):
        # arrange
        ent1, ent2, ent3 = self.entities

        # act
        actual = self.sut.save_remove_by_id(2345)

        # verify
        self.assertFalse(actual, "should have returned False")
        self.assertEqual({ent1, ent2, ent3}, self.sut.get_all(), "all entities should have been preserved")
        self.assertEqual({ent1, ent2}, self.sut.by_kind[ent1.kind], "all entities should have been preserved for kind1")
        self.assertEqual({ent3}, self.sut.by_kind[ent3.kind], "all entities should have been preserved for kind2")

    def test_save_remove_known_entity(self):
        # arrange
        ent1, ent2, ent3 = self.entities

        # act
        actual = self.sut.save_remove_by_id(ent2.id)

        # verify
        self.assertTrue(actual, "should have returned True")
        self.assertEqual({ent1, ent3}, self.sut.get_all(), "should have removed entity 2")
        self.assertEqual({ent1}, self.sut.by_kind[ent1.kind], "should leave only one entity for kind1")
        self.assertEqual({ent3}, self.sut.by_kind[ent3.kind], "should leave only one entity for kind2")

    def test_register_same_entity_twice(self):
        # arrange
        ent1, ent2, ent3 = self.entities

        # act / verify
        self.assertRaises(KeyError, self.sut.register, ent1, ent1.id, ent1.kind)

    def test_same_entity_registered_with_different_id_and_kind_raises_warnings(self):
        # arrange
        self.sut = mut.Registry()
        ent = _Entity(2, 1<<2)

        # act
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            self.sut.register(ent, ent.id, ent.kind)
            self.sut.register(ent, ent.id + 1, ent.kind)
            self.sut.register(ent, ent.id + 2, ent.kind + 1<<3)

            # verify
            actual = self.sut.get_all()
            expected = {ent}
            self.assertEqual(expected, actual)
            self.assertEqual(5, len(w))

    def test_get_all_entities_belonging_to_a_group_kind(self):
        # arrange
        self.sut = mut.Registry()
        kind1 = 1 << 0
        kind2 = 1 << 1
        kind3 = 1 << 2
        ent1 = _Entity(90, kind1)
        ent2 = _Entity(91, kind2)
        ent3 = _Entity(92, kind3)
        self.sut.register(ent1, ent1.id, ent1.kind)
        self.sut.register(ent2, ent2.id, ent2.kind)
        self.sut.register(ent3, ent3.id, ent3.kind)

        group = kind1 | kind2

        # act
        actual = self.sut.get_by_bit_kind(group)

        # verify
        expected = {ent1, ent2}
        self.assertEqual(expected, actual, "should have retrieved all entities belonging to the group")

    def test_register_entity_fires_registered_event(self):
        # arrange
        events = []

        def on_registered(*args):
            events.append(args)

        self.sut.event_entity_registered.add(on_registered)
        ent = _Entity(1, 1)

        # act
        self.sut.register(ent, ent.id, ent.kind)

        # verify
        expected = [(ent, ent.id, ent.kind, self.sut)]
        self.assertEqual(expected, events)

    def test_remove_by_id_fires_removed_event(self):
        # arrange
        events = []

        def on_registered(*args):
            events.append(args)

        self.sut.event_entity_removed.add(on_registered)
        ent = _Entity(1, 1)
        self.sut.register(ent, ent.id, ent.kind)

        # act
        self.sut.remove_by_id(ent.id)

        # verify
        expected = [(ent, ent.id, self.sut)]
        self.assertEqual(expected, events)

    def test_save_remove_by_id_fires_removed_event(self):
        # arrange
        events = []

        def on_registered(*args):
            events.append(args)

        self.sut.event_entity_removed.add(on_registered)
        ent = _Entity(1, 1)
        self.sut.register(ent, ent.id, ent.kind)

        # act
        self.sut.save_remove_by_id(ent.id)

        # verify
        expected = [(ent, ent.id, self.sut)]
        self.assertEqual(expected, events)

    def test_clear_fires_removed_events(self):
        # arrange
        events = []

        def on_registered(*args):
            events.append(args)

        self.sut.event_entity_removed.add(on_registered)

        # act
        self.sut.clear()

        # verify
        expected = []
        for ent in self.entities:
            expected.append((ent, ent.id, self.sut))
        self.assertEqual(expected, events)

if __name__ == '__main__':
    unittest.main()
