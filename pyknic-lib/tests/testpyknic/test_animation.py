# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_animation.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.animation module.
"""
from __future__ import print_function, division

import unittest
import warnings

import pyknic.animation as mut  # module under test
from pyknic.timing import Scheduler

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class AnimationTests(unittest.TestCase):

    def setUp(self):
        mut.Animation.scheduler = Scheduler()  # reset to make sure all schedules are correctly tested
        self.frame_count = 100
        self.interval = 1.0
        self.start_index = 10
        self.fps = 1.0 / self.interval
        self.sut = mut.Animation(self.start_index, self.frame_count, self.fps, mut.Animation.scheduler)

    def test_animation_has_current_index(self):
        # act / verify
        self.assertEqual(self.start_index, self.sut.current_index)

    def test_animation_does_not_advance_if_start_was_not_called(self):
        # arrange
        scheduler = Scheduler()

        # act
        scheduler.update(100.0, 100.0)

        # verify
        self.assertEqual(self.start_index, self.sut.current_index)

    def test_animation_does_update_current_index_after_start(self):
        # arrange
        self.sut.start()
        self.assertEqual(self.start_index, self.sut.current_index)

        # act / verify
        mut.Animation.scheduler.update(self.interval, self.interval)
        self.assertEqual(self.start_index + 1, self.sut.current_index)

        mut.Animation.scheduler.update(self.interval, 2 * self.interval)
        self.assertEqual(self.start_index + 2, self.sut.current_index)

        mut.Animation.scheduler.update(self.interval, 3 * self.interval)
        self.assertEqual(self.start_index + 3, self.sut.current_index)

    def test_animation_starts_at_a_specific_index(self):
        # arrange
        initial_index = 2
        self.sut = mut.Animation(initial_index, self.frame_count, self.fps, mut.Animation.scheduler)

        # act / verify
        self.assertEqual(initial_index, self.sut.current_index)

    def test_animation_repeats_a_frame_count(self):
        # arrange
        frame_count = 5
        self.sut = mut.Animation(self.start_index, frame_count, self.fps, mut.Animation.scheduler)

        # act
        # verify
        self.sut.start()
        self.assertEqual(self.start_index, self.sut.current_index)

        self.sut.scheduler.update(self.interval, self.interval)
        self.assertEqual(self.start_index + 1, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 2 * self.interval)
        self.assertEqual(self.start_index + 2, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 3 * self.interval)
        self.assertEqual(self.start_index + 3, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 4 * self.interval)
        self.assertEqual(self.start_index + 4, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 5 * self.interval)
        self.assertEqual(self.start_index, self.sut.current_index)

    def test_animation_repeats_a_frame_count_with_initial_index(self):
        # arrange
        frame_count = 5
        initial_index = 20
        self.sut = mut.Animation(initial_index, frame_count, self.fps, mut.Animation.scheduler)

        # act
        # verify
        self.sut.start()
        self.assertEqual(20, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 1 * self.interval)
        self.assertEqual(21, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 2 * self.interval)
        self.assertEqual(22, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 3 * self.interval)
        self.assertEqual(23, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 4 * self.interval)
        self.assertEqual(24, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 5 * self.interval)
        self.assertEqual(20, self.sut.current_index)

    def test_animation_fire_end_animation_callback(self):
        # arrange
        callbacks = []

        def animation_end_callback(*args, **kwargs):
            callbacks.append((args, kwargs))

        self.sut.event_animation_end += animation_end_callback
        self.sut.start()

        # act
        self.sut.scheduler.update(self.interval * self.frame_count, self.interval * self.frame_count)

        # verify
        self.assertEqual(1, len(callbacks))

    def test_play_animation_only_once_calling_stop_in_animation_end_cb(self):
        # arrange

        # noinspection PyUnusedLocal
        def animation_stop_cb(anim, *args, **kwargs):
            anim.stop()

        self.sut.event_animation_end += animation_stop_cb
        self.sut.start()

        # act
        self.sut.scheduler.update(self.interval * self.frame_count, self.interval * self.frame_count)

        # verify
        self.assertFalse(self.sut.is_running)
        self.assertEqual(self.start_index + self.frame_count - 1, self.sut.current_index, "should stop at last index")

    def test_play_animation_after_stop_start_in_animation_end_cb(self):
        # arrange

        # noinspection PyUnusedLocal
        def animation_stop_cb(anim, *args, **kwargs):
            anim.stop()
            anim.is_looping = False
            anim.start()

        self.sut.event_animation_end += animation_stop_cb
        self.sut.start()

        # act
        self.sut.scheduler.update(self.interval * self.frame_count, self.interval * self.frame_count)

        # verify
        self.assertTrue(self.sut.is_running)
        self.assertEqual(self.start_index + self.frame_count, self.sut.current_index, "should stop at last index")


    def test_animation_calling_start_multiple_times_does_not_alter_index_multiple_times(self):
        # arrange

        # act
        self.sut.start()
        self.sut.start()
        self.sut.start()
        self.sut.scheduler.update(self.interval, self.interval)

        # verify
        self.assertEqual(self.start_index + 1, self.sut.current_index)

    def test_animation_is_not_running_after_creation(self):
        # arrange
        # act
        # verify
        self.assertFalse(self.sut.is_running)

    def test_animation_is_running_after_calling_start(self):
        # arrange
        # act
        self.sut.start()
        # verify
        self.assertTrue(self.sut.is_running)

    def test_animation_is_not_running_after_calling_stop(self):
        # arrange
        # act
        self.sut.start()
        self.sut.stop()
        # verify
        self.assertFalse(self.sut.is_running)

    def test_animation_is_stopped_in_end_callback(self):
        # arrange
        def stop_animation(animation):
            animation.stop()

        self.sut.event_animation_end += stop_animation
        self.sut.start()
        sim_time = self.interval * self.frame_count + self.interval
        self.sut.scheduler.update(self.interval * self.frame_count + self.interval, sim_time)
        self.sut.scheduler.update(self.interval, sim_time + self.interval)

        # act
        self.sut.scheduler.update(self.interval, sim_time + self.interval * 2)

        # verify
        self.assertEqual(self.start_index + self.frame_count - 1, self.sut.current_index,
                         "animation should stay on last frame")

    class SchedulerSpy(Scheduler):

        def __init__(self):
            Scheduler.__init__(self)
            self.removed_ids = []
            self.added_ids = []

        def remove(self, scheduler_id):
            self.removed_ids.append(scheduler_id)
            Scheduler.remove(self, scheduler_id)

        def schedule(self, func, interval, offset=0, *args, **kwargs):
            schedule_id = Scheduler.schedule(self, func, interval, *args, **kwargs)
            self.added_ids.append(schedule_id)
            return schedule_id

    def test_animation_calling_stop_multiple_times_does_not_hurt(self):
        # arrange
        scheduler_spy = self.SchedulerSpy()
        self.sut.scheduler = scheduler_spy

        # act
        self.sut.start()
        self.sut.stop()
        self.sut.stop()
        self.sut.stop()
        self.sut.stop()

        # verify
        self.assertFalse(self.sut.is_running)
        self.assertEqual(1, len(scheduler_spy.removed_ids))

    def test_animation_with_frame_count_1_should_not_bother_the_scheduler_on_start(self):
        # arrange
        self.sut = mut.Animation(self.start_index, 1, self.fps, mut.Animation.scheduler)
        scheduler_spy = self.SchedulerSpy()
        self.sut.scheduler = scheduler_spy

        # act
        self.sut.start()

        # verify
        self.assertEqual(0, len(scheduler_spy.added_ids))
        self.assertTrue(self.sut.is_running)

    def test_animation_with_frame_count_1_should_not_bother_the_scheduler_on_stop(self):
        # arrange
        self.sut = mut.Animation(self.start_index, 1, self.fps, mut.Animation.scheduler)
        scheduler_spy = self.SchedulerSpy()
        self.sut.scheduler = scheduler_spy

        # act
        self.sut.start()
        self.sut.stop()

        # verify
        self.assertEqual(0, len(scheduler_spy.removed_ids))
        self.assertFalse(self.sut.is_running)

    def test_animation_on_reset_the_current_index_is_set_to_start_index(self):
        # arrange
        self.sut.start()
        self.sut.scheduler.update(3 * self.interval, 3 * self.interval)
        self.assertNotEqual(self.start_index, self.sut.current_index)

        # act
        self.sut.reset()

        # verify
        self.assertEqual(self.start_index, self.sut.current_index)

    def test_animation_runs_with_defined_fps(self):
        # arrange
        fps = 20
        self.sut = mut.Animation(self.start_index, self.frame_count, fps, mut.Animation.scheduler)
        _interval = 1.0 / fps
        self.sut.start()

        # act / verify
        self.assertEqual(self.start_index, self.sut.current_index)

        self.sut.scheduler.update(_interval, _interval)
        self.assertEqual(self.start_index + 1, self.sut.current_index)

        self.sut.scheduler.update(_interval / 2, 1.5 * _interval)
        self.assertEqual(self.start_index + 1, self.sut.current_index)

        self.sut.scheduler.update(_interval / 2, 2 * _interval)
        self.assertEqual(self.start_index + 2, self.sut.current_index)

        self.sut.scheduler.update(_interval, 3 * _interval)
        self.assertEqual(self.start_index + 3, self.sut.current_index)

        self.sut.scheduler.update(_interval, 4 * _interval)
        self.assertEqual(self.start_index + 4, self.sut.current_index)

    def test_animation_change_fps_while_running(self):
        # arrange
        fps = 20
        start_index = self.start_index
        self.sut = mut.Animation(start_index, self.frame_count, fps, mut.Animation.scheduler)
        _interval = 1.0 / fps
        self.sut.start()

        # act / verify
        self.assertEqual(start_index, self.sut.current_index)

        self.sut.scheduler.update(_interval, _interval)
        self.assertEqual(start_index + 1, self.sut.current_index)

        self.sut.scheduler.update(_interval / 2, _interval + 0.5 * _interval)
        self.assertEqual(start_index + 1, self.sut.current_index)

        self.sut.scheduler.update(_interval / 2, 2 * _interval)
        self.assertEqual(start_index + 2, self.sut.current_index)

        fps = 50
        _new_interval = 1.0 / fps
        self.sut.fps = fps

        self.sut.scheduler.update(_new_interval, 2 * _interval + _new_interval)
        self.assertEqual(start_index + 3, self.sut.current_index)

        self.sut.scheduler.update(_new_interval, 2 * _interval + 2 * _new_interval)
        self.assertEqual(start_index + 4, self.sut.current_index)

        self.sut.scheduler.update(_new_interval, 2 * _interval + 3 * _new_interval)
        self.assertEqual(start_index + 5, self.sut.current_index)

    def test_animation_read_fps_returns_previously_set_fps(self):
        # arrange
        other_fps = 333.3
        self.sut.fps = other_fps

        # act
        # verify
        self.assertEqual(other_fps, self.sut.fps)
        self.assertFalse(self.sut.is_running)

    def test_animation_fps_returns_initial_fps_value(self):
        # arrange
        # act
        # verify
        self.assertEqual(self.fps, self.sut.fps)

    def test_animation_should_continue_if_current_index_is_set_to_a_valid_value(self):
        # arrange
        self.sut.start()
        self.sut.scheduler.update(10 * self.interval, 10 * self.interval)
        self.assertEqual(self.start_index + 10, self.sut.current_index)

        new_index = self.start_index + 5
        self.sut.set_current_index(new_index)
        tick_count = 11

        # act
        self.sut.scheduler.update(self.interval, tick_count * self.interval)

        # verify
        self.assertEqual(new_index + tick_count, self.sut.current_index)

    def test_animation_should_set_correct_current_index_if_set_too_large(self):
        # arrange
        start_index = 20
        overshoot = self.frame_count + 33
        self.sut = mut.Animation(start_index, self.frame_count, self.fps, mut.Animation.scheduler)

        # act
        self.sut.set_current_index(self.frame_count + overshoot)

        # verify
        expected = start_index + overshoot % self.frame_count
        self.assertEqual(expected, self.sut.current_index)

    def test_animation_should_set_correct_current_index_if_set_too_small(self):
        # arrange
        start_index = 20
        self.sut = mut.Animation(start_index, self.frame_count, self.fps, mut.Animation.scheduler)
        undershoot = self.frame_count - 10

        # act
        self.sut.set_current_index(start_index - undershoot)

        # verify
        expected = self.frame_count - undershoot + start_index
        self.assertEqual(expected, self.sut.current_index)

    def test_animation_does_set_the_last_index_correctly(self):
        # arrange
        self.sut = mut.Animation(self.start_index, self.frame_count, self.fps, mut.Animation.scheduler)
        self.sut.start()

        # act / verify
        end_index = self.start_index + self.frame_count
        self.sut.current_index = end_index - 2
        self.sut.scheduler.update(self.interval, self.interval)
        self.assertEqual(end_index - 2, self.sut.last_index)
        self.assertEqual(end_index - 1, self.sut.current_index)

        self.sut.scheduler.update(self.interval, 2 * self.interval)  # repeat
        self.assertEqual(self.start_index, self.sut.current_index)
        self.assertEqual(end_index - 1, self.sut.last_index)

        self.sut.scheduler.update(self.interval, 3 * self.interval)
        self.assertEqual(self.start_index + 1, self.sut.current_index)
        self.assertEqual(self.start_index, self.sut.last_index)

    def test_animation_last_index_ist_correctly_if_current_index_is_set(self):
        # arrange
        self.sut.start()
        for i in range(77):
            self.sut.scheduler.update(self.interval, self.interval * i)
        expected_last_index = self.sut.current_index

        # act
        self.sut.set_current_index(44)

        # verify
        self.assertEqual(expected_last_index, self.sut.last_index)

    def test_animation_should_have_an_interpolation_value_of_half(self):
        # arrange
        self.sut.start()
        self.sut.scheduler.update(self.interval / 2.0, self.interval / 2.0)

        # act
        actual = self.sut.interpolation

        # verify
        self.assertEqual(0.5, actual)

    def test_animation_should_have_an_interpolation_value_of_zero(self):
        # arrange
        self.sut.start()
        self.sut.scheduler.update(self.interval, self.interval)

        # act
        actual = self.sut.interpolation

        # verify
        self.assertEqual(0.0, actual)

    def test_animation_should_have_an_interpolation_value_of_99percent(self):
        # arrange
        self.sut.start()
        self.sut.scheduler.update(0.0, 0.0)
        self.sut.scheduler.update(self.interval * 0.99, self.interval * 0.99)

        # act
        actual = self.sut.interpolation

        # verify
        self.assertEqual(0.99, actual)

    def test_animation_should_update_interpolation_value(self):
        # arrange
        self.sut.fps = 20
        self.interval = 1.0 / self.sut.fps
        self.sut.start()

        # act
        # verify
        self.sut.scheduler.update(2 * self.interval, 2 * self.interval)
        self.assertEqual(0.0, round(self.sut.interpolation, 2))  # round to avoid float imprecision

        self.sut.scheduler.update(0.25 * self.interval, 2.25 * self.interval)
        self.assertEqual(0.25, round(self.sut.interpolation, 2))

        self.sut.scheduler.update(0.25 * self.interval, 2.5 * self.interval)
        self.assertEqual(0.5, round(self.sut.interpolation, 2))

        self.sut.scheduler.update(0.25 * self.interval, 2.75 * self.interval)
        self.assertEqual(0.75, round(self.sut.interpolation, 2))

    def test_animation_interpolation_value_is_zero_if_animation_is_not_running(self):
        # arrange
        self.sut.scheduler.update(0.5 * self.interval, 0.5 * self.interval)

        # act
        actual = self.sut.interpolation

        # verify
        self.assertEqual(0.0, actual)

    def test_animation_interpolation_value_should_be_in_range(self):
        # arrange
        self.sut.start()
        self.sut.scheduler.update(self.interval, self.interval)
        self.sut.scheduler.current_time += self.interval

        # act
        actual = self.sut.interpolation

        # verify
        self.assertEqual(1.0, actual)

    def test_animation_set_fps_to_zero(self):
        # arrange
        self.sut.fps = 0.0

        # act
        self.sut.start()

        # verify
        self.assertFalse(self.sut.is_running)

    def test_play_once(self):
        # arrange
        self.ended = False

        def on_ended(*args):
            self.ended = True

        self.sut.is_looping = False
        self.sut.event_animation_end += on_ended
        self.sut.start()

        # act
        sim_time = 0.0
        while not self.ended:
            mut.Animation.scheduler.update(self.interval, sim_time)
            sim_time += self.interval

        for i in range(100):
            mut.Animation.scheduler.update(self.interval, sim_time)
            sim_time += self.interval


        actual = self.sut.current_index

        # verify
        expected = self.sut.last_index + 1
        self.assertEqual(expected, actual)
        expected_last_frame = self.start_index + self.frame_count
        self.assertEqual(expected_last_frame, actual)

    def test_play_once_without_callback(self):
        # arrange
        self.sut.is_looping = False
        self.sut.start()
        expected_last_frame = self.start_index + self.frame_count

        # act
        sim_time = 0.0
        while self.sut.current_index != expected_last_frame:
            mut.Animation.scheduler.update(self.interval, sim_time)
            sim_time += self.interval

        for i in range(100):
            mut.Animation.scheduler.update(self.interval, sim_time)
            sim_time += self.interval


        actual = self.sut.current_index

        # verify
        expected = self.sut.last_index + 1
        self.assertEqual(expected, actual)
        self.assertEqual(expected_last_frame, actual)




class AnimationIndexChangedTests(unittest.TestCase):

    def setUp(self):
        mut.Animation.scheduler = Scheduler()  # reset to make sure all schedules are correctly tested
        self.frame_count = 3
        self.interval = 1.0
        self.start_index = 30
        self.fps = 1.0 / self.interval
        self.sut = mut.Animation(self.start_index, self.frame_count, self.fps, mut.Animation.scheduler)
        self.sut.event_index_changed += self._index_change_callback

        self.index_changed_capture = []

    def _index_change_callback(self, *args):
        self.index_changed_capture.append(args)

    def test_index_changed_is_called(self):
        # arrange
        self.sut.start()

        # act
        mut.Animation.scheduler.update(self.interval, 1)
        mut.Animation.scheduler.update(self.interval, 2)
        mut.Animation.scheduler.update(self.interval, 3)

        # verify
        self.assertEqual(3, len(self.index_changed_capture))

    def test_index_changed_is_called_when_looping(self):
        # arrange
        self.sut.start()
        mut.Animation.scheduler.update(self.interval, 1 * self.interval)
        mut.Animation.scheduler.update(self.interval, 2 * self.interval)
        mut.Animation.scheduler.update(self.interval, 3 * self.interval)

        # act
        mut.Animation.scheduler.update(self.interval, 4 * self.interval)

        # verify
        self.assertEqual(4, len(self.index_changed_capture))

    def test_index_changed_has_been_called_on_last_frame(self):
        # arrange
        def stop_animation(animation):
            animation.stop()

        self.sut.event_animation_end += stop_animation
        self.sut.start()
        sim_time = self.interval * self.frame_count + self.interval
        self.sut.scheduler.update(sim_time, sim_time)

        # act
        self.sut.scheduler.update(self.interval, sim_time + self.interval)  # update to last frame

        # verify
        self.assertEqual(2, len(self.index_changed_capture))

    def test_animation_should_not_fire_index_changed_if_same_index_ist_set(self):
        # act
        assert mut.Animation.scheduler is not None
        self.sut.set_current_index(self.sut.current_index)

        # verify
        self.assertEqual(0, len(self.index_changed_capture))


if __name__ == '__main__':
    unittest.main()
