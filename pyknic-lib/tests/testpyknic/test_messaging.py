# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_messaging.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.messaging module.
"""
from __future__ import print_function, division

import logging
import unittest
import warnings

import pyknic.messaging as mut  # module under test
import pyknic.registry

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class _Entity(object):

    def __init__(self, id_to_use, return_value=True):
        self.id = id_to_use
        self.messages = []
        self.return_value = return_value

    def handle_message(self, *args):
        self.messages.append(args)
        return self.return_value


class EntityHelperTests(unittest.TestCase):

    def test_entity_registers_messages(self):
        # arrange
        e = _Entity(22)

        # act
        e.handle_message(1)
        e.handle_message(2)
        e.handle_message(33)

        # verify
        expected = [(1,), (2,), (33, )]
        self.assertEqual(expected, e.messages)


class HandlerForTesting(logging.Handler):

    def __init__(self, level=logging.NOTSET):
        logging.Handler.__init__(self, level)
        self.records = []

    def emit(self, record):
        self.records.append(record)


class MessageDispatcherTests(unittest.TestCase):

    def setUp(self):
        self.registry = pyknic.registry.Registry()
        self.sut = mut.MessageDispatcher(self.registry)
        self.logger = logging.getLogger("MyOwnTextLogger")
        log_level = logging.INFO
        self.logger_handler = HandlerForTesting(log_level)
        self.logger.addHandler(self.logger_handler)
        self.logger.setLevel(log_level)

    def test_message_is_sent_to_receiver(self):
        # arrange
        e_sender = _Entity(1)
        e_rec = _Entity(44)
        self.registry.register(e_sender, e_sender.id)
        self.registry.register(e_rec, e_rec.id)

        # act
        self.sut.send(e_sender.id, e_rec.id, 3, 33)

        # verify
        self.assertEqual(1, len(e_rec.messages))
        self.assertEqual((e_sender.id, e_rec.id, 3, 33), e_rec.messages[0])

    def test_message_is_sent_to_receiver_and_logged_with_debug_level(self):
        # arrange
        e_sender = _Entity(1)
        e_rec = _Entity(44)
        self.registry.register(e_sender, e_sender.id)
        self.registry.register(e_rec, e_rec.id)

        self.logger.setLevel(logging.DEBUG)
        self.logger_handler.setLevel(logging.DEBUG)
        self.sut = mut.MessageDispatcher(self.registry, self.logger)

        # act
        self.sut.send(e_sender.id, e_rec.id, 3, 33)

        # verify
        self.assertEqual(1, len(self.logger_handler.records))

    def test_message_is_sent_to_unknown(self):
        # arrange
        # act / verify
        self.sut.send(5, 999, 3, 33)

    def test_message_is_sent_to_unknown_is_logged_to_custom_logger(self):
        # arrange
        self.sut = mut.MessageDispatcher(self.registry, self.logger)

        # act
        self.sut.send(5, 999, 3, 33)

        # verify
        self.assertEqual(1, len(self.logger_handler.records))

    def test_message_is_sent_to_unknown_is_not_logged_with_disabled_logging(self):
        # arrange
        self.sut = mut.MessageDispatcher(self.registry, False)

        # act
        self.sut.send(5, 999, 3, 33)

        # verify
        self.assertEqual(0, len(self.logger_handler.records))

    def test_message_is_sent_to_unknown_is_logged_to_default_logger(self):
        logger_level = mut.logger.level
        logger_handlers = list(mut.logger.handlers)
        try:
            # arrange
            mut.logger.addHandler(self.logger_handler)
            mut.logger.setLevel(logging.INFO)
            self.sut = mut.MessageDispatcher(self.registry)

            # act
            self.sut.send(5, 999, 3, 33)

            # verify
            self.assertEqual(1, len(self.logger_handler.records))
        finally:
            mut.logger.removeHandler(self.logger_handler)
            mut.logger.setLevel(logger_level)
            mut.logger.handlers = logger_handlers

    def test_receiver_should_return_true_or_false(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        e1 = _Entity(1)
        e2 = _Entity(2)

        # noinspection PyUnusedLocal
        def handle_message_return_none(*args):
            return None

        e2.handle_message = handle_message_return_none
        self.registry.register(e1, e1.id)
        self.registry.register(e2, e2.id)

        # act
        try:
            self.sut.send(e1.id, e2.id, 11)
            self.fail("should have raised assertion error")
        except AssertionError:
            pass


if __name__ == '__main__':
    unittest.main()
