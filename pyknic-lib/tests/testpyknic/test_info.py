# -*- coding: utf-8 -*-

"""
Tests for the pyknic.info module.
"""
from __future__ import print_function, division
import unittest
import warnings

import pyknic

warnings.simplefilter('error')  # make warnings visible for developers


class VersionTests(unittest.TestCase):

    def test_version_number_matches_version(self):
        """
        Just tests if the version and the version number are equal.
        """
        version = pyknic.info.version
        version_number = pyknic.info.version_info

        self.assertTrue(version == '.'.join([str(x) for x in version_number]),
                        "Version and version number do not match!")
