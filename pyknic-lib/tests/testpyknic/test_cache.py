# -*- coding: utf-8 -*-
from __future__ import print_function, division

import unittest
import warnings

from pyknic import cache

warnings.simplefilter('error')  # make warnings visible for developers


class CacheUsingMethodsTests(unittest.TestCase):
    """
    .. todo:: separate tests for Cache/LRUCache and function/method tests (especially in Cached decorator)
    .. todo::

        make sure all methods are tested:

        __class__
        __cmp__
        __contains__
        __copy__
        __delattr__
        __delitem__
        __doc__
        __eq__
        __format__
        __ge__
        __getattribute__
        __getitem__
        __gt__
        __hash__
        __init__
        __iter__
        __le__
        __len__
        __lt__
        __missing__
        __ne__
        __new__
        __reduce__
        __reduce_ex__
        __repr__
        __setattr__
        __setitem__
        __sizeof__
        __str__
        __subclasshook__
        clear
        copy
        default_factory
        fromkeys
        get
        has_key
        items
        iteritems
        iterkeys
        itervalues
        keys
        pop
        popitem
        setdefault
        update
        values
    """

    def miss_factory(self, key):
        """the factory function, just register the calls and return a value"""
        self.miss_keys.append(key)
        return str(key) + '!'

    def setUp(self):
        self.cache = cache.LRUCache(self.miss_factory, None)
        self.miss_keys = []

    def _check_cache_info(self, info, expected_hits, expected_misses, expected_maxsize, expected_currsize):
        self.assertEqual(info.hits, expected_hits)
        self.assertEqual(info.misses, expected_misses)
        self.assertEqual(info.maxsize, expected_maxsize)
        self.assertEqual(info.currsize, expected_currsize)

    def test_cache_returns_value(self):
        """test if the factory function is uesd correctly"""
        result_a = self.cache['a']
        self.assertTrue(result_a=='a'+'!', "factory returned wrong value")
        self._check_cache_info(self.cache.cache_info(), 0, 1, None, 1)

    def test_cache_miss_return_value(self):
        """test if the factory function is uesd correctly"""
        result_a = self.cache['a']
        result_a = self.cache['a'] # cached value
        self.assertTrue(result_a=='a'+'!', "factory returned wrong value")
        self._check_cache_info(self.cache.cache_info(), 1, 1, None, 1)

    def test_cache_miss_count(self):
        """test if the number of misses is correct"""
        result_a = self.cache['a']
        result_b = self.cache[6]
        result_b = self.cache[6]
        result_a = self.cache['a']
        self.assertTrue(len(self.miss_keys) == 2, "should be called 2 timees called times: "  + str(len(self.miss_keys)))
        self._check_cache_info(self.cache.cache_info(), 2, 2, None, 2)

    def test_cache_miss_key(self):
        """test if the number of misses is correct"""
        key1 = 'a'
        key2 = 6
        result_a = self.cache[key1]
        result_b = self.cache[key2]
        result_b = self.cache[key2]
        result_a = self.cache[key1]
        
        self.assertTrue(self.miss_keys[0] == key1, "miss key should be %s, but is %s" % (key1, self.miss_keys[0]))
        self.assertTrue(self.miss_keys[1] == key2, "miss key should be %s, but is %s" % (key2, self.miss_keys[1]))
        self._check_cache_info(self.cache.cache_info(), 2, 2, None, 2)

    def test_cache_len(self):
        """
        test the number of entries in cache after repeated calls with
        same keys to it
        """
        result_a = self.cache['aaa']
        result_b = self.cache[6]
        result_a = self.cache['aaa']
        result_b = self.cache[6]
        result_b = self.cache[5]
        result_b = self.cache[4]
        self.assertTrue(len(self.cache) == 4, \
                            "wrong number of keys in dict should only have 2")
        self._check_cache_info(self.cache.cache_info(), 2, 4, None, 4)

    def test_cache_contains(self):
        """
            test if the cache contains a certain key
        """
        self.cache['a']
        self.assertTrue('a' in self.cache, "cache contains returned False but key should be in cache")
        self.assertTrue(not (6 in self.cache), "cache should not contain key")
        self._check_cache_info(self.cache.cache_info(), 0, 1, None, 1)

    def test_get_default(self):
        self.cache['aaaa']
        self.cache[6]
        res = self.cache.get('aaaa', 123)
        self.assertTrue(res=="aaaa!", "get should return cached value, got: " + str(res))

        res = self.cache.get(99, 333)
        self.assertTrue(res==333, "get should return provided default value")
        # make sure that default value was not added to cache
        res = self.cache.get(333, 444)
        self.assertTrue(res==444, "get should return provided default value")
        self._check_cache_info(self.cache.cache_info(), 1, 4, None, 2)

    def test_clear_cache(self):
        result_a = self.cache['abc']
        result_b = self.cache[6]
        result_a = self.cache['cde']
        result_b = self.cache['def']

        self.cache.clear()
        self.assertTrue(len(self.cache) == 0, "cache not cleared")
        self._check_cache_info(self.cache.cache_info(), 0, 0, None, 0)


miss_keys = []
def miss(key):
    """the factory function, just register the calls and return a value"""
    global miss_keys
    miss_keys.append(key)
    return str(key) + '!'

class CacheUsingFunctionssTests(CacheUsingMethodsTests):

    def setUp(self):
        self.cache = cache.LRUCache(miss, None)
        global miss_keys
        self.miss_keys = miss_keys = []


class MethodDecoratorCacheTests(unittest.TestCase):
    """
    Tests that the Cached decorator works on methods.
    """

    @cache.lru_cached(4)
    def factory(self, val):
        """
        Method to test the Cached decorator.
        """
        self.captured.append(val)
        return val

    def setUp(self):
        self.captured = []
        self.factory.cache.clear()

    def test_cache_one_value(self):
        """
        ..todo:: implement better test methods
        """
        res = self.factory(1)
        self.assertTrue(self.captured[0] == 1, "wrong value was captured!!")
        self.assertTrue(len(self.captured) == 1, "more than one capture!!")
        self.assertTrue(res == 1, "got wrong value from cache")

    def test_cache_only_one_value(self):
        res = self.factory(1)
        res = self.factory(1)
        res = self.factory(1)
        self.assertTrue(len(self.captured) == 1, "more than one capture!!" + str(self.captured))
        self.assertTrue(res == 1, "got wrong value from cache")

    def test_cache_second_value(self):
        res = self.factory(1)
        res = self.factory(2)
        self.assertTrue(self.captured[1] == 2, "wrong value was captured!!")
        self.assertTrue(len(self.captured) == 2, "more than one capture!!")
        self.assertTrue(res == 2, "got wrong value from cache")

    def test_cache_three_values(self):
        res = self.factory(1)
        res = self.factory(2)
        res = self.factory(3)
        self.assertTrue(self.captured[2] == 3, "wrong value was captured!!")
        self.assertTrue(len(self.captured) == 3, "more than one capture!!")
        self.assertTrue(res == 3, "got wrong value from cache")

    def test_cache_four_values(self):
        res = self.factory(1)
        res = self.factory(2)
        res = self.factory(3)
        res = self.factory(4)
        self.assertTrue(self.captured[3] == 4, "wrong value was captured!!")
        self.assertTrue(len(self.captured) == 4, "more than one capture!!")
        self.assertTrue(res == 4, "got wrong value from cache")

    def test_cache_five_values(self):
        res = self.factory(1)
        res = self.factory(2)
        res = self.factory(3)
        res = self.factory(4)
        res = self.factory(5)
        self.assertTrue(self.captured[4] == 5, "wrong value was captured!!")
        self.assertTrue(len(self.captured) == 5, "more than one capture!!")
        self.assertTrue(res == 5, "got wrong value from cache")

func_cap = []

@cache.lru_cached(4)
def add_one(val):
    """adds one, test function for Cached decorator"""
    global func_cap
    func_cap.append(val)
    return val

@cache.lru_cached(4)
def multi_args_function(arg1, arg2, arg3):
    """test function for Cached decorator"""
    global func_cap
    ret = (arg1, arg2, arg3)
    func_cap.append(ret)
    return ret


class FunctionDecoratorCacheTests(MethodDecoratorCacheTests):
    """
    Tests that the Cached decorator works on (module-) functions.
    """

    def setUp(self):
        global func_cap
        self.captured = func_cap = []
        self.factory = add_one
        add_one.cache.clear()

    def test_cached_should_cache_one_value(self):
        """
        Test the Cached ability on functions (not method).
        """
        res = self.factory(9)
        self.assertTrue(self.captured[0] == 9, 'call should have been captured at pos 0')
        self.assertTrue(len(self.captured) == 1, 'call should have been cached')

        res = self.factory(9)
        self.assertTrue(len(self.captured) == 1, 'call should have been cached')

    def test_cached_should_cache_multiple_values(self):
        res = multi_args_function(1, 2, 3)
        res = multi_args_function(11, 12, 13)
        self.assertTrue(len(self.captured) == 2, 'call should have been cached')
        self.assertTrue(self.captured[0] == (1, 2, 3), 'call should have been captured at pos 0')
        self.assertTrue(self.captured[1] == (11, 12, 13), 'call should have been captured at pos 1')

    def test_cached_should_cache_value_only_once(self):
        res = self.factory(1)
        res = self.factory(2)
        res = self.factory(1)
        res = self.factory(2)
        res = self.factory(1)
        res = self.factory(2)
        self.assertTrue(add_one.cache.is_enabled(), "cache should be enabled!")
        self.assertTrue(len(self.captured) == 2, 'call should have been cached 2 times instead of %s' % (len(self.captured)))


@cache.lru_cached(9999)
def add_two(val):
    """adds two, test function for Cached decorator"""
    global func_cap
    func_cap.append(val)
    return val

class FunctionDecoratorLRUTests(unittest.TestCase):
    """
    Tests that the Cached decorator works on (module-) functions.
    """

    def setUp(self):
        global func_cap
        func_cap = []
        add_two.cache.clear()
        
    def test_cached_using_LRU(self):
        global func_cap
        res = add_two(1)
        res = add_two(2)
        add_two(10)
        self.assertTrue(func_cap[2] == 10, 'call should have been captured at pos 4')
        self.assertTrue(len(func_cap) == 3, 'call should have been cached')
        add_two(20)
        self.assertTrue(func_cap[3] == 20, 'call should have been captured at pos 5')
        self.assertTrue(len(func_cap) == 4, 'call should have been cached: ' + str(func_cap))
        add_two(10)
        self.assertTrue(len(func_cap) == 4, 'call should have been cached: ' + str(func_cap))
        lru = add_two.cache.lru
        # in this case the lru contains key value pairs which are:
        # key is the argument list of the function
        # value is the argument + 2 (as the function name says)
        self.assertTrue(lru[0] == (10, 10), "wrong order in lru cache: " + str(lru))
        self.assertTrue(lru[1] == (20, 20), "wrong order in lru cache")
        
        info = add_two.cache.cache_info()
        self.assertEqual(info.hits, 1)
        self.assertEqual(info.misses, 4)
        self.assertEqual(info.maxsize, 9999)
        self.assertEqual(info.currsize, 4)
        self.assertEqual(add_two, add_two.cache)


class LRUCacheTests(unittest.TestCase):

    def miss(self, key):
        """the factory function, just register the calls and return a value"""
        self.miss_key.append(key)
        return str(key) + '!'

    def setUp(self):
        self.cache_lru = cache.LRUCache(self.miss, 3)
        self.miss_key = []

    # def match_order(self, expected, captured):
        # # verify result
        # if len(expected) != len(captured):
            # self.fail("number of calls did not match!" + str(expected) + str(captured))
        # for idx, s in enumerate(expected):
            # if s is not captured[idx]:
                # self.fail("s: " + str(s) + " did not match: " + str(captured[idx]))

    def test_lru_capacity(self):
        """test that it does not hold more than the capacity specified"""
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru['c']
        self.cache_lru['d']
        self.assertTrue(len(self.cache_lru) == 3, \
                 "wrong number of keys in dict can not have more than capacity")
        self._check_cache_info(self.cache_lru.cache_info(), 0, 4, 3, 3)

    def test_lru_miss_count(self):
        """test the number of misses"""
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru[6]
        self.cache_lru['c'] # removes 'a'
        self.cache_lru['d']
        self.cache_lru['c']
        self.assertTrue(len(self.miss_key) == 4, "missed keys count is wrong")

    def test_lru_miss_count_reload_missing_key(self):
        """
        test the number of misses if a key is re-used after it has already
        been removed
        """
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru['c']
        self.cache_lru['d'] # removes 'a'
        self.cache_lru['a'] # removes 6
        self.cache_lru['c']
        self.assertTrue(len(self.miss_key) == 5, \
                    "missed keys count is wrong should reload first key again")

    def test_lru_miss_count_cached_key(self):
        """test the number of misses after one key has been removed"""
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru['c']
        self.cache_lru['d'] # removes 'a'
        self.cache_lru[6]
        self.assertTrue(len(self.miss_key) == 4, \
                    "missed keys count is wrong should reload first key again ")

    def test_lru_order(self):
        """test if the lru is constructed the right way and returned"""
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru['c']
        expected_lru = [('c', 'c!'), (6, '6!'), ('a', 'a!')]
        self.assertTrue(self.cache_lru.lru == expected_lru, "wrong order in lru")

        self.cache_lru['d'] # removes 'a'
        expected_lru = [('d', 'd!'), ('c', 'c!'), (6, '6!')]
        self.assertTrue(self.cache_lru.lru == expected_lru, \
                                  "wrong order in lru after loading new key in")

        self.cache_lru[6]
        expected_lru = [(6, '6!'), ('d', 'd!'), ('c', 'c!')]
        self.assertTrue(self.cache_lru.lru == expected_lru, \
                          "wrong order in lru after calling a cached key again")


    def test_lru_property_immutable(self):
        """test if the lru property is immutable"""
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru['c']
        expected_lru = [('c', 'c!'), (6, '6!'), ('a', 'a!')]

        self.cache_lru.lru.append('u')
        self.assertTrue(self.cache_lru.lru == expected_lru, \
                                                       "should not be editable")

    def test_lru_length(self):
        """test if the length of the lru list and the length of the dict are equal"""
        self.cache_lru['a']
        self.cache_lru[6]
        self.cache_lru['c']
        self.cache_lru['a']
        self.cache_lru['d']
        self.cache_lru['e']
        self.cache_lru['c']
        self.assertTrue(len(self.cache_lru) == len(self.cache_lru.lru), "cache length does not match lru length!")

    def test_return_value(self):
        """test if the returned value is the expected one"""
        key = 'a'
        self._test_return_val(key, self.miss(key))
        key = 'a'
        self._test_return_val(key, self.miss(key))
        key = 6
        self._test_return_val(key, self.miss(key))
        key = 'c'
        self._test_return_val(key, self.miss(key))
        key = 'd'
        self._test_return_val(key, self.miss(key))
        key = 6
        self._test_return_val(key, self.miss(key))

    def _test_return_val(self, key, expected):
        val = self.cache_lru[key]
        self.assertTrue(val == expected, "expected '%s' but got wrong value '%s' for key '%s'" % (expected, val, key))

    def _check_cache_info(self, info, expected_hits, expected_misses, expected_maxsize, expected_currsize):
        self.assertEqual(info.hits, expected_hits)
        self.assertEqual(info.misses, expected_misses)
        self.assertEqual(info.maxsize, expected_maxsize)
        self.assertEqual(info.currsize, expected_currsize)


class DisabledCacheTests(unittest.TestCase):


    def setUp(self):
        self.cache = cache.LRUCache(self.miss, None)
        self.miss_key = []
        self.cache.clear()
        self.cache.enable(True)
        self.assertTrue(len(self.cache) == 0, "cache has not been cleared correctly")
        
    def tearDown(self):
        cache.LRUCache.enable_global(True)

    def miss(self, key):
        self.miss_key.append(key)

        
    def test_disabled_cache_should_have_len_0(self):
        self.cache.enable(False)
        self.cache[1]
        self.cache[2]
        self.cache[3]
        self.assertTrue(len(self.cache)==0, "no entry should have been cached!")
        self.assertFalse(self.cache.is_enabled(), "cache shoule be disabled")

    def test_disabled_cache_should_stop_caching(self):
        self.cache[1]
        self.cache[2]
        self.cache.enable(False)
        self.cache[3]
        self.cache[4]
        self.assertTrue(len(self.cache)==2, "only first two entries should have been cached!")
        self.assertFalse(self.cache.is_enabled(), "cache shoule be disabled")

    def test_disabled_cache_should_do_caching_again(self):
        self.cache.enable(False)
        self.assertFalse(self.cache.is_enabled(), "cache shoule be disabled")
        self.cache[1]
        self.cache[2]
        self.cache.enable(True)
        self.assertTrue(self.cache.is_enabled(), "cache shoule be enabled")
        self.cache[3]
        self.cache[4]
        self.assertTrue(len(self.cache)==2, "only last two entries should have been cached!")
        
    def test_disable_cache_globally(self):
        cache2 = cache.LRUCache(self.miss, None)
        cache.LRUCache.enable_global(False)
        self.assertFalse(self.cache.is_enabled(), "cache shoule be disabled")
        self.assertFalse(cache2.is_enabled(), "cache shoule be disabled")


# class DisabledLRUCacheTests(DisabledCacheTests):

    # def setUp(self):
        # self.cache = cache.LRUCache(self.miss, 5)
        # self.miss_key = []





if __name__ == '__main__':
    unittest.main()

