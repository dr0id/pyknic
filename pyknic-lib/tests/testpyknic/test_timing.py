# -*- coding: utf-8 -*-

from __future__ import print_function, division

import unittest
import warnings

import pyknic
import pyknic.timing
import pyknic.timing as mut  # module under test
from pyknic.timing import FrameCap
from pyknic.timing import GameTime
from pyknic.timing import LockStepper
from pyknic.timing import Scheduler
from pyknic.timing import Timer

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])


class TestVersion(unittest.TestCase):
    def test_version(self):
        self.assertEqual(mut.__version_info__, __version_info__, "version numbers do not match!")


class GameTimeTests(unittest.TestCase):
    def tick(self, *args, **kwargs):
        self.assertEqual(3, len(args), "should have been 3 arguments: dt, sim_time")
        self.tick_args = (args[0],)
        self.tick_kwargs = kwargs

    def setUp(self):
        self.game_time = GameTime()
        self.tick_args = []
        self.tick_kwargs = []
        self.game_time.event_update += self.tick
        self.game_time.max_real_delta_t = 30.0

    def test_tick(self):
        self.game_time.update(0)
        self.assertEqual(self.tick_args, (0,), "tick failed")
        self.assertTrue(self.game_time.time == 0, "game time failed")
        self.assertTrue(self.game_time.real_time == 0, "real time failed")
        self.game_time.update(0.5)
        self.assertEqual(self.tick_args, (0.5,), "0.5 tick failed")
        self.assertTrue(self.game_time.time == 0.5, "0.5 game time failed")
        self.assertTrue(self.game_time.real_time == 0.5, "0.5 real time failed")
        self.game_time.update(0.5)
        self.assertEqual(self.tick_args, (0.5,), "1.0 tick failed")
        self.assertTrue(self.game_time.time == 1.0, "1.0 game time failed")
        self.assertTrue(self.game_time.real_time == 1.0, "1.0 real time failed")
        self.game_time.update(0.5123)
        self.assertEqual(self.tick_args, (0.5123,), "0.5 tick failed")
        self.assertTrue(self.game_time.time == 1.5123, "0.5 game time failed")
        self.assertTrue(self.game_time.real_time == 1.5123, "0.5 real time failed")

    def test_time_shrinkage_slower(self):
        self.game_time.tick_speed = 0.5
        self.game_time.update(0)
        self.assertEqual(self.tick_args, (0,), "tick failed")
        self.assertTrue(self.game_time.time == 0, "game time failed")
        self.assertTrue(self.game_time.real_time == 0, "real time failed")
        self.game_time.update(1.0)
        self.assertEqual(self.tick_args, (0.5,), "0.5 tick failed")
        self.assertTrue(self.game_time.time == 0.5, "0.5 game time failed")
        self.assertTrue(self.game_time.real_time == 1.0, "0.5 real time failed")
        self.game_time.update(1.0)
        self.assertEqual(self.tick_args, (0.5,), "1.0 tick failed")
        self.assertTrue(self.game_time.time == 1.0, "1.0 game time failed")
        self.assertTrue(self.game_time.real_time == 2.0, "1.0 real time failed")

    def test_time_dilatation_faster(self):
        self.game_time.tick_speed = 2.0
        self.game_time.update(0)
        self.assertEqual(self.tick_args, (0,), "tick failed")
        self.assertTrue(self.game_time.time == 0, "game time failed")
        self.assertTrue(self.game_time.real_time == 0, "real time failed")
        self.game_time.update(1.0)
        self.assertEqual(self.tick_args, (2.0,), "0.5 tick failed")
        self.assertTrue(self.game_time.time == 2.0, "0.5 game time failed")
        self.assertTrue(self.game_time.real_time == 1.0, "0.5 real time failed")
        self.game_time.update(1.0)
        self.assertEqual(self.tick_args, (2.0,), "1.0 tick failed")
        self.assertTrue(self.game_time.time == 4.0, "1.0 game time failed")
        self.assertTrue(self.game_time.real_time == 2.0, "1.0 real time failed")

    def test_negative_factor(self):
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            self.assertRaises(Exception, self.game_time._set_factor, -1)
            warnings.simplefilter("ignore")
            self.game_time.tick_speed = -1
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (-30,), "tick failed")
        self.assertTrue(self.game_time.time == -30, "game time failed")
        self.assertTrue(self.game_time.real_time == 30, "real time failed")

    def test_advance_only_when_updated(self):
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (30,), "tick failed")
        self.assertTrue(self.game_time.time == 30, "game time failed")
        self.assertTrue(self.game_time.real_time == 30, "real time failed")
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (30,), "tick failed")
        self.assertTrue(self.game_time.time == 60, "game time failed")
        self.assertTrue(self.game_time.real_time == 60, "real time failed")
        self.game_time.tick_speed = 2.0
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (60,), "tick failed")
        self.assertTrue(self.game_time.time == 120, "game time failed")
        self.assertTrue(self.game_time.real_time == 90, "real time failed")

    def test_negative_tick_speed(self):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.game_time.tick_speed = -1.0
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (-30,), "tick failed")
        self.assertTrue(self.game_time.time == -30, "game time failed")
        self.assertTrue(self.game_time.real_time == 30, "real time failed")
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (-30,), "tick failed")
        self.assertTrue(self.game_time.time == -60, "game time failed")
        self.assertTrue(self.game_time.real_time == 60, "real time failed")
        self.game_time.tick_speed = -2.0
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (-60,), "tick failed")
        self.assertTrue(self.game_time.time == -120, "game time failed")
        self.assertTrue(self.game_time.real_time == 90, "real time failed")

    def test_time_factor_zero(self):
        self.game_time.tick_speed = 0
        self.game_time.update(30)
        self.assertEqual(self.tick_args, (0,), "tick failed")
        self.assertTrue(self.game_time.time == 0, "game time failed")
        self.assertTrue(self.game_time.real_time == 30, "real time failed")

    def test_time_max_dt_is_not_exceeded(self):
        # arrange

        # act
        self.game_time.update(10 * self.game_time.max_real_delta_t)

        # verify
        self.assertEqual(self.game_time.max_real_delta_t, self.game_time.time)
        self.assertEqual(self.game_time.max_real_delta_t, self.game_time.real_time)


class LockStepperTests(unittest.TestCase):
    # noinspection PyUnusedLocal
    def integrate(self, sender, dt, sim_time):
        self.deltas.append(dt)
        self.sim_time_points.append(sim_time)

    def setUp(self):
        self.stepper = LockStepper(max_dt=2.0)
        self.stepper.event_integrate += self.integrate
        self.deltas = []
        self.sim_time_points = []

    def test_undersampling(self):
        dt = 100 * pyknic.timing.LockStepper.TIMESTEP_SECONDS + 0.0000001

        self.stepper.max_steps = 1000
        self.stepper.update(dt, dt)  # 1000 steps so it does not interfere
        self.assertGreater(len(self.deltas), 0, "signal not fired")
        self.assertEqual(len(self.deltas), 100, "signal not fired 100 times")
        self.assertLessEqual(self.stepper.alpha, 0.00001, "alpha is wrong")

        self.stepper.update(dt, 2 * dt)  # 1000 steps so it does not interfere
        self.assertGreater(len(self.deltas), 100, "signal not fired")
        self.assertEqual(len(self.deltas), 200, "signal not fired 100 times")

    def test_oversampling(self):
        num = 1000
        dt = pyknic.timing.LockStepper.TIMESTEP_SECONDS / num

        self.stepper.max_steps = 1000
        self.stepper.update(dt, dt)  # 1000 steps so it does not interfere
        self.assertTrue(len(self.deltas) == 0, "signal called, should not be")
        self.assertTrue(self.stepper.alpha == 1.0 / num, "alpha wrong")

        for i in range(num):
            self.stepper.update(dt, 2 * dt)

        self.assertTrue(len(self.deltas) == 1, "signal called, should not be")
        self.assertTrue(self.stepper.alpha <= 1.0 / num, "alpha wrong2")

    def test_max_steps(self):
        dt = 6 * pyknic.timing.LockStepper.TIMESTEP_SECONDS

        self.stepper.max_steps = 4
        self.stepper.update(dt, dt)
        self.assertEqual(4, len(self.deltas), "updated more times as max_steps")

        dt = pyknic.timing.LockStepper.TIMESTEP_SECONDS
        self.stepper.update(dt, 2 * dt)
        self.assertEqual(6, len(self.deltas), "not catched up")

    def test_custom_delta(self):
        dt = 1.0

        self.stepper.max_steps = 4
        self.stepper.timestep = 1.0
        self.stepper.update(dt, dt)
        self.assertEqual(1, len(self.deltas), "called more than once")
        self.assertEqual(1.0, self.deltas[0], "wrong delta")


class SchedulerTests(unittest.TestCase):
    def func(self, *args, **kwargs):
        # print '???', args, kwargs
        self.called += 1
        self.args.append(args)
        self.kwargs.append(kwargs)
        return 0

    def func_repeat(self, *args, **kwargs):
        # print '???', args, kwargs
        self.called += 1
        self.args.append(args)
        self.kwargs.append(kwargs)
        return 4

    def setUp(self):
        self.longMessage = True
        self.maxDiff = None
        self.scheduler = Scheduler()
        self.scheduler.time = 0
        self.called = 0
        self.args = []
        self.kwargs = []
        self.time = 0
        self.self_removed = 0

    def test_remove_in_callback_even_if_rescheduling_using_return_value(self):
        # arrange
        schedule_id = 0

        def remove_itself(*args):
            self.called += 1
            self.scheduler.remove(schedule_id)
            return 1

        schedule_id = self.scheduler.schedule(remove_itself, 1)

        # act
        self.scheduler.update(1, 1)
        self.scheduler.update(1, 2)
        self.scheduler.update(1, 3)

        # verify
        self.assertEqual(1, self.called)

    def test_reschedule_callback_due_to_back_jump_in_time(self):
        # arrange
        self.scheduler.update(1, 1000)
        self.assertEqual(1000, self.scheduler.current_time)
        self.scheduler.schedule(self.func, 2)

        # act / verify
        self.assertRaises(Exception, self.scheduler.update, 1, 0)

    def test_raise_exception_if_updated_with_same_time_twice(self):
        # arrange
        self.scheduler.update(1, 1000)

        # act / verify
        self.assertRaises(Exception, self.scheduler.update, 1, 1000)

    # noinspection PyTypeChecker
    def test_schedule_timing(self):
        self.scheduler.schedule(self.func, 1.1, "f2")
        self.scheduler.schedule(self.func, 1.0, "f1")

        self.assertTrue(self.called == 0, "callback has been called but should not")
        self.scheduler.update(1.099999, 1.099999)

        self.assertTrue(self.called == 1, "callback f1 not called after delay")
        self.scheduler.update(0.2, 1.099999 + 0.2)

        self.assertTrue(self.called == 2, "callback f2 not called after delay")
        expected = [('f1',), ('f2',)]
        self.assertTrue(expected == self.args, "call order wrong")

    # noinspection PyTypeChecker
    def test_schedule_order(self):
        self.scheduler.schedule(self.func, 0.5, "f2")
        self.scheduler.schedule(self.func, 0.0, "f1")
        self.assertTrue(len(self.args) == 0, "call order already set before calling")
        self.scheduler.update(1.0, 1.0)

        expected = [('f1',), ('f2',)]
        self.assertTrue(expected == self.args, "call order wrong")

    # noinspection PyTypeChecker
    def test_repeated_interval(self):
        self.scheduler.schedule(self.func_repeat, 4, "f")
        # simulate time passing by
        self.scheduler.update(2, 2.0)  # t = 2

        self.assertTrue(self.called == 0, "callback f called before time")
        self.scheduler.update(2, 4.0)  # t = 4

        self.assertTrue(self.called == 1, "callback f not called after 1. time")
        self.scheduler.update(2, 6.0)  # t = 6

        self.assertTrue(self.called == 1, "callback f called before 2. time")
        self.scheduler.update(2, 8.0)  # t = 8

        self.assertTrue(self.called == 2, "callback f not called after 2. time")
        self.scheduler.update(4, 12.0)  # t = 12

        self.assertTrue(self.called == 3, "callback f not called after 3. time")
        expected = [('f',), ('f',), ('f',)]
        self.assertTrue(expected == self.args, "call order wrong")

    # noinspection PyTypeChecker
    def test_repeated_interval_remove_id_works_after_reschedule(self):
        schedule_id = self.scheduler.schedule(self.func_repeat, 4, "f")
        # simulate time passing by
        self.scheduler.update(2, 2)  # t = 2

        self.assertTrue(self.called == 0, "callback f called before time")
        self.scheduler.update(2, 4)  # t = 4

        self.assertTrue(self.called == 1, "callback f not called after 1. time")
        self.scheduler.update(2, 6)  # t = 6

        self.scheduler.remove(schedule_id)

        self.assertTrue(self.called == 1, "callback f called before 2. update")
        self.scheduler.update(2, 8)  # t = 8

        expected = [('f',)]
        self.assertTrue(expected == self.args, "call order wrong")

    # noinspection PyTypeChecker
    def test_repeat_offset(self):
        self.scheduler.schedule_with_offset(self.func_repeat, 4, 1, "f2")
        self.scheduler.schedule_with_offset(self.func_repeat, 4, 0, "f1")
        self.scheduler.schedule_with_offset(self.func_repeat, 4, 2, "f3")

        self.assertTrue(self.called == 0, "callback f1 called before time")
        self.scheduler.update(4, 4.0)

        self.assertTrue(self.called == 1, "callback f1 not called after time")
        self.scheduler.update(0.5, 4.5)

        self.assertTrue(self.called == 1, "only callback f1 should have been called so far")
        self.scheduler.update(0.5, 5.0)  # t = 5

        self.assertTrue(self.called == 2, "callback f2 not called after time")
        self.scheduler.update(0.5, 5.5)

        self.assertTrue(self.called == 2, "callback f not called after time")
        self.scheduler.update(0.5, 6.0)  # t = 6

        self.assertTrue(self.called == 3, "callback f3 not called after time")
        expected = [('f1',), ('f2',), ('f3',)]
        self.assertTrue(expected == self.args, "call order wrong")

    def test_soft_distribution(self):
        pass

    # noinspection PyTypeChecker
    def test_clear(self):
        self.scheduler.schedule(self.func_repeat, 4, 0, "f1")
        self.scheduler.schedule(self.func_repeat, 4, 1, "f2")
        self.scheduler.schedule(self.func_repeat, 4, 2, "f3")
        self.scheduler.clear()
        self.scheduler.update(10.0, 10.0)

        self.assertTrue(self.called == 0, "clear did not clear all registered functions")

    # noinspection PyTypeChecker
    def test_remove_first(self):
        f1 = self.scheduler.schedule(self.func_repeat, 1, "f1")
        self.scheduler.schedule(self.func_repeat, 2, "f2")
        self.scheduler.schedule(self.func_repeat, 3, "f3")
        self.scheduler.remove(f1)
        self.scheduler.update(3.5, 3.5)

        self.assertEqual(2, self.called, "clear did not clear all registered functions")
        expected = [('f2',), ('f3',)]
        self.assertEqual(expected, self.args, "remove should only have removed f1")

    # noinspection PyTypeChecker
    def test_remove_all(self):
        f1 = self.scheduler.schedule(self.func_repeat, 1, "f1")
        f2 = self.scheduler.schedule(self.func_repeat, 2, "f2")
        f3 = self.scheduler.schedule(self.func_repeat, 3, "f3")
        self.scheduler.remove(f1)
        self.scheduler.remove(f2)
        self.scheduler.remove(f3)
        self.scheduler.update(10.0, 10.0)

        self.assertTrue(self.called == 0, "remove did not remove all registered functions")

    def callback(self, *args, **kwargs):
        self.called += 1
        self.args.append(args[1])
        self.kwargs.append(kwargs)
        return args[0]

    # noinspection PyTypeChecker
    def test_short_callback_interval_with_long_update_period(self):
        # arrange
        self.scheduler.schedule_with_offset(self.callback, 1, 0, 1, "f1")
        self.scheduler.schedule_with_offset(self.callback, 2, 0, 2, "f2")
        self.scheduler.schedule_with_offset(self.callback, 8, 0, 8, "f3")
        n = 10000  # set this to a high number (e.g. 10000) to see how this really does perform

        # act
        self.scheduler.update(8 * n, 8 * n)

        # verify
        expected = [
            "f1",  # T = 1
            "f2", "f1",  # T = 2
            "f1",  # T = 3
            "f2", "f1",  # T = 4
            "f1",  # T = 5
            "f2", "f1",  # T = 6
            "f1",  # T = 7
            "f3", "f2", "f1",  # T = 8
        ]
        self.assertEqual(expected * n, self.args)

    # noinspection PyTypeChecker,PyUnusedLocal
    def test_remove_preserves_order_of_schedules(self):
        # arrange
        self.longMessage = True
        #                                 callback, interval,   offset, re-interval, description
        f5 = self.scheduler.schedule_with_offset(self.callback, 1, 4, 1, "f5")
        f4 = self.scheduler.schedule_with_offset(self.callback, 1, 3, 1, "f4")
        f6 = self.scheduler.schedule_with_offset(self.callback, 1, 5, 1, "f6")
        f3 = self.scheduler.schedule_with_offset(self.callback, 1, 2, 1, "f3")
        f7 = self.scheduler.schedule_with_offset(self.callback, 1, 6, 1, "f7")
        f2 = self.scheduler.schedule_with_offset(self.callback, 1, 1, 1, "f2")
        f8 = self.scheduler.schedule_with_offset(self.callback, 1, 7, 1, "f8")
        f1 = self.scheduler.schedule_with_offset(self.callback, 1, 0, 1, "f1")

        # act
        self.scheduler.remove(f7)
        self.scheduler.update(7.5, 7.5)

        # verify
        expected = [
            "f1",
            "f2", "f1",
            "f3", "f2", "f1",
            "f4", "f3", "f2", "f1",
            "f5", "f4", "f3", "f2", "f1",
            "f6", "f5", "f4", "f3", "f2", "f1",
            "f6", "f5", "f4", "f3", "f2", "f1",
        ]

        self.assertEqual(expected, self.args)

    def func_self_remove(self):
        # self.timer.remove(self.func_self_remove)
        self.self_removed += 1
        return False

    def test_self_remove(self):
        self.scheduler.schedule(self.func_self_remove, 1)
        self.scheduler.update(1, 1)

        self.scheduler.update(1, 2)

        self.assertTrue(self.self_removed == 1, "did not call itself")

    # noinspection PyTypeChecker
    def test_exact_frame(self):
        self.scheduler = Scheduler()
        self.scheduler.schedule(self.func_repeat, 1, 0, "f1")
        self.scheduler.schedule(self.func_repeat, 2, 0, "f2")
        self.scheduler.schedule(self.func_repeat, 3, 0, "f3")
        self.scheduler.update(0, 0)
        self.assertTrue(self.called == 0, "1")
        self.scheduler.update(1, 1)
        self.assertTrue(self.called == 1, "1")
        self.scheduler.update(1, 2)
        self.assertTrue(self.called == 2, "2")
        self.scheduler.update(1, 3)
        self.assertTrue(self.called == 3, "3")
        self.scheduler.update(1, 4)
        self.assertTrue(self.called == 3, "4")

    def test_assert_about_returning_none(self):
        # arrange
        # noinspection PyUnusedLocal
        def func(*args):
            return None

        self.scheduler.schedule(func, 1.0)

        # act / verify
        if __debug__:
            self.assertRaises(AssertionError, self.scheduler.update, 2.0, 2.0)


class TimerTests(unittest.TestCase):
    def callback1(self, *args, **kwargs):
        self.callback1_args.append((args, kwargs))
        self.cb1_count += 1

    def callback2(self, *args, **kwargs):
        self.callback2_args.append((args, kwargs))
        self.cb2_count += 1

    def setUp(self):
        self.longMessage = True
        self.callback1_args = []
        self.cb1_count = 0
        self.callback2_args = []
        self.cb2_count = 0
        # this gives control over the current time of scheduler
        Timer.scheduler = Scheduler()
        self.timer = Timer(Timer.scheduler, interval=0.5)
        self.timer.event_elapsed += self.callback1

    def test_timer_not_started(self):
        Timer.scheduler.update(0.0, 0.0)
        self.assertTrue(self.cb1_count == 0, "callback should not have been called")
        Timer.scheduler.update(1.0, 1.0)
        self.assertTrue(self.cb1_count == 0, "callback should not have been called")

    def _test_timer_no_repeat(self):
        Timer.scheduler.update(0.0, 0.0)
        self.assertTrue(self.cb1_count == 0, "callback should not have been called")
        Timer.scheduler.update(1.0, 1.0)
        self.assertTrue(self.cb1_count == 1, "callback not called, should be")
        Timer.scheduler.update(1.0, 2.0)
        self.assertTrue(self.cb1_count == 1, "callback should not be called twice")

    def test_timer_started_no_repeat(self):
        self.timer.start()
        self._test_timer_no_repeat()

    def _test_timer_repeat(self):
        Timer.scheduler.update(0.4, 0.4)
        self.assertEqual(0, self.cb1_count, "callback should not have been called")
        Timer.scheduler.update(0.4, 0.8)
        self.assertEqual(1, self.cb1_count, "callback not called, should be")
        Timer.scheduler.update(0.4, 1.2)
        self.assertEqual(2, self.cb1_count, "callback not called the second time")

    def test_timer_started_repeat_start_arg(self):
        self.timer.start(repeat=True)
        self._test_timer_repeat()

    def test_timer_started_repeat_variable(self):
        self.timer.repeat = True
        self.timer.start()
        self._test_timer_repeat()

    def test_timer_started_repeat_false(self):
        self.timer.repeat = True
        self.timer.start(repeat=False)  # no repeat
        self._test_timer_no_repeat()

    def test_timer_stop(self):
        self.timer.start(repeat=True)
        Timer.scheduler.update(0.4, 0.4)
        self.assertEqual(0, self.cb1_count, "callback should not have been called")
        Timer.scheduler.update(0.4, 0.8)
        self.assertEqual(1, self.cb1_count, "callback not called, should be")

        self.timer.stop()

        Timer.scheduler.update(1.0, 1.8)
        self.assertEqual(1, self.cb1_count, "callback should not be called  after calling stop")

    def test_timer_raises_value_error_with_unset_interval(self):
        # arrange
        self.timer = mut.Timer(scheduler=Timer.scheduler)

        # act / verify
        self.assertRaises(ValueError, self.timer.start)

    def test_timer_raises_value_error_with_invalid_interval_at_start(self):
        # arrange
        self.timer = mut.Timer(scheduler=Timer.scheduler)
        self.timer.interval = 3

        # act / verify
        self.assertRaises(ValueError, self.timer.start, -2)

    def test_timer_raises_value_error_setting_interval_to_invalid_value(self):
        # arrange
        self.timer = mut.Timer(scheduler=Timer.scheduler)

        # act / verify
        try:
            self.timer.interval = -34
        except ValueError:
            return
        except Exception as ex:
            self.fail(ex)
        self.fail("should have raised ValueError for invalid value of interval")

    def test_multiple_timers(self):
        self.timer.start(repeat=True)
        self.timer2 = Timer(Timer.scheduler, interval=0.65, repeat=True)
        self.timer2.event_elapsed += self.callback2
        self.timer2.start()
        Timer.scheduler.update(0.0, 0.0)

        self.assertTrue(self.cb1_count == 0, "callback1 should not have been called yet")
        self.assertTrue(self.cb2_count == 0, "callback2 should not have been called yet")

        Timer.scheduler.update(0.6, 0.6)

        self.assertTrue(self.cb1_count == 1, "callback1 not called")
        self.assertTrue(self.cb2_count == 0, "callback2 should not have been called yet")

        Timer.scheduler.update(0.33, 0.6 + 0.33)

        self.assertTrue(self.cb1_count == 1, "too many calls to callback1")
        self.assertTrue(self.cb2_count == 1, "callback2 not called")

    def test_timer_using_different_scheduler(self):
        my_scheduler = Scheduler()
        self.timer.scheduler = my_scheduler
        self.timer.start(repeat=True)

        dt = 0.4
        Timer.scheduler.update(dt, dt)
        self.assertEqual(0, self.cb1_count, "callback should not have been called")
        Timer.scheduler.update(dt, 2 * dt)
        self.assertEqual(0, self.cb1_count, "callback should not have been called because using different scheduler")

        my_scheduler.update(dt, 1 * dt)
        self.assertEqual(0, self.cb1_count, "callback should not have been called")
        my_scheduler.update(dt, 2 * dt)
        self.assertEqual(1, self.cb1_count, "callback not called")

    def test_timer_event_elapsed_arguments(self):
        self.timer.start(repeat=False)

        Timer.scheduler.update(1.0, 1.0)
        self.assertTrue(self.callback1_args == [((self.timer,), {})], "wrong arguments")
        self.assertTrue(self.cb1_count == 1, "callback not called")

    def test_starting_timer_multiple_times_should_start_only_once(self):
        # arrange
        self.timer.start()
        self.timer.start()
        self.timer.start()
        expected = 1

        # act
        Timer.scheduler.update(1.0, 1.0)

        # verify
        self.assertEqual(expected, self.cb1_count, "timer should only be started once!")

    def test_scheduler_stop_only_removes_callback_once_from_scheduler(self):
        # arrange
        self.timer.start()
        _orig_remove = self.timer.scheduler.remove

        TimerTests.remove_call_count = 0

        def cb_remove(*args):
            TimerTests.remove_call_count += 1
            _orig_remove(*args)

        self.timer.scheduler.remove = cb_remove

        # act
        self.timer.stop()
        self.timer.stop()
        self.timer.stop()
        self.timer.stop()
        Timer.scheduler.update(1.0, 1.0)

        # verify
        self.assertEqual(0, self.cb1_count, "timer stop should only unregister once")
        self.assertEqual(1, TimerTests.remove_call_count, "unregister should only be called once")

    def test_using_other_scheduler_does_not_fire_with_normal_way(self):
        # arrange
        other_scheduler = mut.Scheduler()
        timer = mut.Timer(other_scheduler, interval=1.0)
        timer.event_elapsed += self.callback1
        timer.start()

        # act
        Timer.scheduler.update(2.0, 2.0)

        # verify
        self.assertEqual(0, self.cb1_count)

    def test_using_other_scheduler(self):
        # arrange
        other_scheduler = mut.Scheduler()
        timer = mut.Timer(other_scheduler, interval=1.0)
        timer.event_elapsed += self.callback1
        timer.start()

        # act
        other_scheduler.update(2.0, 2.0)

        # verify
        self.assertEqual(1, self.cb1_count)


class TimerManipulationsDuringCallbackTests(unittest.TestCase):
    def setUp(self):
        self.longMessage = True
        self.interval = 1.0
        Timer.scheduler = Scheduler()  # make sure nothing is re-used from other tests
        self.timer = Timer(Timer.scheduler, interval=self.interval)
        self._cb_log = []

    def cb_restart_timer(self, *args):
        self._cb_log.append((self.cb_restart_timer.__name__, args))
        self.timer.start()

    def test_restart_timer_from_callback(self):
        # arrange
        self.timer.event_elapsed += self.cb_restart_timer
        self.timer.repeat = False
        self.timer.start()

        # act
        Timer.scheduler.update(self.interval, self.interval)
        Timer.scheduler.update(self.interval, 2 * self.interval)

        # verify
        self.assertEqual(2, len(self._cb_log), "the cb should have been called as many times the interval passed")
        self.assertEqual(True, self.timer._is_running, "time should be still running if started during callback")

    def cb_stop_timer(self, *args):
        self._cb_log.append((self.cb_stop_timer, args))
        self.timer.stop()

    def test_stop_timer_from_callback(self):
        # arrange
        self.timer.event_elapsed += self.cb_stop_timer
        self.timer.repeat = True
        self.timer.start()

        # act
        Timer.scheduler.update(self.interval, self.interval)
        Timer.scheduler.update(self.interval, 2 * self.interval)
        Timer.scheduler.update(self.interval, 3 * self.interval)
        Timer.scheduler.update(self.interval, 4 * self.interval)

        # verify
        self.assertEqual(1, len(self._cb_log), "cb should have been called only once since it stops the timer")

    def cb_just_log(self, *args):
        self._cb_log.append((self.cb_just_log.__name__, args))

    def test_callback_is_executed_repeatedly(self):
        # arrange
        self.timer.event_elapsed += self.cb_just_log
        self.timer.start(self.interval, repeat=True)

        # act
        Timer.scheduler.update(self.interval, self.interval)
        Timer.scheduler.update(self.interval, 2 * self.interval)
        Timer.scheduler.update(self.interval, 3 * self.interval)

        # verify
        self.assertEqual(3, len(self._cb_log), "repeat should work with a callback that does not change the timer")

    def test_callback_is_executed_once(self):
        # arrange
        self.timer.event_elapsed += self.cb_just_log
        self.timer.start(self.interval, repeat=False)

        # act
        Timer.scheduler.update(self.interval, self.interval)
        Timer.scheduler.update(self.interval, 2 * self.interval)
        Timer.scheduler.update(self.interval, 3 * self.interval)

        # verify
        self.assertEqual(1, len(self._cb_log), "repeat should work with a callback that does not change the timer")


class ChainedTimingClassesIntegrationTests(unittest.TestCase):

    def setUp(self):
        self.capture = []
        self._sim_time = 0

        # simulation
        self.game_time = GameTime()
        self.lock_stepper = LockStepper()
        self.scheduler = Scheduler()

        # graphics
        self.frame_cap = FrameCap()
        self.animation_scheduler = Scheduler()

        # connect
        #
        #                   +-> lock_stepper --> scheduler ----------------> timer
        #                   |
        #   game_time ------+
        #                   |
        #                   +-> frame_cap -----> animation_scheduler -----> animation(s)
        #
        #
        self.game_time.event_update += self.lock_stepper.update
        self.game_time.event_update += self.frame_cap.update
        self.lock_stepper.event_integrate += self.scheduler.update
        self.frame_cap.event_update += self.animation_scheduler.update

    def _do_capture(self, *args):
        self.capture.append(args[0])

    def _do_next_update(self, dt):
        self._sim_time += dt
        self._do_capture("!update! {0} {1}".format(str(dt), str(round(self._sim_time, 1))))
        self.game_time.update(dt)

    def test_X(self):
        # arrange
        self.lock_stepper.timestep = 0.5
        self.lock_stepper.event_integrate += lambda *args, **kwargs: self._do_capture("lock stepper", args, kwargs)
        self.frame_cap.max_fps = 3
        self.frame_cap.event_update += lambda *args, **kwargs: self._do_capture("frame cap", args, kwargs)
        timer = pyknic.timing.Timer(self.scheduler, interval=0.5)
        timer.start(0.5)
        timer.event_elapsed += lambda *args, **kwargs: self._do_capture("timer through scheduler", args, kwargs)

        # act
        self._do_next_update(1.0)
        self._do_next_update(0.2)
        self._do_next_update(0.2)
        self._do_next_update(0.2)

        # verify
        expected = [
            '!update! 1.0 1.0',
            'frame cap',
            'lock stepper',
            'timer through scheduler',
            'lock stepper',
            '!update! 0.2 1.2',
            '!update! 0.2 1.4',
            'frame cap',
            '!update! 0.2 1.6',
            'lock stepper'
        ]

        self.assertEqual(expected, self.capture)
        self.assertEqual(len(expected), len(self.capture))


class ChainGameTimeSchedulerLockStepperIntegrationTests(unittest.TestCase):
    # noinspection PyUnusedLocal
    def integrate(self, dt, sim_time, sender):
        self.deltas.append(dt)
        self.sim_time_points.append(sim_time)

    def schedule_callback(self, *args, **kwargs):
        self.schedule_callback_called += 1
        self.schedule_callback_args.append(args[:-1])
        self.schedule_callback_kwargs.append(kwargs)
        return 0  # only once

    def gametime_callback(self, delta_t, sim_time, sender):
        self.game_delta_time = delta_t

    def setUp(self):
        self.longMessage = True
        self.maxDiff = None
        self.game_time = GameTime()
        self.game_delta_time = 0

        self.scheduler = Scheduler()
        self.schedule_callback_called = 0
        self.schedule_callback_args = []
        self.schedule_callback_kwargs = []

        self.lock_stepper = LockStepper(max_dt=0.5)
        self.lock_stepper.event_integrate += self.integrate
        self.deltas = []
        self.sim_time_points = []

        self.game_time.event_update += self.lock_stepper.update
        self.game_time.event_update += self.scheduler.update
        self.game_time.event_update += self.gametime_callback

    def test_tick_normal(self):
        self.scheduler.schedule(self.schedule_callback, 0.1)
        # test initial conditions
        self.assertTrue(self.game_delta_time == 0, "gdt should be 0")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")
        self.assertTrue(self.deltas == [], "deltas should be empty")
        self.assertTrue(self.sim_time_points == [], "should not have any time point")

        self.game_time.update(0.05)

        self.assertTrue(self.game_delta_time == 0.05, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")
        self.assertTrue(self.deltas == [0.01, 0.01, 0.01, 0.01], "deltas wrong")
        self.assertEqual(self.sim_time_points, [0.01, 0.02, 0.03, 0.04], "time points wrong")

        self.game_time.update(0.04)

        self.assertTrue(self.game_delta_time == 0.04, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 8, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 8, "time points wrong")

        self.game_time.update(0.02)

        self.assertTrue(self.game_delta_time == 0.02, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 10, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 10, "time points wrong")

        self.game_time.update(0.011)

        self.assertTrue(self.game_delta_time == 0.011, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 12, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 12, "time points wrong")

    def test_tick_slowmotion(self):
        self.scheduler.schedule(self.schedule_callback, 0.01)

        self.game_time.tick_speed = 0.1

        # test initial conditions
        self.assertTrue(self.game_delta_time == 0, "gdt should be 0")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")
        self.assertTrue(self.deltas == [], "deltas should be empty")
        self.assertTrue(self.sim_time_points == [], "should not have any time point")

        self.game_time.update(0.05)

        self.assertTrue(self.game_delta_time >= 0.005, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")
        self.assertTrue(len(self.deltas) == 0, "deltas wrong")
        self.assertTrue(self.sim_time_points == [], "time points wrong")

        self.game_time.update(0.1)

        self.assertTrue(self.game_delta_time >= 0.01, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 1, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 1, "time points wrong")

        self.game_time.update(0.1)

        self.assertTrue(self.game_delta_time >= 0.01, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 2, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 2, "time points wrong")

        self.game_time.update(0.011)

        self.assertTrue(self.game_delta_time == 0.0011, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 2, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 2, "time points wrong")

    def test_tick_fastmotion(self):
        self.scheduler.schedule(self.schedule_callback, 0.1)

        self.game_time.tick_speed = 2.0

        # test initial conditions
        self.assertTrue(self.game_delta_time == 0, "gdt should be 0")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")
        self.assertTrue(self.deltas == [], "deltas should be empty")
        self.assertTrue(self.sim_time_points == [], "should not have any time point")

        self.game_time.update(0.04)

        self.assertTrue(self.game_delta_time == 0.08, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 0, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [], "kwargs should be empty")
        self.assertEqual(7, len(self.deltas), "deltas wrong")
        self.assertEqual(7, len(self.sim_time_points), "time points wrong")

        self.game_time.update(0.01)

        self.assertTrue(self.game_delta_time == 0.02, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")
        self.assertTrue(len(self.deltas) == 9, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 9, "time points wrong")

        self.game_time.update(0.1)

        self.assertTrue(self.game_delta_time == 0.2, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 17, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 17, "time points wrong")

        self.game_time.update(0.011)

        self.assertTrue(self.game_delta_time == 0.022, "gdt wrong")
        self.assertTrue(self.schedule_callback_called == 1, "scheduler should not have fired")
        self.assertTrue(self.schedule_callback_args == [tuple()], "args should be empty")
        self.assertTrue(self.schedule_callback_kwargs == [{}], "kwargs should be empty")

        self.assertTrue(len(self.deltas) == 25, "deltas wrong")
        self.assertTrue(len(self.sim_time_points) == 25, "time points wrong")


class TestFrameCap(unittest.TestCase):

    def setUp(self):
        self.sut = mut.FrameCap()
        self.interval = 1.0 / 60.0
        self.sut.max_fps = 1.0 / self.interval
        self.sut.event_update += self._capture
        self.captured = []

    def _capture(self, *args, **kwargs):
        self.captured.append((args, kwargs))

    def test_should_call_draw(self):
        # arrange
        # act
        sim_time = self.interval
        self.sut.update(self.interval, sim_time)
        actual = len(self.captured)

        # verify
        self.assertEqual(1, actual)
        self.assertEqual(((self.interval, sim_time, self.sut), {}), self.captured[0])

    def test_should_call_draw_every_time_if_fps_is_lower_than_max_fps(self):
        # arrange
        count = 10
        round_compensation = 0.001
        dt = self.interval + round_compensation  # slower, since fps = 1.0 / interval

        # act
        for i in range(count):
            self.sut.update(dt, dt + i * dt)

        # verify
        self.assertEqual(count, len(self.captured))

    def test_should_not_call_draw_if_fps_is_over_max_fps(self):
        # arrange
        count = 10
        dt = self.interval / count  # faster, since fps = 1.0 / interval

        # act
        for i in range(count):
            self.sut.update(dt, dt + i * dt)
        self.sut.update(dt, 2 * dt)

        # verify
        self.assertEqual(1, len(self.captured))


if __name__ == '__main__':
    unittest.main()
