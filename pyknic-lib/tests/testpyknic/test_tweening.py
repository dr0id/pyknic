# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.tweening module.
"""
from __future__ import print_function, division

import unittest
import warnings

import pyknic.tweening as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '2.0.1.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class ObjectForTesting(object):
    def __init__(self):
        self.x = 0
        self.y = 0


class TweenStateTests(unittest.TestCase):

    def test_tween_state_active_as_string(self):
        # arrange
        expected = "active"

        # act
        actual = mut.TweenStateNames[mut.Tweener.TweenStates.active]

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_paused_as_string(self):
        # arrange
        expected = "paused"

        # act
        actual = mut.TweenStateNames[mut.Tweener.TweenStates.paused]

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_ended_as_string(self):
        # arrange
        expected = "ended"

        # act
        actual = mut.TweenStateNames[mut.Tweener.TweenStates.ended]

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_error_as_string(self):
        # arrange
        expected = "error"

        # act
        actual = mut.TweenStateNames[mut.Tweener.TweenStates.error]

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_created_as_string(self):
        # arrange
        expected = "created"

        # act
        actual = mut.TweenStateNames[mut.Tweener.TweenStates.created]

        # verify
        self.assertEqual(expected, actual)


class TweenTests(unittest.TestCase):

    def setUp(self):
        self.obj = ObjectForTesting()
        self.tweener = mut.Tweener()
        self.change = 10
        self.begin = 5
        self.duration = 3

    def test_tween_end_value_is_correct(self):
        # arrange

        # act
        self.create_tween()
        self.tweener.update(self.duration)

        # verify
        expected = self.begin + self.change
        self.assertEqual(expected, self.obj.x)

    def test_tween_start_value_is_not_set(self):
        # arrange
        expected = self.obj.x  # save value before it is changed

        # act
        self.create_tween(immediate=False)

        # verify
        self.assertEqual(expected, self.obj.x)

    def test_tween_start_value_is_altered_with_immediate(self):
        # arrange

        # act
        self.create_tween()

        # verify
        expected = self.begin
        self.assertEqual(expected, self.obj.x)

    def test_tween_is_started_after_delay(self):
        # arrange
        delay = 4
        expected_start_value = self.obj.x

        # act
        self.create_tween(immediate=False, delay=delay)
        self.tweener.update(2)
        self.assertEqual(expected_start_value, self.obj.x)
        self.tweener.update(1)
        self.assertEqual(expected_start_value, self.obj.x)
        self.tweener.update(1)
        self.assertEqual(self.begin, self.obj.x)
        self.tweener.update(self.duration)

        # verify
        expected = self.begin + self.change
        self.assertEqual(expected, self.obj.x)

    def test_tween_has_correct_value_half_way(self):
        # arrange

        # act
        self.create_tween()
        self.tweener.update(self.duration * 0.5)

        # verify
        expected = self.begin + self.change * 0.5
        self.assertEqual(expected, self.obj.x)

    def test_tween_pause_resume(self):
        # arrange
        tween = self.create_tween()

        # act / verify
        self.tweener.update(self.duration * 0.5)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)
        tween.pause()
        self.tweener.update(self.duration)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)
        self.tweener.update(self.duration)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)
        self.tweener.update(self.duration)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)
        tween.resume()
        self.tweener.update(self.duration * 0.5)
        self.assertEqual(self.begin + self.change, self.obj.x)

    def create_tween(self, cb_end=None, cb_args=tuple(), immediate=True, delay=0.0, obj=None, do_start=True):
        if obj is None:
            obj = self.obj
        return self.tweener.create_tween(obj, "x", self.begin, self.change, self.duration, cb_end=cb_end,
                                         cb_args=cb_args, immediate=immediate, delay=delay, delta_update=False,
                                         do_start=do_start)

    def test_tween_stop_resume_does_nothing(self):
        # arrange
        tween = self.create_tween()

        # act / verify
        self.tweener.update(0.5 * self.duration)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)
        tween.stop()
        self.tweener.update(0.5 * self.duration)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)
        tween.resume()
        self.tweener.update(0.5 * self.duration)
        self.assertEqual(self.begin + self.change * 0.5, self.obj.x)

    def test_tween_is_removed_even_if_paused(self):
        # arrange
        tween = self.create_tween()

        self.tweener.update(0.5 * self.duration)
        self.assertEqual(self.begin + 0.5 * self.change, self.obj.x)

        tween.pause()
        self.tweener.update(0.5 * self.duration)
        self.assertEqual(self.begin + 0.5 * self.change, self.obj.x)

        # act
        tween.stop()  # remove it
        tween.resume()  # can't resume since removed

        # verify
        self.tweener.update(0.5 * self.duration)
        self.assertEqual(self.begin + 0.5 * self.change, self.obj.x)

    def test_tween_is_set_first_value_immediately(self):
        # arrange
        self.obj.x = -30
        expected = self.begin

        # act
        self.create_tween()
        actual = self.obj.x

        # verify
        self.assertEqual(expected, actual)

    def test_tween_is_set_first_value_immediately_even_with_delay(self):
        # arrange
        self.obj.x = -30
        expected = self.begin

        # act
        self.create_tween(delay=10)
        actual = self.obj.x

        # verify
        self.assertEqual(expected, actual)

    def test_tweener_pause_tweens(self):
        # arrange
        self.create_tween()
        obj2 = self.obj.__class__()
        obj2.x = self.obj.x
        self.create_tween(obj=obj2)

        self.tweener.update(0.5 * self.duration)

        # act
        self.tweener.pause_tweens()
        self.tweener.update(0.5 * self.duration)

        # verify
        self.assertEqual(self.begin + 0.5 * self.change, self.obj.x)
        self.assertEqual(self.begin + 0.5 * self.change, obj2.x)

    def test_tweener_resume_tweens(self):
        # arrange
        self.create_tween()
        obj2 = self.obj.__class__()
        obj2.x = self.obj.x
        self.create_tween(obj=obj2)
        self.tweener.update(0.5 * self.duration)
        self.tweener.pause_tweens()
        self.tweener.update(0.5 * self.duration)

        # act
        self.tweener.resume_tweens()
        self.tweener.update(0.25 * self.duration)

        # verify
        self.assertEqual(self.begin + 0.75 * self.change, self.obj.x)
        self.assertEqual(self.begin + 0.75 * self.change, obj2.x)

    # noinspection PyUnusedLocal
    def test_tweener_get_all_tweens_of_object(self):
        # arrange
        tween1 = self.create_tween()
        tween2 = self.create_tween()
        tween2.pause()
        other = self.obj.__class__()
        tween3 = self.create_tween(obj=other)
        tween4 = self.create_tween(obj=other)
        expected = [tween1, tween2]

        # act
        actual = self.tweener.get_all_tweens_for(self.obj)

        # verify
        self.assertEqual(expected, actual)

    def test_tweener_get_all_tweens_for_throws_exception_for_invalid_state(self):
        # arrange
        # act / verify
        self.assertRaises(Exception, self.tweener.get_all_tweens_for, self.obj, 34234)

    # noinspection PyUnusedLocal
    def test_tweener_get_all_active_tweens_of_object(self):
        # arrange
        tween1 = self.create_tween()
        tween2 = self.create_tween()
        tween2.pause()
        other = self.obj.__class__()
        tween3 = self.create_tween(obj=other)
        tween4 = self.create_tween(obj=other)
        expected = [tween1]

        # act
        actual = self.tweener.get_all_tweens_for(self.obj, self.tweener.TweenStates.active)

        # verify
        self.assertEqual(expected, actual)

    # noinspection PyUnusedLocal
    def test_tweener_get_all_paused_tweens_of_object(self):
        # arrange
        tween1 = self.create_tween()
        tween2 = self.create_tween()
        tween2.pause()
        other = self.obj.__class__()
        tween3 = self.create_tween(obj=other)
        tween4 = self.create_tween(obj=other)
        expected = [tween2]

        # act
        actual = self.tweener.get_all_tweens_for(self.obj, self.tweener.TweenStates.paused)

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_after_create(self):
        # arrange
        tween = self.create_tween()
        expected = self.tweener.TweenStates.active

        # act
        actual = tween.state

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_after_pause(self):
        # arrange
        tween = self.create_tween()
        expected = self.tweener.TweenStates.paused

        # act
        tween.pause()
        actual = tween.state

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_after_resume(self):
        # arrange
        tween = self.create_tween()
        tween.pause()
        expected = self.tweener.TweenStates.active

        # act
        tween.resume()
        actual = tween.state

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_after_ended(self):
        # arrange
        tween = self.create_tween()
        expected = self.tweener.TweenStates.ended

        # act
        self.tweener.update(self.duration * 2)
        actual = tween.state

        # verify
        self.assertEqual(expected, actual)

    def test_tween_state_after_create_but_delayed(self):
        # arrange
        tween = self.create_tween(delay=self.duration)
        expected = self.tweener.TweenStates.active

        # act
        self.tweener.update(self.duration * 0.5)
        actual = tween.state

        # verify
        self.assertEqual(expected, actual)

    def test_tween_callback(self):
        # arrange
        cb_args = []

        def callback(*args):
            cb_args.append(args)

        tween = self.create_tween(cb_end=callback, cb_args=(1, 2, 3))
        expected = [(1, 2, 3, tween)]

        # act
        self.tweener.update(self.duration)  # end tween and at the end the end cb is called
        actual = cb_args

        # verify
        self.assertEqual(expected, actual)

    def test_create_tween_as_callback(self):
        # arrange
        cb_args = []

        def callback(*args):
            cb_args.append(args)

        tween = self.create_tween(cb_end=self.tweener.create_tween, cb_args=(self.obj, "y", self.begin, self.change,
                                                                             self.duration, mut.ease_linear, None,
                                                                             callback, cb_args, False, 0.0, False,
                                                                             False))
        expected = [(1, 2, 3, tween)]

        # act
        self.tweener.update(self.duration)  # end tween and at the end the end cb is called
        self.tweener.update(self.duration)  # end tween and at the end the end cb is called
        actual = cb_args

        # verify
        self.assertEqual(expected[:-1], actual[:-1])

    def test_do_not_start_tween_by_creation(self):
        # arrange
        tween1 = self.create_tween(do_start=False)

        # act
        self.tweener.update(self.duration / 2)

        # verify
        self.assertEqual(mut.Tweener.TweenStates.created, tween1.state)

    def test_start_tween_at_creation(self):
        # arrange
        tween1 = self.create_tween(do_start=True)

        # act
        self.tweener.update(self.duration / 2)

        # verify
        self.assertEqual(mut.Tweener.TweenStates.active, tween1.state)

    def test_start_tween_separately(self):
        # arrange
        tween1 = self.create_tween(do_start=False)
        tween1.start()

        # act
        self.tweener.update(self.duration / 2)

        # verify
        self.assertEqual(mut.Tweener.TweenStates.active, tween1.state)

    def test_restart_tween_separately(self):
        # arrange
        tween1 = self.create_tween(do_start=True)
        self.tweener.update(2 * self.duration)
        self.assertEqual(tween1.States.ended, tween1.state)

        # act
        tween1.start()

        # verify
        self.assertEqual(self.tweener.TweenStates.active, tween1.state)

    def test_start_tween_from_other_tweener(self):
        # arrange
        other_tweener = mut.Tweener()
        tween_from_other = other_tweener.create_tween(self.obj, 'x', 1, 2, self.duration, do_start=False)

        # act
        self.assertRaises(mut.TweenBelongsToOtherTweenerException, self.tweener.start_tween, tween_from_other)

    def test_get_active_tweens(self):
        # arrange
        tween1 = self.create_tween()
        tween2 = self.create_tween()
        tween2.pause()
        other = self.obj.__class__()
        tween3 = self.create_tween(obj=other)
        tween4 = self.create_tween(obj=other)
        expected = [tween1, tween3, tween4]

        # act
        actual = self.tweener.get_all_tweens(self.tweener.TweenStates.active)

        # verify
        self.assertEqual(expected, actual)

    # noinspection PyUnusedLocal
    def test_get_paused_tweens(self):
        # arrange
        tween1 = self.create_tween()
        tween2 = self.create_tween()
        tween2.pause()
        other = self.obj.__class__()
        tween3 = self.create_tween(obj=other)
        tween4 = self.create_tween(obj=other)
        expected = [tween2]

        # act
        actual = self.tweener.get_all_tweens(self.tweener.TweenStates.paused)

        # verify
        self.assertEqual(expected, actual)

    def test_get_all_tweens(self):
        # arrange
        tween1 = self.create_tween()
        tween2 = self.create_tween()
        tween2.pause()
        other = self.obj.__class__()
        tween3 = self.create_tween(obj=other)
        tween4 = self.create_tween(obj=other)
        expected = [tween1, tween3, tween4, tween2]

        # act
        actual = self.tweener.get_all_tweens()

        # verify
        self.assertEqual(expected, actual)

    def test_tweener_get_all_tweens_throws_exception_for_invalid_state(self):
        # arrange
        # act / verify
        self.assertRaises(Exception, self.tweener.get_all_tweens, 34234)

    def test_create_tween_by_end(self):
        # arrange
        end_value = 2

        # act
        tween1 = self.tweener.create_tween_by_end(self.obj, 'x', 1, end_value, self.duration)

        # verify
        self.assertIsInstance(tween1, mut._Tween)

    def test_create_tween_by_end_has_end_value(self):
        # arrange
        end_value = 2
        tween1 = self.tweener.create_tween_by_end(self.obj, 'x', 1, end_value, self.duration)

        # act
        self.tweener.update(self.duration)

        # verify
        expected = end_value
        self.assertEqual(expected, self.obj.x)
        self.assertEqual(self.tweener.TweenStates.ended, tween1.state)

    def test_create_tween_by_end_has_smaller_end_value(self):
        # arrange
        end_value = -2
        tween1 = self.tweener.create_tween_by_end(self.obj, 'x', 1, end_value, self.duration)

        # act
        self.tweener.update(self.duration)

        # verify
        expected = end_value
        self.assertEqual(expected, self.obj.x)
        self.assertEqual(self.tweener.TweenStates.ended, tween1.state)

    def test_tween_is_in_state_ended_after_calling_stop(self):
        # arrange
        tween = self.tweener.create_tween(self.obj, 'x', 1, 100, self.duration)
        tween.start()

        # act
        tween.stop()

        # verify
        self.assertEqual(mut.TweenStates.ended, tween.state)


class TweenUsingDeltaUpdateTests(TweenTests):

    def setUp(self):
        self.obj = ObjectForTesting()
        self.tweener = mut.Tweener()
        self.change = 10
        self.begin = 5
        self.duration = 3
        self.obj.x = self.begin  # this is important so that the same values are generated, otherwise obj.x + delta!

    def create_tween(self, cb_end=None, cb_args=tuple(), immediate=True, delay=0.0, obj=None, do_start=True):
        if obj is None:
            obj = self.obj
        return self.tweener.create_tween(obj, "x", self.begin, self.change, self.duration, cb_end=cb_end,
                                         cb_args=cb_args, immediate=immediate, delay=delay, delta_update=True,
                                         do_start=do_start)


class TweenCreateByChangeTests(TweenTests):

    def setUp(self):
        self.obj = ObjectForTesting()
        self.tweener = mut.Tweener()
        self.change = 10
        self.begin = 5
        self.duration = 3
        self.obj.x = self.begin  # this is important so that the same values are generated, otherwise obj.x + delta!

    def create_tween(self, cb_end=None, cb_args=tuple(), immediate=True, delay=0.0, obj=None, do_start=True):
        if obj is None:
            obj = self.obj
        return self.tweener.create_tween_by_change(obj, "x", self.begin, self.change, self.duration, cb_end=cb_end,
                                                   cb_args=cb_args, immediate=immediate, delay=delay, delta_update=True,
                                                   do_start=do_start)


class TweenBasicShould(unittest.TestCase):
    """
    Contains tests that should be valid vor a Tween, a SerialGroup and a ParallelGroup.
    """

    def setUp(self):
        self.x = -1
        self.y = -1
        self.z = -1
        self.duration = 1
        self.tweener = mut.Tweener()
        self.tween = self.tweener.create_tween(self, 'x', 0, 100, self.duration, do_start=False)
        self.tween1 = self.tween
        self.tween2 = self.tweener.create_tween(self, 'y', 0, 100, self.duration, do_start=False)

    def test_set_the_original_cb_to_all_tweens_when_stopped(self):
        # arrange
        class _Obj(object):
            def cb(self, *args):
                pass

        o1 = _Obj()
        o2 = _Obj()
        self.tween1.cb = o1.cb
        self.tween2.cb = o2.cb
        self.tween.start()
        self.tweener.update(0.1 * self.duration)

        # act
        self.tween.stop()

        # verify
        self.assertEqual(o1.cb, self.tween1.cb, "cb of tween 1 should have been restored")
        self.assertEqual(o2.cb, self.tween2.cb, "cb of tween 2 should have been restored")

    def test_have_time_0_after_start_reset(self):
        # arrange
        self.tween.start()
        self.tweener.update(self.duration)

        # act
        self.tween.start()

        # verify
        self.assertEqual(0, self.tween.t)

    def test_reset_after_calling_start_again(self):
        # arrange
        self.tween.start()
        self.tweener.update(0.1 * self.duration)
        self.tweener.update(0.1 * self.duration)
        self.tweener.update(0.1 * self.duration)
        self.tween.start()
        self.tweener.update(0.1 * self.duration)
        self.tweener.update(0.1 * self.duration)
        self.tweener.update(0.1 * self.duration)
        self.tween.start()
        self.tweener.update(0.1 * self.duration)
        self.tweener.update(0.1 * self.duration)
        self.tween.start()
        self.tweener.update(0.1 * self.duration)
        self.tweener.update(0.1 * self.duration)
        self.tween.start()

        # act / verify
        self.tweener.update(10 * self.duration)

    def test_only_stop_tweens_once_even_stop_is_called_multiple_times(self):
        # arrange
        self.tween.stop()
        active_tweens = self.tweener.get_all_tweens()

        # act
        self.tween.stop()

        # verify
        self.assert_no_tweens_are_in_states(self.tweener, self.get_states_except(mut.TweenStates.ended))
        self.assertEqual(len(active_tweens), len(self.tweener.get_all_tweens()))
        other_tweens = self.tweener.get_all_tweens()
        for idx, _t in enumerate(active_tweens):
            other = other_tweens.pop(other_tweens.index(_t))
            self.assertEqual(_t, other)

    def test_only_start_tweens_once_even_start_is_called_multiple_times(self):
        # arrange
        self.tween.start()
        active_tweens = self.tweener.get_all_tweens()

        # act
        self.tween.start()

        # verify
        self.assert_no_tweens_are_in_states(self.tweener, self.get_states_except(mut.TweenStates.active))
        self.assertEqual(len(active_tweens), len(self.tweener.get_all_tweens()))
        other_tweens = self.tweener.get_all_tweens()
        for idx, _t in enumerate(active_tweens):
            other = other_tweens.pop(other_tweens.index(_t))
            self.assertEqual(_t, other)

    def test_only_resume_tweens_once_even_resume_is_called_multiple_times(self):
        # arrange
        self.tween.start()
        self.tween.pause()
        self.tween.resume()
        active_tweens = self.tweener.get_all_tweens()

        # act
        self.tween.resume()

        # verify
        self.assert_no_tweens_are_in_states(self.tweener, self.get_states_except(mut.TweenStates.active))
        self.assertEqual(len(active_tweens), len(self.tweener.get_all_tweens()))
        other_tweens = self.tweener.get_all_tweens()
        for idx, _t in enumerate(active_tweens):
            other = other_tweens.pop(other_tweens.index(_t))
            self.assertEqual(_t, other)

    def test_stop_tweens_in_state_stopped(self):
        # arrange
        self.tween.start()

        # act
        self.tween.stop()

        #  verify
        self.assertEqual(mut.TweenStates.ended, self.tween.state)
        self.assertEqual(mut.TweenStates.ended, self.tween1.state)
        if hasattr(self.tween, 'count'):  # tween has no count
            self.assertEqual(mut.TweenStates.ended, self.tween2.state)

    def test_create_tweens_in_state_created(self):
        # act / verify
        self.assertEqual(mut.TweenStates.created, self.tween.state)
        self.assertEqual(mut.TweenStates.created, self.tween1.state)
        self.assertEqual(mut.TweenStates.created, self.tween2.state)

    def test_set_active_state_after_resume(self):
        # arrange
        self.tween.start()
        self.tween.pause()

        # act
        self.tween.resume()

        # verify
        self.assertEqual(mut.TweenStates.active, self.tween.state)

    def test_set_tweens_to_active_state_after_resume(self):
        # arrange
        self.tween.start()
        self.tween.pause()

        # act
        self.tween.resume()

        # verify
        states = self.get_states_except(mut.TweenStates.active)
        self.assert_no_tweens_are_in_states(self.tweener, states)

    def assert_no_tweens_are_in_states(self, tweener, states):
        for _st in states:
            msg = "no tweens in state '{0}' should be in tweener".format(mut.TweenStateNames[_st])
            tweens_of_state = tweener.get_all_tweens(_st)
            self.assertEqual(0, len(tweens_of_state), msg)

    @staticmethod
    def get_states_except(*ignored_states):
        states = [
            mut.TweenStates.active,
            mut.TweenStates.created,
            mut.TweenStates.error,
            mut.TweenStates.ended,
            mut.TweenStates.paused
        ]
        for ignored in ignored_states:
            states.remove(ignored)
        return states

    def test_set_paused_state_after_pause(self):
        # arrange
        self.tween.start()

        # act
        self.tween.pause()

        # verify
        self.assertEqual(mut.TweenStates.paused, self.tween.state)

    def test_set_all_tweens_to_paused(self):
        # arrange
        self.tween.start()

        # act
        self.tween.pause()

        # verify
        states = self.get_states_except(mut.TweenStates.paused)
        self.assert_no_tweens_are_in_states(self.tweener, states)

    def test_set_active_state_after_start(self):
        # act
        self.tween.start()

        # verify
        self.assertEqual(mut.TweenStates.active, self.tween.state)

    def test_report_its_time_correctly(self):
        # arrange
        self.tween.start()
        long_duration = 10 * self.duration

        # act
        self.tweener.update(long_duration)
        self.tween.start()
        self.tweener.update(long_duration)

        # verify
        self.assertEqual(long_duration, self.tween.t)

    def test_have_a_parallel_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'parallel'), "tween should have a parallel method")
        self.assertTrue(callable(self.tween.parallel), "parallel method of tween should be callable")

    def test_have_a_next_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'next'), "tween should have a next method")
        self.assertTrue(callable(self.tween.next), "next method of tween should be callable")

    def test_have_a_repeat_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'repeat'), "tween should have a repeat method")
        self.assertTrue(callable(self.tween.repeat), "repeat method of tween should be callable")

    def test_have_a_start_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'start'), "tween should have a start method")
        self.assertTrue(callable(self.tween.start), "start method of tween should be callable")

    def test_have_a_stop_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'stop'), "tween should have a stop method")
        self.assertTrue(callable(self.tween.stop), "stop method of tween should be callable")

    def test_have_a_resume_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'resume'), "tween should have a resume method")
        self.assertTrue(callable(self.tween.resume), "resume method of tween should be callable")

    def test_have_a_pause_method(self):
        # verify
        self.assertTrue(hasattr(self.tween, 'pause'), "tween should have a pause method")
        self.assertTrue(callable(self.tween.pause), "pause method of tween should be callable")

    def test_return_a_serial_group_for_next(self):
        # act
        actual = self.tween.next(self.tween2)

        # verify
        self.assertIsInstance(actual, mut._Serial)

    def test_return_a_serial_group_for_next_with_multiple_tweens(self):
        # arrange
        extra_tween = self.tweener.create_tween(self, 'z', 0, 100, self.duration, do_start=False)

        # act
        actual = self.tween.next(self.tween2, extra_tween)

        # verify
        self.assertEqual(3, actual.count)

    def test_return_a_serial_group_for_next_tweens(self):
        # act
        extra_tween = self.tweener.create_tween(self, 'z', 0, 100, self.duration, do_start=False)
        actual = self.tween.next(self.tween2, extra_tween)

        # verify
        self.assertIsInstance(actual, mut._Serial)
        self.assertEqual(3, actual.count)

    def test_return_a_serial_group_for_repeat(self):
        # act
        count_of_tweens = 4
        actual = self.tween.repeat(count_of_tweens)

        # verify
        self.assertIsInstance(actual, mut._Serial)
        self.assertEqual(count_of_tweens, actual.count)

    def test_return_a_parallel_group_for_parallel(self):
        # act
        actual = self.tween.parallel(self.tween2)

        # verify
        self.assertIsInstance(actual, mut._Parallel)
        self.assertEqual(2, actual.count)

    def test_should_raise_TweenBelongsToOtherTweenerException_for_next(self):
        # arrange
        tweener2 = mut.Tweener()
        other_tween = tweener2.create_tween(self, 'x', 0, 100, self.duration)

        # act
        self.assertRaises(mut.TweenBelongsToOtherTweenerException, self.tween.next, other_tween)

    def test_should_raise_TweenBelongsToOtherTweenerException_for_parallel(self):
        # arrange
        tweener2 = mut.Tweener()
        other_tween = tweener2.create_tween(self, 'x', 0, 100, self.duration)

        # act
        self.assertRaises(mut.TweenBelongsToOtherTweenerException, self.tween.parallel, other_tween)

    def test_callback_is_called_when_ended(self):
        # arrange
        cb_args = []

        def _cb(*args):
            cb_args.append(args)

        self.tween.cb = _cb
        self.tween.start()

        # act
        self.tweener.update(self.duration)
        self.tweener.update(self.duration)  # call it twice since this is the shortest serial

        # verify
        self.assertEqual(1, len(cb_args))
        self.assertEqual([(self.tween,)], cb_args)

    def test_start_next_tween_immediate(self):
        # arrange
        s = self.tween.next(self.tween2)

        # act
        s.start()

        # verify
        self.assertEqual(0, self.x)

    def test_start_next_tween_immediate_false(self):
        # arrange
        s = self.tween.next(self.tween2)

        # act
        s.start(immediate=False)

        # verify
        self.assertEqual(-1, self.x)

    def test_update_object_values(self):
        # arrange
        o1 = _assign_obj_to_tween(self.tween1)
        self.tween.start()

        # act
        self.tweener.update(4 * self.duration)

        # verify
        self.assertEqual(self.tween1.b + self.tween1.c, o1.x, "tween1 should be updated")

    def test_repeat_infinite_with_value_0(self):
        # arrange
        captured = []

        def do_capture(*args):
            captured.append(args)

        self.tween.cb = do_capture
        self.tween1.cb = do_capture
        self.tween2.cb = do_capture
        chain = self.tween.repeat(0)

        chain.start()
        count = 100

        # act
        for i in range(count):
            self.tweener.update(self.duration)

        # verify
        actual = len(captured)
        expected = count
        self.assertGreaterEqual(actual, expected)


class SerialBasicsShould(TweenBasicShould):

    def setUp(self):
        self.x = -1
        self.y = -1
        self.duration = 1
        self.tweener = mut.Tweener()
        self.tween2 = self.tweener.create_tween(self, 'y', 0, 100, self.duration, do_start=False)
        self.tween1 = self.tweener.create_tween(self, 'x', 0, 100, self.duration, do_start=False)
        self.tween = self.tween1.next(self.tween2)


class ParallelBasicsShould(TweenBasicShould):

    def setUp(self):
        self.x = -1
        self.y = -1
        self.duration = 1
        self.tweener = mut.Tweener()
        self.tween1 = self.tweener.create_tween(self, 'x', 0, 100, self.duration, do_start=False)
        self.tween2 = self.tweener.create_tween(self, 'y', 0, 100, self.duration, do_start=False)
        self.tween = self.tween1.parallel(self.tween2)


class TweenShould(unittest.TestCase):

    def setUp(self):
        self.x = -1
        self.y = -1
        self.duration = 1
        self.tweener = mut.Tweener()
        self.tween = self.tweener.create_tween(self, 'x', 0, 100, self.duration, do_start=False)
        self.tween2 = self.tweener.create_tween(self, 'y', 0, 100, self.duration, do_start=False)

    def test_await_all_parallel_tweens(self):
        # arrange
        cb_args = []

        def _cb(*args):
            cb_args.append(args)

        t1 = self.tweener.create_tween(self, 'x', 0, 100, 2.0 * self.duration, do_start=False)
        t2 = self.tweener.create_tween(self, 'x', 0, 100, 1.5 * self.duration, do_start=False)
        t3 = self.tweener.create_tween(self, 'x', 0, 100, 1.25 * self.duration, do_start=False)
        tween = self.tween.parallel(t1, t2, t3)
        tween.cb = _cb
        tween.start()

        # act / verify
        self.assertEqual(3 + 1, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(self.duration)
        self.assertEqual(3, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.3 * self.duration)  # 1.2
        self.assertEqual(2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.3 * self.duration)  # 2.0
        self.assertEqual(1, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.4 * self.duration)  # 2.0
        self.assertEqual(0, len(self.tweener.get_all_tweens(mut.TweenStates.active)),
                         "there should not tween been left")

        # verify
        self.assertEqual(1, len(cb_args), "callback of parallel should have been called")


class _TestObject(object):

    def __init__(self, x):
        self.x = x


def _assign_obj_to_tween(tween):
    o1 = _TestObject(0)
    tween.o = o1
    tween.a = 'x'
    return o1


def concat_args(args, other):
    return tuple(list(args) + [other])


class SerialGroupShould(unittest.TestCase):

    def setUp(self):
        self.tweener = mut.Tweener()
        self.duration = 1
        self.tween1 = self.tweener.create_tween(self, 'a1', 0, 100, self.duration, do_start=False)
        self.tween2 = self.tweener.create_tween(self, 'a2', 0, 100, self.duration, do_start=False)
        self.tween3 = self.tweener.create_tween(self, 'a3', 0, 100, self.duration, do_start=False)
        self.tween4 = self.tweener.create_tween(self, 'a4', 0, 100, self.duration, do_start=False)

    def test_skipped_tweens_set_value(self):
        # arrange
        serial = self.tween1.next(self.tween2)
        o1 = _assign_obj_to_tween(self.tween1)
        o2 = _assign_obj_to_tween(self.tween2)
        serial.start()

        # act
        self.tweener.update(4 * self.duration)

        # verify
        self.assertEqual(self.tween1.b + self.tween1.c, o1.x)
        self.assertEqual(self.tween2.b + self.tween2.c, o2.x)

    def test_skipped_tweens_are_executed(self):
        # arrange

        callbacks = []

        def call_back(*args):
            callbacks.append(args)

        serial = self.tween1.next(self.tween2)

        self.tween1.cb = call_back
        self.tween2.cb = call_back
        serial.cb = call_back

        serial.start()

        # act
        self.tweener.update(4 * self.duration)

        # verify
        self.assertEqual(3, len(callbacks))
        self.assertEqual((self.tween1,), callbacks[0])
        self.assertEqual((self.tween2,), callbacks[1])
        self.assertEqual((serial,), callbacks[2])

    # noinspection PyCompatibility
    def test_all_tween_callbacks_are_called(self):
        # arrange

        callbacks = []

        def call_back(*args):
            callbacks.append(args)

        serial = self.tween1.next(self.tween2).next(self.tween3)

        self.tween1.cb = call_back
        self.tween2.cb = call_back
        self.tween3.cb = call_back
        serial.cb = call_back

        cb_args = (1, 2)
        self.tween1.cb_args = cb_args
        self.tween2.cb_args = cb_args
        self.tween3.cb_args = cb_args
        serial.cb_args = cb_args

        serial.start()

        # act
        self.tweener.update(self.duration)
        self.tweener.update(self.duration)
        self.tweener.update(self.duration)

        # verify
        self.assertEqual(4, len(callbacks))
        self.assertEqual(concat_args(cb_args, self.tween1), callbacks[0])
        self.assertEqual(concat_args(cb_args, self.tween2), callbacks[1])
        self.assertEqual(concat_args(cb_args, self.tween3), callbacks[2])
        self.assertEqual(concat_args(cb_args, serial), callbacks[3])

    def test_await_all_parallel_tweens(self):
        # arrange
        cb_args = []

        def _cb(*args):
            cb_args.append(args)

        tween = self.tween1.next(self.tween2)
        tween_duration = tween.d
        t1 = self.tweener.create_tween(self, 'x', 0, 100, 2.0 * tween_duration, do_start=False)
        t2 = self.tweener.create_tween(self, 'x', 0, 100, 1.5 * tween_duration, do_start=False)
        t3 = self.tweener.create_tween(self, 'x', 0, 100, 1.25 * tween_duration, do_start=False)
        tween = tween.parallel(t1, t2, t3)
        tween.cb = _cb
        tween.start()

        # act / verify
        self.assertEqual(3 + 1, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(tween_duration)
        self.assertEqual(3, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.3 * tween_duration)  # 1.3
        self.assertEqual(2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.3 * tween_duration)  # 1.6
        self.assertEqual(1, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.4 * tween_duration)  # 2.0
        self.assertEqual(0, len(self.tweener.get_all_tweens(mut.TweenStates.active)),
                         "there should not tween been left")

        # verify
        self.assertEqual(1, len(cb_args), "callback of parallel should have been called")

    def test_return_correct_duration(self):
        # act
        actual = self.tween1.next(self.tween2).d

        # verify
        self.assertEqual(self.duration * 2, actual, "duration should be sum of serial tweens")

    def test_contains_two_tweens_after_next(self):
        # act
        serial = self.tween1.next(self.tween2)

        # verify
        self.assertEqual(2, serial.count)

    def test_reject_tween_from_another_tweener(self):
        # arrange
        other_tween = mut.Tweener().create_tween(self, 'z', 0, 100, 1)

        # act / verify
        self.assertRaises(mut.TweenBelongsToOtherTweenerException, self.tween1.next, other_tween)

    def test_reject_tweens_that_are_not_in_state_stopped(self):
        # arrange
        self.tween2.start()

        # act / verify
        self.assertRaises(mut.TweenNotInAppropriateStateException, self.tween1.next, self.tween2)

    def test_schedule_first_tween_when_started(self):
        # arrange
        serial = self.tween1.next(self.tween2)

        # act
        serial.start()

        # verify
        self.assertEqual([self.tween1], self.tweener.get_all_tweens())

    def test_callback_of_first_tween_should_point_to_serial_group(self):
        # arrange
        serial = self.tween1.next(self.tween2)

        # act
        serial.start()

        # verify
        actual = self.tween1.cb
        self.assertEqual(serial.call_back_to_serial, actual,
                         "callback should point to the callback method of the serial group")

    def test_callback_is_triggered_and_next_tween_is_started(self):
        # arrange
        serial = self.tween1.next(self.tween2)
        serial.start()

        # act
        self.tweener.update(self.duration)  # trigger callback of tween1

        # verify
        self.assertEqual([self.tween2], self.tweener.get_all_tweens())

    def test_not_add_any_tween_if_all_have_been_called(self):
        # arrange
        serial = self.tween1.next(self.tween2)
        serial.start()

        # act
        self.tweener.update(self.duration)  # trigger callback of tween1
        self.tweener.update(self.duration)  # trigger callback of tween2

        # verify
        self.assertEqual(0, len(self.tweener.get_all_tweens()))

    def test_add_tween_when_next_is_called(self):
        # arrange
        serial = self.tween1.next(self.tween2)

        # act
        actual = serial.next(self.tween3)

        # verify
        # self.assertEqual(serial, actual)
        # self.assertEqual(3, actual.count)
        self.assertEqual(2, actual.count)

    def test_repeat_itself_n_times(self):
        # arrange
        serial = self.tween1.next(self.tween2)
        repeat_count = 3

        # act
        actual = serial.repeat(repeat_count)

        # verify
        self.assertIsInstance(actual, mut._Serial)
        self.assertEqual(repeat_count, actual.count)

    def test_call_callback_when_ended(self):
        # arrange
        called = []

        def cb(*args):
            called.append(args)

        serial = self.tween1.next(self.tween2)
        serial.cb = cb
        serial.start()

        # act
        self.tweener.update(self.duration)  # finish first tween
        self.tweener.update(self.duration)  # finish second tween

        # assert
        self.assertEqual(1, len(called))


class ParallelGroupShould(unittest.TestCase):

    def setUp(self):
        self.tweener = mut.Tweener()
        self.duration = 1
        self.tween1 = self.tweener.create_tween(self, 'a1', 0, 100, self.duration, do_start=False)
        self.tween2 = self.tweener.create_tween(self, 'a2', 0, 100, self.duration, do_start=False)
        self.tween3 = self.tweener.create_tween(self, 'a3', 0, 100, self.duration, do_start=False)
        self.tween4 = self.tweener.create_tween(self, 'a4', 0, 100, self.duration, do_start=False)

    def test_parallel_group_next_serial_group_with_multiple_tweens(self):
        # arrange

        # act
        actual = self.tween1.parallel(self.tween2).next(self.tween3, self.tween4)

        # verify
        self.assertEqual(3, actual.count)

    # noinspection PyCompatibility
    def test_all_tween_callbacks_are_called(self):
        # arrange

        callbacks = []

        def call_back(*args):
            callbacks.append(args)

        parallel = self.tween1.parallel(self.tween2, self.tween3)

        self.tween1.cb = call_back
        self.tween2.cb = call_back
        self.tween3.cb = call_back
        parallel.cb = call_back

        self.tween1.cb_args = (1, 1)
        self.tween2.cb_args = (1, 2)
        self.tween3.cb_args = (1, 3)
        parallel.cb_args = (1, 4)

        parallel.start()

        # act
        self.tweener.update(self.duration)

        # verify
        self.assertEqual(4, len(callbacks))
        callbacks.sort(key=lambda c: tuple(c))  # since the callback can happen in random order just sort them here
        self.assertEqual(concat_args((1, 1), self.tween1), callbacks[0], "first tween cb")
        self.assertEqual(concat_args((1, 2), self.tween2), callbacks[1], "second tween cb")
        self.assertEqual(concat_args((1, 3), self.tween3), callbacks[2], "third tween cb")
        self.assertEqual(concat_args((1, 4), parallel), callbacks[3], "parallel group cb")

    def test_await_all_parallel_tweens(self):
        # arrange
        cb_args = []

        def _cb(*args):
            cb_args.append(args)

        tween = self.tween1.parallel(self.tween2)
        t1 = self.tweener.create_tween(self, 'x', 0, 100, 2.0 * self.duration, do_start=False)
        t2 = self.tweener.create_tween(self, 'x', 0, 100, 1.5 * self.duration, do_start=False)
        t3 = self.tweener.create_tween(self, 'x', 0, 100, 1.25 * self.duration, do_start=False)
        tween = tween.parallel(t1, t2, t3)
        tween.cb = _cb
        tween.start()

        # act / verify
        self.assertEqual(3 + 2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(self.duration)
        self.assertEqual(3, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.3 * self.duration)  # 1.2
        self.assertEqual(2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.tweener.update(0.3 * self.duration)  # 1.6
        self.assertEqual(1, len(self.tweener.get_all_tweens(mut.TweenStates.active)), "there should 1 tween be left")
        self.tweener.update(0.4 * self.duration)  # 2.0
        self.assertEqual(0, len(self.tweener.get_all_tweens(mut.TweenStates.active)), "there should no tween be left")

        # verify
        self.assertEqual(1, len(cb_args), "callback of parallel should have been called")

    def test_await_all_parallel_tweens_does_set_t_correctly(self):
        # arrange
        cb_args = []

        def _cb(*args):
            cb_args.append(args)

        tween = self.tween1.parallel(self.tween2)
        t3 = self.tweener.create_tween(self, 'x', 0, 100, 2.0 * self.duration, do_start=False)
        t2 = self.tweener.create_tween(self, 'x', 0, 100, 1.5 * self.duration, do_start=False)
        t1 = self.tweener.create_tween(self, 'x', 0, 100, 1.25 * self.duration, do_start=False)
        tween = tween.parallel(t1, t2, t3)
        tween.cb = _cb
        tween.start()

        # act / verify
        self.assertEqual(3 + 2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.assertEqual(0.0, tween.t, "1")

        self.tweener.update(self.duration)
        self.assertEqual(3, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.assertEqual(self.duration, tween.t, "2")

        self.tweener.update(0.3 * self.duration)  # 1.2
        self.assertEqual(2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))
        self.assertEqual(1.3 * self.duration, tween.t, "3")

        self.tweener.update(0.3 * self.duration)  # 1.6
        self.assertEqual(1, len(self.tweener.get_all_tweens(mut.TweenStates.active)), "there should 1 tween be left")
        self.assertEqual(1.6 * self.duration, tween.t, "4")

        self.tweener.update(0.4 * self.duration)  # 2.0
        self.assertEqual(0, len(self.tweener.get_all_tweens(mut.TweenStates.active)), "there should no tween be left")
        self.assertEqual(2.0 * self.duration, tween.t, "5")

        # verify
        self.assertEqual(1, len(cb_args), "callback of parallel should have been called")
        self.assertEqual(2.0 * self.duration, tween.t)

    def test_return_correct_duration(self):
        # arrange
        t1 = self.tweener.create_tween(self, 'x', 0, 100, 2.0 * self.duration, do_start=False)
        t2 = self.tweener.create_tween(self, 'x', 0, 100, 1.5 * self.duration, do_start=False)
        t3 = self.tweener.create_tween(self, 'x', 0, 100, 1.25 * self.duration, do_start=False)

        tween = self.tween1.parallel(self.tween2).parallel(t1, t2, t3)

        # act
        actual = tween.d

        # verify
        self.assertEqual(2.0 * self.duration, actual, "duration should be max of parallel tween")

    def test_that_at_start_all_tweens_are_scheduled(self):
        # arrange
        parallel = self.tween1.parallel(self.tween2)

        # act
        parallel.start()

        # verify
        self.assertEqual(2, len(self.tweener.get_all_tweens(mut.TweenStates.active)))

    def test_finish_without_error_without_callback(self):
        # arrange
        parallel = self.tween1.parallel(self.tween2)
        parallel.start()

        # act
        self.tweener.update(self.duration)

        # verify
        self.assertEqual(0, len(self.tweener.get_all_tweens()))

    def test_run_tweens_to_end_and_restart_them_on_start(self):
        # arrange
        parallel = self.tween1.parallel(self.tween2, self.tween3)
        parallel.start()
        self.tweener.update(self.duration)
        self.tweener.update(self.duration)
        self.assertEqual(mut.TweenStates.ended, self.tween1.state, "tween1 should have ended")
        self.assertEqual(mut.TweenStates.ended, self.tween2.state, "tween2 should have ended")
        self.assertEqual(mut.TweenStates.ended, self.tween3.state, "tween3 should have ended")

        # act
        parallel.start()

        # verify
        self.assertEqual(mut.TweenStates.active, self.tween1.state, "tween1 should have ended")
        self.assertEqual(mut.TweenStates.active, self.tween2.state, "tween2 should have ended")
        self.assertEqual(mut.TweenStates.active, self.tween3.state, "tween3 should have ended")

    def test_run_parallel_repeat(self):
        # arrange
        parallel = self.tween1.parallel(self.tween2, self.tween3)
        tween = parallel.repeat(3)

        # act
        tween.start()
        self.assertEqual(mut.TweenStates.active, self.tween1.state, "tween1 should be active 1")
        self.assertEqual(mut.TweenStates.active, self.tween2.state, "tween2 should be active 1")
        self.assertEqual(mut.TweenStates.active, self.tween3.state, "tween3 should be active 1")

        self.tweener.update(self.duration)
        self.assertEqual(mut.TweenStates.active, self.tween1.state, "tween1 should be active 2")
        self.assertEqual(mut.TweenStates.active, self.tween2.state, "tween2 should be active 2")
        self.assertEqual(mut.TweenStates.active, self.tween3.state, "tween3 should be active 2")

        self.tweener.update(self.duration)
        self.assertEqual(mut.TweenStates.active, self.tween1.state, "tween1 be active 3")
        self.assertEqual(mut.TweenStates.active, self.tween2.state, "tween2 be active 3")
        self.assertEqual(mut.TweenStates.active, self.tween3.state, "tween3 be active 3")

        self.tweener.update(self.duration)

        # verify
        self.assertEqual(mut.TweenStates.ended, self.tween1.state, "tween1 should have ended 4")
        self.assertEqual(mut.TweenStates.ended, self.tween2.state, "tween2 should have ended 4")
        self.assertEqual(mut.TweenStates.ended, self.tween3.state, "tween3 should have ended 4")


class TestTweenerShould(unittest.TestCase):

    def setUp(self):
        self.x = -10
        self.y = -20
        self.z = -30
        self.duration = 10
        self._sut = mut.Tweener()
        self.tween1 = self._sut.create_tween(self, 'x', 10, 100, self.duration, do_start=False)
        self.tween2 = self._sut.create_tween(self, 'y', 20, 100, self.duration, do_start=False)
        self.tween3 = self._sut.create_tween(self, 'z', 30, 100, self.duration, do_start=False)

    def test_create_parallel_tweens(self):
        # arrange

        # act
        actual = self._sut.parallel(self.tween1, self.tween2, self.tween3)

        # verify
        self.assertIsInstance(actual, mut._Parallel)

    def test_create_serial_tweens(self):
        # arrange

        # act
        actual = self._sut.next(self.tween1, self.tween2, self.tween3)

        # verify
        self.assertIsInstance(actual, mut._Serial)


if __name__ == '__main__':
    unittest.main()
