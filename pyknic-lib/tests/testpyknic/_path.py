# # -*- coding: utf-8 -*-
#
# import sys
# import os
#
# # run in right directory
# if not sys.argv[0]:
#     appdir = os.path.abspath(os.path.dirname(__file__))
# else:
#     appdir = os.path.abspath(os.path.dirname(sys.argv[0]))
#
# os.chdir(appdir)
#
#
# appdir = os.path.abspath(os.path.join(appdir, os.pardir, os.pardir))
#
#
# # if not appdir in sys.path:
# sys.path.insert(0, appdir)
#
# test_dir_part = os.path.join("pyknic", "tests")
# test_dirs = [_td for _td in sys.path if _td.find(test_dir_part) != -1]
# if test_dirs:
#     idx = test_dirs[0].find(test_dir_part)
#     test_dir = test_dirs[0][:idx]
#     sys.path.insert(0, test_dir)
#
# if __debug__:
#     import logging
#     logger = logging.getLogger('tests')
#     logger.debug("loading pyknic module for tests from:" + str(appdir))
#
#
