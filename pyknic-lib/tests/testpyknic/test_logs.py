# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.logs module.
"""
from __future__ import print_function, division

import logging
import unittest
import warnings

import pyknic.logs as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")

msg1 = "msg1"


class TestLoggingSetup(unittest.TestCase):
    # noinspection PyUnresolvedReferences
    def setUp(self):
        # hack to store loggers and handlers
        self.orig_loggers = logging.Logger.manager.loggerDict
        logging.Logger.manager.loggerDict = {}
        self.orig_root_handlers = logging.getLogger().handlers
        logging.getLogger().handlers = []
        self.orig_root_log_level = logging.getLogger().level

        # set up
        class RecordingHandler(logging.Handler):
            def __init__(self):
                logging.Handler.__init__(self)
                self.records = []

            def emit(self, record):
                self.records.append(record)

            @property
            def messages(self):
                return [r.msg for r in self.records]

        self.recording_handler_class = RecordingHandler
        self.recording_handler = RecordingHandler()
        self.logger_name = "test_logs"
        self.logger = logging.getLogger(self.logger_name)

    # noinspection PyUnresolvedReferences
    def tearDown(self):
        # hack to restore any loggers and handlers
        logging.Logger.manager.loggerDict = self.orig_loggers
        logging.getLogger().handlers = self.orig_root_handlers
        logging.getLogger().setLevel(self.orig_root_log_level)

    def recording_handler_factory(self, level):
        handler = self.recording_handler
        handler.setLevel(level)
        return handler

    # noinspection PyUnresolvedReferences
    def test_setup_sub_logger(self):
        # arrange
        lc = self.logger
        lc.addHandler(self.recording_handler)
        lc.setLevel(logging.WARNING)

        sub_logger = logging.getLogger(self.logger_name + ".bla")
        sub_logger.setLevel(logging.DEBUG)

        # act
        sub_logger.debug(msg1)

        # verify
        expected_records = [msg1]
        self.assertEqual(expected_records, self.recording_handler.messages)

        # clean up
        del logging.Logger.manager.loggerDict[self.logger_name + ".bla"]

    def test_no_handlers_attached_if_root_logger_is_setup(self):
        # arrange
        root = logging.getLogger()
        root_records = self.recording_handler_class()
        root.addHandler(root_records)

        lc = self.logger
        lc.log_level = logging.WARNING

        sub_logger = logging.getLogger(self.logger_name + ".bla")
        sub_logger.setLevel(logging.DEBUG)

        # act
        sub_logger.debug(msg1)

        # verify
        expected_records = [msg1]
        self.assertEqual(expected_records, root_records.messages, "root handler should record")
        self.assertEqual([], self.recording_handler.messages, "this handler should not have been added to logger")

    class InMemoryStream(object):
        def __init__(self):
            self.lines = []

        def write(self, msg):
            self.lines.append(msg)

    def test_print_loggers_correctly(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        logging.getLogger().setLevel(logging.WARNING)
        a = logging.getLogger('a')
        logging.getLogger('a.a')
        logging.getLogger('a.a.a')
        logging.getLogger('a.a.b')
        logging.getLogger('a.b.a')
        b = logging.getLogger('b')
        ba = logging.getLogger('b.a')
        logging.getLogger('b.e.e')
        logging.getLogger('root')

        a.setLevel(logging.DEBUG)
        b.setLevel(logging.CRITICAL)
        ba.setLevel(logging.ERROR)

        file_mock = self.InMemoryStream()

        # act
        mut.print_logging_hierarchy(file_mock)
        actual = file_mock.lines

        # verify
        universal_linesep = '\n'
        expected = [
            "loggers:" + universal_linesep,
            "    root |1|WARNING|f: [] |h: []" + universal_linesep,
            "        a |1|DEBUG|f: [] |h: []" + universal_linesep,
            "            a.a |1|DEBUG|f: [] |h: []" + universal_linesep,
            "                a.a.a |1|DEBUG|f: [] |h: []" + universal_linesep,
            "                a.a.b |1|DEBUG|f: [] |h: []" + universal_linesep,
            "            a.b.a |1|DEBUG|f: [] |h: []" + universal_linesep,
            "        b |1|CRITICAL|f: [] |h: []" + universal_linesep,
            "            b.a |1|ERROR|f: [] |h: []" + universal_linesep,
            "            b.e.e |1|CRITICAL|f: [] |h: []" + universal_linesep,
            "        test_logs |1|WARNING|f: [] |h: []" + universal_linesep,
            ]
        self.assertEqual(expected, actual)

    def test_print_loggers_with_handlers_and_filters_correctly(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        logging.getLogger().setLevel(logging.WARNING)
        logger = logging.getLogger('a')
        logger.addFilter(logging.Filter('abc'))
        logger.addFilter(logging.Filter('zzz'))
        handler = logging.NullHandler()
        # handler.name = '1'
        handler.addFilter(logging.Filter('zip'))
        logger.addHandler(handler)
        handler2 = logging.NullHandler()
        # handler2.name = '2'
        logger.addHandler(handler2)

        stream = self.InMemoryStream()

        # act
        mut.print_logging_hierarchy(stream)
        actual = stream.lines

        # verify
        universal_linesep = '\n'
        expected = [
            "loggers:" + universal_linesep,
            "    root |1|WARNING|f: [] |h: []" + universal_linesep,
            "        a |1|WARNING|f: [\"Filter('abc')\", \"Filter('zzz')\"] |h: [\"NullHandler('None')\", "
            "\"NullHandler('None')\"]" + universal_linesep,
            "        test_logs |1|WARNING|f: [] |h: []" + universal_linesep,
            "handlers:" + universal_linesep,
            "    NullHandler('None')|NOTSET|f: []",
            "    NullHandler('None')|NOTSET|f: [\"Filter('zip')\"]",
            "filters:" + universal_linesep,
            "    Filter('abc')",
            "    Filter('zip')",
            "    Filter('zzz')",
        ]
        self.assertEqual(expected, actual)

    def test_log_dict_logs_entries(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        dict_to_log = {'a': 1, 'b': 2, 42: 'answer for everything'}
        self.logger.addHandler(self.recording_handler)
        self.logger.setLevel(logging.INFO)
        log_separator_width = 10

        # act
        mut.log_dict(dict_to_log, "test dict", log_separator_width=log_separator_width, logger_to_use=self.logger, )
        actual = self.recording_handler.messages

        from pyknic.compatibility import StringIO
        import pprint
        s = StringIO()
        pprint.pprint(dict_to_log, s)

        # verify
        expected = [
            "-" * log_separator_width,
            "test dict",
            s.getvalue(),
            "-" * log_separator_width,
        ]
        self.assertEqual(expected, actual)

    # def test_logger_parent_tuples_contains_same_named_loggers(self):
    #     # arrange
    #     self.maxDiff = None
    #     self.longMessage = True
    #     logging.getLogger('test_logs')
    #     logging.getLogger('root')
    #     logging.getLogger()
    #
    #     # act / verify
    #     print(list(mut.get_logger_parent_tuples()))
    #     self.assertRaises(Exception, list, mut.get_logger_parent_tuples())  # use the list to materialize the generator

    def test_logger_parent_tuples_contains_no_same_named_loggers_generates_list(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        logging.getLogger('a')
        logging.getLogger('a.a')
        logging.getLogger('a.a.a')
        logging.getLogger('a.a.b')
        logging.getLogger('a.b.a')
        logging.getLogger('b')
        logging.getLogger('b.a')
        logging.getLogger('b.e.e')

        # act
        actual = list(mut.get_logger_parent_tuples())

        # verify
        expected = [
            ('a', 'root'),
            ('a.a', 'a'),
            ('a.a.a', 'a.a'),
            ('a.a.b', 'a.a'),
            ('a.b.a', 'a'),
            ('b', 'root'),
            ('b.a', 'b'),
            ('b.e.e', 'b'),
            ('test_logs', 'root'),
        ]

        expected.sort()
        actual.sort()
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
