# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.resource module.

"""
from __future__ import print_function, division
import unittest
import warnings

import pyknic.resource as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")

class LoaderMock(mut.AbstractResourceLoader):
    def __init__(self):
        mut.AbstractResourceLoader.__init__(self)

    def load(self, *args):
        return 11

    def unload(self, res):
        pass

RES_TYPE = 1

class ResourcesTests(unittest.TestCase):
    def test_something(self):
        # arrange
        sut = mut.Resources()
        loader_mock = LoaderMock()

        # act
        sut.register_loader(RES_TYPE, loader_mock)
        res_id = sut.load(RES_TYPE, "foo", "bar")
        res = sut.get_resource(res_id)

        # verify
        self.assertEqual(11, res)

    def test_register_loader_twice_raises_error(self):
        # arrange
        sut = mut.Resources()
        loader_mock = object()
        resource_type = 1
        sut.register_loader(resource_type, loader_mock)

        # act / verify
        self.assertRaises(mut.ResourceLoaderAlreadyExistsException, sut.register_loader, resource_type, loader_mock)


    def test_resource_loader_raises_if_no_loader_registered(self):
        # arrange
        sut = mut.Resources()

        # act / verify
        self.assertRaises(mut.ResourceLoaderNotFoundException, sut.load, "foo", "bar")

    def test_resource_unload(self):
        # arrange
        sut = mut.Resources()
        resource_loader_mock = LoaderMock()
        sut.register_loader(RES_TYPE, resource_loader_mock)
        id = sut.load(RES_TYPE, "aa")
        res = sut.get_resource(id)
        self.assertIsNotNone(res)

        # act
        sut.unload(id)

        # verify
        self.assertRaises(Exception, sut.get_resource, id)


    def test_get_resource_info(self):
        # arrange
        sut = mut.Resources()
        resource_loader_mock = LoaderMock()
        sut.register_loader(RES_TYPE, resource_loader_mock)
        args = ("**", 1, "booh")
        id = sut.load(RES_TYPE, *args)

        # act
        actual_info = sut.get_resource_info(id)

        # verify
        expected = {sut.RESOURCE_INFO_TYPE_KEY: RES_TYPE, sut.RESOURCE_INFO_ARGS_KEY : args}
        self.assertEqual(expected, actual_info)



if __name__ == '__main__':
    unittest.main()
