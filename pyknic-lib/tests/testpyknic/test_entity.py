# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_entity.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.entity module.
"""
from __future__ import print_function, division
import unittest
import warnings

import pyknic.entity as mut  # module under test
from pyknic.mathematics import Vec2

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class IdentityEntityTests(unittest.TestCase):

    def setUp(self):
        self.sut = self._create_instance_of_sut()

    def _create_instance_of_sut(self, init_id=None):
        return mut.IdentityEntity(init_id=init_id)

    def test_has_id_attribute(self):
        # arrange

        # act / verify
        failure_msg = "{0} should have an 'id' attribute".format(self.sut.__class__.__name__)
        self.assertTrue(hasattr(self.sut, 'id'), failure_msg)

    def test_entities_have_unique_ids(self):
        # arrange
        ent1 = self._create_instance_of_sut()
        ent2 = self._create_instance_of_sut()

        # act / verify
        self.assertNotEqual(ent1.id, ent2.id, "each entity should have a unique id")

    def test_set_id_while_loading_entities(self):
        # arrange
        _id = mut.IdentityEntity._id_generator.last_id + 1  # don't use this in code
        ent = self._create_instance_of_sut(init_id=_id)

        # act
        actual = ent.id

        # verify
        expected = _id
        self.assertEqual(expected, actual, "id of entity should be the one that has been set")

    def test_set_id_does_not_conflict_with_id_generation(self):
        # arrange
        ent0 = self._create_instance_of_sut()
        _id = ent0.id + 1
        ent1 = self._create_instance_of_sut(init_id=_id)
        ent2 = self._create_instance_of_sut()

        # act
        actual = ent2.id

        # verify
        expected = ent1.id
        self.assertLess(expected, actual, "id generation should have accounted for manually set id")


class TypedEntityTests(IdentityEntityTests):

    def setUp(self):
        self.kind = 1
        self.sut = self._create_instance_of_sut()

    def _create_instance_of_sut(self, init_id=None):
        return mut.TypedEntity(self.kind, init_id=init_id)

    def test_that_kind_is_set_correctly(self):
        # arrange
        # act
        actual = self.sut.kind

        # verify
        expected = self.kind
        self.assertEqual(expected, actual, "kind of entity should have been set")


class BaseEntityTests(TypedEntityTests):

    def setUp(self):
        self.kind = 11
        self.position = Vec2(8, 9)
        self.aabb = ('x', 'y', 'w', 'h')
        self.radius = 99
        self.sut = self._create_instance_of_sut()

    def _create_instance_of_sut(self, init_id=None):
        return mut.BaseEntity(self.kind, self.position, self.radius, self.aabb, init_id)

    def test_that_position_is_set_correctly(self):
        # arrange
        # act
        actual = self.sut.position

        # verify
        expected = self.position
        self.assertEqual(expected, actual, "position should have been set")

    def test_that_radius_is_set_correctly(self):
        # arrange
        # act
        actual = self.sut.radius

        # verify
        expected = self.radius
        self.assertEqual(expected, actual, "radius should have been set")

    def test_that_aabb_is_set_correctly(self):
        # arrange
        # act
        actual = self.sut.aabb

        # verify
        expected = self.aabb
        self.assertEqual(expected, actual, "aabb should have been set")


if __name__ == '__main__':
    unittest.main()
