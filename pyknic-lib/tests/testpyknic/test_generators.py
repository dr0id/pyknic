# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.generators module.
"""
from __future__ import print_function, division
import unittest
import warnings

import pyknic.generators as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class GlobalIdGeneratorTests(unittest.TestCase):

    def test_id_are_generated(self):
        # arrange
        sut = mut.GlobalIdGenerator(0)

        # act
        sut.next()
        sut.next()
        next_id = sut.next()

        # verify
        self.assertEqual(3, next_id)

    def test_multiple_instances(self):
        # arrange
        mut.GlobalIdGenerator._id_generator = None  # hack to setup the initial value once!
        other = mut.GlobalIdGenerator(10)
        sut = mut.GlobalIdGenerator(0)

        # act
        other.next()
        next_id = sut.next()

        # verify
        self.assertEqual(12, next_id)


class IdGeneratorTests(unittest.TestCase):

    def setUp(self):
        self.sut = mut.IdGenerator(0)

    def test_id_are_generated(self):
        # arrange
        # act
        self.sut.next()
        self.sut.next()
        next_id = self.sut.next()

        # verify
        self.assertEqual(3, next_id)

    def test_multiple_instances(self):
        # arrange
        other = mut.IdGenerator(10)

        # act
        other.next()
        next_id = self.sut.next()

        # verify
        self.assertEqual(1, next_id)

    def test_current_id_is_increased_correctly(self):
        # arrange
        count = 10
        for i in range(count):
            self.sut.next()

        # act
        actual = self.sut.last_id

        # verify
        expected = count
        self.assertEqual(expected, actual, "current id should be 10")

    def test_current_id_is_changed_if_a_bigger_value_is_set(self):
        # arrange
        for i in range(12):
            self.sut.next()

        # act
        self.sut.set_last_id(13)
        actual = self.sut.next()

        # verify
        expected = 14
        self.assertEqual(expected, actual, "current id should have been increased to set value")

    def test_current_id_jumps_if_a_bigger_value_is_set(self):
        # arrange
        for i in range(12):
            self.sut.next()
        min_id = 333

        # act
        self.sut.set_last_id(min_id)

        # verify
        actual = self.sut.next()
        expected = min_id + 1
        self.assertEqual(expected, actual, "current id should have been increased to set value")


class FlagsGeneratorTests(unittest.TestCase):

    def test_ids_are_generated(self):
        # arrange
        sut = mut.FlagGenerator()

        # act / verify
        self.assertEqual(1 << 0, sut.next())
        self.assertEqual(1 << 1, sut.next())
        self.assertEqual(1 << 2, sut.next())
        self.assertEqual(1 << 3, sut.next())



if __name__ == '__main__':
    unittest.main()
