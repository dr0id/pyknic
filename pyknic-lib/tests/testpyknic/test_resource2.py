# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.resource module.

"""
from __future__ import print_function, division

import unittest
import warnings

import pyknic.resource2 as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class DummyResourceConfig(mut.AbstractResourceConfig):

    def __init__(self, info):
        mut.AbstractResourceConfig.__init__(self)
        self.info = info


class DummyResource(mut.AbstractResource):

    def __init__(self, resource_config):
        mut.AbstractResource.__init__(self, resource_config)


class LoaderMock(mut.AbstractResourceLoader):
    def __init__(self):
        mut.AbstractResourceLoader.__init__(self, DummyResourceConfig)
        self.capture = []

    def load(self, resource_config, file_cache):
        self.capture.append((self.load.__name__, resource_config, file_cache))
        return DummyResource(resource_config)

    def unload(self, res):
        self.capture.append((self.unload.__name__, res))


class ResourceManagerShould(unittest.TestCase):

    def setUp(self):
        self._sut = mut.ResourceManager()

    def test_register_loader_without_error(self):
        # arrange
        loader_mock = LoaderMock()

        # act
        self._sut.register_loader(loader_mock)

        # verify
        self.assertEqual(1, len(self._sut.resource_loaders))
        self.assertEqual(loader_mock, self._sut.resource_loaders[loader_mock.resource_config_type])

    def test_raise_exception_if_loader_is_registered_twice(self):
        # arrange
        loader_mock = LoaderMock()
        self._sut.register_loader(loader_mock)

        # act / verify
        self.assertRaises(mut.ResourceLoaderAlreadyExistsException, self._sut.register_loader, loader_mock)

    def test_raise_exception_if_no_loader_is_registered_when_loading_resource_configuration(self):
        # arrange
        sut = mut.ResourceManager()
        test_config = DummyResourceConfig("abc")

        # act / verify
        self.assertRaises(mut.ResourceLoaderNotFoundException, sut.load_resource_configuration, 1, test_config)

    def test_remove_loader_when_loader_is_unregistered(self):
        # arrange
        loader = LoaderMock()
        self._sut.register_loader(loader)

        # act
        self._sut.unregister_loader(loader.resource_config_type)

        # verify
        self.assertEqual(0, len(self._sut.resource_loaders))

    def test_remove_loader_when_loader_is_unregistered_by_config(self):
        # arrange
        loader = LoaderMock()
        self._sut.register_loader(loader)

        # act
        self._sut.unregister_loader(DummyResourceConfig(33))

        # verify
        self.assertEqual(0, len(self._sut.resource_loaders))

    def test_raise_exception_if_resources_are_loaded_when_loader_is_unregistered(self):
        # arrange
        loader = LoaderMock()
        self._sut.register_loader(loader)
        res_id = 23
        self._sut.load_resource_configuration(res_id, DummyResourceConfig(239))

        # act / verify
        self.assertRaises(mut.ResourcesStillPresentForLoaderException, self._sut.unregister_loader, loader.resource_config_type)

    def test_raise_exception_if_resources_are_loaded_when_loader_is_unregistered2(self):
        # arrange
        loader = LoaderMock()
        self._sut.register_loader(loader)
        res_id = 23
        self._sut.load_resource_configuration(res_id, DummyResourceConfig(123))

        # act / verify
        self.assertRaises(mut.ResourcesStillPresentForLoaderException, self._sut.unregister_loader, DummyResourceConfig)

    def test_raise_exception_if_resources_are_loaded_when_loader_is_unregistered3(self):
        # arrange
        loader = LoaderMock()
        self._sut.register_loader(loader)
        res_id = 23
        the_resource = self._sut.load_resource_configuration(res_id, DummyResourceConfig(123))

        # act / verify
        self.assertRaises(mut.ResourcesStillPresentForLoaderException, self._sut.unregister_loader, the_resource.resource_config)

    def test_return_resource_by_load_resource_configuration(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id = 1

        # act
        the_resource = self._sut.load_resource_configuration(the_res_id, resource_config)

        # verify
        self.assertIsNotNone(the_resource)

    def test_return_resource_in_resources_dictionary_after_load_resource_configuration(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id = 1

        # act
        the_resource = self._sut.load_resource_configuration(the_res_id, resource_config)

        # verify
        self.assertIsNotNone(the_resource)
        self.assertEqual(self._sut.resources[the_res_id], the_resource)

    def test_load_multiple_resources_of_same_resource_configuration_type(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id1 = 1
        the_res_id2 = 2

        # act
        the_resource1 = self._sut.load_resource_configuration(the_res_id1, resource_config)
        the_resource2 = self._sut.load_resource_configuration(the_res_id2, resource_config)

        # verify
        self.assertIsNotNone(the_resource1)
        self.assertIsNotNone(the_resource2)
        self.assertIsNotNone(self._sut.resources[the_res_id1])
        self.assertIsNotNone(self._sut.resources[the_res_id2])
        self.assertEqual(self._sut.resources[the_res_id1], the_resource1)
        self.assertEqual(self._sut.resources[the_res_id2], the_resource2)

    def test_unload_resource(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id = 1
        the_resource = self._sut.load_resource_configuration(the_res_id, resource_config)
        self.assertIsNotNone(the_resource)

        # act
        self._sut.unload_resource(the_resource)

        # verify
        self.assertEqual((resource_loader_mock.unload.__name__, the_resource), resource_loader_mock.capture[1])

    def test_unload_resource_by_id(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id = 1
        the_resource = self._sut.load_resource_configuration(the_res_id, resource_config)
        self.assertIsNotNone(the_resource)

        # act
        self._sut.unload_resource_by_id(the_res_id)

        # verify
        self.assertEqual((resource_loader_mock.unload.__name__, the_resource), resource_loader_mock.capture[1])

    def test_raise_loader_not_found_exception_for_unknown_resource_config_in_unload_resource(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id = 1
        the_resource = self._sut.load_resource_configuration(the_res_id, resource_config)
        self.assertIsNotNone(the_resource)
        the_resource.resource_config = object()  # point to an unknown resource config

        # act / assert
        self.assertRaises(mut.ResourceLoaderNotFoundException, self._sut.unload_resource, the_resource)

    def test_raise_exception_in_get_resource_after_unload_of_resource(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id = 1
        the_resource = self._sut.load_resource_configuration(the_res_id, resource_config)
        self.assertIsNotNone(the_resource)

        self._sut.unload_resource(the_resource)

        # act / verify
        self.assertRaises(Exception, self._sut.get_resource, the_res_id)

    def test_raise_exception_if_same_resource_id_is_loaded_twice(self):
        # arrange
        res_id = 123
        res_config = DummyResourceConfig(43)
        self._sut.register_loader(LoaderMock())
        self._sut.load_resource_configuration(res_id, res_config)

        # act / assert
        self.assertRaises(mut.ResourceIdAlreadyLoadedException, self._sut.load_resource_configuration, res_id, res_config)

    def test_load_resource_configurations(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_config = DummyResourceConfig("aha")
        the_res_id1 = 1
        the_res_id2 = 2
        resource_configurations = {
            the_res_id1: resource_config,
            the_res_id2: resource_config
        }

        # act
        self._sut.load_resources(resource_configurations)

        # verify
        self.assertIsNotNone(self._sut.resources[the_res_id1])
        self.assertIsNotNone(self._sut.resources[the_res_id2])

    def test_call_progress_callback_when_load_resource_configurations_is_executed(self):
        # arrange
        resource_loader_mock = LoaderMock()
        self._sut.register_loader(resource_loader_mock)
        resource_configurations = {
            10: (DummyResourceConfig("aha1")),
            20: (DummyResourceConfig("aha2")),
            30: (DummyResourceConfig("aha3")),
            40: (DummyResourceConfig("aha4")),
            50: (DummyResourceConfig("aha5")),
            60: (DummyResourceConfig("aha6")),
            70: (DummyResourceConfig("aha7")),
            80: (DummyResourceConfig("aha8")),
            90: (DummyResourceConfig("aha9")),
            100: (DummyResourceConfig("aha10")),
        }
        
        progress_capture = []
        def progress_callback(*args):
            progress_capture.append(args)
            
        # act
        self._sut.load_resources(resource_configurations, progress_callback)

        # verify
        self.assertEqual(10, len(progress_capture))
        self.assertEqual(1.0, progress_capture[9][2], "last progress trigger should be 100 percent")

        for i in range(len(progress_capture)):
            self.assertEqual(i + 1, progress_capture[i][0], "progress trigger current should show " + str(i+1))

        # assert that all res ids have been loaded once and passed to the progress callback
        actual_res_ids = [_res_id for _,_,_,_res_id,_ in progress_capture]
        actual_res_ids.sort()
        expected_res_ids = list(range(10, 110, 10))
        self.assertSequenceEqual(expected_res_ids, actual_res_ids)

        # assert that total is always the same in the progress callback
        for i in range(len(progress_capture)):
            self.assertEqual(10, progress_capture[i][1], "total should be 10 in all progress triggers, failed at " + str(i))


if __name__ == '__main__':
    unittest.main()
