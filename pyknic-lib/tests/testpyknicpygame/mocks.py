# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'mocks.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["PygameMock"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

logger.debug("imported")


class _RectMock(object):
    def __init__(self, x, y, w=None, h=None, parent=None):
        self._x = x
        self._y = y
        self._w = w
        self._h = h
        if w is None:
            _x, _y = x
            _w, _h = y
            self._x = _x
            self._y = _y
            self._w = _w
            self._h = _h
        self.capture = [] if parent is None else parent.capture

    def move(self, dx, dy):
        self.capture.append(("move", (dx, dy)))
        return _RectMock(self._x + dx, self._y + dy, self._w, self._h, parent=self)

    @property
    def size(self):
        self.capture.append(("size",))
        return self._w, self._h

    @property
    def width(self):
        self.capture.append(("width",))
        return self._w

    @property
    def height(self):
        self.capture.append(("height",))
        return self._h


class PygameMock(object):

    SRCCOLORKEY= 0x1000
    SRCALPHA= 0x10000

    def __init__(self, parent=None):
        self.capture = [] if parent is None else parent.capture
        self._image_module = PygameImageModuleMock(self)

    @property
    def image(self):
        self.capture.append(("pygame.image", ))
        return self._image_module

    def Rect(self, x, y, w=None, h=None):
        self.capture.append(("Rect", (x, y, w, h)))
        return _RectMock(x, y, w, h, self)

    def Surface(self, size, flags=0, depth=0, **kwargs): # todo pass args!
        self.capture.append(("Surface", (size, flags, depth), kwargs))
        return _SurfaceMock(size, flags, depth, parent=self)


class _SurfaceMock(object):

    def __init__(self, size=(10, 10), flags=0, depth=0, parent=None):
        self._depth = depth
        self._flags = flags
        self._size = size
        self.capture = [] if parent is None else parent.capture
        self._color_key = None

    def blit(self, *args, **kwargs):
        self.capture.append(("blit", args, kwargs))

    def get_flags(self, *args):
        self.capture.append(("get_flags", args))
        return self._flags

    def convert_alpha(self, *args):
        self.capture.append(("convert_alpha", args))
        return _SurfaceMock(self._size, self._flags | PygameMock.SRCALPHA, self._depth, parent=self)

    def get_size(self):
        self.capture.append(("get_size", None))
        return self._size

    def copy(self):
        self.capture.append(("copy", None))
        return _SurfaceMock(self._size, self._flags, self._depth, parent=self)

    def set_colorkey(self, color, flags=0):
        self.capture.append(("set_colorkey", (color, flags)))
        self._color_key = color
        self._flags |= PygameMock.SRCCOLORKEY | flags

    def get_colorkey(self):
        return self._color_key


class PygameImageModuleMock(object):

    def __init__(self, parent=None):
        self.capture = [] if parent is None else parent.capture

    def load(self, *args):
        self.capture.append(("load", args))
        return _SurfaceMock(parent=self)
