# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_eventmapper.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.pyknic_python.resource module.
"""
from __future__ import print_function, division

import unittest
import warnings

import logging
import pygame

import pyknic.pyknic_pygame.eventmapper as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class EventMapperTests(unittest.TestCase):
    def test_quit_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.QUIT: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        events = [pygame.event.Event(pygame.QUIT, {})]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_mouse_motion_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEMOTION: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'rel': (3, 4), 'buttons': [1, 1, 1]}
        events = [pygame.event.Event(pygame.MOUSEMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        expected_extras = (values['pos'], values['rel'], values['buttons'])
        self.assertEqual([(ACTION, expected_extras)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_mouse_motion_event_with_buttons(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEMOTION: {(1, 1, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'rel': (3, 4), 'buttons': [1, 1, 1]}
        events = [pygame.event.Event(pygame.MOUSEMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        expected_extras = (values['pos'], values['rel'], values['buttons'])
        self.assertEqual([(ACTION, expected_extras)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_mouse_motion_event_with_buttons_no_action_for_wrong_buttons(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEMOTION: {(1, 0, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'rel': (3, 4), 'buttons': [1, 1, 1]}
        events = [pygame.event.Event(pygame.MOUSEMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(events, actual[1], "all events should be in unmapped")

    def test_mouse_button_up_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEBUTTONUP: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'button': 1}
        events = [pygame.event.Event(pygame.MOUSEBUTTONUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, (values['pos']))], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_mouse_button_up_event_no_action_for_other_button_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEBUTTONUP: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'button': 2}
        events = [pygame.event.Event(pygame.MOUSEBUTTONUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(events, actual[1], "all events should be in unmapped")

    def test_mouse_button_down_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEBUTTONDOWN: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'button': 1}
        events = [pygame.event.Event(pygame.MOUSEBUTTONDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, (values['pos']))], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_mouse_button_down_event_no_action_for_other_button_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.MOUSEBUTTONDOWN: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'button': 3}
        events = [pygame.event.Event(pygame.MOUSEBUTTONDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(events, actual[1], "all events should be in unmapped")

    def test_active_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.ACTIVEEVENT: {(1, 0): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'state': 1, 'gain': 0}
        events = [pygame.event.Event(pygame.ACTIVEEVENT, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_video_resize_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.VIDEORESIZE: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'size': (200, 100), 'w': 200, 'h': 100}
        events = [pygame.event.Event(pygame.VIDEORESIZE, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['size'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_video_exposure_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.VIDEOEXPOSE: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {}
        events = [pygame.event.Event(pygame.VIDEOEXPOSE, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_user_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.USEREVENT: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'a': 1, 'b': 2}
        events = [pygame.event.Event(pygame.USEREVENT, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, {'a': values['a'], 'b': values['b']})], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_scancode_any_key(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {mut.ANY_KEY: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "", 'key': pygame.K_F1, 'mod': mut.NO_MOD, 'scancode': 59}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_scancode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {33: ACTION, 59: ACTION, }, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "", 'key': pygame.K_F1, 'mod': mut.NO_MOD, 'scancode': 59}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_scancode_no_action_for_wrong_scancode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "", 'key': pygame.K_F1, 'mod': mut.NO_MOD, 'scancode': 59}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_up_event_scancode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {99: ACTION, 59: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_F1, 'mod': mut.NO_MOD, 'scancode': 59}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_scancode_any_key(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {mut.ANY_KEY: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_F1, 'mod': mut.NO_MOD, 'scancode': 59}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_scancode_no_action_for_wrong_scancode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {9999: ACTION, 1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "", 'key': pygame.K_F1, 'mod': mut.NO_MOD, 'scancode': 59}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_down_event_any_key_any_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(mut.ANY_KEY, mut.ANY_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': mut.NO_MOD, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        actual_actions, unmapped_events = actual
        self.assertEqual([(ACTION, values['unicode'])], actual_actions)
        self.assertEqual([], unmapped_events, "unmapped should be empty")

    def test_key_down_event_no_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.K_a, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': mut.NO_MOD, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_no_mod_no_action_for_event_with_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.K_a, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_CTRL, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(events, actual[1], "all events should be in unmapped")

    def test_key_down_event_with_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.K_a, pygame.KMOD_ALT): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_with_mod_no_action_for_event_without_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.K_a, pygame.KMOD_ALT): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_NONE, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_down_event_mod_key_only(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.KMOD_ALT, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.KMOD_ALT, 'mod': pygame.KMOD_NONE, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_mod_key_only_no_action_for_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.KMOD_ALT, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.KMOD_ALT, 'mod': pygame.KMOD_CAPS, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_down_event_map_any_key_any_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(mut.ANY_KEY, mut.ANY_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_map_any_key_specific_mode_no_action_for_different_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(mut.ANY_KEY, pygame.KMOD_META): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_down_event_map_any_key_specific_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(mut.ANY_KEY, pygame.KMOD_META): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_META, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_down_event_map_any_key_specific_mode2(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(mut.ANY_KEY, pygame.KMOD_META): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_NONE, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_down_event_map_no_key_no_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(mut.NO_KEY, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        events = [
            pygame.event.Event(pygame.KEYDOWN,
                               {'unicode': "a", 'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}),
            pygame.event.Event(pygame.KEYDOWN,
                               {'unicode': "a", 'key': mut.NO_KEY, 'mod': pygame.KMOD_LSHIFT, 'scancode': 4556}),
            pygame.event.Event(pygame.KEYDOWN,
                               {'unicode': "a", 'key': pygame.K_a, 'mod': mut.NO_MOD, 'scancode': 4556}),
            pygame.event.Event(pygame.KEYDOWN,
                               {'unicode': "a", 'key': mut.ANY_KEY, 'mod': mut.ANY_MOD, 'scancode': 4556}),
        ]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_down_event_key_with_any_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYDOWN: {(pygame.K_b, mut.ANY_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'unicode': "a", 'key': pygame.K_b, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['unicode'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_no_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.K_a, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': mut.NO_MOD, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_any_mod_no_mod_pressed(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.K_RETURN, mut.ANY_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_RETURN, 'mod': pygame.KMOD_NONE, 'scancode': 28}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_no_mod_no_action_for_event_with_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.K_a, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_CTRL, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_up_event_with_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.K_a, pygame.KMOD_ALT): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_with_mod_no_action_for_event_without_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.K_a, pygame.KMOD_ALT): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_NONE, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_up_event_mod_key_only(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.KMOD_ALT, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.KMOD_ALT, 'mod': pygame.KMOD_NONE, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_mod_key_only_no_action_for_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.KMOD_ALT, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.KMOD_ALT, 'mod': pygame.KMOD_CAPS, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_key_up_event_map_any_key_any_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(mut.ANY_KEY, mut.ANY_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual(1, len(actual[0]))
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_map_any_key_specific_mode_no_action_for_different_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(mut.ANY_KEY, pygame.KMOD_META): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(events, actual[1], "all events should be in unmapped")

    def test_key_up_event_map_any_key_specific_mode(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(mut.ANY_KEY, pygame.KMOD_META): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_META, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual(1, len(actual[0]))
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_key_up_event_map_any_key_specific_mode2(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(mut.ANY_KEY, pygame.KMOD_META): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_a, 'mod': pygame.KMOD_NONE, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(events, actual[1], "all events should be in unmapped")

    def test_key_up_event_map_no_key_no_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(mut.NO_KEY, mut.NO_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        events = [
            pygame.event.Event(pygame.KEYUP,
                               {'key': pygame.K_a, 'mod': pygame.KMOD_ALT, 'scancode': 4556}),
            pygame.event.Event(pygame.KEYUP,
                               {'key': mut.NO_KEY, 'mod': pygame.KMOD_LSHIFT, 'scancode': 4556}),
            pygame.event.Event(pygame.KEYUP,
                               {'key': pygame.K_a, 'mod': mut.NO_MOD, 'scancode': 4556}),
            pygame.event.Event(pygame.KEYUP,
                               {'key': mut.ANY_KEY, 'mod': mut.ANY_MOD, 'scancode': 4556}),
        ]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all event should be unmapped")

    def test_key_up_event_key_with_any_mod(self):
        # arrange
        ACTION = 34
        event_map = {pygame.KEYUP: {(pygame.K_b, mut.ANY_MOD): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'key': pygame.K_b, 'mod': pygame.KMOD_ALT, 'scancode': 4556}
        events = [pygame.event.Event(pygame.KEYUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_joy_button_down_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYBUTTONDOWN: {(0, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'button': 1}
        events = [pygame.event.Event(pygame.JOYBUTTONDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_joy_button_down_event_no_action_for_different_button(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYBUTTONDOWN: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'button': 3}
        events = [pygame.event.Event(pygame.JOYBUTTONDOWN, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])

    def test_joy_button_up_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYBUTTONUP: {(0, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'button': 1}
        events = [pygame.event.Event(pygame.JOYBUTTONUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, None)], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_joy_button_up_event_no_action_for_different_button(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYBUTTONUP: {1: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'button': 3}
        events = [pygame.event.Event(pygame.JOYBUTTONUP, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_joy_hat_motion_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYHATMOTION: {(0, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'hat': 1, 'value': 345}
        events = [pygame.event.Event(pygame.JOYHATMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['value'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_joy_hat_motion_event_no_action_for_other_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYHATMOTION: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'rel': (3, 4), 'buttons': [1, 1, 1]}
        events = [pygame.event.Event(pygame.MOUSEMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_joy_ball_motion_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYBALLMOTION: {(0, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'ball': 1, 'rel': 345}
        events = [pygame.event.Event(pygame.JOYBALLMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['rel'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_joy_ball_motion_event_no_action_for_other_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYBALLMOTION: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'pos': (1, 2), 'rel': (3, 4), 'buttons': [1, 1, 1]}
        events = [pygame.event.Event(pygame.MOUSEMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_joy_axis_motion_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYAXISMOTION: {(0, 1): ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'axis': 1, 'value': 345}
        events = [pygame.event.Event(pygame.JOYAXISMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([(ACTION, values['value'])], actual[0])
        self.assertEqual([], actual[1], "unmapped should be empty")

    def test_joy_axis_motion_event_no_action_for_other_event(self):
        # arrange
        ACTION = 34
        event_map = {pygame.JOYAXISMOTION: {None: ACTION}, }
        self.mapper.set_event_map(event_map)

        values = {'joy': 0, 'axis': 1, 'rel': 345}
        events = [pygame.event.Event(pygame.MOUSEMOTION, values)]

        # act
        actual = self.mapper.get_actions(events)

        # verify
        self.assertEqual([], actual[0])
        self.assertEqual(len(events), len(actual[1]), "all events should be in unmapped")

    def test_missing_event_in_map_is_logged_once(self):
        # arrange
        handler = _InMemoryHandler(logging.DEBUG)
        mut.logger.addHandler(handler)
        mut.logger.setLevel(logging.DEBUG)

        event_map = {pygame.QUIT: {None, 23}}
        self.mapper.set_event_map(event_map)

        events = [pygame.event.Event(pygame.MOUSEMOTION, {}), pygame.event.Event(pygame.MOUSEMOTION, {})]

        # act
        self.mapper.get_actions(events)

        # verify
        self.assertEqual(1, len(handler.messages))

    def test_missing_action_in_event_map_is_logged_once(self):
        # arrange
        handler = _InMemoryHandler(logging.DEBUG)
        mut.logger.addHandler(handler)
        mut.logger.setLevel(logging.DEBUG)

        event_map = {pygame.KEYDOWN: {(pygame.K_a, mut.ANY_MOD): 22}, }
        self.mapper.set_event_map(event_map)

        events = [
            pygame.event.Event(pygame.KEYDOWN, {
                'key': pygame.K_b, 'mod': pygame.KMOD_NONE, 'scancode': 33, 'unicode': "@"}),
            pygame.event.Event(pygame.KEYDOWN, {
                'key': pygame.K_c, 'mod': pygame.KMOD_NONE, 'scancode': 33, 'unicode': "&"}),
        ]

        # act
        self.mapper.get_actions(events)

        # verify
        self.assertEqual(1, len(handler.messages))

    def setUp(self):
        self.mapper = mut.EventMapper()

    def test_get_actions_keys_for_event(self):
        # arrange
        event_map = {
            pygame.KEYDOWN: {
                (pygame.K_a, mut.NO_MOD): 1,
                (pygame.K_q, mut.NO_MOD): 2,
                (pygame.K_F4, pygame.KMOD_ALT): 99,
            },
            pygame.MOUSEBUTTONDOWN: {
                2: 10,
                3: 11,
            },
            pygame.QUIT: {
                None: 99
            }
        }

        self.mapper.set_event_map(event_map)

        # act
        actual = self.mapper.get_mapped_event_types()

        # verify
        expected = list(event_map.keys())
        self.assertEqual(expected, actual)

    def test_get_actions_returns_also_unhandled_events(self):
        # arrange
        event_map = {pygame.QUIT: {None: 99}}

        self.mapper.set_event_map(event_map)

        events = [
            pygame.event.Event(pygame.ACTIVEEVENT, {}),
            pygame.event.Event(pygame.VIDEORESIZE, {}),
        ]

        # act
        actions, unhandled_events = self.mapper.get_actions(events)

        # verify
        expected = events
        self.assertEqual([], actions, "actions should be empty")
        self.assertEqual(expected, unhandled_events)

    def test_get_actions_returns_also_unhandled_actions(self):
        # arrange
        event_map = {pygame.QUIT: {23: 99}}

        self.mapper.set_event_map(event_map)

        events = [
            pygame.event.Event(pygame.QUIT, {})
        ]

        # act
        actions, unhandled_events = self.mapper.get_actions(events)

        # verify
        expected = events
        self.assertEqual([], actions, "actions should be empty")
        self.assertEqual(expected, unhandled_events)

    def test_get_actions_returns_also_unhandled_actions_for_key_up_and_down(self):
        # arrange
        event_map = {
            pygame.KEYDOWN: {23: 99},
            pygame.KEYUP: {23: 99},
        }

        self.mapper.set_event_map(event_map)

        events = [
            pygame.event.Event(pygame.KEYDOWN, {'scancode': 0, 'unicode': '*', 'key': 34, 'mod': mut.NO_MOD}),
            pygame.event.Event(pygame.KEYUP, {'scancode': 0, 'key': 33, 'mod': mut.NO_MOD})
        ]

        # act
        actions, unhandled_events = self.mapper.get_actions(events)

        # verify
        expected = events
        self.assertEqual([], actions, "actions should be empty")
        self.assertEqual(expected, unhandled_events)


class _InMemoryHandler(logging.Handler):
    def __init__(self, loglevel):
        logging.Handler.__init__(self, loglevel)
        self.messages = []

    def emit(self, record):
        self.messages.append(record)


class EventMapValidationTests(unittest.TestCase):

    def setUp(self):
        self.mapper = mut.EventMapper()

    def set_event_map(self, event_map):
        self.mapper.set_event_map(event_map)

    def test_empty_map_raises_validation_error(self):
        # arrange
        event_map = {}

        # act / verify
        self.assertRaises(mut.EventMapValidationError, self.set_event_map, event_map)

    def test_empty_action_map_raises_validation_error(self):
        # arrange
        event_map = {pygame.QUIT: {}}

        # act / verify
        self.assertRaises(mut.EventMapValidationError, self.set_event_map, event_map)


class EventMapValidationInContstructorTests(EventMapValidationTests):

    def set_event_map(self, event_map):
        self.mapper = mut.EventMapper(event_map)


if __name__ == '__main__':
    unittest.main()
