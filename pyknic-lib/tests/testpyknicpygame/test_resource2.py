# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_resource2.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import unittest
import warnings

import pyknic.pyknic_pygame.resource2 as mut  # module under test
from testpyknicpygame import mocks
from testpyknicpygame.mocks import PygameMock

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# if mut.__version_info__ != __version_info__:
#     raise Exception("version numbers do not match!")


class PygameSpriteSheetLoaderShouldTests(unittest.TestCase):

    def setUp(self):
        self._pygame_mock = PygameMock()
        self.sut = mut.SpriteSheetLoader(self._pygame_mock)
        self._spritesheet = mut.SpriteSheetConfig("img.png", (100, 100), 10, 10, 20)
        self._file_cache = {}

    def test_cache_loaded_image_in_file_cache(self):
        # act
        self.sut.load(self._spritesheet, self._file_cache)

        # verify
        self.assertTrue(self._spritesheet.path_to_file in self._file_cache,
                        f"should cache original file, cache: {','.join(self._file_cache.keys())}")

    def test_cache_transformed_image_in_file_cache(self):
        # arrange
        self._spritesheet.transformation = mut.AlphaTransform()

        # act
        self.sut.load(self._spritesheet, self._file_cache)

        # verify
        self.assertTrue((self._spritesheet.path_to_file, self._spritesheet.transformation.key) in self._file_cache)

    def test_not_load_image_again_if_already_in_cache(self):
        # arrange
        self._file_cache[self._spritesheet.path_to_file] = mocks.PygameMock().Surface((100, 100))

        # act
        self.sut.load(self._spritesheet, self._file_cache)

        # verify
        self.assertFalse(('load', (self._spritesheet.path_to_file,)) in self._pygame_mock.capture)

    def test_not_load_transformed_image_again_if_already_in_cache(self):
        # arrange
        self._file_cache[self._spritesheet.path_to_file] = mocks.PygameMock().Surface((100, 100))
        transform = mut.AlphaTransform()
        self._spritesheet.transformation = transform
        self._file_cache[
            (self._spritesheet.transformation.key, self._spritesheet.path_to_file)] = mocks.PygameMock().Surface(
            (100, 100))

        # act
        self.sut.load(self._spritesheet, self._file_cache)

        # verify
        self.assertFalse(('load', ((transform.key, self._spritesheet.path_to_file),)) in self._pygame_mock.capture)

    def test_return_multiple_images(self):
        # act
        actual = self.sut.load(self._spritesheet, self._file_cache)

        # verify
        self.assertIsInstance(actual, mut.SpriteSheetResource)

    def test_return_correct_image_count(self):
        # arrange
        row_count = self._spritesheet.row_count
        col_count = self._spritesheet.column_count

        # act
        actual = self.sut.load(self._spritesheet, self._file_cache)

        # verify
        expected = row_count * col_count
        self.assertEqual(expected, len(actual.data))

    def test_return_images_sized_as_defined(self):
        # act
        actual = self.sut.load(self._spritesheet, self._file_cache)

        # verify
        expected = self._spritesheet.tile_size
        for idx, img in enumerate(actual.data):
            self.assertEqual(expected, img.get_size(), "image size wrong at index " + str(idx))
            self.assertEqual(0, img.get_flags() & self._pygame_mock.SRCALPHA)

    def test_return_images_alpha_converted(self):
        # act
        self._spritesheet.transformation = mut.AlphaTransform()
        actual = self.sut.load(self._spritesheet, self._file_cache)

        # verify
        for idx, img in enumerate(actual.data):
            self.assertEqual(self._pygame_mock.SRCALPHA, img.get_flags() & self._pygame_mock.SRCALPHA)

    def test_return_images_with_colorkey(self):
        # arrange
        self._spritesheet.transformation = mut.ColorKeyTransform((1, 2, 3))

        # act
        actual = self.sut.load(self._spritesheet, self._file_cache)

        # verify
        expected = self._pygame_mock.SRCCOLORKEY
        for idx, img in enumerate(actual.data):
            self.assertEqual(expected, img.get_flags() & self._pygame_mock.SRCCOLORKEY)


class AbstractTransformationShould(unittest.TestCase):

    def __init__(self, *args):
        unittest.TestCase.__init__(self, *args)

    def setUp(self):
        self.sut = self.create_another()

    def test_compare_equal(self):
        # act / verify
        expected = self.create_another()
        self.assertEqual(expected, self.sut)

    def test_hash_equal(self):
        # act
        actual = self.sut

        # act / verify
        expected = self.create_another()
        self.assertEqual(hash(expected), hash(actual))

    def test_not_compare_equal_for_different_parameters(self):
        # act
        actual = self.create_another()

        # verify
        expected = self.create_different()
        self.assertNotEqual(expected, actual)
        self.assertTrue(expected != actual)
        self.assertFalse(expected == actual)
        self.assertNotEqual(actual, expected)
        self.assertTrue(actual != expected)
        self.assertFalse(actual == expected)

    def test_not_have_same_hash_for_different_parameters(self):
        # act
        actual = self.create_different()

        # verify
        expected = self.create_another()
        self.assertNotEqual(hash(expected), hash(actual))
        self.assertTrue(hash(expected) != hash(actual))
        self.assertFalse(hash(expected) == hash(actual))

    def test_not_compare_equal_with_something_else(self):
        # act
        actual = self.create_another()

        # verify
        expected = self._create_something_else()
        self.assertTrue(actual != expected)
        self.assertTrue(expected != actual)
        self.assertFalse(expected == actual)
        self.assertFalse(actual == expected)
        self.assertNotEqual(expected, actual)
        self.assertNotEqual(actual, expected)

    def create_another(self):
        return None

    def create_different(self):
        return 0

    @staticmethod
    def _create_something_else():
        return object()


class AlphaTransformationShould(AbstractTransformationShould):

    def __init__(self, *args):
        AbstractTransformationShould.__init__(self, *args)

    def create_another(self):
        return mut.AlphaTransform()

    def create_different(self):
        return None


class ColorkeyTransformationShould(AbstractTransformationShould):

    def create_another(self):
        return mut.ColorKeyTransform((1, 2, 3))

    def create_different(self):
        return mut.ColorKeyTransform((100, 99, 0), 5)


class ColorkeyTransformationWithFlagsShould(AbstractTransformationShould):

    def create_another(self):
        return mut.ColorKeyTransform((1, 2, 3), 45)

    def create_different(self):
        return mut.ColorKeyTransform((1, 2, 3), 33)


if __name__ == '__main__':
    unittest.main()
