# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Tests for the pyknic.pyknic_python.resource module.
"""
from __future__ import print_function, division

import os

from pyknic.pyknic_pygame import audio

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import unittest
from pyknic.pyknic_pygame import resource as mut

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class MyTestCase(unittest.TestCase):
    def test_something(self):
        # arrange
        sut = mut.resources
        foo_id = sut.load(mut.SOUND, "foo.ogg", 0.8)
        audio_system = audio.SoundSystem(sut)

        # act
        audio_system.play_sound(foo_id)

        # verify
        self.assertTrue(isinstance(sut.get_resource(foo_id), mut.SoundLoader._DummySound))


class LoaderMock(object):

    def __init__(self, returns=None, loader_id="gfx"):
        self.capture = []
        self.returns = [] if returns is None else returns
        self._returns = list(self.returns)
        self.id = loader_id
        self.config_place_holder = False
        self.config_specific_attribute = False
        self.cb = None

    def load(self, args, load_cached_cb):
        self.capture.append((args, load_cached_cb))
        # self.capture.append(copy.deepcopy(args))
        if self.cb:
            return self.cb(args, load_cached_cb)
        return self._returns.pop(0)


class ResourceLoaderShould(unittest.TestCase):

    def setUp(self):
        # self.resources_definition = {
        #     "config_base_dir": ".",
        #     "config_place_holder": True,
        #     "gfx": {
        #         1: dict(name="foo.png"),
        #         1.1: dict(name="foo.png"),  # todo: should this load the same image twice from the disk? No!
        #         2: dict(name="bar.png", angle=45),
        #         2.1: dict(name="bar.png", angle=30),
        #         "config_place_holder": True,  # optional
        #         "config_place_holder_width": 100,  # optional
        #         "config_place_holder_height": 50,  # optional
        #         "config_place_holder_color_rgba": (255, 255, 0, 255),  # optional
        #         "config_place_holder_ppu": 1.0,  # optional
        #     },
        #     "sfx": {
        #         3: dict(name="foo.wav", volume=1.0),
        #         4: dict(name='building_up.ogg', volume=1.0, reserved_channel_id=1),
        #         5: dict(name='building_down.ogg', volume=0.8, reserved_channel_id=3),
        #         "config_place_holder": True,  # optional
        #         "config_place_holder_volume": 0.5,  # optional
        #     },
        #     "music": {
        #         6: dict(name='Wolf Arm - The Singularity.ogg', volume=1.0),
        #         7: dict(name='Retrology - Hyperspace.ogg', volume=0.5),
        #         "config_place_holder": True,  # optional
        #         "config_place_holder_volume": 0.5,  # optional
        #     },
        #     "font": {
        #         8: dict(name="super.ttf", id=None, ),
        #         "config_place_holder": True,  # optional
        #     },
        # }
        # self.resource_ids = [k for v in self.resources_definition.values() if isinstance(v, dict) for k in v.keys()]

        self.sut = mut.ResourceLoader()

    def test_register_loader(self):
        # arrange
        loader = LoaderMock([1, 2, 3])

        # act
        self.sut.register(loader)

        # verify
        expected = [loader.id]
        self.assertEqual(expected, list(self.sut.types))

    def test_return_empty_resource_dict_if_no_loaders_have_been_registered(self):
        # arrange
        resource_definition = {}

        # act
        actual = self.sut.load(resource_definition)

        # verify
        expected = {}
        self.assertEqual(expected, actual)

    def test_return_empty_resource_dict_for_empty_definition(self):
        # arrange
        resource_definition = {}
        loader = LoaderMock([100, 300])
        self.sut.register(loader)

        # act
        actual = self.sut.load(resource_definition)

        # verify
        expected = {}
        self.assertEqual(expected, actual)

    def test_raise_exception_if_id_is_not_found(self):
        # arrange
        loader = LoaderMock(loader_id="other")
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="foo.png"),
                2: dict(name="bar.png", angle=45),
            },
        }

        # act / verify
        self.assertRaises(mut.LoaderNotFoundException, self.sut.load, self.resources_definition)

    def test_raise_exception_if_is_registered_twice(self):
        # arrange
        loader = LoaderMock()
        self.sut.register(loader)

        # act / verify
        self.assertRaises(mut.LoaderAlreadyRegisteredException, self.sut.register, loader)

    def test_should_only_use_the_correct_loader(self):
        # arrange
        loader = LoaderMock(returns=[100, 200])
        self.sut.register(loader)
        other_loader = LoaderMock(loader_id="other")
        self.sut.register(other_loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="foo.png"),
                2: dict(name="bar.png", angle=45),
            },
        }

        # act
        self.sut.load(self.resources_definition)

        # verify
        self.assertEqual(0, len(other_loader.capture))

    def test_return_image_resource_in_load_result(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="foo.png"),
                2: dict(name="bar.png", angle=45),
            },
        }

        # act
        actual = self.sut.load(self.resources_definition)

        # verify
        expected_keys = (1, 2)
        for idx, k in enumerate(expected_keys):
            self.assertTrue(k in actual.keys(), "expected key {0} not found in {1}".format(k, actual.keys()))
            self.assertEqual(loader.returns[idx], actual[k])
        self.assertEqual(2, len(loader.capture))

    def test_respect_absolute_base_path_value(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        abs_base_dir = "/abc"
        self.resources_definition = {
            "gfx": {
                1: dict(name="foo.png"),
            },
            "config_base_dir": abs_base_dir,
        }

        # act
        self.sut.load(self.resources_definition)

        # verify
        expected_base_dir = os.path.abspath(abs_base_dir)
        expected = [({'name': os.path.join(expected_base_dir, 'foo.png')}, self.sut._load_cached)]
        self.assertEqual(expected, loader.capture)

    def test_respect_relative_base_path_value(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        base_dir = "../abc"
        self.resources_definition = {
            "gfx": {
                1: dict(name="./fooDir/foo.png"),
            },
            "config_base_dir": base_dir,
        }

        # act
        self.sut.load(self.resources_definition)

        # verify
        expected_base_dir = os.path.normpath(os.path.join(os.getcwd(), base_dir))
        expected = [
            ({'name': os.path.normpath(os.path.join(expected_base_dir, 'fooDir/foo.png'))}, self.sut._load_cached)]
        self.assertEqual(expected, loader.capture)

    def test_raises_abs_path_not_supported_exception(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        base_dir = "../abc"
        self.resources_definition = {
            "gfx": {
                1: dict(name="/foo.png"),  # absolute path should not be allowed
            },
            "config_base_dir": base_dir,
        }

        # act / verify
        self.assertRaises(mut.ResourceAbsolutePathNotAllowedException, self.sut.load, self.resources_definition)

    def test_raises_unknown_config_attribute(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
            },
            "config_non_existing": "../abc",
        }

        # act / verify
        self.assertRaises(mut.UnknownConfigAttributeException, self.sut.load, self.resources_definition)

    def test_set_global_config_place_holder_to_loader(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
            },
            "config_place_holder": True,
        }

        # act
        self.sut.load(self.resources_definition)

        # verify
        self.assertTrue(loader.config_place_holder)

    def test_specific_config_place_holder_overrides_global_value(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
                "config_place_holder": True
            },
            "config_place_holder": False,
        }

        # act
        self.sut.load(self.resources_definition)

        # verify
        self.assertTrue(loader.config_place_holder)

    def test_specific_config_attribute_is_set_on_loader(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
                "config_specific_attribute": 123
            }
        }

        # act
        self.sut.load(self.resources_definition)

        # verify
        self.assertEqual(123, loader.config_specific_attribute)

    def test_raise_exception_for_unknown_specific_config_attribute_on_loader(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
                "config_unknown": 123
            }
        }

        # act
        self.assertRaises(mut.UnknownConfigAttributeException, self.sut.load, self.resources_definition)

    def test_raise_exception_for_double_resource_ids(self):
        # arrange
        loader = LoaderMock([100, 200])
        self.sut.register(loader)
        other = LoaderMock(loader_id='other')
        self.sut.register(other)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
            },
            "other": {
                1: dict(name="./bar.png"),
            }

        }

        # act
        self.assertRaises(mut.DuplicateResourceIdException, self.sut.load, self.resources_definition)

    def test_load_same_resource_only_once(self):
        # arrange
        _res = object()
        loader = LoaderMock([_res, 100])
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
                2: dict(name="./foo.png"),
            }
        }

        # act
        actual = self.sut.load(self.resources_definition)

        # verify
        self.assertEqual(2, len(actual))
        self.assertEqual(actual[1], actual[2])
        expected_capture = [({'name': os.path.abspath(
            os.path.join(os.curdir, self.resources_definition[loader.id][1]['name']))}, self.sut._load_cached)]
        self.assertEqual(expected_capture, loader.capture)

    def test_loader_loads_resource_recursively(self):
        # arrange
        _res = object()
        loader = LoaderMock([_res, 100])

        def f(args, load_cached):
            load_cached(args, loader)
            return 2

        loader.cb = f
        self.sut.register(loader)
        self.resources_definition = {
            "gfx": {
                1: dict(name="./foo.png"),
                2: dict(name="./foo.png"),
            }
        }

        # act / verify
        self.assertRaises(RuntimeError, self.sut.load, self.resources_definition)


if __name__ == '__main__':
    unittest.main()
