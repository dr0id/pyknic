# -*- coding: utf-8 -*-
"""
This is the main tester class. It will automatically collect all tests and run
 the tests.
"""

import glob
import logging
import os
import sys
import unittest

try:
    import coverage
except ImportError as ie:
    print("coverage could not be imported, install using: python -m pip install -U coverage", ie)
    raise

# setup logging to log to console, no root logger configured because the code under test should not log anything
logger = logging.getLogger()
_log_level = logging.INFO
logger.setLevel(_log_level)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(_log_level)
logger.addHandler(handler)

__version__ = '1.0.0.0'

# find the local copy
TEST_ROOT_PATH = os.path.dirname(__file__)
TEST_ROOT_PATH = TEST_ROOT_PATH if len(TEST_ROOT_PATH) > 0 else "."
logger.info("TEST_ROOT_PATH = %s (cwd: %s)", TEST_ROOT_PATH, os.getcwd())
REPO_PATH = os.path.normpath(os.path.abspath(os.path.join(TEST_ROOT_PATH, os.pardir)))
sys.path.insert(0, REPO_PATH)

COVERAGE_WARNING = \
    """WARNING: Just because a statement is covered by a test case doesn't mean that
         the test case checks that the statement does the right thing. So it's 
         not a good idea to organize your testing around statement coverage or to 
         set targets for statement coverage. However, coverage testing is a good 
         tool for discovering features that haven't been tested and suggesting 
         profitable new areas to test."""


def _split_path_to_list(p):
    path_list = []
    dir_name, leaf = os.path.split(p)
    while leaf:
        path_list.insert(0, leaf)
        dir_name, leaf = os.path.split(dir_name)
    return path_list


def my_import(name):
    # if os.path.isfile(name):
    #     path, filename = os.path.split(name)
    #     name = '.'.join((os.path.split(path)[1], os.path.splitext(filename)[0]))
    mod_name = name
    if os.path.isdir(name) or os.path.isfile(name):
        abs_path = os.path.abspath(name)
        name_list = _split_path_to_list(abs_path)
        idx = name_list.index('tests') + 1
        # base, rest = name.split("tests")
        # rest = rest.strip("/\\")
        # full_name = '.'.join((os.path.split(path)[1], os.path.splitext(filename)[0]))
        mod_name = os.path.splitext('.'.join(name_list[idx:]))[0]
    try:
        mod = __import__(mod_name, globals(), locals(), [], 0)
        components = mod_name.split('.')
        for comp in components[1:]:  # not sure if this is correct
            mod = getattr(mod, comp)
        return mod
    except Exception as ex:
        logger.error("could not load module %s", name, exc_info=True)
        logger.error(ex)
        return None


# noinspection PyUnresolvedReferences
def run_the_tests(test_order):
    suites = []
    logger.info("collecting tests...")
    for test_name in test_order:
        try:
            module = my_import(test_name)
            suites.append(unittest.TestLoader().loadTestsFromModule(module))
            logger.debug("%s", module)
        except Exception as ex:
            logger.error(ex)
    all_tests = unittest.TestSuite(suites)
    logger.info("...found: %s test!", sum([s.countTestCases() for s in suites]))
    logger.info("running tests")
    unittest.TextTestRunner(verbosity=1).run(all_tests)


def _get_filenames(root_dir, ext='*.py', excluded_files=None):
    if excluded_files is None:
        excluded_files = []
    src_files = []
    logger.debug("getting file names from %s with ext %s", root_dir, ext)
    for dirpath, dirnames, filenames in os.walk(root_dir):
        for ex in ext.split(','):
            logger.debug("walking %s %s %s", dirpath, dirnames, filenames)
            names = glob.glob(os.path.join(dirpath, ex))
            logger.debug("globbed files %s %s", ex, names)
            for name in names:
                if _is_name_excluded(name, excluded_files):
                    continue
                src_files.append(name)
    logger.debug("files found: %s", str(src_files))
    return src_files


def _is_name_excluded(name, excluded_files):
    for exclude in excluded_files:
        if name.find(exclude) >= 0:
            return True
    return False


def clean_directory(root, ext='*.pyc'):
    logger.info("going to clean directory %s from %s files...", root, ext)
    del_names = _get_filenames(root, ext)
    for name in del_names:
        try:
            os.remove(name)
        except OSError:
            sys.stdout.write('ERROR: %s could not be deleted %s' %
                             (name, os.linesep))


def main():
    global TEST_ROOT_PATH
    ORIG_WORKING_DIR = os.getcwd()
    if ORIG_WORKING_DIR != os.path.abspath(TEST_ROOT_PATH):
        os.chdir(os.path.abspath(TEST_ROOT_PATH))
        TEST_ROOT_PATH = os.curdir
    logger.setLevel(logging.DEBUG)
    logger.info("working dir: %s", os.getcwd())
    logger.info("TEST_ROOT_PATH: %s", TEST_ROOT_PATH)

    # remove *.pyc and *.pyo files first to make coverage more accurate
    pyknic_path = os.path.join(REPO_PATH, 'pyknic')
    clean_directory(pyknic_path, '*.pyc,*.pyo')

    pyknic_tests_path = os.path.join(REPO_PATH, 'tests')
    clean_directory(pyknic_tests_path, '*.pyc,*.pyo')

    excluded_files = ["extern"]
    names = [
        name for name in _get_filenames(TEST_ROOT_PATH, excluded_files=excluded_files)
        if os.path.basename(name).startswith('test')
    ]

    # prepare coverage
    cov = coverage.Coverage(branch=True)
    cov.erase()

    # tests
    cov.start()
    run_the_tests(names)
    cov.stop()
    cov.save()

    # results
    sys.stdout.write("%s%s%s" % (os.linesep, COVERAGE_WARNING, os.linesep))

    mods = _get_filenames(pyknic_tests_path, excluded_files=excluded_files)
    cov.report(mods, ignore_errors=0, show_missing=1)

    mods = _get_filenames(pyknic_path, excluded_files=excluded_files)
    cov.report(mods, ignore_errors=0, show_missing=1)

    import pyknic
    # answer = '?'
    # while answer != 'y' and answer != 'n':
    logger.info("")
    answer = pyknic.compatibility.input("Show html formated covereage report? y/n  ")
    if answer == 'y':
        cov_html_path = "./covhtml"
        if os.path.exists(cov_html_path):
            logger.info("deleting './covhtml' ...")
            import shutil
            shutil.rmtree(cov_html_path)
        logger.info("saving html...")
        cov.html_report(directory='covhtml')
        url = os.path.abspath("./covhtml/index.html")
        logger.info("opening in browser: " + str(url))
        import webbrowser
        webbrowser.open_new(url)

    os.chdir(ORIG_WORKING_DIR)


if __name__ == '__main__':
    main()
