#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
sphinx-autopackage-script

This script parses a directory tree looking for python modules and packages and
creates ReST files appropriately to create code documentation with Sphinx.
It also creates a modules index (named modules.<suffix>).
"""

# Copyright 2008 Société des arts technologiques (SAT), http://www.sat.qc.ca/
# Copyright 2010 Thomas Waldmann <tw AT waldmann-edv DOT de>
# All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://bitbucket.org/etienned/sphinx-autopackage-script/src/7199e9725789/generate_modules.py
from __future__ import print_function, division

import optparse
import os
import sys


# automodule options
OPTIONS = ['members',
           'undoc-members',
           # 'inherited-members', # disabled because there's a bug in sphinx
           'show-inheritance',
          ]

INIT = '__init__.py'


class DirNode(object):

    def __init__(self, dir_path, dir_names, file_names, registry):
        self.registry = registry
        self.dir_path = dir_path
        self.file_names = file_names
        self.dir_names = dir_names
        self.parent = None

        if self.dir_path in registry:
            raise Exception("already exists: " + str(self.dir_path))

    @property
    def package_name(self):
        names = []
        cur_dir_path = self.dir_path
        cur_node = self.registry.get(cur_dir_path, None)
        if cur_node.is_module:
            cur_dir_path, cur_name = os.path.split(cur_dir_path)
            cur_node = self.registry.get(cur_dir_path, None)

        while cur_node is not None and cur_node.is_package:
            cur_dir_path, cur_name = os.path.split(cur_dir_path)
            names.append(cur_name)
            cur_node = self.registry.get(cur_dir_path, None)
        return ".".join(reversed(names))

    @property
    def is_part_of_package(self):
        if self.is_module:
            parent_dir_path = os.path.split(self.dir_path)[0]
            cur_node = self.registry.get(parent_dir_path, None)
            return cur_node.is_package
        return False

    @property
    def module_paths(self):
        # _file_names = [_f for _f in self.file_names if os.path.splitext(_f)[1] == '.py' and _f != INIT and not _f.startswith('_')]
        _file_names = [_f for _f in self.file_names if os.path.splitext(_f)[1] == '.py' and _f != INIT]
        return sorted([os.path.join(self.dir_path, _f) for _f in _file_names])

    @property
    def is_module(self):
        return os.path.splitext(self.dir_path)[1] == '.py' and self.dir_path != INIT

    @property
    def module_file_name(self):
        return os.path.split(self.dir_path)[1]

    @property
    def full_module_file_name(self):
        return self.package_name + "." + self.module_file_name if self.package_name else self.module_file_name

    @property
    def full_module_name(self):
        return self.package_name + "." + self.module_name if self.package_name else self.module_name

    @property
    def module_name(self):
        if self.is_module:
            return os.path.splitext(self.module_file_name)[0]
        return None

    @property
    def modules(self):
        return [self.registry[_m] for _m in self.module_paths if self.registry.get(_m, None) is not None]

    @property
    def is_package(self):
        return INIT in self.file_names

    @property
    def is_dir(self):
        return not (self.is_package or self.is_module)

    @property
    def sub_packages(self):
        paths = sorted([os.path.join(self.dir_path, _dd) for _dd in self.dir_names])
        return [self.registry[_d] for _d in paths if _d in self.registry and self.registry[_d].is_package]

    @property
    def is_empty(self):
        if self.is_package:
            if len(self.module_paths) > 1:
                return False

            if len(self.sub_packages) > 0:
                return False

            if len(self.module_paths) < 1:
                return True

            if os.path.getsize(self.module_paths[0]) < 3:
                return True

        elif self.is_module:
            if os.path.getsize(self.dir_path) < 3:
                return True

        return False


def write_file(name, text, opts):
    """Write the output file for module/package <name>."""
    if opts.dryrun:
        return
    fname = os.path.join(opts.destdir, "%s.%s" % (name, opts.suffix))
    if not opts.force and os.path.isfile(fname):
        print('File %s already exists, skipping.' % fname)
    else:
        print('Creating file %s.' % fname)
        f = open(fname, 'w')
        f.write(text)
        f.close()

def format_heading(level, text):
    """Create a heading of <level> [1, 2 or 3 supported]."""
    underlining = ['=', '-', '~', ][level-1] * len(text)
    return '%s\n%s\n\n' % (text, underlining)

def create_automodule(name):
    directive = '.. automodule:: %s\n' % name
    for option in OPTIONS:
        directive += '    :%s:\n' % option
    return directive

def create_toc(content_list, opts):
    text = '.. toctree::\n'
    text += '   :maxdepth: %s\n\n   ' % opts.maxdepth
    text += "\n   ".join(content_list)
    text += "\n\n"
    return text

def recurse_tree(path, excludes, opts):
    """
    Look for every file in the directory tree and create the corresponding
    ReST files.
    """
    # use absolute path for root, as relative paths like '../../foo' cause
    # 'if "/." in root ...' to filter out *all* modules otherwise
    path = os.path.abspath(path)

    nodes_registry = {}
    tree = os.walk(path, False)
    for root, subs, files in tree:
        if not is_excluded(root, excludes):
            node = DirNode(root, subs, files, nodes_registry)
            if not node.is_empty:
                nodes_registry[root] = node

                for module in node.module_paths:
                    if not is_excluded(module, excludes):
                        sub_node = DirNode(module, [], [], nodes_registry)
                        if not sub_node.is_empty:
                            nodes_registry[module] = sub_node

    nodes = sorted(nodes_registry.values(), key=lambda n: n.package_name)
    for node in nodes:
        print("inserting path to sys.path: ", node.dir_path)
        sys.path.insert(0, node.dir_path)  # make sure that autodoc extension finds the module/package
        if node.is_package:
            # write package toc
            pack_links = [sp.package_name for sp in node.sub_packages]
            mod_links = [m.full_module_file_name for m in node.modules]

            text =  format_heading(1, node.package_name)
            text +=  "Subpackages and modules:\n\n"
            text += create_toc(pack_links+mod_links, opts)
            text += create_automodule(node.package_name)
            write_file(node.package_name, text, opts)

        if node.is_module:
            # write module toc
            text = format_heading(1, node.full_module_file_name)
            text += "Package :mod:`%s`\n\n" % node.package_name
            text += create_automodule(node.full_module_name)
            write_file(node.full_module_file_name, text, opts)

def normalize_excludes(rootpath, excluded_dirs):
    """
    Normalize the excluded directory list:
    * must be either an absolute path or start with rootpath,
    * otherwise it is joined with rootpath
    * with trailing slash
    """
    sep = os.path.sep
    f_excludes = []
    for exclude in excluded_dirs:
        normed_exclude = os.path.normpath(os.path.abspath(exclude))
        if not os.path.isabs(normed_exclude) and not normed_exclude.startswith(rootpath):
            normed_exclude = os.path.join(rootpath, normed_exclude)
        if not normed_exclude.endswith(sep):
            normed_exclude += sep
        f_excludes.append(normed_exclude)
    return f_excludes

def is_excluded(root, excludes):
    """
    Check if the directory is in the exclude list.

    Note: by having trailing slashes, we avoid common prefix issues, like
          e.g. an exclude "foo" also accidentally excluding "foobar".
    """
    sep = os.path.sep
    if root.endswith("_path.py"):
        return True
    if not root.endswith(sep):
        root += sep
    for exclude in excludes:
        if root.startswith(exclude):
            return True
    return False

def main(args):
    """
    Parse and check the command line arguments.
    """
    parser = optparse.OptionParser(usage="""usage: %prog [options] <package path> [exclude paths, ...]

Note: By default this script will not overwrite already created files.""")
    parser.add_option("-n", "--doc-header", action="store", dest="header", help="Documentation Header (default=Project)", default="Project")
    parser.add_option("-d", "--dest-dir", action="store", dest="destdir", help="Output destination directory", default="")
    parser.add_option("-s", "--suffix", action="store", dest="suffix", help="module suffix (default=txt)", default="txt")
    parser.add_option("-m", "--maxdepth", action="store", dest="maxdepth", help="Maximum depth of submodules to show in the TOC (default=4)", type="int", default=4)
    parser.add_option("-r", "--dry-run", action="store_true", dest="dryrun", help="Run the script without creating the files")
    parser.add_option("-f", "--force", action="store_true", dest="force", help="Overwrite all the files")
    parser.add_option("-t", "--no-toc", action="store_true", dest="notoc", help="Don't create the table of content file")
    (opts, args) = parser.parse_args(args)
    if not args:
        parser.error("package path is required.")
    else:
        rootpath = args[0]
        exclude_dirs = args[1:] # TODO exclude this script in all cases!?
        if os.path.isdir(rootpath):
            # check if the output destination is a valid directory
            if opts.destdir and os.path.isdir(opts.destdir):
                rootpath = os.path.normpath(os.path.abspath(rootpath)) + os.path.sep
                exclude_dirs = normalize_excludes(rootpath, exclude_dirs)
                recurse_tree(rootpath, exclude_dirs, opts)
            else:
                print('%s is not a valid output destination directory.' % opts.destdir)
        else:
            print('%s is not a valid directory.' % rootpath)


if __name__ == '__main__':
    # main(sys.argv[1:])
    # main("-n 'pyknic' -t -d . -f ..\\..\\ ..\\..\\experimental".split())
    # main("-n 'pyknic' -t -d . -f ..\\..\\ ..\\..\\experimental ..\\..\\tests\\extern ..\\..\\tests\\covhtml".split())
    main("-n 'pyknic' -t -d . -f ..\\..\\ "
         "..\\..\\experimental "
         "..\\..\\tests\\extern "
         "..\\..\\tests\\covhtml "
         "..\\..\\doc "
         "..\\..\\experimental".split())
