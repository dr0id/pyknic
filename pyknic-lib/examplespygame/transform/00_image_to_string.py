# -*- coding: utf-8 -*-
from __future__ import print_function, division

try:
    import _path
except:
    pass

    
import sys
import logging
logging.basicConfig(level=logging.DEBUG)

import pygame
from pyknic import pyknic_pygame as pyknicpygame

def main():
    print(sys.argv)
    if len(sys.argv) < 3:
        print(u"Usage: python img_to_string.py image_name output_name")
        sys.exit(-1)

    img_name = sys.argv[1]
    out_name = sys.argv[2]
    img = pygame.image.load(img_name)
    s = pyknicpygame.transform.image_to_string(img)


    f = open(out_name, 'wb')
    f.write(s)
    f.close()

    # f = open("compressed"+out_name, 'wb')
    # f.write(base64.b64encode(zlib.compress(s, 9)))
    # f.close()



    f = open(out_name, 'rb')
    s_dec = f.read()
    f.close()


    imgg = pyknicpygame.transform.string_to_image(s_dec)

    print(img==imgg)

    if s_dec == s:
        print('OK')
    else:
        print('BAD')


if __name__ == '__main__':
    main()
