# -*- coding: utf-8 -*-
from __future__ import print_function, division

try:
    import _path
except:
    pass

import logging
logging.basicConfig(level=logging.DEBUG)

import pygame

from pyknic import pyknic_pygame as pyknicpygame

def main():
    pygame.init()

    screen = pygame.display.set_mode((800, 600))
    screen.fill((0, 0, 0))

    # draw an interesting image
    surf = pygame.Surface((100, 100), pygame.SRCALPHA)
    surf.fill((0, 0, 0, 0))
    surf.fill((255, 200, 120, 255))
    surf.fill((0, 0, 255, 255), pygame.Rect(0, 0, 50, 50))
    surf.fill((255, 0, 255, 255), pygame.Rect(50, 50, 30, 30))
    pygame.draw.circle(surf, (0, 255, 0), (80, 30), 20)



    blurred = pyknicpygame.transform.blur_surf(surf, 50, 50, 50)


    # make a checker background
    background = screen.copy()
    width, height = background.get_size()
    checker_size = 50
    for xpos in range(width // checker_size):
        for ypos in range(height // checker_size):
            if (xpos % 2) ^ (ypos % 2):
                color = (255, 255, 255)
            else:
                color = (0, 0, 0)
            background.fill(color, pygame.Rect(xpos * checker_size, ypos * checker_size, checker_size, checker_size))

    amount_x = 1.0
    amount_y = 1.0
    border = 0
    color_r = 0
    color_g = 0
    color_b = 0
    color_a = 0

    multiplier = 1.0
    dirty = True
    draw_rect = False

    running = True
    while running:
        if pygame.key.get_mods() & pygame.KMOD_SHIFT:
            multiplier = 10.0
        else:
            multiplier = 1.0
        # print('multp', multiplier, pygame.key.get_mods(), pygame.KMOD_SHIFT, pygame.KMOD_CTRL, pygame.key.get_mods() & pygame.KMOD_SHIFT)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_q:
                    amount_x += 0.1 * multiplier
                    dirty = True
                elif event.key == pygame.K_a:
                    amount_x -= 0.1 * multiplier
                    amount_x = max(amount_x, 1.0)
                    dirty = True
                elif event.key == pygame.K_w:
                    amount_y += 0.1 * multiplier
                    dirty = True
                elif event.key == pygame.K_s:
                    amount_y -= 0.1 * multiplier
                    amount_y = max(amount_y, 1.0)
                    dirty = True
                elif event.key == pygame.K_e:
                    border += 1 * multiplier
                    dirty = True
                elif event.key == pygame.K_d:
                    border -= 1 * multiplier
                    border = max(border, 0)
                    dirty = True
                elif event.key == pygame.K_z:
                    color_r += 1 * multiplier
                    color_r = min(255, color_r)
                    dirty = True
                elif event.key == pygame.K_h:
                    color_r -= 1 * multiplier
                    color_r = max(border, 0)
                    dirty = True
                elif event.key == pygame.K_u:
                    color_g += 1 * multiplier
                    color_g = min(255, color_g)
                    dirty = True
                elif event.key == pygame.K_j:
                    color_g -= 1 * multiplier
                    color_g = max(border, 0)
                    dirty = True
                elif event.key == pygame.K_i:
                    color_b += 1 * multiplier
                    color_b = min(255, color_g)
                    dirty = True
                elif event.key == pygame.K_k:
                    color_b -= 1 * multiplier
                    color_b = max(border, 0)
                    dirty = True
                elif event.key == pygame.K_o:
                    color_a += 1 * multiplier
                    color_a = min(255, color_a)
                    dirty = True
                elif event.key == pygame.K_l:
                    color_a -= 1 * multiplier
                    color_a = max(border, 0)
                    dirty = True
                elif event.key == pygame.K_SPACE:
                    draw_rect = not draw_rect
                    dirty = True
        if dirty:
            print('amount x/y:', amount_x, amount_y, 'border', border)
            dirty = False
            color = map(int, (color_r, color_g, color_b, color_a))
            print('color', color)
            blurred = pyknicpygame.transform.blur_surf(surf, amount_x, amount_y, int(border), color)
            if draw_rect:
                pygame.draw.rect(blurred, (255, 0, 0), blurred.get_rect(), 1)

        screen.blit(background, (0, 0))
        screen.blit(surf, (50, 50))
        screen.blit(blurred, (200, 50))

        pygame.display.flip()


if __name__ == '__main__':
    main()

