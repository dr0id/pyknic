# -*- coding: utf-8 -*-

# _path is only needed if run from the examples directory
try:
    import _path  # has to be first import to make sure it finds the 'context' package
except:
    pass

import random
import logging
logging.basicConfig(level=logging.DEBUG)

import pygame

import pyknic
from pyknic import pyknic_pygame as pyknicpygame

pyknic.logs.print_logging_hierarchy()

WINDOW_SIZE = (800, 600)
DURATION = 1.0


class EffectShowContext(pyknic.context.Context):
    effects = [
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[0], pyknicpygame.context.effects.Slide.LEFT),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[0], pyknicpygame.context.effects.Slide.RIGHT),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[1], pyknicpygame.context.effects.Slide.UP),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[1], pyknicpygame.context.effects.Slide.DOWN),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[0], pyknicpygame.context.effects.Slide.LEFT, False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[0], pyknicpygame.context.effects.Slide.RIGHT, False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[1], pyknicpygame.context.effects.Slide.UP, False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[1], pyknicpygame.context.effects.Slide.DOWN, False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[0], pyknicpygame.context.effects.Slide.LEFT, True,
                                           False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[0], pyknicpygame.context.effects.Slide.RIGHT, True,
                                           False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[1], pyknicpygame.context.effects.Slide.UP, True,
                                           False),
        pyknicpygame.context.effects.Slide(DURATION, WINDOW_SIZE[1], pyknicpygame.context.effects.Slide.DOWN, True,
                                           False),
        pyknicpygame.context.effects.FadeOutEffect(DURATION),
        pyknicpygame.context.effects.ExplosionEffect(WINDOW_SIZE),
        pyknicpygame.context.effects.DisappearingRectsTransition(WINDOW_SIZE),
        pyknicpygame.context.effects.Meltdown(WINDOW_SIZE[0], WINDOW_SIZE[1], y_influence=1.1, min_dist=50,
                                              width_add=100),
        pyknicpygame.context.effects.Meltdown(WINDOW_SIZE[0], WINDOW_SIZE[1], y_influence=1.0, min_dist=30,
                                              direction=(0, -1)),
        pyknicpygame.context.effects.Meltdown(WINDOW_SIZE[0], WINDOW_SIZE[1], y_influence=1.0, min_dist=30,
                                              direction=(1, 0)),
        pyknicpygame.context.effects.Meltdown(WINDOW_SIZE[0], WINDOW_SIZE[1], y_influence=1.0, min_dist=30,
                                              direction=(-1, 0)),
        pyknicpygame.context.effects.DirectionalSweep(WINDOW_SIZE),
        pyknicpygame.context.effects.DirectionalSweep(WINDOW_SIZE, direction=(-1, 1), bg_color=(255, 255, 255)),
        pyknicpygame.context.effects.DirectionalSweep(WINDOW_SIZE, direction=(-1, 0), flip_duration=0.75, duration=1.0),
    ]
    current_effect = -1
    old_update = False
    new_update = False
    font = None
    font_small = None

    def __init__(self):
        pyknic.context.Context.__init__(self)
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.text = ""
        self.trans_description = ""
        self.context_name_image = None
        self.text_image = None

    def exit(self):
        # clear events after transition
        pygame.event.clear()

    def enter(self):
        EffectShowContext.current_effect += 1
        # if EffectShowContext.current_effect >= len(EffectShowContext.effects):
        #     EffectShowContext.current_effect = 0
        self.text = "Context B" if self.current_effect % 2 == 1 else "Context A"
        _current_idx = EffectShowContext.current_effect % len(EffectShowContext.effects)
        self.trans_description = str(EffectShowContext.effects[_current_idx])

    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def update(self, dt, sim_t):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pyknic.context.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknic.context.pop()
                elif event.key == pygame.K_SPACE:
                    _current_idx = EffectShowContext.current_effect % len(EffectShowContext.effects)
                    trans_effect = EffectShowContext.effects[_current_idx]
                    new_context = pyknic.context.transitions.Transition(EffectShowContext(), trans_effect,
                                                                        EffectShowContext.old_update,
                                                                        EffectShowContext.new_update)
                    pyknic.context.push(new_context)
            pyknic.context.print_stack()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.fill(self.color)

        if self.context_name_image is None:
            self.context_name_image = self.font.render(self.text, 0, (255, 255, 255))

        rect = self.context_name_image.get_rect(center=screen.get_rect().center)
        screen.blit(self.context_name_image, rect)

        if self.text_image is None:
            self.text_image = self.font_small.render(self.trans_description, 0, (255, 255, 255))
            screen_w = screen.get_size()[0]
            text_w = self.text_image.get_size()[0]
            if text_w > screen_w:
                self.text_image = pygame.transform.rotozoom(self.text_image, 0, screen_w / float(text_w))

        rect = self.text_image.get_rect(center=screen.get_rect().center)
        screen.blit(self.text_image, rect.move(0, 60))


def main():
    # init pygame
    pygame.init()
    screen = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("effects demo, press 'SPACE' for next transition")
    clock = pygame.time.Clock()

    # init context
    con = EffectShowContext()
    pyknic.context.push(con)
    EffectShowContext.font = pygame.font.Font(None, 60)
    EffectShowContext.font_small = pygame.font.Font(None, 40)
    sim_t = 0

    # loop
    while pyknic.context.top():
        dt = clock.tick(60.0) / 1000.0  # convert to seconds
        pyknic.context.top().draw(screen)
        sim_t += dt
        pyknic.context.top().update(dt, sim_t)
        pygame.display.flip()


if __name__ == "__main__":

    main()
