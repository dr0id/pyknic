# -*- coding: utf-8 -*-

import sys
import os

# run in right directory
if not sys.argv[0]:
    appdir = os.path.abspath(os.path.dirname(__file__))
else:
    appdir = os.path.abspath(os.path.dirname(sys.argv[0]))
    
# os.chdir(appdir)

appdir = os.path.abspath(os.path.join(appdir, os.pardir, os.pardir))

sys.path.insert(0, appdir)

if __debug__:
    import logging
    logger = logging.getLogger("examples.pygame")
    logger.debug("inserted at position 0 to sys.path:" + str(appdir))

