# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'entity.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains the base entity classes.
"""
from __future__ import print_function

import logging

from pyknic.generators import IdGenerator

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["BaseEntity", "IdentityEntity", "TypedEntity"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class IdentityEntity(object):
    _id_generator = IdGenerator(0)

    def __init__(self, init_id=None):
        if init_id is None:
            self._id = IdentityEntity._id_generator.next()
        else:
            self._id = init_id
            if init_id > IdentityEntity._id_generator.last_id:
                IdentityEntity._id_generator.set_last_id(init_id)
            elif init_id <= IdentityEntity._id_generator.last_id:
                raise Exception("id \'{0}\'probably already in use. Generator.last_id: '{1}'".format(init_id,
                                                                                                     IdentityEntity._id_generator.last_id))

        if not __debug__:  # skipped in __debug__
            self.id = self._id

    if __debug__:  # skipped in -OO    make sure that id is read-only in __debug__ mode
        @property
        def id(self):
            return self._id


class TypedEntity(IdentityEntity):

    def __init__(self, kind, init_id=None):
        IdentityEntity.__init__(self, init_id)
        self.kind = kind


class BaseEntity(TypedEntity):

    def __init__(self, kind, position, radius=2, rect=None, init_id=None):
        TypedEntity.__init__(self, kind, init_id)
        self.rect = rect
        self.position = position
        self.old_position = position.copy()
        self.radius = radius


logger.debug("imported")
