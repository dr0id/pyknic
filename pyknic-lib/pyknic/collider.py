# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'collider.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Collider class to manage collision callbacks.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Collider"]  # list of public visible parts of this module

from collections import defaultdict

logger = logging.getLogger(__name__)
logger.debug("importing...")


# todo unittests!
class Collider(object):
    def __init__(self):
        self._collision_funcs = {}  # {(kind0, kind1): func}

    def register(self, kind0, kind1, func):
        if kind1 < kind0:
            raise Exception("kind0 > kind1 !")
        key = (kind0, kind1)
        existing_func = self._collision_funcs.get(key, None)
        if existing_func:
            raise Exception("func for kinds already registered " + key)
        self._collision_funcs[key] = func

    # def check_collision(self, ent1, ent2):
    #     assert ent1.kind <= ent2.kind
    #     key = (ent1.kind, ent2.kind)
    #     func = self._collision_funcs.get(key, None)
    #     if func:
    #         func(ent1, ent2)
    #     else:
    #         logger.warning("no collision func registered for entity kinds: " + key)

    def check_all_collisions(self, entity_list):
        g = defaultdict(set)
        # g = defaultdict(list) # use list if there are unhashable entities
        for ent in entity_list:
            g[ent.kind].add(ent)
            # g[ent.kind].append(ent) # use list if there are unhashable entities

        entity_kinds = g.keys()
        for kind_tuple, func in self._collision_funcs.items():
            if kind_tuple[0] in entity_kinds:
                if kind_tuple[1] in entity_kinds:
                    func(g[kind_tuple[0]], g[kind_tuple[1]])

    # def check_all_collisions2(self, entity_list):
    #     # g = defaultdict(set)
    #     g = defaultdict(list) # use list if there are unhashable entities
    #     for ent in entity_list:
    #         # g[ent.kind].add(ent)
    #         g[ent.kind].append(ent) # use list if there are unhashable entities
    #
    #     entity_kinds = g.keys()
    #     for kind_tuple, func in self._collision_funcs.items():
    #         if kind_tuple[0] in entity_kinds:
    #             if kind_tuple[1] in entity_kinds:
    #                 func(g[kind_tuple[0]], g[kind_tuple[1]])
    #
    # def check_all_collisions3(self, entity_list):
    #     g = defaultdict(set)
    #     # g = defaultdict(list) # use list if there are unhashable entities
    #     for ent in entity_list:
    #         g[ent.kind].add(ent)
    #         # g[ent.kind].append(ent) # use list if there are unhashable entities
    #
    #     entity_kinds = g.keys()
    #     for kind_tuple, func in self._collision_funcs.items():
    #         if kind_tuple[0] in entity_kinds:
    #             if kind_tuple[1] in entity_kinds:
    #                 func(g[kind_tuple[0]], g[kind_tuple[1]])

    # def check_all_collisions2(self, entity_list):
    #     g = defaultdict(set)
    #     # g = defaultdict(list) # use list if there are unhashable entities
    #     for ent in entity_list:
    #         g[ent.kind].add(ent)
    #         # g[ent.kind].append(ent) # use list if there are unhashable entities
    #
    #     g_get = g.get
    #     for kind_tuple, func in self._collision_funcs.items():
    #         entities0 = g_get(kind_tuple[0], None)
    #         if entities0 is None:
    #             continue
    #         entities1 = g_get(kind_tuple[1], None)
    #         if entities1 is None:
    #             continue
    #         func(entities0, entities1)
    #
    # def check_all_collisions2(self, entities):
    #     for key, func in self._collision_funcs.items():
    #         func((e for e in entities if e.kind == key[0]), (o for o in entities if o.kind == key[1]))
    #
    # # def check_all_collisions3(self, entities):
    # #     g = dict(itertools.groupby(sorted(entities, key=lambda e:e.kind), key=lambda e: e.kind))
    # #     for key, func in self._collision_funcs.items():
    # #         func(g.get(key[0], []), g.get(key[1], []))
    #
    #
    #
    # def check_all_collisions3(self, entity_list):
    #     g = defaultdict(set)
    #     # g = defaultdict(list) # use list if there are unhashable entities
    #     for ent in entity_list:
    #         g[ent.kind].add(ent)
    #         # g[ent.kind].append(ent) # use list if there are unhashable entities
    #
    #     for kind_tuple, func in self._collision_funcs.items():
    #         if kind_tuple[0] in g.keys():
    #             if kind_tuple[1] in g.keys():
    #                 entities0 = g[kind_tuple[0]]
    #                 entities1 = g[kind_tuple[1]]
    #                 func(entities0, entities1)


if __name__ == '__main__':

    class A(object):
        def __init__(self, kind, id):
            self.kind = kind
            self.id = id


    all_entities = []
    kinds = [0, 1, 2, 3, 4]
    for i in range(1000):
        all_entities.append(A(kinds[i % len(kinds)], i))


    def f1(e1, e2):
        e1 = list(e1)
        e2 = list(e2)
        print(None if len(e1) == 0 else list(e1)[0].kind, len(e1), None if len(e2) == 0 else list(e2)[0].kind, len(e2))


    collider = Collider()

    collider.register(kinds[0], kinds[1], f1)
    collider.register(kinds[2], kinds[3], f1)
    collider.register(kinds[1], kinds[4], f1)
    collider.register(kinds[3], kinds[3], f1)
    for i in range(1000, 100 * 1000, 1):
        collider.register(i, i, f1)

    from timeit import default_timer as timer

    start = timer()
    count = 1
    for i in range(count):
        collider.check_all_collisions(all_entities)
    end = timer()
    print(end - start, "s")

    start = timer()
    for i in range(count):
        collider.check_all_collisions2(all_entities)
    end = timer()
    print(end - start, "s")

    start = timer()
    for i in range(count):
        collider.check_all_collisions3(all_entities)
    end = timer()
    print(end - start, "s")

logger.debug("imported")
