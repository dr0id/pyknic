# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'highlight.py' is part of pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import random

# import pyknic_pygame.transform

try:
    import sys
    import traceback
    import logging
    import timeit
    import time

    logging.basicConfig()
    import pygame

    # import pyknic
    # import pyknic.pyknic_pygame

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2021"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 1024, 768


    # def get_hover_image(image, border):
    #     border_size = 2 * border
    #     r = image.get_rect().inflate(border_size, border_size)  # add 10 border
    #     r.move_ip(-r.x, -r.y)
    #     i = pygame.Surface(image.get_size(), pygame.SRCALPHA)
    #     mask = pygame.mask.from_surface(image, 100)
    #     yellow = (255, 219, 15)
    #     mask.to_surface(i, setcolor=yellow, unsetcolor=(0, 0, 0, 0))
    #     # i.fill((0, 0, 0, 0), r, pygame.BLEND_RGBA_MULT)
    #     # step_size = 1
    #     # outline = mask.outline(step_size)
    #     # pygame.draw.polygon(i, yellow, outline)
    #     # factor = 0.5
    #     # i = pyknic.pyknic_pygame.transform.blur_surf(i, border * factor, border * factor, border_size)
    #     i = pyknic.pyknic_pygame.transform.box_blur(i, (21, 21), iterations=4, border=border_size)
    #     iw, ih = i.get_size()
    #     i = pygame.transform.smoothscale(i, (iw + 21, ih + 21))  # not sure if this is needed
    #     return i, r.center, mask.to_surface(setcolor=yellow)

    def box_blur_per_pixel(image, box):
        """
        Brute force implementation for comparison. Too slow to be used.
        """
        image_rect = image.get_rect()
        dest = pygame.Surface(image_rect.size, pygame.SRCALPHA)
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)
        total_weight = bw * bh
        for px in range(image_rect.width):
            for py in range(image_rect.height):
                region.center = (px, py)
                clipped = region.clip(image_rect)
                sr = sg = sb = sa = 0
                for x in range(clipped.left, clipped.right):
                    for y in range(clipped.top, clipped.bottom):
                        r, g, b, a = image.get_at((x, y))
                        sr += r
                        sg += g
                        sb += b
                        sa += a
                dest_color = (
                    int(sr / total_weight), int(sg / total_weight), int(sb / total_weight), int(sa / total_weight))
                dest.set_at((px, py), dest_color)
        return dest


    def box_blur_per_pixel2(image, box):
        image_rect = image.get_rect()
        dest = pygame.Surface(image_rect.size, pygame.SRCALPHA)
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)

        pygame_transform_average_color = pygame.transform.average_color
        for px in range(image_rect.width):
            for py in range(image_rect.height):
                region.center = (px, py)
                c = pygame_transform_average_color(image, region)
                dest.set_at(region.center, c)

                #         r, g, b, a = image.get_at((x, y))
                #         sr += r
                #         sg += g
                #         sb += b
                #         sa += a
                # dest_color = (
                #     int(sr / total_weight), int(sg / total_weight), int(sb / total_weight), int(sa / total_weight))
                # dest.set_at((px, py), dest_color)
        return dest


    # winner
    def box_blur_per_pixel3(image, box):
        bw, bh = box
        pygame_transform_average_color = pygame.transform.average_color
        destination = image.copy()
        with pygame.PixelArray(destination) as w_array:
            for x, col in enumerate(w_array[:]):
                col[:] = [pygame_transform_average_color(image, (x, y, bw, bh)) for y, c in enumerate(col)]
        return destination


    def box_blur_per_pixel4(image, box):
        bw, bh = box
        pygame_transform_average_color = pygame.transform.average_color
        destination = image.copy()
        with pygame.PixelArray(destination) as w_array:
            for x, col in enumerate(w_array[:]):
                col[:] = [pygame_transform_average_color(image, (x - bw // 2, y - bh // 2, bw, bh)) for y, c in
                          enumerate(col)]
        return destination


    def box_blur_per_pixel5(image, box):
        bw, bh = box
        box = pygame.Rect(0, 0, bw, bh)
        surf_rect = image.get_rect()
        dest = image.copy()
        pygame_transform_average_color = pygame.transform.average_color
        dest_set_at = dest.set_at
        for x in range(surf_rect.width):
            for y in range(surf_rect.height):
                box.center = (x, y)
                dest_set_at(box.center, pygame_transform_average_color(image, box, True))
        return dest


    def box_blur_per_pixel6(image, box):
        bw, bh = box
        pygame_transform_average_color = pygame.transform.average_color
        destination = image.copy()
        with pygame.PixelArray(destination) as w_array:
            for x, col in enumerate(w_array[:]):
                col[:] = [pygame_transform_average_color(image, (x, y, bw, bh), True) for y, c in enumerate(col)]
        return destination


    def box_blur_per_pixel_alpha(image, box):
        """
        Brute force implementation for comparison only!
        Its too slow (its O^2 complexity).

        Implements algorithm from here: https://stackoverflow.com/a/35481968

        """
        image_rect = image.get_rect()
        dest = pygame.Surface(image_rect.size, pygame.SRCALPHA)
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)
        total_weight = bw * bh
        for px in range(image_rect.width):
            for py in range(image_rect.height):
                region.center = (px, py)
                clipped = region.clip(image_rect)
                sr = sg = sb = sa = saf = 0
                for x in range(clipped.left, clipped.right):
                    for y in range(clipped.top, clipped.bottom):
                        r, g, b, a = image.get_at((x, y))
                        a_factor = a / 255
                        saf += a_factor
                        sr += r * a_factor
                        sg += g * a_factor
                        sb += b * a_factor
                        sa += a
                saf = total_weight if sa == 0 else saf  # use total_weight if alpha is 0, not visible anyway
                dest_color = (
                    int(sr / saf), int(sg / saf), int(sb / saf), int(sa / total_weight))
                dest.set_at((px, py), dest_color)
        return dest


    def box_blur_per_pixel_alpha2(image, box):

        # working
        # alpha_factor = image.copy()
        # alpha_array = pygame.PixelArray(alpha_factor)
        # alpha_factor_unmap_rgb = alpha_factor.unmap_rgb # speed up
        # for col in alpha_array[:]:
        #     col[:] = [(alpha_factor_unmap_rgb(c)[3],)*3 + (255,) for c in col]
        # alpha_array.close()

        # working, using numpy!
        # alpha = pygame.surfarray.pixels_alpha(alpha_factor)
        # pygame.surfarray.pixels3d(alpha_factor)[..., :3] = alpha[..., None]
        # del alpha

        # pre multiply with alpha
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        # w_array = pygame.PixelArray(weighted_img)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for col in w_array[:]:
                col[:] = [(int(r * a / 255.0),
                           int(g * a / 255.0),
                           int(b * a / 255.0),
                           a)
                          for r, g, b, a in (w_unmap_rgb(c) for c in col)]
        # w_array.close()

        # blur
        dest = image.copy()
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)
        total_weight = bw * bh
        transform_average_color = pygame.transform.average_color

        def _trans(cx, cy, r, tac, wi):
            r.center = (cx, cy)
            return tac(wi, r)

        with pygame.PixelArray(dest) as dest_array:
            for x, col in enumerate(dest_array[:]):
                col[:] = [_trans(x, y, region, transform_average_color, weighted_img) for y, c in enumerate(col)]

        # post correct with sum of alpha weights
        total_weight_factor = total_weight / 255.0
        dest_unmap_rgb = dest.unmap_rgb  # speed up
        with pygame.PixelArray(dest) as dest_array:
            for col in dest_array[:]:
                col[:] = [(int(r * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           int(g * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           int(b * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           a)
                          for r, g, b, a in (dest_unmap_rgb(c) for c in col)]

        return dest


    def box_blur_per_pixel_alpha3(image, box):
        # pre multiply with alpha
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for col in w_array[:]:
                col[:] = [(int(r * a / 255.0),
                           int(g * a / 255.0),
                           int(b * a / 255.0),
                           a)
                          for r, g, b, a in (w_unmap_rgb(c) for c in col)]

        # blur
        dest = image.copy()
        bw, bh = box
        bw2 = bw // 2
        bh2 = bh // 2
        transform_average_color = pygame.transform.average_color

        def _trans(cx, cy, tac=transform_average_color, wi=weighted_img):
            return tac(wi, (cx - bw2, cy - bh2, bw, bh))

        with pygame.PixelArray(dest) as dest_array:
            for x, col in enumerate(dest_array[:]):
                col[:] = [_trans(x, y) for y, c in enumerate(col)]

        # post correct with sum of alpha weights
        total_weight = bw * bh
        total_weight_factor = total_weight / 255.0
        dest_unmap_rgb = dest.unmap_rgb  # speed up
        with pygame.PixelArray(dest) as dest_array:
            for col in dest_array[:]:
                col[:] = [(int(r * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           int(g * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           int(b * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           a)
                          for r, g, b, a in (dest_unmap_rgb(c) for c in col)]

        return dest


    def box_blur_per_pixel_alpha4(image, box):
        # if image.get_flags() | pygame.SRCALPHA:  # <- is this the way to check for alpha presence??
        # pre multiply with alpha
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for col in w_array[:]:
                col[:] = [(int(r * a / 255.0),
                           int(g * a / 255.0),
                           int(b * a / 255.0),
                           a)
                          for r, g, b, a in (w_unmap_rgb(c) for c in col)]

        # prepare blur and post correction
        dest = image.copy()
        bw, bh = box
        bw2 = bw // 2
        bh2 = bh // 2
        transform_average_color = pygame.transform.average_color

        def _trans(cx, cy, tac=transform_average_color, wi=weighted_img):
            return tac(wi, (cx - bw2, cy - bh2, bw, bh))

        # post correct with sum of alpha weights
        total_weight = bw * bh
        total_weight_factor = total_weight / 255.0
        with pygame.PixelArray(dest) as dest_array:
            for x, col in enumerate(dest_array[:]):
                col[:] = [(int(r * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           int(g * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           int(b * total_weight / (a * total_weight_factor if a > 0 else total_weight)),
                           a)
                          for r, g, b, a in (_trans(x, y) for y, c in enumerate(col))]

        return dest


    def box_blur_per_pixel_alpha5(image, box):
        # if image.get_flags() | pygame.SRCALPHA:  # <- is this the way to check for alpha presence??
        # pre multiply with alpha
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for col in w_array[:]:
                col[:] = [(int(r * a / 255.0), int(g * a / 255.0), int(b * a / 255.0), a)
                          for r, g, b, a in (w_unmap_rgb(c) for c in col)]

        # prepare blur and post correction
        dest = image.copy()
        bw, bh = box
        bw2 = bw // 2
        bh2 = bh // 2
        transform_average_color = pygame.transform.average_color

        # post correct with sum of alpha weights
        total_weight = bw * bh
        total_weight_factor = total_weight / 255.0
        iw, ih = image.get_size()
        with pygame.PixelArray(dest) as dest_array:
            for x in range(iw):
                for y in range(ih):
                    r, g, b, a = transform_average_color(weighted_img, (x - bw2, y - bh2, bw, bh))
                    w = total_weight / (a * total_weight_factor if a > 0 else total_weight)
                    dest_array[x, y] = (int(r * w), int(g * w), int(b * w), a)

        return dest


    def box_blur_per_pixel_alpha6(image, box):
        # if image.get_flags() | pygame.SRCALPHA:  # <- is this the way to check for alpha presence??
        # pre multiply with alpha
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for col in w_array[:]:
                col[:] = [(int(r * a / 255.0), int(g * a / 255.0), int(b * a / 255.0), a)
                          for r, g, b, a in (w_unmap_rgb(c) for c in col)]

        # prepare blur and post correction
        dest = image.copy()
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)
        transform_average_color = pygame.transform.average_color

        # post correct with sum of alpha weights
        total_weight = bw * bh
        total_weight_factor = total_weight / 255.0
        iw, ih = image.get_size()
        with pygame.PixelArray(dest) as dest_array:
            for x in range(iw):
                for y in range(ih):
                    region.center = x, y
                    r, g, b, a = transform_average_color(weighted_img, region)
                    w = total_weight / (a * total_weight_factor if a > 0 else total_weight)
                    dest_array[x, y] = (int(r * w), int(g * w), int(b * w), a)

        return dest


    # noinspection PyShadowingBuiltins
    def box_blur_per_pixel_alpha7(image, box, int=int):
        # if image.get_flags() | pygame.SRCALPHA:  # <- is this the way to check for alpha presence??
        # pre multiply with alpha
        iw, ih = image.get_size()
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for x in range(iw):
                for y in range(ih):
                    r, g, b, a = w_unmap_rgb(w_array[x, y])
                    af = a / 255.0
                    w_array[x, y] = int(r * af), int(g * af), int(b * af), a

        # prepare blur and post correction
        dest = image.copy()
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)
        transform_average_color = pygame.transform.average_color

        # post correct with sum of alpha weights
        total_weight = bw * bh
        total_weight_factor = total_weight / 255.0
        with pygame.PixelArray(dest) as dest_array:
            for x in range(iw):
                for y in range(ih):
                    region.center = x, y
                    r, g, b, a = transform_average_color(weighted_img, region)
                    w = total_weight / (a * total_weight_factor if a > 0 else total_weight)
                    dest_array[x, y] = int(r * w), int(g * w), int(b * w), a

        return dest


    # noinspection PyShadowingBuiltins
    def box_blur_per_pixel_alpha8(image, box, int=int):
        # if image.get_flags() | pygame.SRCALPHA:  # <- is this the way to check for alpha presence??
        # pre multiply with alpha
        iw, ih = image.get_size()
        weighted_img = image.copy()  # (r*a, g*a, b*a, a)
        with pygame.PixelArray(weighted_img) as w_array:
            w_unmap_rgb = weighted_img.unmap_rgb  # speed up
            for x in range(iw):
                for y in range(ih):
                    r, g, b, a = w_unmap_rgb(w_array[x, y])
                    af = a / 255.0
                    w_array[x, y] = int(r * af), int(g * af), int(b * af), a

        # prepare blur and post correction
        dest = image.copy()
        bw, bh = box
        region = pygame.Rect(0, 0, bw, bh)
        transform_average_color = pygame.transform.average_color

        # post correct with sum of alpha weights
        total_weight = bw * bh
        total_weight_factor = total_weight / 255.0
        with pygame.PixelArray(dest) as dest_array:
            for x in range(iw):
                for y in range(ih):
                    region.center = x, y
                    r, g, b, a = transform_average_color(weighted_img, region)
                    w = total_weight / (a * total_weight_factor if a > 0 else total_weight)
                    dest_array[x, y] = int(r * w), int(g * w), int(b * w), a

        return dest


    def profile(number_of_samples, dots, method, *args):

        # start_time = time.time()
        title = f"{method.__name__} {args}"
        # print(title)
        pygame.display.set_caption(title, title)
        pygame.event.pump()
        result = method(*args)
        # timing = time.time() - start_time
        pygame.event.pump()

        def f():
            method(*args)

        # timing = timeit.timeit(lambda: method(*args), number=number_of_samples)
        timing = timeit.timeit(f, number=number_of_samples)
        pygame.event.pump()
        # print(timing)
        return dots, result, method.__name__, timing


    def generate_test_image():
        dest = pygame.Surface((256, 256), pygame.SRCALPHA)
        for a in range(255, -1, -1):
            for cc in range(256):
                dest.set_at((cc, 255 - a), (cc, cc, cc, a))
        return dest


    def main():

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE)
        screen_background_color = (255, 0, 255, 0)
        screen.fill(screen_background_color)
        pygame.display.flip()

        # test_img = generate_test_image()
        # pygame.image.save(test_img, "test_image.png")
        # return

        # shape = pygame.image.load("shape.png").convert_alpha()
        dots = pygame.image.load("dots.png").convert_alpha()
        # dots = pygame.image.load("test_image.png").convert_alpha()
        box = (21, 21)
        # noinspection PyListCreation
        blur_args = []
        blur_args.append((dots, box_blur_per_pixel2, dots, box))
        blur_args.append((dots, box_blur_per_pixel3, dots, box))
        # blur_args.append((dots, box_blur_per_pixel4, dots, box))
        blur_args.append((dots, box_blur_per_pixel5, dots, box))
        blur_args.append((dots, box_blur_per_pixel6, dots, box))

        # # test with alpha
        # blur_args.append((dots, box_blur_per_pixel_alpha, dots, box))  # ultra slow!
        blur_args.append((dots, box_blur_per_pixel_alpha2, dots, box))
        # blur_args.append((dots, box_blur_per_pixel_alpha3, dots, box)) # slowest as per statistics!
        blur_args.append((dots, box_blur_per_pixel_alpha4, dots, box))
        blur_args.append((dots, box_blur_per_pixel_alpha5, dots, box))
        blur_args.append((dots, box_blur_per_pixel_alpha6, dots, box))  # fastest as per statistics!
        blur_args.append((dots, box_blur_per_pixel_alpha7, dots, box))
        blur_args.append((dots, box_blur_per_pixel_alpha8, dots, box))

        random.shuffle(blur_args)
        print("test order: ", ", ".join((b[1].__name__ for b in blur_args)))
        blurs = [profile(5, *args) for args in blur_args]
        pygame.display.set_caption("Results!")
        blurs.sort(key=lambda t: t[3])
        print("result order: ", ", ".join((f"({b[2]}, {b[3]:.3})" for b in blurs)))

        for idx, blur in enumerate(blurs):
            i0, i1, name, timing = blur
            pygame.image.save(i0, f"i_{idx}_0.png")
            pygame.image.save(i1, f"i_{idx}_1.png")

        font = pygame.font.Font(None, 15)

        running = True
        while running:
            # events
            for event in [pygame.event.wait()] + pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False

            # draw
            w = 150
            h = 150

            m = SCREENSIZE[0] // (2 * w)
            for idx, b in enumerate(blurs):
                orig_img, blur_img, name, timing = b
                x = (idx % m) * 2 * w
                y = (idx // m) * h  # + 3 * h
                screen.blit(orig_img, (x, y))
                screen.blit(blur_img, (x + w, y))
                label = font.render(f"{name}, {timing:.4}s", True, (255, 255, 255))
                screen.blit(label, (x, y))
            pygame.display.flip()
            screen.fill(screen_background_color)

        pygame.quit()


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
