# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

try:
    import sys
    import traceback
    import pygame

    from mathematics import Vec3 as Vec
    from mathematics import Point3 as Point

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(d) for d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2015"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 800, 600

    def min3(v, vm):
        """
        Component wise min.
        :param v:
        :param vm:
        :return:
        """
        return v.__class__(v.x if v.x < vm.x else vm.x,
                           v.y if v.y < vm.y else vm.y,
                           v.z if v.z < vm.z else vm.z,
                           v.w)

    def max3(v, vm):
        """
        Component wise max.
        :param v:
        :param vm:
        :return:
        """
        return v.__class__(v.x if v.x > vm.x else vm.x,
                           v.y if v.y > vm.y else vm.y,
                           v.z if v.z > vm.z else vm.z,
                           v.w)

    class TimeStepper(object):

        def __init__(self, time_step, max_delta_time=0.050):
            self.time_step = time_step * 1000.0  # ms
            self.left_over_time = 0  # ms
            self.max_delta_time = max_delta_time  # s
            self.virtual_time = 0
            self.time_passed = 0

        def steps(self, dt):
            if dt > self.max_delta_time:
                dt = self.max_delta_time

            self.virtual_time += dt
            dt *= 1000.0  # ms

            step_count = int((dt + self.left_over_time) // self.time_step)
            self.left_over_time += dt - step_count * self.time_step
            self.time_passed += step_count * self.time_step
            return step_count

    class ParticleSystem(object):

        def __init__(self, time_step, num_constraint_iterations=1):
            self.dist_constraint = []
            self.num_constraint_iterations = range(num_constraint_iterations)
            self.time_step = time_step
            self.time_step_sq = time_step * time_step
            self.damping = 0.999

            self.pos = []
            self.old_pos = []
            self.acc = []
            self.inv_m = []
            self.constraints = []

            # TODO: should be in a force generator
            self.gravity = 9.81

        def verlet(self):
            for idx in range(0, len(self.pos), 9):
                # temp = p.clone()
                # old_p = self.old_pos[idx]
                # vel = p - old_p
                # p += vel * self.damping + self.acc[idx] * self.time_step_sq
                # old_p.copy_values(temp)
                temp_x = self.pos[idx]
                temp_y = self.pos[idx + 1]
                temp_z = self.pos[idx + 2]

                old_p_x = self.pos[idx + 3]
                old_p_y = self.pos[idx + 4]
                old_p_z = self.pos[idx + 5]

                self.pos[idx + 0] = temp_x + (temp_x - old_p_x) * self.damping + self.pos[6] * self.time_step_sq
                self.pos[idx + 1] = temp_y + (temp_y - old_p_y) * self.damping + self.pos[7] * self.time_step_sq
                self.pos[idx + 2] = temp_z + (temp_z - old_p_z) * self.damping + self.pos[8] * self.time_step_sq

                self.pos[idx + 3] = temp_x
                self.pos[idx + 4] = temp_y
                self.pos[idx + 5] = temp_z

        def accumulate_forces(self):
            # TODO: should be in a force generator
            g = self.gravity
            # for a in self.acc:
            #     a.copy_values(g)
            for idx in range(0, len(self.pos), 9):
                self.pos[idx + 7] = g

        def step(self):
            self.accumulate_forces()
            self.verlet()
            self.satisfy_constraints()

        def satisfy_constraints(self):
            # particles within a box
            # for p in self.pos:
            #     p.copy_values(min3(max3(p, Point(0.0, 0.0, 0.0)), Point(4.0, 4.0, 4.0)))
            for count in self.num_constraint_iterations:
                to_delete = []
                for idx in range(len(self.dist_constraint)):
                    idx_a, idx_b, rest_sq, tear_sq = self.dist_constraint[idx]
                    pa_x = self.pos[idx_a]
                    pa_y = self.pos[idx_a + 1]
                    pa_z = self.pos[idx_a + 2]
                    # idx_b = self.dist_constraint[idx + 1]
                    pb_x = self.pos[idx_b]
                    pb_y = self.pos[idx_b + 1]
                    pb_z = self.pos[idx_b + 2]
                    # delta = pb - pa
                    # delta_len = delta.length
                    dx = pb_x - pa_x
                    dy = pb_y - pa_y
                    dz = pb_z - pa_z
                    # if dx * dx + dy * dy + dz * dz > self.dist_constraint[idx+3] ** 2:  # tear_length
                    delta_len_sq = dx * dx + dy * dy + dz * dz
                    if delta_len_sq > tear_sq:  # tear_length
                        # to_delete.append(idx)
                        continue
                    inv_m_a = self.inv_m[idx_a // 9]
                    inv_m_b = self.inv_m[idx_b // 9]

                    # r_sq = self.dist_constraint[idx+2] ** 2  # rest_lenth
                    # r_sq = rest * rest

                    f = ((2 * rest_sq) / ((delta_len_sq) + rest_sq) - 1.0) / (inv_m_a + inv_m_b)
                    dx *= f
                    dy *= f
                    dz *= f
                    # delta *= ((2 * rest_sq) / (delta.dot(delta) + rest_sq) - 1.0) / (inv_m_a + inv_m_b)
                    self.pos[idx_a] = pa_x - dx * inv_m_a
                    self.pos[idx_a + 1] = pa_y - dy * inv_m_a
                    self.pos[idx_a + 2] = pa_z - dz * inv_m_a
                    # pb += delta * inv_m_b
                    self.pos[idx_b] = pb_x + dx * inv_m_b
                    self.pos[idx_b + 1] = pb_y + dy * inv_m_b
                    self.pos[idx_b + 2] = pb_z + dz * inv_m_b

                for idx in to_delete:
                    del self.dist_constraint[idx]

                to_delete = []

                for c in self.constraints:
                    res = c.satisfy(self)
                    if res == False:
                        to_delete.append(c)
                for c in to_delete:
                    self.constraints.remove(c)

        def add_constraint(self, constraint):
            if isinstance(constraint, DistConstraint):
                self.dist_constraint.append((constraint.idx_a, constraint.idx_b, constraint.rest_length ** 2, constraint.tear_dist ** 2))
            else:
                self.constraints.append(constraint)

        def add_particle(self, pos, acc=Vec(0.0, 0, 0), old_pos=None, m=1.0):
            if old_pos is None:
                old_pos = pos

            idx = len(self.pos)
            self.pos.append(pos.x)
            self.pos.append(pos.y)
            self.pos.append(pos.z)
            self.pos.append(old_pos.x)
            self.pos.append(old_pos.y)
            self.pos.append(old_pos.z)
            self.pos.append(acc.x)
            self.pos.append(acc.y)
            self.pos.append(acc.z)
            self.inv_m.append(1.0 / m)
            return idx

        def clear(self):
            self.pos = []
            self.old_pos = []
            self.acc = []
            self.inv_m = []
            self.constraints = []


    class Constraint(object):

        def satisfy(self, particle_system):
            raise NotImplementedError

    class PinConstraint(Constraint):

        def __init__(self, idx, position):
            self.idx = idx
            self.position = position

        def satisfy(self, particle_system):
            # particle_system.pos[self.idx].copy_values(self.position)
            particle_system.pos[self.idx] = self.position.x
            particle_system.pos[self.idx + 1] = self.position.y
            particle_system.pos[self.idx + 2] = self.position.z

    class BoxConstraint(Constraint):

        def __init__(self, min_point, max_point):
            self.min_point = min_point
            self.max_point = max_point

        def satisfy(self, particle_system):
            # for p in particle_system.pos:
            for idx in range(0, len(particle_system.pos), 9):
                # p.copy_values(min3(max3(p, self.min_point), self.max_point))
                p = particle_system.pos[idx + 0]
                p = self.min_point.x if p < self.min_point.x else p
                particle_system.pos[idx + 0] = self.max_point.x if p > self.max_point.x else p

                p = particle_system.pos[idx + 1]
                p = self.min_point.y if p < self.min_point.y else p
                particle_system.pos[idx + 1] = self.max_point.y if p > self.max_point.y else p

                p = particle_system.pos[idx + 2]
                p = self.min_point.z if p < self.min_point.z else p
                particle_system.pos[idx + 2] = self.max_point.z if p > self.max_point.z else p

    class DistConstraint(Constraint):

        def __init__(self, idx_a, idx_b, rest_length, tear_dist=sys.maxsize):
            self.idx_a = idx_a
            self.idx_b = idx_b
            self.rest_length = rest_length
            self.tear_dist = tear_dist

        def satisfy_sqrt(self, particle_system):
            pa = particle_system.pos[self.idx_a]
            pb = particle_system.pos[self.idx_b]
            delta = pb - pa
            delta_len = delta.length
            if delta_len > self.tear_dist:
                return False
            inv_m_b = particle_system.inv_m[self.idx_b]
            inv_m_a = particle_system.inv_m[self.idx_a]
            diff = (delta_len - self.rest_length) / (delta_len + (inv_m_a + inv_m_b))
            pa += delta * inv_m_a * diff
            pb -= delta * inv_m_b * diff
            return True

        def satisfy(self, particle_system):
            pa = particle_system.pos[self.idx_a]
            pb = particle_system.pos[self.idx_b]
            delta = pb - pa
            # delta_len = delta.length
            if delta.length_sq > self.tear_dist * self.tear_dist:
                return False
            inv_m_b = particle_system.inv_m[self.idx_b]
            inv_m_a = particle_system.inv_m[self.idx_a]

            r_sq = self.rest_length * self.rest_length
            delta = delta / (inv_m_a + inv_m_b) * ((2 * r_sq) / (delta.dot(delta) + r_sq) - 1.0)
            pa -= delta * inv_m_a
            pb += delta * inv_m_b
            return True

            # r_sq = self.rest_length * self.rest_length
            # delta *= r_sq / (delta.dot(delta) + r_sq) - 0.5
            # pa -= delta
            # pb += delta
            # return True


    def create_cloth(ps):
        offset = Vec(3, 1, 0)
        dist = 0.2
        mesh = {}  # (x,y) : idx
        x_count = 20
        y_count = 20
        for y in range(y_count):
            for x in range(x_count):
                p = offset + Point(x * dist, y * dist, 10)
                m = 1.0 if y != y_count - 1 else 3.0
                idx = ps.add_particle(p, m=m)
                mesh[(x, y)] = idx

                if y == 0:
                    ps.add_constraint(PinConstraint(idx, p.clone()))

                if x > 0:
                    idx_left = mesh[(x-1, y)]
                    ps.add_constraint(DistConstraint(idx, idx_left, dist, tear_dist=2*dist))

                if y > 0:
                    idx_up = mesh[(x, y-1)]
                    ps.add_constraint(DistConstraint(idx, idx_up, dist, tear_dist=2*dist))





    def main():

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE)

        fixed_time_step = 0.016
        time_stepper = TimeStepper(fixed_time_step, max_delta_time=0.030)
        clock = pygame.time.Clock()
        ps = ParticleSystem(fixed_time_step, num_constraint_iterations=2)

        setup_scene(ps)



        ppu = 100

        running = True
        while running:
            # events
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_r:
                        ps.clear()
                        setup_scene(ps)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x, y = event.pos
                    m = Point(x / ppu, y / ppu, 9.9)
                    for idx in range(0, len(ps.pos), 9):
                        p = Vec(ps.pos[idx] + 0, ps.pos[idx + 1], ps.pos[idx + 2])
                        d = p - m
                        p += d.normalized / d.length_sq * 0.01
                        ps.pos[idx + 0] = p.x
                        ps.pos[idx + 1] = p.y
                        ps.pos[idx + 2] = p.z

            # work
            dt = clock.tick(60)
            dt /= 1000.0  # seconds
            print(dt, clock.get_fps())
            steps = time_stepper.steps(dt)
            for step in range(steps):
                ps.step()

            # if ps.pos[0].y >= 4:
            #     running = False
            # print("floor", ps.pos[0].y, time_stepper.virtual_time, time_stepper.time_passed, pygame.time.get_ticks()-562, clock.get_fps())

            # draw
            for idx in range(0, len(ps.pos), 9):
                px = ps.pos[idx]
                py = ps.pos[idx + 1]
                pz = ps.pos[idx + 2]

                if pz > 0.5:
                    radius = int(1.0 / ((ps.inv_m[idx // 9] + 1) * pz * 0.01))
                    pygame.draw.circle(screen, (100, 100, 100, 100), (int(px * ppu), int(py * ppu)), radius)

            for c in ps.constraints:
                if isinstance(c, DistConstraint):
                    a = ps.pos[c.idx_a]
                    b = ps.pos[c.idx_b]
                    pygame.draw.line(screen, (0, 0, 0), (int(a.x * ppu), int(a.y * ppu)), (int(b.x * ppu), int(b.y * ppu)))
                elif isinstance(c, PinConstraint):
                    a_x = ps.pos[c.idx]
                    a_y = ps.pos[c.idx + 1]
                    rect = pygame.Rect(0, 0, 5, 5)
                    rect.center = (a_x * ppu, a_y * ppu)
                    pygame.draw.rect(screen, (100, 100, 100, 100), rect, 0)


            pygame.display.flip()
            screen.fill((255, 255, 255))

        pygame.quit()


    def setup_scene(ps):
        # ps.add_particle(Point(1.0, 1.0, 10.0), old_pos=Point(1.0, 1.0, 10.0))
        ps.add_constraint(BoxConstraint(Point(0.0, 0.0, 1.0), Point(7.5, 5.5, 15.0)))
        # ia = ps.add_particle(Point(3.5, 1.0, 10.0), old_pos=Point(3.5, 1.0, 10.0))
        # ib = ps.add_particle(Point(3.5, 1.0, 10.0) + Vec(1.0, -1.0, 10.0))
        # ps.add_constraint(DistConstraint(ia, ib, 1.5))
        # ia = ps.add_particle(Point(1.5, 1.0, 10.0))
        # ib = ps.add_particle(Point(1.5, 1.0, 10.0) + Vec(1.0, -1.0, 10.0), m=1)
        # ps.add_constraint(DistConstraint(ia, ib, 1.4))
        # ps.add_constraint(PinConstraint(ia, Point(2.5, 1.0, 10.0)))
        # ic = ps.add_particle(Point(2.0, 1.0, 10.0) + Vec(1.5, -1.0, 10.0), m=10)
        # ps.add_constraint(DistConstraint(ic, ib, 0.5))
        create_cloth(ps)


    if __name__ == '__main__':

        import cProfile
        cProfile.run("main()", "stats.txt")
        import pstats
        p = pstats.Stats('stats.txt')
        p.strip_dirs().sort_stats('time').print_stats()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stdin.readline()
