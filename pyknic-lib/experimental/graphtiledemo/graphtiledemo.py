# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Demo of using a special tiles to craate a graph from it for navigation.
"""
from __future__ import print_function, division

from collections import defaultdict

try:
    import sys
    import traceback
    import json
    import math

    import pygame

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(d) for d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2016"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 1024, 1024


    class Sprite(object):

        def __init__(self, x, y, image, layer):
            self.image = image
            self.pos = (x, y)
            self.layer = layer


    class TileInfo(object):

        def __init__(self, tileset, spritesheet_x, spritesheet_y, properties, angle=0, flip_x=False, flip_y=False):
            self.tileset = tileset  # TileSetInfo
            self.flip_x = flip_x
            self.flip_y = flip_y
            self.angle = angle
            self.properties = properties
            self.spritesheet_x = spritesheet_x
            self.spritesheet_y = spritesheet_y


    class LayerTileInfo(object):

        def __init__(self, tile_x, tile_y, tile_info):
            self.tile_x = tile_x
            self.tile_y = tile_y
            self.tile_info = tile_info  # TileInfo


    class TileSetInfo(object):

        def __init__(self, name, tile_width, tile_height, properties, image_name, first_gid, transparent_color):
            self.name = name
            self.tile_width = tile_width
            self.tile_height = tile_height
            self.properties = properties
            self.image_name = image_name
            self.first_gid = first_gid
            self.transparent_color = transparent_color


    class LayerInfo(object):
        def __init__(self, name, x, y, width, height, tile_w, tile_h, visible, opacity, layer_type, data):
            self.name = name
            self.offset_x = x
            self.offset_y = y
            self.width = width
            self.height = height
            self.tile_w = tile_w
            self.tile_h = tile_h
            self.visible = visible
            self.opacity = opacity
            self.layer_type = layer_type
            self.data_yx = data  # [y][x] -> LayerTileInfo


    class MapInfo(object):
        def __init__(self, layers, tiles):
            self.tiles = tiles  # {gid : TileInfo}
            self.layers = layers  # [LayerInfo]


    class Node(object):

        _id = 0

        def __init__(self, node_id=None):
            self.id = node_id
            if self.id is None:
                self.id = Node._id
                Node._id += 1
            else:
                if node_id >= Node._id:
                    Node._id = node_id + 1


    class NavNode(Node):

        def __init__(self, world_x, world_y, tile_x, tile_y, edges, edge_properties, layer_index, node_id=None):
            Node.__init__(self, node_id)
            self.edge_properties = edge_properties
            self.layer_index = layer_index
            self.edges = edges  # set(e_i, n_o, ...)
            self.tile_x = tile_x
            self.tile_y = tile_y
            self.world_x = world_x
            self.world_y = world_y


    class Edge(object):

        def __init__(self, from_node_id, to_node_id):
            self.from_node_id = from_node_id
            self.to_node_id = to_node_id


    class NavEdge(Edge):

        def __init__(self, direction, layer_change, from_node_id, to_node_id):
            Edge.__init__(self, from_node_id, to_node_id)
            self.layer_change = layer_change
            self.direction = direction  # TODO. not so sure if this is really needed


    class Graph(object):

        def __init__(self, node_type=None, edge_type=None):
            self.edge_type = edge_type
            self.node_type = node_type

            self.nodes = {}  # {node_id: node}
            self.edges = {}  # {node_id: {to_node_id: edge}}

        def add_node(self, node):
            if self.node_type and not isinstance(node, self.node_type):
                raise TypeError("wrong node type {0}, should be {1}".format(type(node), self.node_type))

            if node.id in self.nodes:
                raise KeyError("node with id '{0}' already exists".format(node.id))

            self.nodes[node.id] = node
            self.edges[node.id] = {}

        def remove_node(self, node):
            # delete edges from node
            if node.id in self.nodes:
                self.edges[node.id] = {}
            # delete edges to node
            # TODO: make this more efficient by adding a 'to_node_edges' dict? -> add/remove gets more complex!
            for edges in self.edges.values():
                if node.id in edges:
                    del edges[node.id]

        def add_edge(self, edge):
            if self.edge_type and not isinstance(edge, self.edge_type):
                raise TypeError("wrong edge type {0}, should be {1}".format(type(edge), self.edge_type))

            if edge.from_node_id not in self.nodes:
                raise KeyError("from_node_id '{0}' does not exist!".format(edge.from_node_id))
            if edge.to_node_id not in self.nodes:
                raise KeyError("to_node_id '{0}' does not exist!".format(edge.to_node_id))

            # check if edge is already present
            if edge.to_node_id in self.edges[edge.from_node_id]:
                raise KeyError("edge from '{0}' to '{1}' already exists!".format(edge.from_node_id, edge.to_node_id))

            self.edges[edge.from_node_id][edge.to_node_id] = edge

            # TODO: simplify digraph construction by adding here an edge in opposite direction?

        def remove_edge(self, edge):
            if edge.from_node_id not in self.nodes:
                raise KeyError("from_node_id '{0}' does not exist!".format(edge.from_node_id))
            if edge.to_node_id not in self.nodes:
                raise KeyError("to_node_id '{0}' does not exist!".format(edge.to_node_id))

            if edge.from_node_id in self.edges:
                if edge.to_node_id in self.edges[edge.from_node_id]:
                    del self.edges[edge.from_node_id][edge.from_node_id]

                    # TODO: simplify digraph destruction by removing here an edge in opposite direction?

        def is_node_present(self, node_id):
            return node_id in self.nodes

        def is_edge_present(self, from_node_id, to_node_id):
            return from_node_id in self.edges and to_node_id in self.edges[from_node_id]

        def set_edge_attr(self, from_node_id, to_node_id, name, value):
            if from_node_id not in self.nodes:
                raise KeyError("from_node_id '{0}' does not exist!".format(from_node_id))
            if to_node_id not in self.nodes:
                raise KeyError("to_node_id '{0}' does not exist!".format(to_node_id))

            edge = self.edges[from_node_id].get(to_node_id, None)
            if edge:
                if not hasattr(edge, name):
                    raise AttributeError("edge type '{0}' has not attribute '{1}'".format(type(edge), name))
                setattr(edge, name, value)
            else:
                # TODO: logging
                print("edge '{0}'->'{1}' not found for setting attr '{2}={3}'".format(from_node_id, to_node_id, name,
                                                                                      value))


    FLIPPED_HORIZONTALLY_FLAG = 0x80000000
    FLIPPED_VERTICALLY_FLAG = 0x40000000
    FLIPPED_DIAGONALLY_FLAG = 0x20000000
    # D V H
    # 1 1 1     270° -> H
    # 1 1 0 270°
    # 1 0 1  90°
    # 1 0 0     90° -> H
    # 0 1 1 180°
    # 0 1 0     V
    # 0 0 1     H
    # 0 0 0   0°
    flag_to_rot_flips = {
        (True, True, True): (270, False, True),
        (True, True, False): (270, False, False),
        (True, False, True): (90, False, False),
        (True, False, False): (90, False, True),
        (False, True, True): (180, False, False),
        (False, True, False): (0, True, False),
        (False, False, True): (0, False, True),
        (False, False, False): (0, False, False),
    }
    # ROTATED90 = FLIPPED_DIAGONALLY_FLAG | FLIPPED_HORIZONTALLY_FLAG
    # ROTATED180 = FLIPPED_VERTICALLY_FLAG | FLIPPED_HORIZONTALLY_FLAG
    # ROTATED270 = FLIPPED_DIAGONALLY_FLAG | FLIPPED_VERTICALLY_FLAG
    ALL_FLAGS = FLIPPED_DIAGONALLY_FLAG | FLIPPED_HORIZONTALLY_FLAG | FLIPPED_VERTICALLY_FLAG

    # ALL32_BITS = 0xFFFFFFFF

    # edge constants
    #
    #              n_i n_o
    #                | ^
    #                v |
    #         w_o <- +-+ <- e_i
    #                |*|
    #         w_i -> +-+ -> e_o
    #                | ^
    #                v |
    #              s_o s_i
    #

    e_i = 1 << 0  # east in
    e_o = 1 << 1  # east out
    s_i = 1 << 2  # south in
    s_o = 1 << 3  # south out
    w_i = 1 << 4  # west in
    w_o = 1 << 5  # west out
    n_i = 1 << 6  # north in
    n_o = 1 << 7  # north out

    se_i = 1 << 8  # south east in
    se_o = 1 << 9  # south east out
    sw_i = 1 << 10  # south west in
    sw_o = 1 << 11  # south west out
    nw_i = 1 << 12  # north west in
    nw_o = 1 << 13  # north west out
    ne_i = 1 << 14  # north east in
    ne_o = 1 << 15  # north east out

    edge_in_set = {e_i, s_i, w_i, n_i, se_i, sw_i, nw_i, ne_i}
    edge_out_set = {e_o, s_o, w_o, n_o, se_o, sw_o, nw_o, ne_o}
    edges_set = edge_in_set | edge_out_set
    all_edges = reduce(lambda a, b: a | b, edges_set)

    rot45_mapping = {
        e_i: se_i,
        e_o: se_o,

        s_i: sw_i,
        s_o: sw_o,

        w_i: nw_i,
        w_o: nw_o,

        n_i: ne_i,
        n_o: ne_o,

        se_i: s_i,
        se_o: s_o,

        sw_i: w_i,
        sw_o: w_o,

        nw_i: n_i,
        nw_o: n_o,

        ne_i: e_i,
        ne_o: e_o,
    }

    property_mapping = {
        e_i: 'ei',
        e_o: 'eo',

        s_i: 'si',
        s_o: 'so',

        w_i: 'wi',
        w_o: 'wo',

        n_i: 'ni',
        n_o: 'no',

        se_i: 'sei',
        se_o: 'seo',

        sw_i: 'swi',
        sw_o: 'swo',

        nw_i: 'nwi',
        nw_o: 'nwo',

        ne_i: 'nei',
        ne_o: 'neo',

        'ei': e_i,
        'eo': e_o,

        'si': s_i,
        'so': s_o,

        'wi': w_i,
        'wo': w_o,

        'ni': n_i,
        'no': n_o,

        'sei': se_i,
        'seo': se_o,

        'swi': sw_i,
        'swo': sw_o,

        'nwi': nw_i,
        'nwo': nw_o,

        'nei': ne_i,
        'neo': ne_o,
    }

    connectors = {
        e_i: w_o,
        e_o: w_i,

        s_i: n_o,
        s_o: n_i,

        w_i: e_o,
        w_o: e_i,

        n_i: s_o,
        n_o: s_i,

        se_i: nw_o,
        se_o: nw_i,

        sw_i: ne_o,
        sw_o: ne_i,

        nw_i: se_o,
        nw_o: se_i,

        ne_i: sw_o,
        ne_o: sw_i,
    }

    bi_e = e_i | e_o
    bi_s = s_i | s_o
    bi_w = w_i | w_o
    bi_n = n_i | n_o

    bi_se = se_i | se_o
    bi_sw = sw_i | sw_o
    bi_nw = nw_i | nw_o
    bi_ne = ne_i | ne_o

    bi_edges = {bi_e, bi_s, bi_w, bi_n, bi_se, bi_sw, bi_nw, bi_ne}


    def _load_tiles_info_from_tilesets(map_data):
        tilesets = map_data.get("tilesets", None)
        tiles = {}  # {gid: TileInfo}
        if tilesets:
            for tileset in tilesets:
                tile_height = tileset["tileheight"]
                tile_width = tileset["tilewidth"]
                first_gid = tileset["firstgid"]
                name = tileset["name"]
                tileset_properties = tileset["properties"]
                image = tileset["image"]
                transparent_color = tileset.get("transparentcolor", None)  # optional
                tile_set_info = TileSetInfo(name, tile_width, tile_height, tileset_properties, image, first_gid,
                                            transparent_color)

                tile_count = tileset["tilecount"]
                image_width = tileset["imagewidth"]
                # image_height = tileset["imageheight"]
                margin = tileset["margin"]
                spacing = tileset["spacing"]
                tile_width_with_space = tile_width + spacing
                tile_height_with_space = tile_height + spacing
                row_count = image_width // tile_width_with_space
                tiles_properties = tileset.get("tileproperties", {})
                for idx in range(tile_count):
                    x = margin + (idx % row_count) * tile_width_with_space
                    y = margin + (idx // row_count) * tile_height_with_space
                    key = first_gid + idx
                    properties_of_tile = tiles_properties.get(str(idx), {})
                    tiles[key] = TileInfo(tile_set_info, x, y, properties_of_tile)
        else:
            print("tilesets None")
        return tiles


    def load_map_info(map_data):
        tiles_info = _load_tiles_info_from_tilesets(map_data)  # {gid: TileInfo}
        converted_layers = []

        tile_w = map_data["tilewidth"]
        tile_h = map_data["tileheight"]
        json_layers = map_data.get("layers", None)
        if json_layers:
            for json_layer in json_layers:
                width = json_layer["width"]
                height = json_layer["height"]
                name = json_layer["name"]
                opacity = json_layer["opacity"]
                layer_type = json_layer["type"]
                visible = json_layer["visible"]
                offset_x = json_layer["x"]
                offset_y = json_layer["y"]
                data = []
                json_data = json_layer["data"]
                for y in range(height):
                    data.append([])
                    for x in range(width):
                        gid = json_data[y * width + x]
                        # data[y].append(gid)
                        if gid == 0:
                            data[y].append(None)
                            continue
                        if gid & ALL_FLAGS:
                            # modified tile detected, add it if not already present
                            if gid not in tiles_info:
                                _tile = _get_rotated_or_flipped_tile(gid, tiles_info)
                                tiles_info[gid] = _tile  # add the rotated/flipped tile as a new tile with its gid
                        tile_info = tiles_info[gid]
                        data[y].append(LayerTileInfo(x * tile_w, y * tile_h, tile_info))

                layer = LayerInfo(name, offset_x, offset_y, width, height, tile_w, tile_h, visible, opacity, layer_type,
                                  data)
                converted_layers.append(layer)
        else:
            print("layers None")
        return MapInfo(converted_layers, tiles_info)


    def _get_rotated_or_flipped_tile(gid, tiles_info):
        flag_key = (
            gid & FLIPPED_DIAGONALLY_FLAG > 0,
            gid & FLIPPED_VERTICALLY_FLAG > 0,
            gid & FLIPPED_HORIZONTALLY_FLAG > 0
        )
        rot, flip_v, flip_h = flag_to_rot_flips[flag_key]
        actual_gid = gid & ~ALL_FLAGS  # clear flags to get the gid of the used tile
        source_tile = tiles_info[actual_gid]
        tile = TileInfo(source_tile.tileset, source_tile.spritesheet_x,
                        source_tile.spritesheet_y, source_tile.properties, rot, flip_h, flip_v)
        return tile


    def create_sprite_layers(map_info):
        sprite_layers = []  # sprite_layers[0][y][x]
        for idx_layer, layer in enumerate(map_info.layers):
            if layer.layer_type == "tilelayer":
                sprite_layers.append([])
                for idx_y, row in enumerate(layer.data_yx):
                    for lti in row:
                        if lti is not None:
                            sprite = Sprite(lti.tile_x, lti.tile_y, lti.tile_info.image, idx_layer)
                            sprite_layers[idx_layer].append(sprite)
        return sprite_layers


    def load_images(map_info, img_cache=None):
        if img_cache is None:
            img_cache = {}
        for tile in map_info.tiles.values():
            tileset = tile.tileset
            image_name = tileset.image_name

            # load source image first if not already cached
            image_source = img_cache.get(image_name, None)
            if image_source is None:
                image_source = pygame.image.load(image_name)
                if tileset.transparent_color:
                    transparent_color = pygame.Color(tileset.transparent_color)
                    image_source.set_colorkey(transparent_color, pygame.RLEACCEL)
                    image_source = image_source.convert()
                else:
                    image_source = image_source.convert_alpha()
                img_cache[image_name] = image_source

            # load tile image from source image
            key = (image_name, tile.spritesheet_x, tile.spritesheet_y, tile.angle, tile.flip_x, tile.flip_y)
            tile_image = img_cache.get(key, None)
            if tile_image is None:
                tile_image = pygame.Surface((tileset.tile_width, tileset.tile_height), pygame.SRCALPHA)
                # tile_image.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
                source_rect = pygame.Rect(tile.spritesheet_x, tile.spritesheet_y, tileset.tile_width, tileset.tile_height)
                tile_image.blit(image_source, (0, 0), source_rect)
                # important: order of operations matter: rotate first and then flip the image
                if tile.angle > 0:
                    tile_image = pygame.transform.rotate(tile_image, -tile.angle)
                if tile.flip_x or tile.flip_y:
                    tile_image = pygame.transform.flip(tile_image, tile.flip_x, tile.flip_y)
                img_cache[key] = tile_image
            tile.image = tile_image


    def load_graph(map_info):
        graph = Graph(NavNode, NavEdge)
        # add nodes
        _nodes = defaultdict(dict)  # {layer_idx : {(tile_x, tile_y): cur_node}}
        for idx_layer, layer in enumerate(map_info.layers):
            if not layer.name.startswith("graph"):
                continue
            for idx_y, row in enumerate(layer.data_yx):
                for idx_x, layer_tile in enumerate(row):
                    if layer_tile is None:
                        pass
                    else:
                        tile = layer_tile.tile_info
                        if 'edges' not in tile.properties:
                            raise KeyError("edges should be in a cur_node tile")
                        angle = layer_tile.tile_info.angle
                        if angle != 0:
                            raise ArithmeticError("no rotated graph tiles allowed!")
                        edges = set([property_mapping[_edge.strip()] for _edge in tile.properties['edges'].split(',')])
                        edge_properties = dict([(property_mapping[_key], _val) for _key, _val in tile.properties.items()
                                                if _key in property_mapping.keys()])
                        wx = layer_tile.tile_x + layer.tile_w // 2
                        wy = layer_tile.tile_y + layer.tile_h // 2
                        cur_node = NavNode(wx, wy, layer_tile.tile_x, layer_tile.tile_y, edges, edge_properties,
                                           idx_layer)
                        _nodes[idx_layer][(idx_x, idx_y)] = cur_node
                        if abs(idx_x - layer_tile.tile_x // 64) > 1 or abs(idx_y - layer_tile.tile_y // 64) > 0:
                            print(layer_tile)
                        graph.add_node(cur_node)

        # add edges
        neighbour_coordinates = (
            (1, 0, e_i, e_o), (1, 1, se_i, se_o), (0, 1, s_i, s_o), (-1, 1, sw_i, sw_o), (-1, 0, w_i, w_o),
            (-1, -1, nw_i, nw_o), (0, -1, n_i, n_o), (1, -1, ne_i, ne_o)
        )
        layer_indices = sorted(_nodes.keys())
        for layer_idx_idx, layer_idx in enumerate(layer_indices):
            # for key, _orig_cur_node in _nodes[layer_idx].items():
            sorted_keys = sorted(_nodes[layer_idx].keys())
            for key in sorted_keys:
                _orig_cur_node = _nodes[layer_idx][key]
                idx_x, idx_y = key
                for dx, dy, d_i, d_o in neighbour_coordinates:
                    cur_x = idx_x + dx
                    cur_y = idx_y + dy

                    cur_d = (d_i, d_o)
                    relevant_edge_props = dict([(_edge_key, _v) for _edge_key, _v in cur_node.edge_properties.items()
                                                if _edge_key in cur_d])
                    for d_d in cur_d:
                        neighbour_node = _nodes[layer_idx].get((cur_x, cur_y), None)
                        cur_node = _orig_cur_node
                        if neighbour_node is None:
                            # check for special node properties
                            prop_value = relevant_edge_props.get(d_d, "")
                            # check for layer change property
                            if prop_value.startswith("layer"):
                                if d_d in edge_in_set and prop_value.endswith('++'):
                                    raise NotImplemented("at the moment only upper layers are supported for in edges")
                                if d_d in edge_out_set and prop_value.endswith('--'):
                                    raise NotImplemented("at the moment only upper layers are supported for out edges")
                                # find potentially connecting node on other layer
                                for upper_layer_idx in layer_indices[layer_idx_idx + 1:]:
                                    neighbour_node = _nodes[upper_layer_idx].get((cur_x, cur_y), None)
                                    if neighbour_node is not None:
                                        break

                        # if it is still None (none found in other layers) continue
                        if neighbour_node is None:
                            continue

                        # swap nodes to make sure that the right edge is inserted
                        # the swap of the edge direction is needed too
                        if d_d in edge_in_set:
                            neighbour_node, cur_node = cur_node, neighbour_node
                            d_d = connectors[d_d]

                        # treat all edges as if they where a in edge on the node (e.g. neighbour_node -> node)
                        if d_d in cur_node.edges and connectors[d_d] in neighbour_node.edges \
                                and not graph.is_edge_present(cur_node.id, neighbour_node.id):
                            layer_change = neighbour_node.layer_index - cur_node.layer_index
                            edge = NavEdge(d_d, layer_change, cur_node.id, neighbour_node.id)
                            if cur_node.id == neighbour_node.id:
                                raise "same nodes {0}".format(cur_node.id)
                            graph.add_edge(edge)

        return graph


    def draw_graph(screen, graph):
        # return
        for from_n_id, edge_dict in graph.edges.items():
            for to_n_id, edge in edge_dict.items():
                from_node = graph.nodes[from_n_id]
                to_node = graph.nodes[to_n_id]
                pygame.draw.line(screen, (255, 0, 0), (from_node.world_x, from_node.world_y),
                                 (to_node.world_x, to_node.world_y), 3)
        for node in sorted(graph.nodes.values(), key=lambda a: a.layer_index):
            pygame.draw.circle(screen, (255 - 80 * node.layer_index, 255, 80 * node.layer_index, 50),
                               (node.world_x, node.world_y), 12 - 3 * node.layer_index)


    def main():

        json_map_data = json.load(open("tunnel2.json"))
        # map_info = load_map_info(json_map_data)  # MapInfo -> layers:[LayerInfo, ...], tilesets: {gid: TileInfo}
        import pytmxloader
        map_info = pytmxloader.load_map_from_data(json_map_data)  # MapInfo -> layers:[LayerInfo, ...], tilesets: {gid: TileInfo}

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE, pygame.SRCALPHA)

        # load images and create sprites
        _img_cache = {}  # {key: img}
        load_images(map_info, _img_cache)

        graph = load_graph(map_info)

        sprite_layers = create_sprite_layers(map_info)
        layer_visibility = [_layer.visible for _layer in map_info.layers if _layer.layer_type == "tilelayer"]
        sprites = [_spr for _layer_idx, _layer in enumerate(sprite_layers) for _spr in _layer if
                   layer_visibility[_layer_idx]]
        current_layer_idx = 0

        # hero
        start_node = graph.nodes.values()[-1]
        hero_sprite = Sprite(start_node.world_x, start_node.world_y, pygame.Surface((15, 15), pygame.SRCALPHA), start_node.layer_index + 0.5 - 1.0)
        sprites.append(hero_sprite)
        vel = 80
        dx = 0
        dy = 0
        current_node = start_node
        current_edge = None

        hero_mode = 1

        def sort_key(_s): return _s.layer, _s.pos[0], _s.pos[1]
        sprites.sort(key=sort_key)
        angles = {e_o: 0, n_o: 90, w_o: 180, s_o: 270}

        clock = pygame.time.Clock()
        screen_blit = screen.blit
        running = True
        while running:
            # events
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_SPACE:
                        current_layer_idx += 1
                        current_layer_idx %= len(layer_visibility)
                    elif event.key == pygame.K_v:
                        layer_visibility[current_layer_idx] = not layer_visibility[current_layer_idx]
                        if layer_visibility[current_layer_idx]:
                            # is visible, add sprites
                            sprites.extend(sprite_layers[current_layer_idx])
                        else:
                            # is invisible now, remove sprites
                            for spr in sprite_layers[current_layer_idx]:
                                sprites.remove(spr)
                        sprites.sort(key=sort_key)
                    elif event.key == pygame.K_m:
                        hero_mode += 1
                        hero_mode %= 3
                    elif event.key == pygame.K_a:
                        hero_sprite.layer -= 1
                        sprites.sort(key=sort_key)
                    elif event.key == pygame.K_q:
                        hero_sprite.layer += 1
                        sprites.sort(key=sort_key)
                    elif hero_mode == 0:
                        dx = 0
                        dy = 0
                        if event.key == pygame.K_DOWN:
                            dy += 1
                        elif event.key == pygame.K_UP:
                            dy -= 1
                        elif event.key == pygame.K_LEFT:
                            dx -= 1
                        elif event.key == pygame.K_RIGHT:
                            dx += 1
                    elif hero_mode >= 1:
                        key_direction = None
                        if event.key == pygame.K_DOWN:
                            key_direction = s_o
                        elif event.key == pygame.K_UP:
                            key_direction = n_o
                        elif event.key == pygame.K_LEFT:
                            key_direction = w_o
                        elif event.key == pygame.K_RIGHT:
                            key_direction = e_o
                        if key_direction:
                            nx = current_node.world_x - hx
                            ny = current_node.world_y - hy
                            n_dist_sq = nx * nx + ny * ny
                            if n_dist_sq < 32 * 32:

                                if key_direction in current_node.edges:
                                    node_edges = graph.edges[current_node.id]
                                    for to_id, edge in node_edges.items():
                                        if edge.direction == key_direction:
                                            current_node = graph.nodes[to_id]
                                            current_edge = edge
                                            if edge.layer_change != 0:
                                                hero_sprite.layer += edge.layer_change
                                                sprites.sort(key=sort_key)
                                            break

                elif event.type == pygame.KEYUP:
                    if event.key == pygame.K_DOWN:
                        dy -= 1
                    elif event.key == pygame.K_UP:
                        dy += 1
                    elif event.key == pygame.K_LEFT:
                        dx += 1
                    elif event.key == pygame.K_RIGHT:
                        dx -= 1

            delta_t = clock.tick() / 1000.0  # seconds
            current_time = pygame.time.get_ticks() / 1000.0  # seconds
            pulse_value = math.sin(current_time * 2.0 * math.pi / 0.5)
            print(clock.get_fps())

            # draw
            # for idx_layer, col in enumerate(sprite_layers):
            #     if layer_visibility[idx_layer]:
            #         for row in col:
            #             for spr in row:
            #                 screen.blit(spr.image, spr.pos)
            for spr in sprites:
                screen_blit(spr.image, spr.pos)
            # draw_graph(screen, graph)
            hero_sprite.image.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
            # pygame.draw.circle(hero_sprite.image, (255, 255, 255), (7, 7), int(5 + pulse_value * 3))
            # arc(Surface, color, Rect, start_angle, stop_angle, width=1)
            angle = 0.5 * math.radians(45 * pulse_value + 45)
            pygame.draw.arc(hero_sprite.image, (255, 255, 255), hero_sprite.image.get_rect(), angle, 2 * math.pi - angle, 7)
            if current_edge:
                hero_sprite.image = pygame.transform.rotate(hero_sprite.image, angles[current_edge.direction])
            hx, hy = hero_sprite.pos
            if hero_mode == 0:
                hero_sprite.pos = (hx + dx * vel * delta_t, hy + dy * vel * delta_t)
            elif hero_mode >= 1:
                nx = current_node.world_x - hx
                ny = current_node.world_y - hy
                n_dist_sq = nx * nx + ny * ny
                if n_dist_sq > 3 * 3:
                    # too far away, move to node
                    n_dist = n_dist_sq ** 0.5
                    dx = nx / n_dist
                    dy = ny / n_dist
                    hero_sprite.pos = (hx + dx * vel * delta_t, hy + dy * vel * delta_t)
                else:
                    if current_edge is not None and hero_mode == 2:
                        # close enough to move along next edge if direction matches
                        key_direction = current_edge.direction
                        if key_direction in current_node.edges:
                            node_edges = graph.edges[current_node.id]
                            for to_id, edge in node_edges.items():
                                if edge.direction == key_direction:
                                    current_node = graph.nodes[to_id]
                                    current_edge = edge
                                    if edge.layer_change != 0:
                                        hero_sprite.layer += edge.layer_change
                                        sprites.sort(key=sort_key)
                                    break
            else:
                raise Exception("unknown hero mode " + hero_mode)

            pygame.display.flip()
            screen.fill((255, 0, 255))

        pygame.quit()


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex.__class__.__name__))
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    # sys.stdin.readline()
