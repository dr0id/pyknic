# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Demo to generate a special tileset to create a connected graph for navigation.
"""
from __future__ import print_function, division


# sys.path.insert(0, '../')  # find the lib is only the repository has been cloned
import itertools

try:
    import sys
    import traceback

    import os

    # os.environ['SDL_VIDEODRIVER'] = 'dummy'
    import pygame

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2016"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    class TileSetInfo(object):
        def __init__(self, name, tile_w, tile_h, tiles, num_tiles_x):
            self.name = name
            self.tile_width = tile_w
            self.tile_height = tile_h
            self.tile_count = len(tiles)
            self.num_tiles_x = num_tiles_x
            self.image_source = ""  # image file name
            self.image_trans = ""
            self.image_width = ""
            self.image_height = ""
            self.tiles = tiles


    class Tile(object):
        def __init__(self, tile_id, image, properties):
            self.tile_id = tile_id
            self.properties = properties  # {name: value}
            self.image = image

    #
    #              n_i n_o
    #                | ^
    #                v |
    #         w_o <- +-+ <- e_i
    #                |*|
    #         w_i -> +-+ -> e_o
    #                | ^
    #                v |
    #              s_o s_i
    #

    e_i = 1 << 0  # east in
    e_o = 1 << 1  # east out
    s_i = 1 << 2  # south in
    s_o = 1 << 3  # south out
    w_i = 1 << 4  # west in
    w_o = 1 << 5  # west out
    n_i = 1 << 6  # north in
    n_o = 1 << 7  # north out

    se_i = 1 << 8  # south east in
    se_o = 1 << 9  # south east out
    sw_i = 1 << 10  # south west in
    sw_o = 1 << 11  # south west out
    nw_i = 1 << 12  # north west in
    nw_o = 1 << 13  # north west out
    ne_i = 1 << 14  # north east in
    ne_o = 1 << 15  # north east out

    edge_in_set = {e_i, s_i, w_i, n_i, se_i, sw_i, nw_i, ne_i}
    edge_out_set = {e_o, s_o, w_o, n_o, se_o, sw_o, nw_o, ne_o}
    edges_set = edge_in_set | edge_out_set
    all_edges = reduce(lambda a, b: a | b, edges_set)

    rot45_mapping = {
        e_i: se_i,
        e_o: se_o,

        s_i: sw_i,
        s_o: sw_o,

        w_i: nw_i,
        w_o: nw_o,

        n_i: ne_i,
        n_o: ne_o,

        se_i: s_i,
        se_o: s_o,

        sw_i: w_i,
        sw_o: w_o,

        nw_i: n_i,
        nw_o: n_o,

        ne_i: e_i,
        ne_o: e_o,
    }

    property_mapping = {
        e_i: 'ei',
        e_o: 'eo',

        s_i: 'si',
        s_o: 'so',

        w_i: 'wi',
        w_o: 'wo',

        n_i: 'ni',
        n_o: 'no',

        se_i: 'sei',
        se_o: 'seo',

        sw_i: 'swi',
        sw_o: 'swo',

        nw_i: 'nwi',
        nw_o: 'nwo',

        ne_i: 'nei',
        ne_o: 'neo',
    }

    bi_e = e_i | e_o
    bi_s = s_i | s_o
    bi_w = w_i | w_o
    bi_n = n_i | n_o

    bi_se = se_i | se_o
    bi_sw = sw_i | sw_o
    bi_nw = nw_i | nw_o
    bi_ne = ne_i | ne_o

    bi_edges = {bi_e, bi_s, bi_w, bi_n, bi_se, bi_sw, bi_nw, bi_ne}


    def match(node, edges):
        if ((node | all_edges) ^ all_edges) != 0:
            return True

        edges = set(edges)
        for b in edges:
            if b & node:
                return True

        return False


    def rotate45(node):
        rotated = 0

        for key_direction in rot45_mapping.keys():
            if node & key_direction:
                rotated |= rot45_mapping[key_direction]
        return rotated


    def rotate90(node):
        return rotate45(rotate45(node))


    def rotate135(node):
        return rotate45(rotate90(node))


    def rotate180(node):
        return rotate90(rotate90(node))

    def rotate225(node):
        return rotate45(rotate180(node))

    def rotate270(node):
        return rotate90(rotate180(node))


    def rotate315(node):
        return rotate45(rotate270(node))

    def rotate_nodes(nodes, func):
        return set([func(x) for x in nodes])


    def print_nodes(name, nodes):
        _nodes = nodes
        max_count = 200
        if len(nodes) > max_count:
            _nodes = []
            for idx, val in enumerate(nodes):
                _nodes.append(val)
                if idx > max_count:
                    break
        print("'{0}'".format(name), len(nodes), map(bin, _nodes))
        # print("'{0}'".format(name), len(nodes), [bin(x) for x in _nodes])


    def remove_rotational_symmetry(nodes):
        rotational_unique_nodes = set()
        duplicates = set()
        for node in nodes:
            r45 = rotate45(node)
            r90 = rotate90(node)
            r135 = rotate135(node)
            r180 = rotate180(node)
            r225 = rotate225(node)
            r270 = rotate270(node)
            r315 = rotate315(node)
            if r45 not in rotational_unique_nodes and \
                    r90 not in rotational_unique_nodes and \
                    r135 not in rotational_unique_nodes and \
                    r180 not in rotational_unique_nodes and \
                    r225 not in rotational_unique_nodes and \
                    r270 not in rotational_unique_nodes and \
                    r315 not in rotational_unique_nodes:
                rotational_unique_nodes.add(node)
            else:
                duplicates.add(node)
        return rotational_unique_nodes, duplicates


    def filter_edges(edges, excluded):
        return set([edge for edge in edges if not match(edge, excluded)])


    def get_basis(excluded):
        basis_in = all_combinations(edge_in_set, excluded)
        basis_out = all_combinations(edge_out_set, excluded)
        return sorted(basis_in), sorted(basis_out)


    def create_tile_properties(node):
        props = {}
        edges_names = []

        for edge in edges_set:
            if node & edge:
                value = property_mapping[edge]
                edges_names.append(value)
        props['edges'] = ",".join(edges_names)

        return props


    def all_combinations(edges, excluded=set()):
        def func(a, b): return a | b
        combinations = [0]
        for i in range(len(edges)):
            combinations += [reduce(func, x) for x in itertools.combinations(edges, i+1)]

        combinations = filter_edges(combinations, excluded)
        return combinations


    bi_edges_combinations = all_combinations(bi_edges)


    def convert_color(color):
        if len(color) == 3:
            return color[1], color[0], color[2]
        return color[1], color[0], color[2], color[3]


    def create_tile_image(node, tile_w, tile_h):
        pygame.event.pump()  # just to make sure that pygame does not freeze

        #                            w/2
        #                             |                 w
        #           +-----------------|-----------------+
        #           |            |n1  |  n3|            |
        #           |            |    |    |            |
        #           |            *n2  |  n4v            |
        #           |          +------|------+          |
        #           |--------->|      |      |*---------|
        #           |w3      w4|      |      |e2      e1|
        #     h/2 --------------------+--------------------
        #           |w1      w2|      |      |e4      e3|
        #           |---------*|      |      |<---------|
        #           |          +------|------+          |
        #           |            ^s4  |  s2*            |
        #           |            |    |    |            |
        #           |            |s3  |  s1|            |
        #         h +-----------------|-----------------+
        #                             |
        #

        # TODO: make the value a fraction of the tile size!
        half_w = tile_w // 2
        half_h = tile_h // 2
        node_half_size = tile_w // 16 + 1  # 5
        c = node_half_size / 3.0
        one_over_root2 = 1.0 / (2**0.5)
        d = c * one_over_root2
        # edge_dist = node_half_size - 1  # 4
        import math
        edge_dist = int(math.ceil(d))
        c = int(math.ceil(c)) + 1
        padding_x = padding_y = 3
        edge_len_x = half_w - node_half_size - padding_x
        edge_len_y = half_h - node_half_size - padding_y
        arrow_length = edge_len_x // 3 + 1  # 15
        arrow_width = arrow_length // 3 + 1  # 5
        line_width = 1
        node_border_size = 2
        edge_color = (255, 255, 0)  # yellow
        bi_edge_color = (255, 178, 0)  # purple
        node_color = (255, 255, 0, 100)  # yellow
        bi_node_color = (255, 178, 0, 100)  # yellow
        node_border_color = (255, 0, 0)  # red
        bi_node_border_color = (255, 0, 255)  # purple
        edge_color = (0, 200, 255)  # yellow
        bi_edge_color = (0, 255, 255)  # purple
        node_color = (128, 128, 128, 255)
        bi_node_color = node_color
        node_border_color = edge_color
        bi_node_border_color = bi_edge_color

        assert node_half_size > edge_dist

        e1 = (tile_w, half_h - edge_dist)
        e2 = (tile_w - edge_len_x, half_h - edge_dist)
        e3 = (tile_w, half_h + edge_dist)
        e4 = (tile_w - edge_len_x, half_h + edge_dist)
        e5 = (tile_w - edge_len_x + arrow_length, half_h + edge_dist + arrow_width)

        s1 = (half_w + edge_dist, tile_h)
        s2 = (half_w + edge_dist, tile_h - edge_len_y)
        s3 = (half_w - edge_dist, tile_h)
        s4 = (half_w - edge_dist, tile_h - edge_len_y)
        s5 = (half_w - edge_dist - arrow_width, tile_h - edge_len_y + arrow_length)

        w1 = (0, half_h + edge_dist)
        w2 = (0 + edge_len_x, half_h + edge_dist)
        w3 = (0, half_h - edge_dist)
        w4 = (0 + edge_len_x, half_h - edge_dist)
        w5 = (0 + edge_len_x - arrow_length, half_h - edge_dist - arrow_width)

        n1 = (half_w - edge_dist, 0)
        n2 = (half_w - edge_dist, edge_len_y)
        n3 = (half_w + edge_dist, 0)
        n4 = (half_w + edge_dist, edge_len_y)
        n5 = (half_w + edge_dist + arrow_width, edge_len_y - arrow_length)

        se1 = (tile_w, tile_h - c)
        se2 = (half_w + node_half_size + padding_x, half_h + node_half_size + padding_y - c)
        se3 = (tile_w - c, tile_h)
        se4 = (half_w + node_half_size + padding_x - c, half_h + node_half_size + padding_y)
        se5 = (half_w + node_half_size + padding_x - c + arrow_length * one_over_root2 - arrow_width * one_over_root2,
               half_h + node_half_size + padding_y     + arrow_length * one_over_root2 + arrow_width * one_over_root2
               )

        sw1 = (0 + c, tile_h)
        sw2 = (half_w - node_half_size - padding_x + c, half_h + node_half_size + padding_y)
        sw3 = (0, tile_h - c)
        sw4 = (half_w - node_half_size - padding_x, half_h + node_half_size + padding_y - c)
        sw5 = (half_w - node_half_size - padding_x     - arrow_length * one_over_root2 - arrow_width * one_over_root2,
               half_h + node_half_size + padding_y - c + arrow_length * one_over_root2 - arrow_width * one_over_root2
               )

        nw1 = (0, c)
        nw2 = (half_w - node_half_size - padding_x, half_h - node_half_size - padding_y + c)
        nw3 = (c, 0)
        nw4 = (half_w - node_half_size - padding_x + c, half_h - node_half_size - padding_y)
        nw5 = (half_w - node_half_size - padding_x + c - arrow_length * one_over_root2 + arrow_width * one_over_root2,
               half_h - node_half_size - padding_y     - arrow_length * one_over_root2 - arrow_width * one_over_root2
               )

        ne1 = (tile_w - c, 0)
        ne2 = (half_w + node_half_size + padding_x - c, half_h - node_half_size - padding_y)
        ne3 = (tile_w, 0 + c)
        ne4 = (half_w + node_half_size + padding_x, half_h - node_half_size - padding_y + c)
        ne5 = (half_w + node_half_size + padding_x     + arrow_length * one_over_root2 + arrow_width * one_over_root2,
               half_h - node_half_size - padding_y + c - arrow_length * one_over_root2 + arrow_width * one_over_root2
               )

        c_min = (edge_len_x + padding_x, edge_len_y + padding_y)

        point_map = {
            e_o: [(e1, e2)],
            e_i: [(e3, e4, e5)],
            bi_e: [(e1, e2), (e3, e4, e5)],

            s_o: [(s1, s2)],
            s_i: [(s3, s4, s5)],
            bi_s: [(s1, s2), (s3, s4, s5)],

            w_o: [(w1, w2)],
            w_i: [(w3, w4, w5)],
            bi_w: [(w1, w2), (w3, w4, w5)],

            n_o: [(n1, n2)],
            n_i: [(n3, n4, n5)],
            bi_n: [(n1, n2), (n3, n4, n5)],

            se_o: [(se1, se2)],
            se_i: [(se3, se4, se5)],
            bi_se: [(se1, se2), (se3, se4, se5)],

            sw_o: [(sw1, sw2)],
            sw_i: [(sw3, sw4, sw5)],
            bi_sw: [(sw1, sw2), (sw3, sw4, sw5)],

            nw_o: [(nw1, nw2)],
            nw_i: [(nw3, nw4, nw5)],
            bi_nw: [(nw1, nw2), (nw3, nw4, nw5)],

            ne_o: [(ne1, ne2)],
            ne_i: [(ne3, ne4, ne5)],
            bi_ne: [(ne1, ne2), (ne3, ne4, ne5)],
        }

        surf = pygame.Surface((tile_w, tile_h), pygame.SRCALPHA, 32)
        surf.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)  # transparent surface

        # # hack!!
        # pygame.draw.lines(surf, bi_edge_color, False, (se1, se2), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (se3, se4, se5), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (nw1, nw2), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (nw3, nw4, nw5), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (sw1, sw2), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (sw3, sw4, sw5), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (ne1, ne2), line_width)
        # pygame.draw.lines(surf, bi_edge_color, False, (ne3, ne4, ne5), line_width)

        # draw edges
        for ee in bi_edges:
            if node & ee == ee:
                for points in point_map[ee]:
                    pygame.draw.lines(surf, convert_color(bi_edge_color), False, points, line_width)
                continue
            for e in edges_set:
                if node & e and e & ee:
                    for points in point_map[e]:
                        pygame.draw.lines(surf, convert_color(edge_color), False, points, line_width)

        # draw node center
        node_rect = pygame.Rect(c_min, (2 * node_half_size, 2 * node_half_size))
        n_color = node_color
        b_color = node_border_color
        if node in bi_edges_combinations:
            n_color = bi_node_color
            b_color = bi_node_border_color
        pygame.draw.rect(surf, convert_color(n_color), node_rect, 0)
        pygame.draw.rect(surf, convert_color(b_color), node_rect, node_border_size)
        # pygame.draw.circle(surf, n_color, (half_w, half_h), node_half_size + 1, 0)
        # pygame.draw.circle(surf, b_color, (half_w, half_h), node_half_size + 1, node_border_size)

        return surf


    def create_tileset_info(nodes, name, tile_w, tile_h, num_tiles_x):
        tiles = []
        for tile_id, node in enumerate(nodes):

            img = create_tile_image(node, tile_w, tile_h)
            properties = create_tile_properties(node)

            tile = Tile(tile_id, img, properties)
            tiles.append(tile)

        tileset_info = TileSetInfo(name, tile_w, tile_h, tiles, num_tiles_x)
        return tileset_info


    def create_tileset_info_from_basis(nodes, name, tile_w, tile_h, num_tiles_x):
        tile_nodes = []
        for y in nodes[1]:
            for x in nodes[0]:
                tile_nodes.append(x | y)

        # re-add the diagonal elements at front again
        # now the rows have one element more
        num_tiles_x_ = num_tiles_x + 1
        for idx in range(len(tile_nodes) // num_tiles_x):
            row_idx = idx
            diagonal_tile = tile_nodes[row_idx * num_tiles_x + row_idx + row_idx]
            tile_nodes.insert(row_idx * num_tiles_x_, diagonal_tile)

        return create_tileset_info(tile_nodes, name, tile_w, tile_h, num_tiles_x_)


    def save_image(tileset_info, tile_w, tile_h, image_name):
        num_tiles_x = tileset_info.tile_count
        num_tiles_y = 1
        if tileset_info.num_tiles_x:
            num_tiles_x = tileset_info.num_tiles_x
            num_tiles_y = tileset_info.tile_count // num_tiles_x
        img_w = num_tiles_x * tile_w
        img_h = num_tiles_y * tile_h

        surf = pygame.Surface((img_w, img_h), pygame.SRCALPHA, 32)
        surf.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)

        for tile in tileset_info.tiles:
            x = tile.tile_id
            y = 0
            if tileset_info.num_tiles_x:
                x = tile.tile_id % tileset_info.num_tiles_x
                y = tile.tile_id // tileset_info.num_tiles_x
            surf.blit(tile.image, (x * tile_w, y * tile_h))

        image_name = check_and_get_numbered_name(image_name)

        pygame.image.save(surf, image_name)
        print("written file '{0}'".format(image_name))

        tileset_info.image_source = image_name
        tileset_info.image_height = img_h
        tileset_info.image_width = img_w
        tileset_info.image_trans = "ff00ff"  # TODO: param?


    def check_and_get_numbered_name(image_name):
        root, ext = os.path.splitext(image_name)
        name = root
        i = 0
        while os.path.exists(name + ext):
            i += 1
            name = root + str(i)
        return name + ext


    def save_tsx(tileset_info):
        assert tileset_info.image_source != ""
        assert tileset_info.image_height != 0
        assert tileset_info.image_width != 0
        assert tileset_info.image_trans != ""

        # <?xml version="1.0" encoding="UTF-8"?>
        # <tileset name="graphtiles" tilewidth="64" tileheight="64" tilecount="108">
        #  <image source="graphtileset64x64.png" trans="ff00ff" width="800" height="600"/>
        #  <tile id="0">
        #   <properties>
        #    <property name="aaa" value="aaa"/>
        #   </properties>
        #  </tile>
        # </tileset>
        content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        content += "<tileset name=\"{0}\" tilewidth=\"{1}\" tileheight=\"{2}\" tilecount=\"{3}\">\n".format(
                tileset_info.name, tileset_info.tile_width, tileset_info.tile_height, tileset_info.tile_count)
        content += " <image source=\"{0}\" trans=\"{1}\" width=\"{2}\" height=\"{3}\"/>\n".format(
            tileset_info.image_source, tileset_info.image_trans, tileset_info.image_width, tileset_info.image_height)
        for tile in tileset_info.tiles:
            content += " <tile id=\"{0}\">\n".format(tile.tile_id)
            content += "  <properties>\n"
            for prop_key in tile.properties.keys():
                content += "   <property name=\"{0}\" value=\"{1}\"/>\n".format(prop_key, tile.properties[prop_key])
            content += "  </properties>\n"
            content += " </tile>\n"
        content += "</tileset>\n"

        name = tileset_info.name + ".tsx"
        name = check_and_get_numbered_name(name)

        with open(name, 'w') as new_file:
            new_file.write(content)

        print("written file '{0}'".format(name))


    def main(args=None):
        """
        The main  function.

        :param args: Argument list to use. This is a list of strings like sys.argv. See -h for complete argument list.
            Default=None so the sys.args are used.
        """
        # # check get_all_edge_combinations
        # assert list(all_combinations(edges_set))[0] == 0
        # assert len(all_combinations(edges_set, edges_set)) == 1
        # __turn = all_combinations(edges_set, {w_i, w_o, n_i, n_o})
        # assert sum([n & w_i for n in __turn]) == 0
        # assert sum([n & w_o for n in __turn]) == 0
        # assert sum([n & n_i for n in __turn]) == 0
        # assert sum([n & n_o for n in __turn]) == 0

        # check match
        assert match(e_i | e_o, [all_edges ^ (e_i | e_o)]) == False
        assert match(s_i | s_o, [all_edges ^ (s_i | s_o)]) == False
        assert match(w_i | w_o, [all_edges ^ (w_i | w_o)]) == False
        assert match(n_i | n_o, [all_edges ^ (n_i | n_o)]) == False
        assert match(all_edges, [e_i])
        assert match(all_edges, [e_o])
        assert match(all_edges, [s_i])
        assert match(all_edges, [s_o])
        assert match(all_edges, [w_i])
        assert match(all_edges, [w_o])
        assert match(all_edges, [n_i])
        assert match(all_edges, [n_o])

        # check rotation
        assert rotate90(e_i | e_o) == s_i | s_o
        assert rotate180(e_i | e_o) == w_i | w_o
        assert rotate270(e_i | e_o) == n_i | n_o
        assert rotate90(rotate270(e_i | e_o)) == e_i | e_o
        assert rotate_nodes([e_i | e_o], rotate90) == {s_i | s_o}
        assert rotate_nodes([e_i | e_o], rotate180) == {w_i | w_o}
        assert rotate_nodes([e_i | e_o], rotate270) == {n_i | n_o}
        assert rotate_nodes(rotate_nodes([e_i | e_o], rotate270), rotate90) == {e_i | e_o}

        assert remove_rotational_symmetry([e_i, s_i, w_i, n_i])[0] == {e_i}

        # no_edges = all_combinations(edges_set, edges_set)
        # assert no_edges == {0}

        # turn_excluded_edges = {w_i, w_o, n_i, n_o, se_i, se_o, sw_i, sw_o, nw_i, nw_o, ne_i, ne_o}
        # turn = all_combinations(edges_set, turn_excluded_edges)
        # reduced_turn, duplicated_turn = remove_rotational_symmetry(turn)
        # turn_basis = get_basis(turn_excluded_edges)
        #
        # t_shape_excluded_edges = {n_i, n_o, se_i, se_o, sw_i, sw_o, nw_i, nw_o, ne_i, ne_o}
        # t_shape = all_combinations(edges_set, t_shape_excluded_edges)
        # reduced_t_shape, duplicated_t_shape = remove_rotational_symmetry(t_shape)
        # t_shape_basis = get_basis(t_shape_excluded_edges)

        cross_excluded_edges = {se_i, se_o, sw_i, sw_o, nw_i, nw_o, ne_i, ne_o}
        # cross = all_combinations(edges_set, cross_excluded_edges)
        # reduced_cross, duplicated_cross = remove_rotational_symmetry(cross)
        cross_basis = get_basis(cross_excluded_edges)

        # # printing
        # # for d in (e_i, e_o, s_i, s_o, w_i, w_o, n_i, n_o):
        # #     print(bin(d), hex(d), d)
        # print_nodes("edges e (in/out)", [e_i, e_o])
        # print_nodes("edges s (in/out)", [s_i, s_o])
        # print_nodes("edges w (in/out)", [w_i, w_o])
        # print_nodes("edges n (in/out)", [n_i, n_o])
        # print_nodes("edges se (in/out)", [se_i, se_o])
        # print_nodes("edges sw (in/out)", [sw_i, sw_o])
        # print_nodes("edges nw (in/out)", [nw_i, nw_o])
        # print_nodes("edges ne (in/out)", [ne_i, ne_o])
        #
        # print('all edges', bin(all_edges), hex(all_edges), all_edges)
        # print_nodes('no edges', no_edges)
        # print_nodes('turn', turn)
        # print_nodes('t shape', t_shape)
        # print_nodes('cross', cross)
        # # print('90', bin(rotate90(e_i + e_o)))
        # # print('180', bin(rotate180(e_i + e_o)))
        # # print('270', bin(rotate270(e_i + e_o)))
        # # print('360', bin(rotate90(rotate270(e_i + e_o))))
        #
        # print_nodes("turn reduced", reduced_turn)
        # print_nodes("turn duplicated", duplicated_turn)
        # print_nodes("t shape reduced", reduced_t_shape)
        # print_nodes("t shape duplicated", duplicated_t_shape)
        # print_nodes("cross reduced", reduced_cross)
        # print_nodes("cross duplicated", duplicated_cross)
        #
        # print_nodes("turn basis  in", turn_basis[0])
        # print_nodes("turn basis out", turn_basis[1])
        #
        # print_nodes("t shape basis  in", t_shape_basis[0])
        # print_nodes("t shape basis out", t_shape_basis[1])

        print_nodes("cross basis  in", cross_basis[0])
        print_nodes("cross basis out", cross_basis[1])

        # generating files
        pygame.init()
        screen = pygame.display.set_mode((1024, 1024), 0, 32)
        pygame.event.pump()
        w = 64
        h = 64
        # turn_tileset_info = create_tileset_info(reduced_cross, 'turntileset', w, h)
        turn_tileset_info = create_tileset_info_from_basis(cross_basis, 'graphtileset', w, h, len(cross_basis[0]))

        image_name = "graphTiles.png"
        save_image(turn_tileset_info, w, h, image_name)
        save_tsx(turn_tileset_info)

        # quick hack to see what I'm doing!!
        # pygame.draw.line(screen, (255, 0, 0, 100), (0, h//2), (1024, h//2), 1)
        for tile in turn_tileset_info.tiles:
            x = tile.tile_id % (1024 // w)
            y = tile.tile_id // (1024 // w)
            if turn_tileset_info.num_tiles_x:
                x = tile.tile_id % turn_tileset_info.num_tiles_x
                y = tile.tile_id // turn_tileset_info.num_tiles_x
            screen.blit(tile.image, (x * w, y * h))

        pygame.display.flip()
        running = True
        while running:
            event = pygame.event.wait()
            if event.type == pygame.KEYDOWN:
                running = False

        print(turn_tileset_info)


    if __name__ == '__main__':  # pragma: no cover
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    # sys.stdin.readline()
