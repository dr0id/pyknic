# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'textcomparer.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

try:
    import sys
    import traceback
    import logging

    logging.basicConfig()
    import pygame

    import os
    import collections
    import time

    import pygametext
    import ptext
    # ptext = pygametext

    import sys
    for i in sorted(sys.modules):
        if i.startswith('numpy'):
            print("WARNING USING:", i)
            break

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2017"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 768, 1024


    def main():

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE)

        text = "Tom & Jerry T"

        # named tuple class with default values:
        # https://ceasarjames.wordpress.com/2012/03/19/how-to-use-default-arguments-with-namedtuple/
        class TextParams(
            collections.namedtuple("TextParams",
                "text fontname fontsize sysfontname bold italic underline width widthem strip color background "
                "antialias ocolor owidth scolor shadow gcolor shade alpha align lineheight pspace angle cache")):
            def __new__(cls, text, fontname=None, fontsize=None, sysfontname=None, bold=None, italic=None,
                        underline=None, width=None, widthem=None, strip=None, color=None,
                        background=None, antialias=True, ocolor=None, owidth=None, scolor=None, shadow=None,
                        gcolor=None, shade=None, alpha=1.0, align=None, lineheight=None, pspace=None, angle=0,
                        cache=True):
                # add default values
                return super(TextParams, cls).__new__(cls, text, fontname, fontsize, sysfontname, bold, italic,
                                                      underline, width, widthem, strip, color,
                                                      background, antialias, ocolor, owidth, scolor, shadow,
                                                      gcolor, shade, alpha, align, lineheight, pspace, angle,
                                                      cache)

        images = []
        font_size = 45
        create_images(images, TextParams(text, alpha=1.0, gcolor="blue", fontsize=font_size, shadow=(10, 10), color=(100, 100, 255)))
        create_images(images, TextParams(text, alpha=0.5, gcolor="blue", fontsize=font_size, shadow=(10, 10), color=(100, 100, 255)))
        create_images(images, TextParams(text, fontsize=font_size, color=(100, 100, 255), ocolor=(255, 255, 0), owidth=5))
        create_images(images, TextParams(text, fontsize=font_size, color=(255, 255, 255, 0), ocolor=(255, 255, 0), owidth=5))
        create_images(images, TextParams(text, fontsize=font_size, color=(0, 0, 0, 0), ocolor=(255, 255, 0), owidth=5))
        create_images(images, TextParams(text, alpha=0.5, fontsize=font_size, shadow=(10, 10), color=(100, 100, 255)))

        create_images(images, TextParams(text, fontsize=font_size))
        create_images(images, TextParams(text, fontsize=font_size, sysfontname="freesans", italic=True, underline=True))
        create_images(images, TextParams(text, fontsize=font_size, ))
        create_images(images, TextParams(text, fontsize=font_size, width=180, lineheight=1.5))  # this one behaves strange, only first 'T' is displayed!!
        create_images(images, TextParams(text, fontsize=font_size, owidth=1.5, ocolor=(255, 255, 0), color=(0, 0, 0)))
        create_images(images, TextParams(text, fontsize=font_size, shadow=(2, 2), scolor="#202020"))
        create_images(images, TextParams(text, fontsize=font_size, color="red", gcolor="purple"))
        create_images(images, TextParams(text, fontsize=font_size, alpha=0.1))
        create_images(images, TextParams(text, fontsize=font_size, angle=90))
        create_images(images, TextParams(text, fontsize=font_size, underline=True, color="#AAFF00", gcolor="#66AA00", owidth=1.5, ocolor="black", alpha=0.8))
        create_images(images, TextParams(text, fontsize=font_size, italic=True, underline=True, color="#AAFF00", gcolor="#66AA00", owidth=1.5, ocolor="black", alpha=0.8))

        _faster_count = 0
        _faster_sum = 0
        for _i, _o, r in images:
            if r >= 1:
                _faster_count += 1
                _faster_sum += r
        print("summary: total {0}, faster {1}, slower {2}  %faster {3}".format(len(images), _faster_count, len(images) - _faster_count, _faster_sum / _faster_count))

        running = True
        while running:
            # events
            for event in [pygame.event.wait()]:

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False

            # draw
            px = py = 0
            for pimg, oimg, _sr in images:
                screen.blit(pimg, (0, py))
                _w, _h = pimg.get_size()
                screen.blit(oimg, (_w, py))
                py += _h

            pygame.display.flip()
            screen.fill((255, 0, 255))

        pygame.quit()


    def create_images(images, params):
        _s1 = time.time()
        ptext_img = ptext.getsurf(*params)
        delta1 = time.time() - _s1

        _s2 = time.time()
        otext_img = pygametext.getsurf(*params)
        delta2 = time.time() - _s2

        _speed_ratio = delta2 / delta1 if delta1 > 0 else 1 + delta2
        print("factor: {2:<8.6}    orig: {0}  my: {1}".format(delta1, delta2, _speed_ratio))
        images.append((ptext_img, otext_img,  _speed_ratio))


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
