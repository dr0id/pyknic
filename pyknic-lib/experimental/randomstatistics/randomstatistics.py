# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'randomstatistics.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

import random

try:
    import sys
    import traceback
    import logging

    logging.basicConfig()
    import pygame

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2017"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 800, 600


    def calc_new_random_values(stats, precision, num):
        for i in range(num):
            r = random.random()
            # r = random.gauss(30, 5.5)
            v = int(round(r * precision))
            stats[v] = stats.setdefault(v, 0) + 1
        return num

    def main():

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE)

        stats = {}  # {val: count}
        precision = 1000
        count = 0
        num = 1000
        x_scale = 1
        y_scale = 1
        font = pygame.font.Font(None, 25)

        running = True
        while running:
            # events
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_SPACE:
                        count += calc_new_random_values(stats, precision, num)
            count += calc_new_random_values(stats, precision, num)

            # draw
            while max(stats.values()) * y_scale > SCREENSIZE[1]:
                y_scale *= 0.25
            while max(stats.keys()) * x_scale > SCREENSIZE[0]:
                x_scale *= 0.8

            pygame.draw.line(screen, (128, 128, 128), (0, SCREENSIZE[1]), SCREENSIZE, 1)
            for k, v in stats.items():
                x = k * x_scale
                pygame.draw.line(screen, (255, 255, 0), (x, SCREENSIZE[1]), (x, SCREENSIZE[1] - v * y_scale), 1)

            sorted_values = list(sorted(stats.items()))
            step = 50
            points = []
            for s in range(len(sorted_values) // step):
                rng = [x[1] for x in sorted_values[s * step: (s + 1) * step]]
                avg = int(round(sum(rng) / step))
                points.append((step * x_scale * (s + 0.5), SCREENSIZE[1] - avg * y_scale))
                # pygame.draw.line(screen, [100]*3, (s * step * x_scale, SCREENSIZE[1]), (s * step * x_scale, 0))

            pygame.draw.aalines(screen, (255, 0, 0), False, points)

            img = font.render(str(count), 2, (255, 255, 255))
            rect = img.get_rect(topright=(SCREENSIZE[0] - 100, 50))
            screen.blit(img, rect)

            pygame.display.flip()
            screen.fill((0, 0, 0))

        pygame.quit()


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
