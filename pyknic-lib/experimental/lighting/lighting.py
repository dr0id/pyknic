# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
lighting demo attempting some dynamic lighting in pygame using layers dropping shadows.


Layers:

bird            b                ---
street light    l2           -----------------
tree            t                  -------------------
moped light     l1       ---------------------
street          s     ---------------------------------------
street behind   g     ---------------------------------------

^ normal blit
+ blend add
* blend mult
no operator precedence except the brackets, just left to right
a = ambient color (darkness, assume nearly black)

in spritesystem this was used:
disadvantage: no sprites between layers of light, all layers are illuminated
pixel = (a + l1 + l2) * (g ^ s ^ t ^ b)

can only have max the original color
pixel = (g ^ s) * (a + l1 + l2) ^ (t * (a + l2)) ^ (b * a)

this has color compression!
pixel = (g ^ s) * a + (l1 + l2) ^ (t * a + l2) ^ (b * a)


Instructions:

escape: quit the application
right/left/down/up: move object
a: select ambient to manipulate (up/down)
r or 1: reset to original scene
2...n: other initial scene setup
F1-F12: show/hide corresponding tile
q: show/hide selection highlight
h: show/hide this help
TAB: cycle through objects (shift for back)
SPACE: toggle run main loop vs wait for event
        (also starts/stops animation)
p: print sprites information to console
w: hide current selected sprite


Flaws in the algorithms:

1: bird is illuminated
2: as it should be (hardcoded, no shadow)
3: color compression (play with ambient to
   see how color fades as darget it gets)
4: added some extra light, looks bad at 'day'
5: added extra light the darker it gets
6: same as 2, implemented in generic way
7: shadows everywhere
8: shadows eaten by other lights, no alpha mult
9: shadows eaten by other light with alpha mult


"""
from __future__ import print_function, division
from functools import lru_cache
import pygame
import tweening

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class Sprite(object):
    def __init__(self, image, name, x=0, y=0, z=0, layer=0, is_light=False, shadow=None):
        self.image = image
        self.name = name
        self.rect = image.get_rect() if image else pygame.Rect(0, 0, 0, 0)
        self.rect.topleft = int(x), int(y)
        self.is_light = is_light
        self.layer = layer
        self.z = z
        self.shadow = shadow
        self.inv_shadow = None
        if self.shadow:
            self.inv_shadow = shadow.copy()
            self.inv_shadow.fill((255, 255, 255))
            self.inv_shadow.blit(shadow, (0, 0), None, pygame.BLEND_RGB_SUB)


text_color = (255, 255, 255)
text_background_color = (0, 0, 0)
text_background_color_yellow = (255, 255, 0)


def print_scene(sprites):
    print('=' * 30)
    for spr in sprites:
        print(spr.name, spr.rect, 'z:', spr.z, 'light:', spr.is_light, 'layer:', spr.layer)
    print('=' * 30)


def main():
    screen_width = 1200
    screen_height = 900
    screen_size = screen_width, screen_height
    screen_background_color = (255, 0, 255)
    tile_background_color = (255, 0, 255, 255)
    tile_border_color = (255, 255, 255)
    object_high_light_color = (255, 255, 0)

    pygame.init()
    pygame.display.set_caption("DR0ID's lighting experiment environment")
    pygame.display.set_icon(pygame.image.load("icon.png"))
    # screen = pygame.display.set_mode(screen_size, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    screen = pygame.display.set_mode(screen_size, 0, 32)

    step = 10
    x_count = 4
    y_count = 3
    tile = pygame.Surface((screen_width // x_count, screen_height // y_count), pygame.SRCALPHA)
    tile.fill(tile_background_color)
    tile_rect = tile.get_rect()
    font = pygame.font.Font(None, 20)
    numbers = []
    for i in range(x_count * y_count):
        numbers.append(font.render(str(i + 1), 0, text_color, text_background_color))

    tweener = tweening.Tweener()

    # load sprites
    sprites, ambient_sprite, ambient = init(tile.get_size(), tweener)
    assert len(sprites) < 11
    initializer = [init, init2, init3]
    assert len(initializer) < 11
    selected = sprites[2]
    highlight_selected = True
    show_help = False
    help_image = pygame.image.load("help.png")

    is_tile_visible = [False] * x_count * y_count
    is_tile_visible[0] = True  # False
    is_tile_visible[1] = False
    is_tile_visible[2] = False  # False
    is_tile_visible[3] = False
    is_tile_visible[4] = False
    is_tile_visible[5] = True
    is_tile_visible[6] = False
    is_tile_visible[7] = True
    is_tile_visible[8] = True
    is_tile_visible[9] = False
    is_tile_visible[10] = True
    is_tile_visible[11] = True

    paused = True

    draw_methods = [draw1, draw2, draw3, draw4, draw5, draw6, draw7, draw8, draw9, draw10, draw11, draw12]
    assert len(draw_methods) >= x_count * y_count

    delta_time = 1.0

    running = True
    while running:
        # events
        # for event in pygame.event.get():
        events = [pygame.event.wait()] if paused else pygame.event.get()
        for event in events:

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    selected.rect.x += step
                elif event.key == pygame.K_LEFT:
                    selected.rect.x -= step
                elif event.key == pygame.K_UP:
                    selected.rect.y -= step
                elif event.key == pygame.K_DOWN:
                    selected.rect.y += step

                elif event.key == pygame.K_a:
                    selected = ambient_sprite

                elif event.key == pygame.K_r or event.key == pygame.K_1:
                    sprites, ambient_sprite, ambient = init(tile.get_size(), tweener)
                    selected = sprites[2]

                elif event.key in (pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5, pygame.K_6,
                                   pygame.K_7, pygame.K_8, pygame.K_9, pygame.K_0):
                    index = (event.key - pygame.K_1) % len(initializer)
                    init_method = initializer[index]
                    sprites, ambient_sprite, ambient = init_method(tile.get_size(), tweener)
                    selected = sprites[3]

                elif event.key in (
                        pygame.K_F1, pygame.K_F2, pygame.K_F3, pygame.K_F4, pygame.K_F5, pygame.K_F6, pygame.K_F7,
                        pygame.K_F8, pygame.K_F9, pygame.K_F10, pygame.K_F11, pygame.K_F12):
                    index = (event.key - pygame.K_F1) % len(is_tile_visible)
                    is_tile_visible[index] = not is_tile_visible[index]

                elif event.key == pygame.K_w:
                    # 'hide' sprite by moving if far away!
                    if selected.rect.x < 5000:
                        selected.rect.x += 10000
                        tweener.pause_tweens([selected.rect])
                    else:
                        selected.rect.x -= 10000
                        tweener.resume_tweens([selected.rect])

                elif event.key == pygame.K_q:
                    highlight_selected = not highlight_selected

                elif event.key == pygame.K_h:
                    show_help = not show_help

                elif event.key == pygame.K_TAB:
                    direction = -1 if event.mod & pygame.KMOD_SHIFT else 1
                    index = sprites.index(selected) if selected != ambient_sprite else -direction
                    index = (index + direction) % len(sprites)
                    selected = sprites[index]

                elif event.key == pygame.K_SPACE:
                    paused = not paused
                    if paused:
                        print('paused')

                elif event.key == pygame.K_p:
                    print_scene(sprites)

                elif event.key == pygame.K_t:
                    # hack
                    objs = [t.o for t in tweener._active_tweens]
                    if selected.rect not in objs:
                        tweener.create_tween(selected.rect, 'x', selected.rect.x, -100, 15, tweening.IN_OUT_SINE, None,
                                             cb_restart, [])
                    else:
                        tweener.remove_tween(selected.rect, 'x')
                elif event.key == pygame.K_PAGEUP:
                    selected.z += 5
                elif event.key == pygame.K_PAGEDOWN:
                    selected.z -= 5

                ambient_sprite.rect.y %= 255
                a_value = ambient_sprite.rect.y
                ambient = (a_value,) * 3
                # print(selected.rect, ambient)

        # draw
        for x in range(x_count):
            for y in range(y_count):
                idx = y + x * y_count
                if is_tile_visible[idx]:
                    screen_tile = tile.copy()

                    # use blit algorithm and take the time it takes
                    pg_start = pygame.time.get_ticks()
                    # start = time.time()
                    draw_methods[idx](screen_tile, ambient, sprites)
                    # duration = (time.time() - start) * 1000.0
                    duration = pygame.time.get_ticks() - pg_start

                    # draw highlight of object
                    if highlight_selected:
                        # rect
                        r = selected.rect.clip(screen_tile.get_rect())
                        pygame.draw.rect(screen_tile, object_high_light_color, r, 2)

                    # draw white border
                    pygame.draw.rect(screen_tile, tile_border_color, tile_rect, 1)

                    # number of tile and duration
                    screen_tile.blit(numbers[idx], (2, 2))
                    duration_surf = font.render(str(duration) + " ms", 0, text_color, text_background_color)
                    screen_tile.blit(duration_surf, (22, 2))

                    # blit tile
                    pos = (x * screen_width // x_count, y * screen_height // y_count)
                    screen.blit(screen_tile, pos)

        # if highlight_selected:
        # name
        _name = selected.name + (": " + str(selected.rect.y) if selected == ambient_sprite else "")
        name_surf = font.render(_name, 0, (0, 0, 0), text_background_color_yellow)
        screen.blit(name_surf, (2, 20))

        if show_help:
            help_rect = help_image.get_rect(center=(screen_width // 2, screen_height // 2))
            screen.blit(help_image, help_rect)

        if not paused:
            tweener.update(delta_time)

        pygame.display.flip()
        screen.fill(screen_background_color)

    pygame.quit()


def _y(y, h):
    return int(float(y) / 265.0 * h)


def _x(x, w):
    return int(float(x) / 340.0 * w)


def _z(z):
    return z


def _load_img(name):
    img = pygame.image.load(
        name)  # .convert_alpha()  # convert alpha does not help, it means time is lost some where else than blitting
    return img


def cb_restart(tweener, *args):
    obj, attr_name, begin, change, duration, tween_function, params, cb_end, cb_args = args
    tweener.create_tween(obj, attr_name, begin + change, -change, duration, tween_function, params, cb_end, cb_args)


def init(size, tweener):
    w, h = size
    # print(w, h)  # 340, 265   190 = w * x -> x = 190/w

    tweener.clear()

    street = Sprite(_load_img("street.png"), "street", _x(0, w), _y(0, h), _z(0), layer=0)
    tomato = Sprite(_load_img("tomato.png"), "tomato", _x(190, w), _y(90, h), _z(10), layer=1,
                    shadow=_load_img("tomato_sh.png"))
    front_light = Sprite(_load_img("frontlight.png"), "flashlight", _x(190, w), _y(80, h), _z(15), is_light=True,
                         layer=2)
    tree = Sprite(_load_img("leaves.png"), "leaves", _x(70, w), _y(90, h), _z(30), layer=5,
                  shadow=_load_img("leaves_sh.png"))
    street_light = Sprite(_load_img("streetlight.png"), "streetlight", _x(140, w), _y(90, h), _z(60), is_light=True,
                          layer=10)
    bird = Sprite(_load_img("bird.png"), "bird", _x(170, w), _y(120, h), _z(70), layer=11)

    red_light = Sprite(_load_img("red.png"), "red light", _x(260, w), _y(-10, h), _z(15), is_light=True, layer=2)
    green_light = Sprite(_load_img("green.png"), "green light", _x(260, w), _y(20, h), _z(15), is_light=True, layer=2)
    blue_light = Sprite(_load_img("blue.png"), "blue light", _x(230, w), _y(0, h), _z(15), is_light=True, layer=2)

    # tweener.create_tween(street_light.rect, 'x', street_light.rect.x, -100, 15, tweening.ease_in_out_sine, None, cb_restart, [])
    # tweener.create_tween(street_light.rect, 'y', street_light.rect.x, -100, 15, tweening.ease_in_sine2, None, cb_restart, [])

    ambient_sprite = Sprite(None, "ambient", -1, 40)
    ambient = (ambient_sprite.rect.y,) * 3
    return [street, tomato, front_light, tree, street_light, bird, red_light, green_light,
            blue_light], ambient_sprite, ambient


def init2(size, tweener):
    w, h = size
    # print(w, h)  # 340, 265   190 = w * x -> x = 190/w

    tweener.clear()

    street = Sprite(_load_img("street.png"), "street", _x(0, w), _y(0, h), _z(0), layer=0)
    tomato = Sprite(_load_img("tomato.png"), "tomato", _x(197, w), _y(101, h), _z(10), layer=1,
                    shadow=_load_img("tomato_sh.png"))
    front_light = Sprite(_load_img("frontlight.png"), "flashlight", _x(197, w), _y(90, h), _z(15), is_light=True,
                         layer=2)
    tree = Sprite(_load_img("leaves.png"), "leaves", _x(61, w), _y(101, h), _z(30), layer=5,
                  shadow=_load_img("leaves_sh.png"))
    street_light = Sprite(_load_img("streetlight.png"), "streetlight", _x(140, w), _y(61, h), _z(60), is_light=True,
                          layer=10)
    bird = Sprite(_load_img("bird.png"), "bird", _x(170, w), _y(120, h), _z(70), layer=11)

    red_light = Sprite(_load_img("red.png"), "red light", _x(260, w), _y(-10, h), _z(15), is_light=True, layer=2)
    green_light = Sprite(_load_img("green.png"), "green light", _x(260, w), _y(20, h), _z(15), is_light=True, layer=2)
    blue_light = Sprite(_load_img("blue.png"), "blue light", _x(230, w), _y(0, h), _z(15), is_light=True, layer=2)

    tweener.create_tween(street_light.rect, 'x', street_light.rect.x, -100, 15, tweening.IN_OUT_SINE, None, cb_restart,
                         [])
    # tweener.create_tween(street_light.rect, 'y', street_light.rect.x, -100, 15, tweening.ease_in_sine2, None, cb_restart, [])

    ambient_sprite = Sprite(None, "ambient", -1, 20)
    ambient = (ambient_sprite.rect.y,) * 3
    return [street, tomato, front_light, tree, street_light, bird, red_light, green_light,
            blue_light], ambient_sprite, ambient


def init3(size, tweener):
    w, h = size
    # print(w, h)  # 340, 265   190 = w * x -> x = 190/w

    tweener.clear()
    #
    # street <rect(0, 0, 524, 768)> z: 0 light: False layer: 0
    # tomato <rect(173, 44, 100, 100)> z: 10 light: False layer: 1
    # flashligth <rect(10173, 101, 125, 230)> z: 15 light: True layer: 2
    # leaves <rect(53, 114, 182, 187)> z: 30 light: False layer: 5
    # streetlight <rect(10118, 69, 200, 200)> z: 60 light: True layer: 10
    # bird <rect(150, 135, 50, 30)> z: 70 light: False layer: 11
    # red light <rect(226, -11, 100, 100)> z: 15 light: True layer: 2
    # green light <rect(227, 22, 100, 100)> z: 15 light: True layer: 2
    # blue light <rect(136, 0, 100, 100)> z: 15 light: True layer: 2

    street = Sprite(_load_img("street.png"), "street", _x(0, w), _y(0, h), _z(0), layer=0)
    tomato = Sprite(_load_img("tomato.png"), "tomato", _x(173, w), _y(44, h), _z(10), layer=1,
                    shadow=_load_img("tomato_sh.png"))
    front_light = Sprite(_load_img("frontlight.png"), "flashlight", _x(10197, w), _y(90, h), _z(15), is_light=True,
                         layer=2)
    tree = Sprite(_load_img("leaves.png"), "leaves", _x(53, w), _y(114, h), _z(30), layer=5,
                  shadow=_load_img("leaves_sh.png"))
    street_light = Sprite(_load_img("streetlight.png"), "streetlight", _x(10140, w), _y(61, h), _z(60), is_light=True,
                          layer=10)
    bird = Sprite(_load_img("bird.png"), "bird", _x(150, w), _y(135, h), _z(70), layer=11)

    red_light = Sprite(_load_img("red.png"), "red light", _x(260, w), _y(-11, h), _z(15), is_light=True, layer=2)
    green_light = Sprite(_load_img("green.png"), "green light", _x(260, w), _y(22, h), _z(15), is_light=True, layer=2)
    blue_light = Sprite(_load_img("blue.png"), "blue light", _x(260, w), _y(0, h), _z(15), is_light=True, layer=2)

    tweener.create_tween(street_light.rect, 'x', street_light.rect.x, -100, 15, tweening.IN_OUT_SINE, None, cb_restart,
                         [])

    tweener.create_tween(red_light.rect, 'x', red_light.rect.x, -100, 10, tweening.IN_OUT_SINE, None, cb_restart, [])
    tweener.create_tween(green_light.rect, 'x', green_light.rect.x, -100, 15, tweening.IN_OUT_SINE, None, cb_restart,
                         [])
    tweener.create_tween(blue_light.rect, 'x', blue_light.rect.x, -100, 20, tweening.IN_OUT_SINE, None, cb_restart, [])
    # tweener.create_tween(street_light.rect, 'y', street_light.rect.x, -100, 15, tweening.ease_in_sine2, None, cb_restart, [])

    ambient_sprite = Sprite(None, "ambient", -1, 0)
    ambient = (ambient_sprite.rect.y,) * 3
    return [street, tomato, front_light, tree, street_light, bird, red_light, green_light,
            blue_light], ambient_sprite, ambient


def draw1(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = (a + l1 + l2) * (g ^ s ^ t ^ b)
    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    a = screen  # .copy()
    a.fill(ambient)
    a.blit(front_light.image, front_light.rect, None, pygame.BLEND_ADD)
    a.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    a.blit(red_light.image, red_light.rect, None, pygame.BLEND_ADD)
    a.blit(green_light.image, green_light.rect, None, pygame.BLEND_ADD)
    a.blit(blue_light.image, blue_light.rect, None, pygame.BLEND_ADD)

    screen.blit(street.image, street.rect)
    screen.blit(tomato.image, tomato.rect)
    screen.blit(tree.image, tree.rect)
    screen.blit(bird.image, bird.rect)
    screen.blit(a, (0, 0), None, pygame.BLEND_MULT)


def draw2(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = (g ^ s) * (a + l1 + l2) ^ (t * (a + l2)) ^ (b * a)
    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    a = screen  # .copy()
    a.fill(ambient)

    screen.blit(street.image, street.rect)
    screen.blit(tomato.image, tomato.rect)

    a1 = a.copy()
    a1.blit(front_light.image, front_light.rect, None, pygame.BLEND_ADD)
    a1.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    a1.blit(red_light.image, red_light.rect, None, pygame.BLEND_ADD)
    a1.blit(green_light.image, green_light.rect, None, pygame.BLEND_ADD)
    a1.blit(blue_light.image, blue_light.rect, None, pygame.BLEND_ADD)

    screen.blit(a1, (0, 0), None, pygame.BLEND_MULT)

    a2 = a.copy()
    a2.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    t = tree.image.copy()
    t.blit(a2, (-tree.rect.left, -tree.rect.top), None, pygame.BLEND_MULT)

    screen.blit(t, tree.rect)

    b = bird.image.copy()
    b.fill(ambient, None, pygame.BLEND_MULT)

    screen.blit(b, bird.rect)


def draw3(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = (g ^ s) * a + (l1 + l2) ^ (t * a + l2) ^ (b * a)
    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites

    a = screen  # .copy()
    a.fill(ambient)

    screen.blit(street.image, street.rect)
    screen.blit(tomato.image, tomato.rect)
    screen.blit(a, (0, 0), None, pygame.BLEND_MULT)

    l = screen.copy()
    l.fill((0, 0, 0))
    l.blit(front_light.image, front_light.rect, None, pygame.BLEND_ADD)
    l.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    l.blit(red_light.image, red_light.rect, None, pygame.BLEND_ADD)
    l.blit(green_light.image, green_light.rect, None, pygame.BLEND_ADD)
    l.blit(blue_light.image, blue_light.rect, None, pygame.BLEND_ADD)

    screen.blit(l, (0, 0), None, pygame.BLEND_ADD)

    t = tree.image.copy()
    t.fill(ambient, None, pygame.BLEND_MULT)
    tx = street_light.rect.left - tree.rect.left
    ty = street_light.rect.top - tree.rect.top
    t.blit(street_light.image, (tx, ty), None, pygame.BLEND_ADD)

    screen.blit(t, tree.rect)

    b = bird.image.copy()
    b.fill(ambient, None, pygame.BLEND_MULT)

    screen.blit(b, bird.rect)


def draw4(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = (g ^ s) * (a + l1 + l2) + 0.5*l1 + 0.5*l2 ^ (t * (a + l2) + 0.5*l2) ^ (b * a)
    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    a = screen  # .copy()
    a.fill(ambient)

    l1 = front_light.image.copy()
    factor = (int(0.5 * 255),) * 3
    l1.fill(factor, None, pygame.BLEND_MULT)
    l2 = street_light.image.copy()
    l2.fill(factor, None, pygame.BLEND_MULT)
    r = red_light.image.copy()
    r.fill(factor, None, pygame.BLEND_MULT)
    g = green_light.image.copy()
    g.fill(factor, None, pygame.BLEND_MULT)
    b = blue_light.image.copy()
    b.fill(factor, None, pygame.BLEND_MULT)

    screen.blit(street.image, street.rect)
    screen.blit(tomato.image, tomato.rect)

    a1 = a.copy()
    a1.blit(front_light.image, front_light.rect, None, pygame.BLEND_ADD)
    a1.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    a1.blit(red_light.image, red_light.rect, None, pygame.BLEND_ADD)
    a1.blit(green_light.image, green_light.rect, None, pygame.BLEND_ADD)
    a1.blit(blue_light.image, blue_light.rect, None, pygame.BLEND_ADD)

    screen.blit(a1, (0, 0), None, pygame.BLEND_MULT)

    screen.blit(l1, front_light.rect, None, pygame.BLEND_ADD)
    screen.blit(l2, street_light.rect, None, pygame.BLEND_ADD)
    screen.blit(r, red_light.rect, None, pygame.BLEND_ADD)
    screen.blit(g, green_light.rect, None, pygame.BLEND_ADD)
    screen.blit(b, blue_light.rect, None, pygame.BLEND_ADD)

    a2 = a.copy()
    a2.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    t = tree.image.copy()
    t.blit(a2, (-tree.rect.left, -tree.rect.top), None, pygame.BLEND_MULT)
    t.blit(l2, street_light.rect.move(-tree.rect.left, -tree.rect.top), None, pygame.BLEND_ADD)

    screen.blit(t, tree.rect)

    b = bird.image.copy()
    b.fill(ambient, None, pygame.BLEND_MULT)

    screen.blit(b, bird.rect)


def draw5(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = (g ^ s) * (a + l1 + l2) + (1-a)*0.5*l1 + (1-a)*0.5*0.5*l2 ^ (t * (a + l2) + (1-a)*0.5*0.5*l2) ^ (b * a)
    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    a = screen  # .copy()
    a.fill(ambient)

    amb_factor = (255.0 - ambient[0]) / 255.0
    factor = (int(amb_factor * 0.5 * 255),) * 3
    factor2 = (int(amb_factor * 0.5 * 255 * 0.5),) * 3  # need to divide by number of layer, otherwise n times!
    l1 = front_light.image.copy()
    l1.fill(factor, None, pygame.BLEND_MULT)
    l2 = street_light.image.copy()
    l2.fill(factor2, None, pygame.BLEND_MULT)
    r = red_light.image.copy()
    r.fill(factor, None, pygame.BLEND_MULT)
    g = green_light.image.copy()
    g.fill(factor, None, pygame.BLEND_MULT)
    b = blue_light.image.copy()
    b.fill(factor, None, pygame.BLEND_MULT)

    screen.blit(street.image, street.rect)
    screen.blit(tomato.image, tomato.rect)

    a1 = a.copy()
    a1.blit(front_light.image, front_light.rect, None, pygame.BLEND_ADD)
    a1.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    a1.blit(red_light.image, red_light.rect, None, pygame.BLEND_ADD)
    a1.blit(green_light.image, green_light.rect, None, pygame.BLEND_ADD)
    a1.blit(blue_light.image, blue_light.rect, None, pygame.BLEND_ADD)

    screen.blit(a1, (0, 0), None, pygame.BLEND_MULT)

    # spotlight glimmer
    screen.blit(l1, front_light.rect, None, pygame.BLEND_ADD)
    screen.blit(l2, street_light.rect, None, pygame.BLEND_ADD)
    screen.blit(r, red_light.rect, None, pygame.BLEND_ADD)
    screen.blit(g, green_light.rect, None, pygame.BLEND_ADD)
    screen.blit(b, blue_light.rect, None, pygame.BLEND_ADD)

    a2 = a.copy()
    a2.blit(street_light.image, street_light.rect, None, pygame.BLEND_ADD)
    t = tree.image.copy()
    t.blit(a2, (-tree.rect.left, -tree.rect.top), None, pygame.BLEND_MULT)
    t.blit(l2, street_light.rect.move(-tree.rect.left, -tree.rect.top), None, pygame.BLEND_ADD)

    screen.blit(t, tree.rect)

    b = bird.image.copy()
    b.fill(ambient, None, pygame.BLEND_MULT)

    screen.blit(b, bird.rect)


def draw6(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = (g ^ s) * (a + l1 + l2) ^ (t * (a + l2)) ^ (b * a)
    """
    # street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    sprites = sorted(sprites, key=lambda s: s.layer)
    lights = [light for light in sprites if light.is_light]

    a1 = screen  # .copy()
    a1.fill(ambient)

    amb_factor = (255.0 - ambient[0]) / 255.0
    factor = (int(amb_factor * 0.5 * 255),) * 3
    # factor = (int(0.5 * 255),) * 3

    l_layer = sprites[0].layer
    light_idx = 0
    s = pygame.Surface(screen.get_size(), pygame.SRCALPHA)
    fill_color = (255, 255, 255, 0)
    s.fill(fill_color)  # transparent surface

    for spr in sprites:
        if spr.is_light:

            if l_layer != spr.layer:
                a1.fill(ambient)
                for i in lights[light_idx:]:
                    a1.blit(i.image, i.rect, None, pygame.BLEND_RGB_ADD)

                s.blit(a1, (0, 0), None, pygame.BLEND_RGBA_MULT)

                # for i in lights[light_idx:]:
                #     j = i.image.copy()
                #     j.fill(factor, None, pygame.BLEND_RGB_MULT)
                #     s.blit(j, i.rect, None, pygame.BLEND_RGB_ADD)

                screen.blit(s, (0, 0))

                s.fill(fill_color)

                l_layer = spr.layer

            light_idx += 1
        else:
            s.blit(spr.image, spr.rect)

    s.fill(ambient, None, pygame.BLEND_RGB_MULT)
    screen.blit(s, (0, 0))


def draw7(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    sprites = sorted(sprites, key=lambda s: s.layer)
    lights = [spr for spr in sprites if spr.is_light]
    # print(len(lights))
    # print([(li.name, li.z, li.layer) for li in lights])
    sprites_only = [spr for spr in sprites if not spr.is_light]
    r, g, b = ambient
    darkness = (r, g, b, 255)
    screen.fill(darkness)

    for spr in sprites_only:
        spr_img = spr.image.copy()

        b = spr.image.copy()
        b.fill(darkness)
        for light in lights:
            if light.z > spr.z:

                # light
                pos = light.rect.x - spr.rect.x, light.rect.y - spr.rect.y
                b.blit(light.image, pos, None, pygame.BLEND_ADD)

                # shadow
                # find sprites between sprite and light
                spr_between = [sprx for sprx in sprites_only[sprites_only.index(spr) + 1:] if sprx.z < light.z]
                for betw in spr_between:
                    shadow = betw.image.copy()
                    shadow.fill((0, 0, 0, 255), None, pygame.BLEND_RGBA_MULT)

                    lx, ly, lz = light.rect.centerx, light.rect.centery, light.z
                    bx, by, bz = betw.rect.centerx, betw.rect.centery, betw.z
                    px, py, pz = spr.rect.centerx, spr.rect.centery, spr.z

                    f = float(pz - lz) / (bz - lz)
                    shadow = pygame.transform.rotozoom(shadow, 0, f)
                    srect = shadow.get_rect()

                    srect.center = (lx + (bx - lx) * f - spr.rect.x,
                                    ly + (by - ly) * f - spr.rect.y)
                    b.blit(shadow, srect)

        spr_img.blit(b, (0, 0), None, pygame.BLEND_MULT)
        screen.blit(spr_img, spr.rect)


def draw8(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):


    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    sprites = sorted(sprites, key=lambda s: s.layer)  # [:3]
    lights = [spr for spr in sprites if spr.is_light]
    sprites_only = [spr for spr in sprites if not spr.is_light]
    r, g, b = ambient
    darkness = (r, g, b, 0)
    # darkness = (0, 0, 0, 0)
    screen.fill(darkness)

    for sidx, spr in enumerate(sprites_only):
        spr_img = spr.image.copy()

        sprb = spr.image.copy()
        sprb.fill(darkness)
        for lidx, light in enumerate(lights):
            if light.z > spr.z:

                # light
                li_img = light.image.copy()

                # shadow
                # find sprites between sprite and light
                spr_between = [sprx for sprx in sprites_only[sprites_only.index(spr) + 1:] if sprx.z < light.z]
                for betw in spr_between:
                    # create a shadow surface
                    shadow = betw.image.copy()
                    shadow.fill((0, 0, 0, 255), None, pygame.BLEND_RGBA_MULT)

                    # calculate position ands ize
                    lx, ly, lz = light.rect.centerx, light.rect.centery, light.z
                    bx, by, bz = betw.rect.centerx, betw.rect.centery, betw.z
                    px, py, pz = spr.rect.centerx, spr.rect.centery, spr.z

                    f = float(pz - lz) / (bz - lz)
                    shadow2 = pygame.transform.rotozoom(shadow, 0, f)
                    srect = shadow2.get_rect()

                    srect.center = (lx + (bx - lx) * f - light.rect.x,
                                    ly + (by - ly) * f - light.rect.y)

                    # imprint shadow on light
                    li_img.blit(shadow2, srect)

                # accumulate on light accumulation surface
                pos = light.rect.x - spr.rect.x, light.rect.y - spr.rect.y
                sprb.blit(li_img, pos, None, pygame.BLEND_RGBA_ADD)

        # multiply with sprite surface
        spr_img.blit(sprb, (0, 0), None, pygame.BLEND_MULT)
        screen.blit(spr_img, spr.rect)


def put_on_screen(shadow, pos, name=None):
    disp = pygame.display.get_surface()
    disp.blit(shadow, pos)
    if name is not None:
        font = pygame.font.Font(None, 20)
        font_surf = font.render(str(name), 0, text_color, text_background_color)
        disp.blit(font_surf, pos)


def draw9(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    sprites = sorted(sprites, key=lambda s: s.layer)  # [:3]
    lights = [spr for spr in sprites if spr.is_light]
    sprites_only = [spr for spr in sprites if not spr.is_light]
    r, g, b = ambient
    darkness = (r, g, b, 0)
    # darkness = (0, 0, 0, 0)
    screen.fill(darkness)

    for spr in sprites_only:
        spr_img = spr.image.copy()

        sprb = spr.image.copy()
        sprb.fill(darkness)
        for light in lights:
            if light.z > spr.z:

                # light
                li_img = light.image.copy()

                # shadow
                # find sprites between sprite and light
                spr_between = [sprx for sprx in sprites_only[sprites_only.index(spr) + 1:] if sprx.z < light.z]
                for betw in spr_between:
                    # use given shadow surface
                    shadow = betw.shadow

                    if shadow is None:
                        continue

                    # calculate position and size of shadow
                    lx, ly, lz = light.rect.centerx, light.rect.centery, light.z
                    bx, by, bz = betw.rect.centerx, betw.rect.centery, betw.z
                    px, py, pz = spr.rect.centerx, spr.rect.centery, spr.z

                    f = float(pz - lz) / (bz - lz)
                    shadow2 = pygame.transform.rotozoom(shadow, 0, f)
                    srect = shadow2.get_rect()

                    srect.center = (lx + (bx - lx) * f - light.rect.x,
                                    ly + (by - ly) * f - light.rect.y)

                    # multiply with light surface, this should allow for transparent objects
                    li_img.blit(shadow2, srect, None, pygame.BLEND_RGBA_MULT)

                # accumulate light
                pos = light.rect.x - spr.rect.x, light.rect.y - spr.rect.y
                sprb.blit(li_img, pos, None, pygame.BLEND_RGBA_ADD)

        # apply light on current sprite
        spr_img.blit(sprb, (0, 0), None, pygame.BLEND_MULT)
        screen.blit(spr_img, spr.rect)


def draw10(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    sprites = sorted(sprites, key=lambda s: s.layer)  # [:3]
    lights = [spr for spr in sprites if spr.is_light]
    sprites_only = [spr for spr in sprites if not spr.is_light]
    r, g, b = ambient
    darkness = (r, g, b, 255)
    # darkness = (0, 0, 0, 0)

    # #Start with no light
    # result = 0
    screen.fill(darkness)
    # for S in surfaces:
    for spr in sprites_only:
        sprb = spr.image.copy()
        # sprb.fill(darkness, None, pygame.BLEND_RGBA_MULT)
        for light in lights:
            #     #On this loop iteration, we will consider the
            #     #    light path that starts at the light, travels
            #     #    to S, bounces off, and travels to the eye.
            #
            #     #Figure out the amount of light that gets from
            #     #    the light to the surface.
            #     product_of_alphas_between_light_and_S = 1.0
            spr_alpha = spr.image.copy()
            spr_alpha.fill((255, 255, 255, 255))
            #     for T in surfaces:
            #         if T is between S and the light:
            spr_between = [sprx for sprx in sprites_only[sprites_only.index(spr) + 1:] if sprx.z < light.z]
            for betw in spr_between:

                # calculate position and size of shadow
                lx, ly, lz = light.rect.centerx, light.rect.centery, light.z
                bx, by, bz = betw.rect.centerx, betw.rect.centery, betw.z
                px, py, pz = spr.rect.centerx, spr.rect.centery, spr.z

                f = float(pz - lz) / (bz - lz)
                shadow2 = pygame.transform.rotozoom(betw.shadow, 0, f)
                srect = shadow2.get_rect()

                srect.center = (lx + (bx - lx) * f - spr.rect.x,
                                ly + (by - ly) * f - spr.rect.y)


                #             #Attenuate light by T's transmissivity
                #             #    (alpha in range [0.0,1.0]).
                #             product_of_alphas_between_light_and_S *= T.alpha
                spr_alpha.blit(shadow2, srect, None, pygame.BLEND_RGBA_MULT)
                #     #"product_of_alphas_between_light_and_S" should
                #     #    now be a number between 0.0 and 1.0.  It
                #     #    represents the attenuation of the light
                #     #    as it travels to S.
                #
                #     #This tell you how much light gets to S.  The
                #     #    "intensity" scales how "strong" the light is.
                #     #    Technically, it's radiance, not intensity.
                #     light_at_surface = light_intensity * product_of_alphas_between_light_and_S
            pos = light.rect.x - spr.rect.x, light.rect.y - spr.rect.y
            spr_alpha.blit(light.image, pos, None, pygame.BLEND_RGBA_MULT)
            #
            #     #Some of the light will be absorbed or transmitted
            #     #    through S.  The amount that is scattered back
            #     #    in a particular direction is called the BSDF.
            #     #    The BSDF I'm using here is diffuse, which is a
            #     #    constant factor (surface reflectance).  Because
            #     #    energy is conserved, this factor cannot be
            #     #    negative or larger than 1.0.
            #     #Ultimately, this tells how much light leaves the
            #     #    S heading toward the eye.
            #     reflected_light = light_at_surface * surface_reflectance
            sprb.blit(spr_alpha, (0, 0), None, pygame.BLEND_RGBA_MULT)
            #         sprb.blit(spr_alpha, (0, 0), None, pygame.BLEND_RGBA_ADD)
            #         sprb.blit(spr_alpha, (0, 0))
            #
            #     #Same idea as above.  Here we're attenuating the
            #     #    reflected light as it travels from S to the eye.
            #     product_of_surface_alphas_between_S_and_eye = 1.0
            #     for T in surfaces:
            #         if T is between S and the eye:
            #             product_of_surface_alphas_between_S_and_eye *= T.alpha
            #
            #     #This is the amount of light that finally reaches
            #     #    the eye.
            #     light_at_eye = reflected_light * product_of_surface_alphas_between_S_and_eye
            #
            #     #Accumulate it.  The sum of all paths (in all the
            #     #    loop iterations) is the first order light
            #     #    transport between the light and the eye.
            #     result += light_at_eye
            #         sprb.blit(light_img, pos, None, pygame.BLEND_RGBA_ADD)
            #     screen.blit(light_img, light.rect, None, pygame.BLEND_RGBA_ADD)
            #     screen.blit(spr.image, spr.rect)
        screen.blit(sprb, spr.rect, None, pygame.BLEND_RGBA_ADD)
        #     screen.blit(sprb, spr.rect)


def draw11(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    sprites = sorted(sprites, key=lambda s: s.layer)  # [:3]
    lights = [spr for spr in sprites if spr.is_light]
    sprites_only = [spr for spr in sprites if not spr.is_light]
    r, g, b = ambient
    darkness = (r, g, b, 0)
    # darkness = (0, 0, 0, 0)
    screen.fill(darkness)

    for spr in sprites_only:
        sprb = spr.image.copy()
        sprb.fill(darkness)
        for light in lights:
            if light.z > spr.z:

                # light
                li_img = light.image.copy()

                # shadow
                # find sprites between sprite and light
                spr_between = [sprx for sprx in sprites_only[sprites_only.index(spr) + 1:] if sprx.z < light.z]
                for betw in spr_between:
                    # use given shadow surface
                    shadow = betw.shadow

                    if shadow is None:
                        continue

                    # calculate position and size of shadow
                    lx, ly, lz = light.rect.centerx, light.rect.centery, light.z
                    bx, by, bz = betw.rect.centerx, betw.rect.centery, betw.z
                    px, py, pz = spr.rect.centerx, spr.rect.centery, spr.z

                    f = float(pz - lz) / (bz - lz)
                    shadow2 = get_transformed_shadow(f, shadow)
                    srect = shadow2.get_rect()

                    srect.center = (lx + (bx - lx) * f - light.rect.x,
                                    ly + (by - ly) * f - light.rect.y)

                    # multiply with light surface, this should allow for transparent objects
                    li_img.blit(shadow2, srect, None, pygame.BLEND_RGBA_MULT)



                # accumulate light
                pos = light.rect.x - spr.rect.x, light.rect.y - spr.rect.y
                sprb.blit(li_img, pos, None, pygame.BLEND_RGBA_ADD)

        # apply light on current sprite
        spr_img = spr.image.copy()
        spr_img.blit(sprb, (0, 0), None, pygame.BLEND_MULT)
        screen.blit(spr_img, spr.rect)


@lru_cache(maxsize=100)
def get_transformed_shadow(f, shadow):
    return pygame.transform.rotozoom(shadow, 0, f)


def invert_image(surf):
    white = surf.copy()
    white.fill((255, 255, 255))
    white.blit(surf, (0, 0), None, pygame.BLEND_RGB_SUB)
    return white


def draw12(screen, ambient, sprites):
    """
    Draws the scene applying following algorithm (see module doc string):

    pixel = ?
    """
    street, tomato, front_light, tree, street_light, bird, red_light, green_light, blue_light = sprites
    r, g, b = ambient
    darkness = (r, g, b, 0)
    # darkness = (0, 0, 0, 0)
    screen.fill(darkness)

    luminance = screen.copy()
    luminance.fill((255 - r, 255 - g, 255 - b))

    screen.blit(street.image, street.rect)

    t_shadow, t_rect = get_shadow(tomato, front_light, street)
    screen.blit(t_shadow, t_rect, None, pygame.BLEND_RGB_MULT)

    t_shadow, t_rect = get_shadow(tomato, street_light, street)
    screen.blit(t_shadow, t_rect, None, pygame.BLEND_RGB_MULT)

    t_shadow, t_rect = get_shadow(tree, street_light, street)
    screen.blit(t_shadow, t_rect, None, pygame.BLEND_RGB_MULT)

    screen.blit(tomato.image, tomato.rect)

    t_shadow, t_rect = get_shadow(tree, street_light, tomato)
    screen.blit(t_shadow, t_rect, None, pygame.BLEND_RGB_MULT)
    luminance.blit(t_shadow, t_rect, None, pygame.BLEND_RGB_MULT)
    # luminance.fill((255 - r, 255 - g, 255 - b))

    luminance.blit(invert_image(front_light.image), front_light.rect, None, pygame.BLEND_RGB_MULT)
    screen.blit(luminance, (0, 0), None, pygame.BLEND_RGB_SUB)
    # screen.blit(front_light.image, front_light.rect, None, pygame.BLEND_RGB_MULT)

    screen.blit(tree.image, tree.rect)

    # luminance.blit(invert_image(t_shadow), t_rect, None, pygame.BLEND_RGBA_MULT)
    # luminance.blit(invert_image(tomato.shadow), tomato.rect, None, pygame.BLEND_RGB_SUB)
    # luminance.blit(tomato.inv_shadow, tomato.rect, None, pygame.BLEND_RGB_ADD)
    # luminance.blit(street_light.image, street_light.rect, None, pygame.BLEND_RGB_ADD)
    luminance.blit(invert_image(street_light.image), street_light.rect, None, pygame.BLEND_RGB_MULT)
    # screen.blit(street_light.image, street_light.rect, None, pygame.BLEND_RGB_MULT)


    screen.blit(luminance, (0, 0), None, pygame.BLEND_RGB_SUB)


def get_shadow(betw, light, spr):
    shadow = betw.shadow

    # calculate position and size of shadow
    lx, ly, lz = light.rect.centerx, light.rect.centery, light.z
    bx, by, bz = betw.rect.centerx, betw.rect.centery, betw.z
    px, py, pz = spr.rect.centerx, spr.rect.centery, spr.z

    f = float(pz - lz) / (bz - lz)
    shadow2 = pygame.transform.rotozoom(shadow, 0, f)
    srect = shadow2.get_rect()

    srect.center = (lx + (bx - lx) * f - light.rect.x,
                    ly + (by - ly) * f - light.rect.y)
    srect.center = (lx + (bx - lx) * f,
                    ly + (by - ly) * f)

    return shadow2, srect


if __name__ == '__main__':
    main()
