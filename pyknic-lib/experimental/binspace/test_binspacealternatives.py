# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_binspacealternatives.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division
import unittest
import warnings

# noinspection PyProtectedMember
import binspacealternatives as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class _Rect(object):

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def __getitem__(self, idx):
        return [self.x, self.y, self.w, self.h][idx]

    def colliderect(self, other):
        return not (self.x + self.w < other.x or self.y + self.h < other.y or
                    other.x + other.w < self.x or other.y + other.h < self.y)


class _Entity(object):

    _next_id = 0

    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius
        self.id = _Entity._next_id
        _Entity._next_id += 1
        self.rect = _Rect(x - radius, y - radius, 2 * radius, 2 * radius)

    def __str__(self):
        return "_Entity<{0}>".format(self.id)

    def __repr__(self):
        return self.__str__()


class BinSpaceAlternativesTests(unittest.TestCase):

    def setUp(self):
        self.sut = mut.BinSpace1()

    def test_get_empty_area(self):
        # arrange
        # act
        actual = self.sut.get_in_rect(5, 5, 10, 10)

        # verify
        self.assertEqual(0, len(actual))

    def test_get_entity_in_first_cell(self):
        # arrange
        e1 = _Entity(6, 6, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)

        # act
        actual = self.sut.get_in_rect(5, 5, 10, 10)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_right_of_a_cell(self):
        # arrange
        e1 = _Entity(64 + 6 + 64, 24, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(64 - 3 + 64, 24, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_left_of_a_cell(self):
        # arrange
        e1 = _Entity(64 - 6 + 64, 24, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(64 + 3 + 64, 24, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_top_of_a_cell(self):
        # arrange
        e1 = _Entity(24, 64 - 6 + 64, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(24, 64 + 3 + 64, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_bottom_of_a_cell(self):
        # arrange
        e1 = _Entity(24, 70 + 64, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(24, 61+64, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_diagonal_NE_of_a_cell(self):
        # arrange
        e1 = _Entity(64 + 6 + 128, 64 - 6 + 64, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(64 - 3 + 128, 64 + 3 + 64, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_diagonal_NW_of_a_cell(self):
        # arrange
        e1 = _Entity(64 - 6, 64 - 6 + 64, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(64 + 3, 64 + 3 + 64, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_diagonal_SW_of_a_cell(self):
        # arrange
        e1 = _Entity(64 - 6, 64 + 6 + 128, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(64 + 3, 64 - 3 + 128, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_entity_overlaps_diagonal_SE_of_a_cell(self):
        # arrange
        e1 = _Entity(64 + 6 + 64, 64 + 6 + 64, 5)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(64 - 3 + 64, 64 - 3 + 64, 10)

        # act
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual[e2])

    def test_get_small_entity(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)

        # act
        actual = self.sut.get_in_rect(50, 50, 20, 20)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_different_sized_entities_same_spot(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(60, 60, 3)
        self.sut.add_with_radius(e2, e2.x, e2.y, e2.radius)

        # act
        actual = self.sut.get_in_rect(50, 50, 20, 20)

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(e1 in actual)
        self.assertTrue(e2 in actual)

    def test_get_different_sized_entities_different_spot(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(200, 200, 3)
        self.sut.add_with_radius(e2, e2.x, e2.y, e2.radius)

        # act
        actual = self.sut.get_in_rect(50, 50, 200, 200)

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(e1 in actual)
        self.assertTrue(e2 in actual)

    def test_get_same_sized_entities_in_different_cells(self):
        # arrange
        e1 = _Entity(600, 600, 1)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(20, 20, 1)
        self.sut.add_with_radius(e2, e2.x, e2.y, e2.radius)

        # act
        actual = self.sut.get_in_rect(590, 590, 20, 20)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entities_with_different_sizes_mass_adding(self):
        # arrange
        entities = [_Entity(60, 60, 1), _Entity(200, 200, 3)]
        self.sut.adds_with_radius(((e1, e1.x, e1.y, e1.radius) for e1 in entities))

        # act
        actual = self.sut.get_in_rect(50, 50, 200, 200)

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(entities[0] in actual)
        self.assertTrue(entities[1] in actual)

    def test_element_count_with_entities(self):
        # arrange
        e1 = _Entity(10, 10, 2)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(20, 20, 10)
        self.sut.add_with_radius(e2, e2.x, e2.y, e2.radius)

        # act
        actual = len(self.sut)

        # verify
        self.assertEqual(2, actual, "bin space len should be 1")

    def test_empty_element_count(self):
        # arrange

        # act
        actual = len(self.sut)

        # verify
        self.assertEqual(0, actual, "empty count should be 0")

    def test_remove_entity(self):
        # arrange
        e1 = _Entity(10, 10, 10)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)

        # act
        self.sut.remove(e1)

        # verify
        self.assertEqual(0, len(self.sut))

    def test_mass_removable_of_entities(self):
        # arrange
        entities = [_Entity(60, 60, 1), _Entity(200, 200, 3)]
        self.sut.adds_with_radius(((e1, e1.x, e1.y, e1.radius) for e1 in entities))

        # act
        self.sut.removes(entities)

        # verify
        self.assertEqual(0, len(self.sut))

    def test_partial_mass_removable_of_entities(self):
        # arrange
        entities = [
            _Entity(60, 60, 1),
            _Entity(200, 200, 3),
            _Entity(0, 0, 3),
            _Entity(206, 206, 3),
        ]
        self.sut.adds_with_radius(((e1, e1.x, e1.y, e1.radius) for e1 in entities))

        # act
        self.sut.removes(entities[:3])

        # verify
        self.assertEqual([entities[-1]], self.sut.get_in_rect(0, 0, 250, 250))

    def test_clear(self):
        # arrange
        entities = [
            _Entity(1, 1, 10),
            _Entity(10, 100, 100),
            _Entity(1000, 1000, 1),
            _Entity(125, 1, 50),
            _Entity(1, 221, 20),
        ]
        self.sut.adds_with_radius(((_e, _e.x, _e.y, _e.radius) for _e in entities))
        self.assertEqual(len(entities), len(self.sut), "entities in binspace should be equal the entities added")

        # act
        self.sut.clear()

        # verify
        self.assertEqual(0, len(self.sut), "The binspace instance should be empty after clearing.")

    def test_get_collision_pairs_with_others_same_sizes(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 1)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 1)
        entities = [e1, e2, e3, ]

        o1 = _Entity(1, 1, 1)
        o2 = _Entity(1, 1, 1)
        o3 = _Entity(1, 1, 1)
        others = [o1, o2, o3, ]

        expected_pairs = {
            (o1, e1), (o1, e2), (o1, e3),
            (o2, e1), (o2, e2), (o2, e3),
            (o3, e1), (o3, e2), (o3, e3)}

        expected, actual = self._act(entities, others, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_with_others_different_sizes(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 20)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 50)
        entities = [e1, e2, e3, ]

        o1 = _Entity(1, 1, 5)
        o2 = _Entity(1, 1, 100)
        o3 = _Entity(1, 1, 8)
        others = [o1, o2, o3, ]

        expected_pairs = {
            (o1, e1), (o1, e2), (o1, e3),
            (o2, e1), (o2, e2), (o2, e3),
            (o3, e1), (o3, e2), (o3, e3)}
        expected, actual = self._act(entities, others, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_with_self_collision_same_size(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 1)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 1)
        entities = [e1, e2, e3]

        expected_pairs = {(e1, e1), (e1, e2), (e1, e3), (e2, e2), (e3, e2), (e3, e3)}
        expected, actual = self._act(entities, entities, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_with_self_collision_different_sizes(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 20)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 50)
        entities = [e1, e2, e3]

        expected_pairs = {(e1, e1), (e1, e2), (e1, e3), (e2, e2), (e3, e2), (e3, e3)}
        expected, actual = self._act(entities, entities, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_no_self_collision_same_size(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 1)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 1)
        entities = [e1, e2, e3]

        expected, actual = self._act(entities, entities, {(e1, e2), (e1, e3), (e2, e3)})

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_no_self_collision_different_sizes(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        e1 = _Entity(1, 1, 20)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 50)
        entities = [e1, e2, e3]

        expected, actual = self._act(entities, entities, {(e1, e2), (e1, e3), (e2, e3)})

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_no_self_collision_very_different_sizes(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        # self.sut = mut.BinSpace(cell_to_sphere_ratio=1/2)
        # <rect(268, 232, 8, 38)> vs <rect(270, 208, 2, 32)>
        # e1 = _Entity(270 + 16, 208 + 16, 16)
        # e2 = _Entity(268 + 19, 232 + 19 + 36, 19)
        # <rect(781, 157, 1, 35)> vs <rect(780, 155, 5, 6)>
        # <rect(287, 235, 4, 29)> vs <rect(285, 232, 4, 13)>
        # <rect(7, 70, 3, 9)> vs <rect(5, 77, 3, 30)>
        # <rect(332, 273, 5, 48)> vs <rect(330, 273, 6, 12)>
        # <rect(321, 537, 7, 47)> vs <rect(325, 512, 8, 35)>
        # <rect(430, 497, 26, 22)> vs <rect(450, 515, 3, 1)>
        # < rect(716, 586, 1, 5) > vs < rect(709, 548, 27, 39) >
        e1 = _Entity(29, 29, 3)
        e2 = _Entity(190, 190, 200)
        expected, actual = self._act([e1], [e2], {(e1, e2)})
        self.assertEqual(expected, actual)

        self.sut.clear()
        e3 = _Entity(716, 586 + 5 // 2 + 1, int((1 + 25)**0.5 + 0.5))
        e4 = _Entity(709 + 27//2 + 1, 548 + 39//2 + 1, int((27**2 + 39**2)**0.5 + 0.5))
        expected, actual = self._act([e3], [e4], {(e3, e4)})
        self.assertEqual(expected, actual)

    def _act(self, entities, colliders, expected, self_collision=False):
        # arrange
        self.sut.adds_with_radius(((_e, _e.x, _e.y, _e.radius) for _e in entities))

        # act
        actual = self.sut.collides(((_e, _e.x, _e.y, _e.radius) for _e in colliders), self_collision)

        # verify
        expected = self._get_sorted_pairs(expected)
        actual = self._get_sorted_pairs_from_dict(actual)
        print("expect", expected)
        print("actual", actual)
        return expected, actual

    def _get_sorted_pairs(self, pairs):
        result = list(set([(a, b) if a.id < b.id else (b, a) for a, b in pairs]))
        result.sort(key=lambda t: (t[0].id, t[1].id))
        return result

    def test_add_same_entity_twice(self):
        # arrange
        e = _Entity(1, 1, 1)
        self.sut.add_with_radius(e, e.x, e.y, e.radius)

        # act
        self.sut.add_with_radius(e, e.x, e.y, e.radius)

        # verify
        self.assertEqual(1, len(self.sut))

    def test_remove_entity_not_in_cells(self):
        # arrange
        e = _Entity(1, 1, 1)
        o = _Entity(1, 1, 1)
        self.sut.add_with_radius(o, o.x, o.y, o.radius)

        # act
        self.sut.remove(e)

        # verify
        self.assertEqual(1, len(self.sut))

    def test_remove_entity_twice(self):
        # arrange
        e = _Entity(1, 1, 1)
        self.sut.add_with_radius(e, e.x, e.y, e.radius)
        self.sut.remove(e)

        # act / verify
        self.sut.remove(e)

    def test_entities_in_neighbour_cells_overlap(self):
        # arrange
        e1 = _Entity(31, 31, 3)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(33, 33, 3)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def _get_sorted_pairs_from_dict(self, expected):
        # return self._get_sorted_pairs(expected)
        return self._get_sorted_pairs(((e1, e2) for e1 in expected for e2 in list(expected[e1])))

    def test_get_collision_pair_for_entities_overlapping_near_edge(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 6, 5)
        e2 = _Entity(-6, -1, 5)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def test_get_collision_pair_for_big_vs_small_entity_overlapping_near_edge(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(25, 0, 30)
        e2 = _Entity(-6, -1, 5)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def test_negative_coordinates(self):
        # arrange
        e1 = _Entity(30 * 32, -3 * 32, 1)
        self.sut.add_with_radius(e1, e1.x, e1.y, e1.radius)
        e2 = _Entity(0, 0, 5)

        # act
        # actual = self.sut.intersect_objects(pygame.Rect(-32, -32, 3 * 32, 3 * 32))
        actual = self.sut.collides(((e2, e2.x, e2.y, e2.radius), ))

        # verify
        expected = set()
        self.assertEqual(expected, actual[e1])

    def test_product_vs_loops(self):
        # arrange
        import itertools
        x_min = 0
        x_max = 4
        y_min = 0
        y_max = 4

        # act
        prod_result = list(itertools.product(range(x_min, x_max + 1), range(y_min, y_max + 1)))
        loop_result = []

        for _x in range(x_min, x_max+1):
            for _y in range(y_min, y_max + 1):
                loop_result.append((_x, _y))

        # verify
        self.assertEqual(loop_result, prod_result)


class BinRangeTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = mut.BinRange()


class BinArrayTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = mut.BinArray()


class BinSweepTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = mut.BinSweepVerySlow()


class BinHashTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = mut.AltBinSpace()


class BinHashWhileTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = mut.BinHashWhile()


class _SpritesAdapter(object):

    def __init__(self, adaptee):
        self.adaptee = adaptee

    def adds_with_radius(self, circles):
        entities = [e for e, x, y, r in circles]
        self.adaptee.addlist(entities)

    # noinspection PyUnusedLocal
    def add_with_radius(self, e, x, y, r):
        self.adaptee.add(e)

    def __len__(self):
        return len(self.adaptee)

    def clear(self):
        self.adaptee.clear()

    def collides(self, circles, do_self_collision=False):
        return self.adaptee.collides([e for e, x, y, r in circles], do_self_collision)

    def get_in_rect(self, x, y, w, h):
        return list(self.adaptee.get_in_rect(_Rect(x, y, w, h)))

    def removes(self, entities):
        self.adaptee.removes(entities)

    def remove(self, entity):
        self.removes([entity])


class GummHashTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = _SpritesAdapter(mut.GummHash())


class _EntitiesAdapter(object):

    def __init__(self, adaptee):
        self.adaptee = adaptee

    def adds_with_radius(self, circles):
        entities = [e for e, x, y, r in circles]
        self.adaptee.adds_with_radius(entities)

    # noinspection PyUnusedLocal
    def add_with_radius(self, e, x, y, r):
        self.adaptee.add_with_radius(e)

    def __len__(self):
        return len(self.adaptee)

    def clear(self):
        self.adaptee.clear()

    def collides(self, circles, do_self_collision=False):
        return self.adaptee.collides([e for e, x, y, r in circles], do_self_collision)

    def get_in_rect(self, x, y, w, h):
        return list(self.adaptee.get_in_rect(x, y, w, h))

    def removes(self, entities):
        self.adaptee.removes(entities)

    def remove(self, entity):
        self.removes([entity])


class BinSpaceEntitiesTests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = _EntitiesAdapter(mut.BinSpaceEntities())


class BinSpaceEntities2Tests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = _EntitiesAdapter(mut.BinSpaceEntities2())


class BinSpace2Tests(BinSpaceAlternativesTests):

    def setUp(self):
        self.sut = mut.BinSpace2()


if __name__ == '__main__':
    unittest.main()
