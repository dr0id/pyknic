# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'binspacealternatives.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

import logging
import collections
from collections import defaultdict
from bisect import insort, bisect_left, bisect_right
import sys

import itertools

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class BinSpaceEntities(object):

    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size  # TODO: make it adaptive? but then one need more info about the entity (size, pos)!

    def add(self, e):
        self.adds([e])

    def adds(self, entities, _int=int, _range=range):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        self_remove = self.remove
        for e in entities:
            if e in self_entity_to_cells:
                self_remove(e)

            r = e.rect
            x = r[0]
            y = r[1]
            cx_min = x // self__cell_size
            cx_max = (x + r[2]) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + r[3]) // self__cell_size

            self_entity_to_cells_e__add = self_entity_to_cells[e].add
            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    _cell_id = (_x, _y)

                    self_cells[_cell_id].add(e)
                    self_entity_to_cells_e__add(_cell_id)

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides_orig(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict):
        result = _defaultdict(_set)
        self_cells = self._cells
        self__cell_size = self._cell_size
        
        for e in entities:
            r = e.rect

            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            rcr = r.colliderect
            result_e__add = result[e].add

            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    for other in self_cells[(_x, _y)]:
                        if rcr(other.rect):
                            if other in result:
                                result[other].add(e)
                            else:
                                result_e__add(other)
            if not do_self_collision:
                result[e].discard(e)

        return result

    def collides_(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict, attrn="rect",
                 _chain_from_iterable=itertools.chain.from_iterable, _itertools_product=itertools.product, _getattr=getattr):
        result = _defaultdict(_set)

        self_cells = self._cells
        self__cell_size = self._cell_size
        _set_add = _set.add

        for e in entities:
            r = _getattr(e, attrn)

            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            cell_ids = (self_cells[i] for i in _itertools_product(_range(cx_min, cx_max + 1), _range(cy_min, cy_max + 1)) if i in self_cells)
            others = _chain_from_iterable(cell_ids)

            result_e = result[e]
            for o in others:
                if o is e and not do_self_collision:
                    continue
                _set_add(result_e, o)

        return result

    def collides(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict,
                 attrn="rect", _getattr=getattr):
        result = _defaultdict(_set)

        others = _set()
        others_clear = others.clear
        others_discard = others.discard
        others_update = others.update
        result_pop = result.pop

        self_cells = self._cells
        self__cell_size = self._cell_size
        _set_add = _set.add

        for e in entities:
            r = _getattr(e, attrn)

            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            others_clear()

            x_range = _range(cx_min, cx_max + 1)
            y_range = _range(cy_min, cy_max + 1)
            others_update(*(self_cells[(_x, _y)] for _y in y_range for _x in x_range))

            if not do_self_collision:
                others_discard(e)

            result_e = result[e]
            for other in others:
                if other in result:
                    result[other].add(e)
                else:
                    _set_add(result_e, other)

            if not result_e:
                result_pop(e)

        return result

    def get_in_rect(self, x, y, w, h, _int=int):
        result = set()
        cx_min = _int(x // self._cell_size)
        cx_max = _int((x + w) // self._cell_size)
        cy_min = _int(y // self._cell_size)
        cy_max = _int((y + h) // self._cell_size)
        self_cells = self._cells
        result_update = result.update

        y_range = range(cy_min, cy_max + 1)
        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result_update(self_cells[(_x, _y)])

        # TODO: maybe return same result structure as collides
        return list(result)

    def remove(self, entity):
        self__cells = self._cells
        for cell in self._entity_to_cells.pop(entity, []):
            self__cells[cell].remove(entity)

    def removes(self, entities):
        self__cells = self._cells
        self__entity_to_cells = self._entity_to_cells
        self__entity_to_cells_pop = self__entity_to_cells.pop
        for e in entities:
            for cell in self__entity_to_cells_pop(e, []):
                self__cells[cell].remove(e)


class BinSpaceEntities2(object):
    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size  # TODO: make it adaptive? but then one need more info about the entity (size, pos)!

    def add(self, e):
        self.adds([e])

    def adds(self, entities, _int=int, _range=range):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        self_remove = self.remove
        for e in entities:
            if e in self_entity_to_cells:
                self_remove(e)

            r = e.rect
            x = r[0]
            y = r[1]
            cx_min = x // self__cell_size
            cx_max = (x + r[2]) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + r[3]) // self__cell_size

            self_entity_to_cells_e__add = self_entity_to_cells[e].add
            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    _cell_id = (_x, _y)

                    self_cells[_cell_id].add(e)
                    self_entity_to_cells_e__add(_cell_id)

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides_orig(self, entities, do_self_collision=False, _int=int, _range=range, _set=set,
                      _defaultdict=defaultdict):
        result = _defaultdict(_set)
        self_cells = self._cells
        self__cell_size = self._cell_size

        for e in entities:
            r = e.rect

            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            rcr = r.colliderect
            result_e__add = result[e].add

            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    for other in self_cells[(_x, _y)]:
                        if rcr(other.rect):
                            if other in result:
                                result[other].add(e)
                            else:
                                result_e__add(other)
            if not do_self_collision:
                result[e].discard(e)

        return result

    def collides_(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict,
                 attrn="rect", _getattr=getattr):
        result = _defaultdict(_set)

        others = _set()
        others_clear = others.clear
        others_discard = others.discard
        result_pop = result.pop

        self_cells = self._cells
        self__cell_size = self._cell_size
        _set_add = _set.add

        for e in entities:
            r = _getattr(e, attrn)

            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            others_clear()

            # itertools.chain.from_iterable()
            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    others |= self_cells[(_x, _y)]

            if not do_self_collision:
                others_discard(e)

            # rcr = r.colliderect
            # result_e__add = result[e].add
            for other in others:
                # if rcr(other.rect):
                if other in result:
                    result[other].add(e)
                else:
                    # result_e__add(other)
                    _set_add(result[e], other)

            if not result[e]:
                result_pop(e)

        return result

    def collides(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict,
                 attrn="rect", _getattr=getattr):
        result = _defaultdict(_set)

        others = _set()
        others_clear = others.clear
        others_discard = others.discard
        result_pop = result.pop

        self_cells = self._cells
        self__cell_size = self._cell_size
        _set_add = _set.add

        for e in entities:
            r = _getattr(e, attrn)

            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            others_clear()

            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    others |= self_cells[(_x, _y)]

            if not do_self_collision:
                others_discard(e)

            result_e = result[e]
            for other in others:
                if other in result:
                    result[other].add(e)
                else:
                    _set_add(result_e, other)

            if not result_e:
                result_pop(e)

        return result

    def get_in_rect(self, x, y, w, h, _int=int):
        result = set()
        cx_min = _int(x // self._cell_size)
        cx_max = _int((x + w) // self._cell_size)
        cy_min = _int(y // self._cell_size)
        cy_max = _int((y + h) // self._cell_size)
        self_cells = self._cells
        result_update = result.update

        y_range = range(cy_min, cy_max + 1)
        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result_update(self_cells[(_x, _y)])

        # TODO: maybe return same result structure as collides
        return list(result)

    def remove(self, entity):
        self__cells = self._cells
        for cell in self._entity_to_cells.pop(entity, []):
            self__cells[cell].remove(entity)

    def removes(self, entities):
        self__cells = self._cells
        self__entity_to_cells = self._entity_to_cells
        self__entity_to_cells_pop = self__entity_to_cells.pop
        for e in entities:
            for cell in self__entity_to_cells_pop(e, []):
                self__cells[cell].remove(e)


class AltBinSpace(object):
    def __init__(self, min_cell_size=8, cell_to_sphere_ratio=1.0 / 2.0, cell_to_cell_ratio=2):
        """
        The BinSpace class. Or simply put a hierarchical grid based approach to avoid too many collision checks.

        Typical usage is checking some entity type against another. This type of grid is used in the broad phase
        of the collision checks. It reduces the number of checks because only the entities in the same or neighbour
        cell are checked against.

        Usage::

            space = BinSpace()
            ...
            ...
            ...  # update()
            space.clear()  # remove everything
            space.add_circle(...)  # add the entities to check against
            ...
            pairs = space.collides(...)  # get the potential collision pairs
            ...  # do expensive fine grained collisions checks
            ...  # repeat update()


        :param min_cell_size: the minimal size of a cell, defaults to 8.
        :param cell_to_sphere_ratio: the ration of the cell size to the containing entities, defaults to 0.5 = 1 / 2
        :param cell_to_cell_ratio: the ration between the cell sizes in the hierarchy, defaults to 2.
        """
        self._cells = collections.defaultdict(list)  # {(x,y, size): [entity,]}
        self._entity_to_cell = {}  # {entity: (x, y, size)}
        self._occupied_levels = 0  # bit mask
        self._obj_at_level = {}  # {size: count}
        self.num_checks = 0

        # const
        self._min_cell_size = min_cell_size
        self._cell_to_sphere_ratio = cell_to_sphere_ratio  # largest sphere in cell is 1/4 * cell_size
        self._cell_to_cell_ration = cell_to_cell_ratio  # cells at next level are 2 * size of current cell

    def add(self, entity, x, y, radius):
        """
        Adds an entity with a center point x,y and a radius.
        :param entity: The entity to store.
        :param x: The x coordinate of the center point.
        :param y: The y coordinate of the center point.
        :param radius: The radius of the entity.
        :return: None
        """
        self.adds([(entity, x, y, radius)])

    def adds(self, entities):
        """
        Adds multiple entities from a list.
        :param entities: The entities to add as a list of the form [(entity, x, y, radius), ...]
        :return: None
        """
        for entity, x, y, radius in entities:
            diameter = 2 * radius

            if entity in self._entity_to_cell:
                self.remove(entity)
            size = self._min_cell_size
            level = 0
            while size * self._cell_to_sphere_ratio < diameter:
                size *= self._cell_to_cell_ration
                level += 1

            cell_id = (x // size, y // size, size)
            self._cells[cell_id].append(entity)
            self._entity_to_cell.setdefault(entity, []).append(cell_id)

            self._occupied_levels |= (1 << level)
            self._obj_at_level[size] = self._obj_at_level.get(size, 0) + 1

    def remove(self, entity):
        """
        Remove an entity if present.
        :param entity: The entity to be removed.
        :return: None
        """
        self.removes([entity])

    def removes(self, entities):
        """
        Removes multiple entities.
        :param entities: The iterable entities to be removed.
        :return: None
        """
        for entity in entities:
            cells = self._entity_to_cell.pop(entity, [])
            for _cell_id in cells:
                cell_entities = self._cells[_cell_id]
                if cell_entities:
                    cell_entities.remove(entity)
                if not cell_entities:
                    del self._cells[_cell_id]

                x, y, size = _cell_id
                self._obj_at_level[size] -= 1

                if self._obj_at_level[size] == 0:
                    del self._obj_at_level[size]
                    level = 0
                    while size > self._min_cell_size:
                        size /= self._cell_to_cell_ration
                        level += 1
                    self._occupied_levels &= ~(1 << level)

    def clear(self):
        """
        Clear all entities.
        :return: None
        """
        self._cells.clear()
        self._entity_to_cell.clear()
        self._obj_at_level.clear()
        self._occupied_levels = 0

    def __len__(self):
        """
        The len operator.
        :return: Returns the number of entities stored.
        """
        return sum(self._obj_at_level.values())

    def get_in_rect(self, x, y, w, h):
        """
        Get the entities that could potentially collide with the given rect.
        :param x: The left coordinate of the rect.
        :param y: The top coordinate of the rect.
        :param w: The width of the rect.
        :param h: The height of the rect.
        :return: List of potentially colliding entities.
        """
        result = []
        size = self._min_cell_size
        occupied_levels = self._occupied_levels

        self__cells = self._cells
        result_extend = result.extend
        _cell_to_cell_ration = self._cell_to_cell_ration

        while occupied_levels > 0:
            if occupied_levels & 1 == 0:
                occupied_levels >>= 1
                size *= _cell_to_cell_ration
                continue

            cx1 = x // size - 1  # -1 for additional cell
            cy1 = y // size - 1  # -1 for additional cell
            cx2 = (x + w) // size + 2  # +1 for range, +1 for additional cell
            cy2 = (y + h) // size + 2  # +1 for range, +1 for additional cell

            for dx in range(cx1, cx2):
                for dy in range(cy1, cy2):
                    _cell_id = (dx, dy, size)
                    result_extend(self__cells[_cell_id])

            occupied_levels >>= 1
            size *= _cell_to_cell_ration

        return result

    def collides(self, circles, self_collision=False):
        """
        Get all entities colliding with the entities described by the circles. The returned list has unique potential
        collision pairs (no pairs are doubled).
        :param circles: List of colliding entities in the form of [(entity, x, y, radius), ...]
        :param self_collision: If the entities are collided with themselves then a self collision would be included
            in the result if set to True, defaults to False.
        :return: Returns a dict of unique potential collision pairs in the form of {entity1: set(e2, e3, e6), ...}
        """
        _res = collections.defaultdict(set)

        self_cells = self._cells
        _cell_to_cell_ration = self._cell_to_cell_ration
        ecx = cx_max = ecy = cy_max = 0

        cell_ids = set()
        cell_ids_add = cell_ids.add

        num_processed = 0
        self_cell_to_sphere_ratio = self._cell_to_sphere_ratio

        for e, x, y, r in circles:
            size = self._min_cell_size
            occupied_levels = self._occupied_levels

            cell_ids.clear()

            while occupied_levels > 0:
                if occupied_levels & 1 == 0:
                    occupied_levels >>= 1
                    size *= _cell_to_cell_ration
                    continue

                if size * self_cell_to_sphere_ratio < r:
                    ecx = (x - r) // size
                    cx_max = (x + r) // size
                    ecy = (y - r) // size
                    cy_max = (y + r) // size

                    for _x in range(int(ecx), int(cx_max + 1)):
                        for _y in range(int(ecy), int(cy_max + 1)):
                            cell_ids_add((_x, _y, size))

                else:
                    ecx = x // size
                    ecy = y // size

                    emx = x % size
                    emy = y % size
                    obj_size = int(size * self._cell_to_sphere_ratio + 0.5)

                    cell_ids_add((ecx, ecy, size))
                    if emx < obj_size:
                        if emy < obj_size:
                            cell_ids_add((ecx - 1, ecy, size))
                            cell_ids_add((ecx - 1, ecy - 1, size))
                            cell_ids_add((ecx, ecy - 1, size))
                        elif emy > size - obj_size:
                            cell_ids_add((ecx - 1, ecy, size))
                            cell_ids_add((ecx - 1, ecy + 1, size))
                            cell_ids_add((ecx, ecy + 1, size))
                        else:
                            cell_ids_add((ecx - 1, ecy, size))
                    elif emx > size - obj_size:
                        if emy < obj_size:
                            cell_ids_add((ecx + 1, ecy, size))
                            cell_ids_add((ecx + 1, ecy - 1, size))
                            cell_ids_add((ecx, ecy - 1, size))
                        elif emy > size - obj_size:
                            cell_ids_add((ecx + 1, ecy, size))
                            cell_ids_add((ecx + 1, ecy + 1, size))
                            cell_ids_add((ecx, ecy + 1, size))
                        else:
                            cell_ids_add((ecx + 1, ecy, size))
                    else:
                        if emy < obj_size:
                            cell_ids_add((ecx, ecy - 1, size))
                        elif emy > size - obj_size:
                            cell_ids_add((ecx, ecy + 1, size))

                occupied_levels >>= 1
                size *= _cell_to_cell_ration

            _res_e__add = _res[e].add
            for cell_id in cell_ids:
                cell_entities = self_cells[cell_id]
                for o in cell_entities:
                    if o in _res:
                        _res[o].add(e)
                    else:
                        _res_e__add(o)

                num_processed += len(cell_entities)

            if not self_collision:
                try:
                    _res[e].remove(e)
                except KeyError:
                    pass

        self.num_checks = num_processed

        return _res


class BinRange(object):
    def __init__(self):
        self._sorted_x = []
        self._sorted_y = []
        self._sizes_x = set()
        self._sizes_y = set()
        self._max_size_x = 0
        self._max_size_y = 0
        self._info = {}  # {e:(x_min, x_max, y_min, y_max)}

    def add(self, e, x, y, r):
        self.adds([(e, x, y, r)])

    def adds(self, entities):

        for e, x, y, r in entities:
            _ide = id(e)
            if _ide in self._info:
                self.remove(e)

            insort(self._sorted_x, (x, _ide, e))
            insort(self._sorted_y, (y, _ide, e))
            if r > self._max_size_x:
                self._max_size_x = r
            self._sizes_x.add(r)
            if r > self._max_size_y:
                self._max_size_y = r
            self._sizes_y.add(r)

            self._info[_ide] = (x, y, r)

    def remove(self, e):
        self.removes([e])

    def removes(self, entities):
        for e in entities:
            _ide = id(e)
            if _ide in self._info:
                ex, ey, er = self._info[_ide]
                x_pos = bisect_right(self._sorted_x, (ex, _ide, e))
                del self._sorted_x[x_pos - 1]

                y_pos = bisect_right(self._sorted_y, (ey, _ide, e))
                del self._sorted_y[y_pos - 1]

                try:
                    self._sizes_x.remove(er)
                except KeyError:
                    pass
                try:
                    self._sizes_y.remove(er)
                except KeyError:
                    pass
                self._max_size_x = max(self._sizes_x) if self._sizes_x else 0
                self._max_size_y = max(self._sizes_y) if self._sizes_y else 0

                del self._info[_ide]

    def clear(self):
        self._info.clear()
        self._sorted_x[:] = []
        self._sorted_y[:] = []
        self._sizes_x.clear()
        self._sizes_y.clear()

    def collides(self, circles, self_collision=False):
        result = collections.defaultdict(set)
        _ide = sys.maxsize
        _sorted_x = self._sorted_x
        _sorted_y = self._sorted_y
        for e, x, y, r in circles:

            e_x_min = x - r - self._max_size_x
            e_x_max = x + r + self._max_size_x
            e_x_min_pos = bisect_left(_sorted_x, (e_x_min, -_ide))
            e_x_max_pos = bisect_right(_sorted_x, (e_x_max, _ide), e_x_min_pos)
            # x_range = set((_e for _x, _ide, _e in _sorted_x[e_x_min_pos:e_x_max_pos]))
            x_range = set((_s[-1] for _s in _sorted_x[e_x_min_pos:e_x_max_pos]))

            e_y_min = y - r - self._max_size_y
            e_y_max = y + r + self._max_size_y
            e_y_min_pos = bisect_left(_sorted_y, (e_y_min, -_ide))
            e_y_max_pos = bisect_right(_sorted_y, (e_y_max, _ide), e_y_min_pos)
            # y_range = set((_e for _y, _ide, _e in _sorted_y[e_y_min_pos:e_y_max_pos]))
            y_range = set((_s[-1] for _s in _sorted_y[e_y_min_pos:e_y_max_pos]))

            result[e] = x_range.intersection(y_range)

            if not self_collision:
                try:
                    result[e].remove(e)
                except KeyError:
                    pass
                    # except ValueError:
                    #     pass

        return result

    def get_in_rect(self, x, y, w, h):
        e_x_min = x - w - self._max_size_x
        e_x_max = x + w + self._max_size_x
        _ide = sys.maxsize
        e_x_min_pos = bisect_left(self._sorted_x, (e_x_min, -_ide))
        e_x_max_pos = bisect_right(self._sorted_x, (e_x_max, _ide))
        x_range = set((_e for _x, _ide, _e in self._sorted_x[e_x_min_pos:e_x_max_pos]))

        e_y_min = y - h - self._max_size_y
        e_y_max = y + h + self._max_size_y
        e_y_min_pos = bisect_left(self._sorted_y, (e_y_min, -_ide))
        e_y_max_pos = bisect_right(self._sorted_y, (e_y_max, _ide))
        y_range = set((_e for _y, _ide, _e in self._sorted_y[e_y_min_pos:e_y_max_pos]))

        return list(x_range.intersection(y_range))

    def __len__(self):
        return len(self._info)


class BinArray(object):
    def __init__(self, cell_size=64):
        self._cell_size = cell_size
        self._cells = []  # [x][y]{}
        self._cell_sets = []
        self._x_dim = 0
        self._y_dim = 0
        # for x in range(1000):
        #     self._cells.append([set() for y in range(1000)])
        self._count = 0
        self._entity_to_cell = collections.defaultdict(set)  # {e: {}}

    def add(self, e, x, y, r):
        self.adds([(e, x, y, r)])

    def adds(self, circles, _int=int):
        self_cells = self._cells
        for e, x, y, r in circles:
            self._count += 1
            if e in self._entity_to_cell:
                self.remove(e)

            cx_min = _int((x - r) // self._cell_size)
            cx_max = _int((x + r) // self._cell_size)
            cy_min = _int((y - r) // self._cell_size)
            cy_max = _int((y + r) // self._cell_size)

            if cx_max >= self._x_dim or cy_max >= self._y_dim or -cx_min >= self._x_dim or -cy_min <= self._y_dim:
                self._x_dim = cx_max if -cx_min < cx_max else -cx_min
                self._y_dim = cy_max if -cy_min < cy_max else -cy_min
                len_self_cells = len(self._cells)
                for i in range(len_self_cells, 2 * self._x_dim + 1):
                    sets = [set() for j in range(2 * self._y_dim + 1)]
                    self._cell_sets.extend(sets)
                    self._cells.append(sets)
                for i in range(len_self_cells):
                    if len(self._cells[i]) < self._y_dim + 1:
                        sets = [set() for j in range(2 * self._y_dim + 1)]
                        self._cell_sets.extend(sets)
                        self._cells[i].extend(sets)

            y_range = range(cy_min, cy_max + 1)
            for cx in range(cx_min, cx_max + 1):
                for cy in y_range:
                    self._entity_to_cell[e].add((cx, cy))
                    self_cells[cx][cy].add(e)

    def collides(self, circles, do_self_collision=False, _int=int):
        result = collections.defaultdict(set)  # {e: [other]}
        self_cells = self._cells
        for e, x, y, r in circles:
            cx_min = _int((x - r) // self._cell_size)
            cx_max = _int((x + r) // self._cell_size)
            cy_min = _int((y - r) // self._cell_size)
            cy_max = _int((y + r) // self._cell_size)

            if cx_max >= self._x_dim or cy_max >= self._y_dim or -cx_min >= self._x_dim or -cy_min <= self._y_dim:
                self._x_dim = cx_max if -cx_min < cx_max else -cx_min
                self._y_dim = cy_max if -cy_min < cy_max else -cy_min
                len_self_cells = len(self._cells)
                for i in range(len_self_cells, 2 * self._x_dim + 1):
                    sets = [set() for j in range(2 * self._y_dim + 1)]
                    self._cell_sets.extend(sets)
                    self._cells.append(sets)
                for i in range(len_self_cells):
                    if len(self._cells[i]) < self._y_dim + 1:
                        sets = [set() for j in range(2 * self._y_dim + 1)]
                        self._cell_sets.extend(sets)
                        self._cells[i].extend(sets)

            y_range = range(cy_min, cy_max + 1)
            result_e__add = result[e].add
            for ex in range(cx_min, cx_max + 1):
                for ey in y_range:
                    entities = self_cells[ex][ey]
                    for other in entities:
                        if other in result:
                            result[other].add(e)
                        else:
                            result_e__add(other)

            if not do_self_collision:
                try:
                    result[e].remove(e)
                except KeyError:
                    pass

        return result

    def get_in_rect(self, x, y, w, h):
        result = set()
        cx_min = int(x // self._cell_size)
        cx_max = int((x + w) // self._cell_size)
        cy_min = int(y // self._cell_size)
        cy_max = int((y + h) // self._cell_size)

        if cx_max >= self._x_dim or cy_max >= self._y_dim or -cx_min >= self._x_dim or -cy_min <= self._y_dim:
            self._x_dim = cx_max if -cx_min < cx_max else -cx_min
            self._y_dim = cy_max if -cy_min < cy_max else -cy_min
            for i in range(len(self._cells), self._x_dim + 1):
                sets = [set() for j in range(self._y_dim + 1)]
                self._cell_sets.extend(sets)
                self._cells.append(sets)
            for i in range(len(self._cells)):
                if len(self._cells[i]) < self._y_dim + 1:
                    sets = [set() for j in range(self._y_dim + 1)]
                    self._cell_sets.extend(sets)
                    self._cells[i].extend(sets)

        for ex in range(cx_min, cx_max + 1):
            for ey in range(cy_min, cy_max + 1):
                entities = self._cells[ex][ey]
                result.update(entities)

        return list(result)

    def __len__(self):
        return self._count

    def remove(self, entity):
        self.removes([entity])

    def removes(self, entities):
        for e in entities:
            cells = self._entity_to_cell.pop(e, set())
            if cells:
                self._count -= 1
                for cell in cells:
                    cx, cy = cell
                    cell_set = self._cells[cx][cy]
                    cell_set.remove(e)

    def clear(self):
        self._count = 0
        for cell_set in self._cell_sets:
            cell_set.clear()
        self._entity_to_cell.clear()


class BinSweepVerySlow(object):
    def __init__(self):
        self._sorted_x_s = []
        self._sorted_x = []
        self._sorted_y_s = []
        self._sorted_y = []
        self._info = {}  # {e:(x_min, x_max, y_min, y_max)}

    def add(self, e, x, y, r):
        self.adds([(e, x, y, r)])

    def adds(self, entities):

        for e, x, y, r in entities:
            _ide = id(e)
            # if _ide in self._info:
            self.remove(e)

            x_min = x - r
            x_max = x + r
            y_min = y - r
            y_max = y + r
            self._info[_ide] = (x_min, x_max, y_min, y_max)
            insort(self._sorted_x_s, (x_min, _ide, e))
            insort(self._sorted_x, (x_max, _ide, e))
            insort(self._sorted_y_s, (y_min, _ide, e))
            insort(self._sorted_y, (y_max, _ide, e))

    def remove(self, e):
        self.removes([e])

    def removes(self, entities):
        for e in entities:
            _ide = id(e)
            if _ide in self._info:
                x_min, x_max, y_min, y_max = self._info[_ide]
                x_pos_min = bisect_right(self._sorted_x_s, (x_min, _ide, e))
                del self._sorted_x_s[x_pos_min - 1]
                x_pos_max = bisect_right(self._sorted_x, (x_max, _ide, e))
                del self._sorted_x[x_pos_max - 2]  # x_pos_max - 2 because be ust remove an element
                y_pos_min = bisect_right(self._sorted_y_s, (y_min, _ide, e))
                del self._sorted_y_s[y_pos_min - 1]
                y_pos_max = bisect_right(self._sorted_y, (y_max, _ide, e))
                del self._sorted_y[y_pos_max - 2]

                del self._info[_ide]

    def clear(self):
        self._info.clear()
        self._sorted_x_s[:] = []
        self._sorted_x[:] = []
        self._sorted_y_s[:] = []
        self._sorted_y[:] = []

    def collides(self, circles, self_collision=False):
        result = collections.defaultdict(set)
        for e, x, y, r in circles:
            e_x_min = x - r
            e_x_max = x + r
            e_y_min = y - r
            e_y_max = y + r
            _ide = id(e)
            e_x_min_pos = bisect_right(self._sorted_x_s, (e_x_max, _ide, e))
            e_x_max_pos = bisect_left(self._sorted_x, (e_x_min, _ide, e))
            # e_x_max_pos += 1 if e_x_min_pos == e_x_max_pos else 0
            x_range = set((_e for _x, _ide, _e in self._sorted_x[:e_x_min_pos]))
            x_range |= set((_e for _x, _ide, _e in self._sorted_x[e_x_max_pos:]))

            e_y_min_pos = bisect_right(self._sorted_y_s, (e_y_max, _ide, e))
            e_y_max_pos = bisect_left(self._sorted_y, (e_y_min, _ide, e))
            # e_y_max_pos += 1 if e_y_min_pos == e_y_max_pos else 0
            y_range = set((_e for _y, _ide, _e in self._sorted_y[:e_y_min_pos]))
            y_range |= set((_e for _y, _ide, _e in self._sorted_y[e_y_max_pos:]))

            result[e] = x_range.intersection(y_range)

            if not self_collision:
                try:
                    result[e].remove(e)
                except KeyError:
                    pass

        return result

    def get_in_rect(self, x, y, w, h):
        e_x_min = x
        e_x_max = x + w
        e_y_min = y
        e_y_max = y + h
        e_x_min_pos = bisect_left(self._sorted_x, (e_x_min, None))
        e_x_max_pos = bisect_right(self._sorted_x, (e_x_max, None))
        x_range = set((_e for _x, _ide, _e in self._sorted_x[e_x_min_pos:e_x_max_pos]))

        e_y_min_pos = bisect_left(self._sorted_y, (e_y_min, None))
        e_y_max_pos = bisect_right(self._sorted_y, (e_y_max, None))
        y_range = set((_e for _y, _ide, _e in self._sorted_y[e_y_min_pos:e_y_max_pos]))

        return list(x_range.intersection(y_range))

    def __len__(self):
        return len(self._info)


class BinHashWhile(object):
    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size

    def add(self, e, x, y, r):
        self.adds([(e, x, y, r)])

    def adds(self, circles, _int=int):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        for e, x, y, r in circles:
            if e in self_entity_to_cells:
                self.remove(e)

            cx_min = _int((x - r) // self__cell_size)
            cx_max = (x + r) // self__cell_size
            cy_min = (y - r) // self__cell_size
            cy_max = _int((y + r) // self__cell_size)

            self_entity_to_cells_e__add = self_entity_to_cells[e].add
            # y_range = range(cy_min, cy_max + 1)
            # for _x in range(cx_min, cx_max + 1):
            #     for _y in y_range:
            _x = cx_min
            while _x <= cx_max:
                _y = cy_min
                while _y <= cy_max:
                    _cell_id = (_x, _y)
                    self_cells[_cell_id].add(e)
                    self_entity_to_cells_e__add(_cell_id)
                    _y += 1
                _x += 1

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides(self, circles, do_self_collision=False, _int=int):
        result = collections.defaultdict(set)
        self_cells = self._cells
        for e, x, y, r in circles:
            cx_min = _int((x - r) // self._cell_size)
            cx_max = (x + r) // self._cell_size
            cy_min = (y - r) // self._cell_size
            cy_max = _int((y + r) // self._cell_size)

            result_e__add = result[e].add
            # y_range = range(cy_min, cy_max + 1)
            # for _x in range(cx_min, cx_max + 1):
            #     for _y in y_range:
            _x = cx_min
            while _x <= cx_max:
                _y = cy_min
                while _y <= cy_max:
                    for other in self_cells[(_x, _y)]:
                        if other in result:
                            result[other].add(e)
                        else:
                            result_e__add(other)
                    _y += 1
                _x += 1
            if not do_self_collision:
                try:
                    result[e].remove(e)
                except KeyError:
                    pass

        return result

    def get_in_rect(self, x, y, w, h):
        result = set()
        cx_min = int(x // self._cell_size)
        cx_max = int((x + w) // self._cell_size)
        cy_min = int(y // self._cell_size)
        cy_max = int((y + h) // self._cell_size)
        self_cells = self._cells
        y_range = range(cy_min, cy_max + 1)
        result_update = result.update

        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result_update(self_cells[(_x, _y)])

        return list(result)

    def remove(self, entity):
        self.removes([entity])

    def removes(self, entities):
        for e in entities:
            if e in self._entity_to_cells:
                for cell in self._entity_to_cells.pop(e, []):
                    self._cells[cell].remove(e)


class GummHash(object):
    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size

    def add(self, entity):
        self.addlist([entity])

    def addlist(self, entities):
        self__cells = self._cells
        self__cell_size = self._cell_size
        self__entity_to_cells = self._entity_to_cells
        self__remove = self.remove
        for e in entities:
            rect = e.rect
            x = rect[0]
            y = rect[1]
            w = rect[2]
            h = rect[3]
            if e in self__entity_to_cells:
                self__remove(e)

            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            y_range = range(cy_min, cy_max + 1)
            self__entity_to_cells_e__add = self__entity_to_cells[e].add
            for _x in range(cx_min, cx_max + 1):
                for _y in y_range:
                    _cell_id = (_x, _y)
                    self__cells[_cell_id].add(e)
                    self__entity_to_cells_e__add(_cell_id)

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides(self, entities, do_self_collision=False):
        result = collections.defaultdict(set)
        self__cells = self._cells
        self__cell_size = self._cell_size
        for e in entities:
            rect = e.rect
            colliderect = rect.colliderect
            x = rect[0]
            y = rect[1]
            w = rect[2]
            h = rect[3]
            cx_min = x // self__cell_size
            cx_max = (x + w) // self__cell_size
            cy_min = y // self__cell_size
            cy_max = (y + h) // self__cell_size

            result_e__add = result[e].add
            y_range = range(cy_min, cy_max + 1)
            for _x in range(cx_min, cx_max + 1):
                for _y in y_range:
                    for other in self__cells[(_x, _y)]:
                        if colliderect(other.rect):
                            if other in result:
                                result[other].add(e)
                            else:
                                result_e__add(other)
            if not do_self_collision:
                try:
                    result[e].remove(e)
                    if not result[e]:
                        result.pop(e)
                except KeyError:
                    pass

        return result

    def get_in_rect(self, rect):
        x = rect[0]
        y = rect[1]
        w = rect[2]
        h = rect[3]
        self__cell_size = self._cell_size
        self__cells = self._cells
        result = set()
        result__update = result.update

        cx_min = x // self__cell_size
        cx_max = (x + w) // self__cell_size
        cy_min = y // self__cell_size
        cy_max = (y + h) // self__cell_size

        y_range = range(cy_min, cy_max + 1)
        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result__update(self__cells[(_x, _y)])

        return result

    def remove(self, entity):
        self.removes([entity])

    def removes(self, entities):
        self__entity_to_cells = self._entity_to_cells
        self__entity_to_cells__pop = self__entity_to_cells.pop
        self__cells = self._cells
        for e in entities:
            if e in self__entity_to_cells:
                for cell_id in self__entity_to_cells__pop(e):
                    self__cells[cell_id].remove(e)


class BinSpace1(object):

    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size  # TODO: make it adaptive? but then one need more info about the entity (size, pos)!

    def add(self, e, x, y, r):
        self.adds([(e, x, y, r)])

    def adds(self, circles, _int=int, _range=range):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        for e, x, y, r in circles:
            if e in self_entity_to_cells:
                self.remove(e)

            cx_min = _int((x - r) // self__cell_size)
            cx_max = _int((x + r) // self__cell_size)
            cy_min = _int((y - r) // self__cell_size)
            cy_max = _int((y + r) // self__cell_size)

            y_range = _range(cy_min, cy_max + 1)
            self_entity_to_cells_e__add = self_entity_to_cells[e].add
            for _x in _range(cx_min, cx_max + 1):
                for _y in y_range:
                    _cell_id = (_x, _y)
                    self_cells[_cell_id].add(e)
                    self_entity_to_cells_e__add(_cell_id)

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides(self, circles, do_self_collision=False, _int=int, _range=range):
        result = collections.defaultdict(set)
        self_cells = self._cells
        for e, x, y, r in circles:
            cx_min = _int((x - r) // self._cell_size)
            cx_max = _int((x + r) // self._cell_size)
            cy_min = _int((y - r) // self._cell_size)
            cy_max = _int((y + r) // self._cell_size)

            result_e__add = result[e].add
            y_range = _range(cy_min, cy_max + 1)
            for _x in _range(cx_min, cx_max + 1):
                for _y in y_range:
                    for other in self_cells[(_x, _y)]:
                        if other in result:
                            result[other].add(e)
                        else:
                            result_e__add(other)
            if not do_self_collision:
                try:
                    result[e].remove(e)
                except KeyError:
                    pass

        return result

    def get_in_rect(self, x, y, w, h, _int=int):
        result = set()
        cx_min = _int(x // self._cell_size)
        cx_max = _int((x + w) // self._cell_size)
        cy_min = _int(y // self._cell_size)
        cy_max = _int((y + h) // self._cell_size)
        self_cells = self._cells
        result_update = result.update

        y_range = range(cy_min, cy_max + 1)
        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result_update(self_cells[(_x, _y)])

        # TODO: maybe return same result structure as collides
        return list(result)

    def remove(self, entity):
        self.removes([entity])

    def removes(self, entities):
        for e in entities:
            if e in self._entity_to_cells:
                for cell in self._entity_to_cells.pop(e, []):
                    self._cells[cell].remove(e)


class BinSpace2(object):

    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size  # TODO: make it adaptive? but then one need more info about the entity (size, pos)!

    def add(self, e, x, y, r):
        self.adds([(e, x, y, r)])

    def adds(self, circles, _int=int, _range=range):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        self_remove = self.remove
        for e, x, y, r in circles:
            if e in self_entity_to_cells:
                self_remove(e)

            cx_min = _int((x - r) // self__cell_size)
            cx_max = _int((x + r) // self__cell_size)
            cy_min = _int((y - r) // self__cell_size)
            cy_max = _int((y + r) // self__cell_size)

            y_range = _range(cy_min, cy_max + 1)
            self_entity_to_cells_e__add = self_entity_to_cells[e].add
            for _x in _range(cx_min, cx_max + 1):
                for _y in y_range:
                    _cell_id = (_x, _y)
                    self_cells[_cell_id].add(e)
                    self_entity_to_cells_e__add(_cell_id)

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides(self, circles, do_self_collision=False, _int=int, _range=range):
        result = collections.defaultdict(set)
        self_cells = self._cells
        self__cell_size = self._cell_size
        others = set()
        others_clear = others.clear
        others_discard = others.discard
        for c in circles:
            # c = e, x, y, r
            r = _int(c[3])
            e = c[0]
            cx_min = _int((c[1] - r) // self__cell_size)
            cx_max = _int((c[1] + r) // self__cell_size)
            cy_min = _int((c[2] - r) // self__cell_size)
            cy_max = _int((c[2] + r) // self__cell_size)

            result_e = result[e]
            result_e__add = result_e.add
            others_clear()
            # for cell_id in _product(_range(cx_min, cx_max + 1), _range(cy_min, cy_max + 1)):
            x_range = _range(cx_min, cx_max + 1)
            for _y in _range(cy_min, cy_max + 1):
                for _x in x_range:
                    others |= self_cells[(_x, _y)]

            if not do_self_collision:
                others_discard(e)

            for other in others:
                if other in result:
                    result[other].add(e)
                else:
                    result_e__add(other)

        return result

    def get_in_rect(self, x, y, w, h, _int=int):
        result = set()
        cx_min = _int(x // self._cell_size)
        cx_max = _int((x + w) // self._cell_size)
        cy_min = _int(y // self._cell_size)
        cy_max = _int((y + h) // self._cell_size)
        self_cells = self._cells
        result_update = result.update

        y_range = range(cy_min, cy_max + 1)
        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result_update(self_cells[(_x, _y)])

        # TODO: maybe return same result structure as collides
        return list(result)

    def remove(self, entity):
        self.removes([entity])

    def removes(self, entities):
        for e in entities:
            if e in self._entity_to_cells:
                for cell in self._entity_to_cells.pop(e, []):
                    self._cells[cell].remove(e)


logger.debug("imported")
