# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'performancecomparison.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

import random
import timeit

import gc

try:
    # sys.path.insert(0, '../')  # find the lib is only the repository has been cloned
    import sys
    import traceback
    import logging
    import math
    import collections
    from itertools import chain
    from bisect import insort, bisect_left, bisect_right

    import pygame

    logging.basicConfig()
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    logger.info("")
    try:
        # raise ImportError()
        import pyknic.mathematics.geometry.binspace as binspace
        logger.info("using pyknic.mathematics.geometry.binspace")
    except ImportError:
        # noinspection PyUnresolvedReferences
        import binspace
        logger.info("using binspace")

    import spatialhash
    import binspacealternatives
    import spacez
    import spacez01
    import spacez02

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2017"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    if sys.version_info[0] == 3:
        xrange = range

    NUM_SPRITES = 50
    SPRITE_SIZE = 10, 10
    PROFILE = False
    RESOLUTION = 800, 600


    class SpriteOrig(pygame.sprite.Sprite):
        def __init__(self, image, rect):
            super(SpriteOrig, self).__init__()
            self.image = image
            self.reset_image = image
            self.rect = rect
            self.world_rect = pygame.display.get_surface().get_rect()
            self.x = rect.x
            self.y = rect.y
            self.dirx = random.choice([-75, -50, 50, 75])
            self.diry = random.choice([-75, -50, 50, 75])
            self.moved = False
            self.radius = math.hypot(rect.w, rect.h) / 2

        def update(self, dt):
            dirx = dt * self.dirx
            diry = dt * self.diry
            self.image = self.reset_image
            self.moved = False
            rect = self.rect
            newx = self.x + dirx
            newy = self.y + diry
            world_rect = self.world_rect
            if newx < 0 or newx >= world_rect.right:
                self.dirx = -self.dirx
            if newy < 0 or newy >= world_rect.bottom:
                self.diry = -self.diry
            rect.x = self.x = newx
            rect.y = self.y = newy
            self.moved = True

        def dim(self):
            return self, self.rect.centerx, self.rect.centery, self.radius

    class PosContainter(object):

        def __init__(self):
            self.x = 0
            self.y = 0


    class Sprite(pygame.sprite.Sprite):
        def __init__(self, image, rect):
            super(Sprite, self).__init__()
            self.image = image
            self.reset_image = image
            self.rect = rect
            self.aabb = rect
            self.world_rect = pygame.display.get_surface().get_rect()
            self.x = rect.centerx
            self.y = rect.centery
            self.position = PosContainter()
            self.position.x = self.x
            self.position.y = self.y
            self.dirx = random.choice([-75, -50, 50, 75])
            self.diry = random.choice([-75, -50, 50, 75])
            self.moved = False
            self.radius = math.hypot(rect.w, rect.h) / 2
            self.dim = [self, self.x, self.y, self.radius]

        def update(self, dt):
            dirx = dt * self.dirx
            diry = dt * self.diry
            self.image = self.reset_image
            self.moved = False
            rect = self.rect
            newx = self.x + dirx
            newy = self.y + diry
            world_rect = self.world_rect
            if newx < 0 or newx >= world_rect.right:
                self.dirx = -self.dirx
            if newy < 0 or newy >= world_rect.bottom:
                self.diry = -self.diry
            rect.centerx = self.position.y = self.x = newx
            rect.centery = self.position.x = self.y = newy
            self.dim[1] = rect.centerx
            self.dim[2] = rect.centery
            self.moved = True

        # def dim(self):
        #     return self, self.rect.centerx, self.rect.centery, self.radius

        def __str__(self):
            return str(id(self))

        def __repr__(self):
            return self.__str__()


    class IType(object):
        Unknown = 0,
        BTuples = 1,
        GummHashEntities = 2,
        Spatial = 3,
        BSprites = 4,
        Spacez = 5


    class PerformanceTester(object):

        def __init__(self):
            self.running = True
            self.sprites = []
            self.other_sprites = []
            self.spatial = None
            self.spatial_idx = -2
            self.space = None
            self.spaces = []
            self.space_idx = 0
            self.space_type = 6
            self.spatial_type = 0
            self.cell_size = 32
            self.paused = False
            self.step = False
            self.draw_sprites = True
            self.img_spat = None
            self.img_bin = None
            self._show_grid = True

            self.frame_nr = 0
            self.s_sum = 0
            self.b_sum = 0
            self.dims = []

        def run(self):
            logger.info("")
            logger.info("usage:")
            logger.info("'enter' to pause/un-pause")
            logger.info("'space' to step")
            logger.info("'s: see grid")
            logger.info("'a: show/hide grid")
            logger.info("'tab: toggle sprites shapes")
            logger.info("'q|w and e|r: cycle left binspace implementations or right bucket implementation")
            logger.info("u|j increase/decrease sprite count about 100")
            logger.info("")
            logger.info("")
            logger.info("press enter to start...")
            pygame.init()
            pygame.display.set_mode(RESOLUTION)

            self.init_spaces()

            self.init_sprites(NUM_SPRITES)

            while self.running:
                dt = 1.0 / 270
                self.handle_events()
                if not self.paused or self.step:
                    self.update(dt)
                    self.draw()
                    self.step = False

        def init_spaces(self):
            self.spaces = [
                self._create_spherebinspace2,
                self._create_aabbbinspace2,
                self._create_alt_binspace2,
                self._create_alt_binspace1,
                self._create_alt_binhashwhile,
                self._create_alt_binarray,
                self._create_alt_altbinspace,
                self._create_alt_binrange,
                # (binspacealternatives.BinSweepVerySlow(), IType.BTuples),
                self._create_spatialhash,
                self._create_gummhash,
                self._create_alt_binspaceentities2,
                self._create_alt_binspaceentities,
                self._create_spacez,
                # self._create_spacez1,
                self._create_spacez2,
            ]
            self.space_idx = -1
            self.spatial_idx = -2
            self.space, self.space_type = self.spaces[self.space_idx]()
            self.spatial, self.spatial_type = self.spaces[self.spatial_idx]()

        def _create_spacez(self):
            return spacez.Spacez("spacez", self.cell_size), IType.Spacez

        def _create_spacez1(self):
            return spacez01.new("spacez", self.cell_size), IType.Spatial

        def _create_spacez2(self):
            return spacez02.Spacez("spacez", self.cell_size), IType.Spatial

        def _create_alt_binspaceentities(self):
            return binspacealternatives.BinSpaceEntities(self.cell_size), IType.BSprites

        def _create_alt_binspaceentities2(self):
            return binspacealternatives.BinSpaceEntities2(self.cell_size), IType.BSprites

        def _create_gummhash(self):
            return binspacealternatives.GummHash(self.cell_size), IType.GummHashEntities

        def _create_spatialhash(self):
            return spatialhash.SpatialHash(self.cell_size), IType.Spatial

        def _create_alt_binrange(self):
            return binspacealternatives.BinRange(), IType.BTuples

        def _create_alt_altbinspace(self):
            return binspacealternatives.AltBinSpace(cell_to_sphere_ratio=1 / 2, min_cell_size=self.cell_size), IType.BTuples

        def _create_alt_binarray(self):
            return binspacealternatives.BinArray(self.cell_size), IType.BTuples

        def _create_alt_binhashwhile(self):
            return binspacealternatives.BinHashWhile(self.cell_size), IType.BTuples

        def _create_alt_binspace1(self):
            return binspacealternatives.BinSpace1(self.cell_size), IType.BTuples

        def _create_alt_binspace2(self):
            return binspacealternatives.BinSpace2(self.cell_size), IType.BTuples

        def _create_aabbbinspace2(self):
            return binspace.AabbBinSpace2(self.cell_size), IType.BSprites

        def _create_spherebinspace2(self):
            return binspace.SphereBinSpace2(self.cell_size), IType.BSprites

        def handle_events(self):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.KEYDOWN:
                    current_sprite_count = len(self.sprites)
                    if event.key == pygame.K_ESCAPE:
                        self.running = False
                    elif event.key in (pygame.K_p, pygame.K_RETURN):
                        self.paused = not self.paused
                    elif event.key == pygame.K_SPACE:
                        self.step = True
                        if not self.paused:
                            self.paused = True
                    elif event.key == pygame.K_s:
                        self.draw_sprites = not self.draw_sprites
                    elif event.key == pygame.K_TAB:
                        self.sprites, self.other_sprites = self.other_sprites, self.sprites
                        self.dims = [s.dim for s in self.sprites]
                        self.reset_avg_timings()
                    elif event.key == pygame.K_e:
                        self.space_idx += 1
                        self.space_idx = self.space_idx % len(self.spaces)
                        self.space, self.space_type = self.spaces[self.space_idx]()
                        self.reset_avg_timings()
                    elif event.key == pygame.K_r:
                        self.space_idx -= 1
                        self.space_idx = self.space_idx % len(self.spaces)
                        self.space, self.space_type = self.spaces[self.space_idx]()
                        self.reset_avg_timings()
                    elif event.key == pygame.K_a:
                        self._show_grid = not self._show_grid
                    elif event.key == pygame.K_q:
                        self.spatial_idx += 1
                        self.spatial_idx = self.spatial_idx % len(self.spaces)
                        self.spatial, self.spatial_type = self.spaces[self.spatial_idx]()
                        self.reset_avg_timings()
                    elif event.key == pygame.K_w:
                        self.spatial_idx -= 1
                        self.spatial_idx = self.spatial_idx % len(self.spaces)
                        self.spatial, self.spatial_type = self.spaces[self.spatial_idx]()
                        self.reset_avg_timings()
                    elif event.key == pygame.K_t:
                        self.cell_size += 5
                        self.init_spaces()
                    elif event.key == pygame.K_g:
                        self.cell_size -= 5
                        self.init_spaces()
                    elif event.key == pygame.K_u:
                        self.init_sprites(current_sprite_count + 100)
                        self.reset_avg_timings()
                    elif event.key == pygame.K_j:
                        count = current_sprite_count if current_sprite_count <= 100 else current_sprite_count - 100
                        self.init_sprites(count)
                        self.reset_avg_timings()
                    self.reset_avg_timings()  # do it for every key stroke

                    logger.info("spaces: %s vs %s", self.spatial.__class__.__name__, self.space.__class__.__name__)

        def reset_avg_timings(self):
            self.frame_nr = 0
            self.s_sum = 0
            self.b_sum = 0

        def update(self, dt):
            for spr in self.sprites:
                spr.update(dt)

            s_col, s_up = self.update_timeit("self.spatial", self.spatial_type)
            b_col, b_up = self.update_timeit("self.space", self.space_type)

            logger.info(">>> spatial  tot: %s <- %s -> %s :binspace tot",
                        s_up + s_col, (b_up + b_col)/(s_up + s_col), b_up + b_col)

            self.frame_nr += 1
            self.s_sum += s_up + s_col
            self.b_sum += b_up + b_col

            s_avg = self.s_sum / self.frame_nr
            b_avg = self.b_sum / self.frame_nr
            logger.info("<<< spatial avg: %s -> %s <- %s :binspace avg", s_avg, b_avg / s_avg, b_avg)

            logger.info("-"*20)

            caption = "{0} {1:.5f} |  {2:.5f} | {3:.5f} {4}    cell size: {5}    #sprites: {6}".format(
                # self.spatial.__class__.__name__, s_avg, b_avg / s_avg, b_avg, self.space.__class__.__name__,
                self.spatial.__class__, s_avg, b_avg / s_avg, b_avg, self.space.__class__,
                self.cell_size, len(self.sprites))
            pygame.display.set_caption(caption)

        def update_timeit(self, spatial, spatial_type):
            gc.collect()
            gc.disable()

            if spatial_type == IType.Spatial:
                s_up = self.timeit("self.update_sprites(" + spatial + ")")
                s_col = self.timeit("self.get_spatial_collisions(" + spatial + ")")
            elif spatial_type == IType.BTuples:
                s_up = self.timeit("self.update_space_tuples(" + spatial + ")")
                s_col = self.timeit("self.get_binspace_collisions(" + spatial + ")")
            elif spatial_type == IType.GummHashEntities:
                s_up = self.timeit("self.update_sprites(" + spatial + ")")
                s_col = self.timeit("self.get_entities_collisions(" + spatial + ")")
            elif spatial_type == IType.BSprites:
                s_up = self.timeit("self.update_entities(" + spatial + ")")
                s_col = self.timeit("self.get_binspace_entities_collisions(" + spatial + ")")
            elif spatial_type == IType.Spacez:
                s_up = self.timeit("self.update_sprites(" + spatial + ")")
                s_col = self.timeit("self.get_spacez_collisions(" + spatial + ")")
            else:
                raise Exception("not supported interface type!")
            gc.enable()
            return s_col, s_up

        def update_sprites(self, spatial):
            spatial.clear()
            spatial.addlist(self.sprites)

        def update_entities(self, spatial):
            spatial.clear()
            spatial.adds(self.sprites)

        def update_space_tuples(self, space):
            space.clear()
            space.adds([s.dim for s in self.sprites])

        def get_spacez_collisions(self, space):
            col_pairs = space.collideallpairs()
            bin_col_objs = set()
            bin_col_objs.update([c[0] for c in col_pairs])
            bin_col_objs.update([c[1] for c in col_pairs])
            return bin_col_objs

        def get_binspace_collisions(self, space):
            bin_col_pairs = space.collides(self.dims)

            col_pairs_count = 0

            # use this part if collides returns a dict
            bin_col_objs = set()
            bin_col_objs__add = bin_col_objs.add
            for a, cell in bin_col_pairs.items():
                hits = False
                a__rect__colliderect = a.rect.colliderect
                for b in cell:
                    if a__rect__colliderect(b):
                        bin_col_objs__add(b)
                        hits = True
                if hits:
                    bin_col_objs__add(a)

            return bin_col_objs, col_pairs_count

        def get_binspace_entities_collisions(self, space):
            # assert isinstance(space, binspace.AabbBinSpace2) or \
            #        isinstance(space, binspacealternatives.BinSpaceEntities) or \
            #         isinstance(space, binspacealternatives.BinSpaceEntities2) or \
            #         isinstance(space, binspace.SphereBinSpace2)
            bin_col_pairs = space.collides(self.sprites)

            bin_col_objs = set()
            for a, b in bin_col_pairs.items():
                bin_col_objs |= b
                if len(b) > 0:
                    bin_col_objs.add(a)
            return bin_col_objs

        def get_spatial_collisions(self, spatial):
            # assert isinstance(spatial, spatialhash.SpatialHash)
            bin_col_pairs = spatial.collidedict(self.sprites)

            bin_col_objs = set(bin_col_pairs)
            bin_col_objs.update(chain.from_iterable(bin_col_pairs.values()))
            # for a, b in bin_col_pairs.items():
            #     bin_col_objs |= b
            #     if len(b) > 0:
            #         bin_col_objs.add(a)
            return bin_col_objs

        def get_entities_collisions(self, space):
            assert isinstance(space, binspacealternatives.GummHash), "{0}".format(space)
            bin_col_pairs = space.collides(self.sprites)

            # use this part if collides returns a dict
            bin_col_objs = set()
            for a, b in bin_col_pairs.items():
                bin_col_objs |= b
                if len(b) > 0:
                    bin_col_objs.add(a)
            return bin_col_objs

        def init_sprites(self, num_sprites):
            self.sprites[:] = []
            self.other_sprites[:] = []
            for idx in range(2):
                self.sprites, self.other_sprites = self.other_sprites, self.sprites
                for i in range(num_sprites):
                    spr_size = (10, 10)
                    if idx == 1:
                        spr_size = (random.randrange(1, 50), random.randrange(1, 50))
                    spr = self._create_sprite(spr_size)
                    self.sprites.append(spr)

            # <rect(430, 497, 26, 22)> vs <rect(450, 515, 3, 1)>
            s1 = self._create_sprite((26, 22))
            s1.x = 430
            s1.y = 497
            s1.dirx = 0
            s1.diry = 0
            self.other_sprites.append(s1)

            s1 = self._create_sprite((3, 1))
            s1.x = 450
            s1.y = 515
            s1.dirx = 0
            s1.diry = 0
            self.other_sprites.append(s1)

            self.dims = [s.dim for s in self.sprites]

        def _create_sprite(self, spr_size):
            img = pygame.Surface(spr_size, pygame.SRCALPHA, 32)
            col = 80
            img.fill((col, col, col, col), None, pygame.BLEND_RGBA_ADD)
            pygame.draw.rect(img, pygame.Color('darkgoldenrod3'), img.get_rect(), 1)
            x = random.randrange(0, RESOLUTION[0])
            y = random.randrange(0, RESOLUTION[1])
            rect = img.get_rect(center=(x, y))
            spr = Sprite(img, rect)
            return spr

        def timeit(self, stmt, number=1):
            time_spent = timeit.timeit(stmt, globals=locals(), number=number)
            logger.info("%s\t %s", time_spent, stmt)
            return time_spent

        def draw(self):
            # clear
            screen = pygame.display.get_surface()
            screen.fill((0, 0, 0))

            if self._show_grid:
                steps = [10000]
                if hasattr(self.space, '_obj_at_level'):
                    # noinspection PyProtectedMember
                    steps = list(self.space._obj_at_level.keys())
                steps.sort()
                color_step = int(230 // len(steps))
                colors = [[0, color_step * i + 25, 0] for i in range(1, len(steps) + 1)]
                for idx, step in enumerate(steps):
                    line_color = colors[idx]
                    for x in range(0, RESOLUTION[0], step):
                        pygame.draw.line(screen, line_color, (x, 0), (x, RESOLUTION[1]), 1)
                    for y in range(0, RESOLUTION[1], step):
                        pygame.draw.line(screen, line_color, (0, y), (RESOLUTION[0], y), 1)

            # sprites
            if self.draw_sprites:
                for spr in self.sprites:
                    screen.blit(spr.image, spr.rect, None, pygame.BLEND_RGBA_ADD)

            col_pairs_count = 0

            spat_col_objs = self.get_col_objs(self.spatial, self.spatial_type)
            bin_col_objs = self.get_col_objs(self.space, self.space_type)

            # check differences
            set1 = set(spat_col_objs)
            set2 = set(bin_col_objs)
            logger.info("coll obj count: spat: %s <-> %s   bin pairs %s", len(set1), len(set2), col_pairs_count)
            if len(set1) != len(set2):
                diff1 = set1 - set2
                logger.info("diff left - right: %s", diff1)
                diff2 = set2 - set1
                logger.info("diff right - left: %s", diff2)

                diff = diff1 | diff2
                logger.info("diff: %s", diff)
                colliders = set()
                for d in diff:
                    pygame.draw.rect(screen, (255, 0, 0), d.rect, 3)
                    objs_ = list(spat_col_objs)
                    for idx in d.rect.collidelistall(objs_):
                        other = objs_[idx]
                        if d is other:
                            continue
                        colliders.add((d, other))
                if colliders:
                    self.paused = True
                    logger.info("pausing due to different number of collisions: " + str(len(colliders)))
                    colliders = set(((a, b) if id(a) < id(b) else (b, a) for a, b in colliders))
                    for a, b in colliders:
                        logger.info("missed entities: %s vs %s", a.rect, b.rect)
                        pygame.draw.rect(screen, (0, 255, 255), a.rect)
                        pygame.draw.rect(screen, (255, 0, 0), b.rect)

            # flip
            pygame.display.flip()

        def get_col_objs(self, spatial, spatial_type):
            if spatial_type == IType.Spatial:
                spat_col_objs = self.get_spatial_collisions(spatial)
            elif spatial_type == IType.BTuples:
                spat_col_objs, _pairs_count = self.get_binspace_collisions(spatial)
            elif spatial_type == IType.GummHashEntities:
                spat_col_objs = self.get_entities_collisions(spatial)
            elif spatial_type == IType.BSprites:
                spat_col_objs = self.get_binspace_entities_collisions(spatial)
            elif spatial_type == IType.Spacez:
                spat_col_objs = self.get_spacez_collisions(spatial)
            else:
                raise Exception("not supported interface type!")
            return spat_col_objs


    def main(args=None):
        """
        The main  function.

        :param args: Argument list to use. This is a list of strings like sys.argv. See -h for complete argument list.
            Default=None so the sys.args are used.
        """
        performer = PerformanceTester()
        performer.run()

    if __name__ == '__main__':
        if PROFILE:
            import cProfile
            import pstats
            cProfile.run('main()', 'stats.prof')
            p = pstats.Stats('stats.prof')
            p.sort_stats('time').print_stats()
        else:
            main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
