# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_binspace.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division
import unittest
import warnings

import pygame

import spatialhash as mut  # module under test

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


class _Entity(object):

    _next_id = 0

    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius
        r = radius
        self.rect = pygame.Rect(x-r, y-r, 2*r, 2*r)
        self.id = _Entity._next_id
        _Entity._next_id += 1

    def __str__(self):
        return "_Entity<{0}>".format(self.id)

    def __repr__(self):
        return self.__str__()


class SpatialHashTests(unittest.TestCase):

    def setUp(self):
        self.sut = mut.SpatialHash(32)

    def test_get_empty_area(self):
        # arrange
        # act
        actual = self.sut.intersect_entities(pygame.Rect(5, 5, 10, 10))

        # verify
        self.assertEqual(0, len(actual))

    def test_get_entity_in_first_cell(self):
        # arrange
        e1 = _Entity(6, 6, 5)
        self.sut.add(e1)

        # act
        actual = self.sut.intersect_entities(pygame.Rect(5, 5, 10, 10))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_right_of_a_cell(self):
        # arrange
        e1 = _Entity(64 + 6 + 64, 24, 5)
        self.sut.add(e1)

        # act
        actual = self.sut.collide(_Entity(64 - 3 + 64, 24, 10))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_left_of_a_cell(self):
        # arrange
        e1 = _Entity(64 - 6 + 64, 24, 5)
        self.sut.add(e1)

        # act
        actual = self.sut.collide(_Entity(64 + 3 + 64, 24, 10))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_top_of_a_cell(self):
        # arrange
        e1 = _Entity(24, 64 - 6 + 64, 5)
        self.sut.add(e1)

        # act
        actual = self.sut.collide(_Entity(24, 64 + 3 + 64, 10))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_bottom_of_a_cell(self):
        # arrange
        e1 = _Entity(24, 70 + 64, 5)
        self.sut.add(e1)

        # act
        e2 = _Entity(24, 61 + 64, 10)
        actual = self.sut.collide(e2)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_diagonal_NE_of_a_cell(self):
        # arrange
        e1 = _Entity(64 + 6 + 128, 64 - 6 + 64, 5)
        self.sut.add(e1)
        e2 = _Entity(64 - 3 + 128, 64 + 3 + 64, 10)

        # act
        actual = self.sut.collide(e2)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_diagonal_NW_of_a_cell(self):
        # arrange
        e1 = _Entity(64 - 6, 64 - 6 + 64, 5)
        self.sut.add(e1)
        e2 = _Entity(64 + 3, 64 + 3 + 64, 10)

        # act
        actual = self.sut.collide(e2)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_diagonal_SW_of_a_cell(self):
        # arrange
        e1 = _Entity(64 - 6, 64 + 6 + 128, 5)
        self.sut.add(e1)
        e2 = _Entity(64 + 3, 64 - 3 + 128, 10)

        # act
        actual = self.sut.collide(e2)

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entity_overlaps_diagonal_SE_of_a_cell(self):
        # arrange
        e1 = _Entity(64 + 6 + 64, 64 + 6 + 64, 5)
        self.sut.add(e1)

        # act
        actual = self.sut.collide(_Entity(64 - 3 + 64, 64 - 3 + 64, 10))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_small_entity(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add(e1)

        # act
        actual = self.sut.intersect_entities(pygame.Rect(50, 50, 20, 20))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_different_sized_entities_same_spot(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add(e1)
        e2 = _Entity(60, 60, 3)
        self.sut.add(e2)

        # act
        actual = self.sut.intersect_entities(pygame.Rect(50, 50, 20, 20))

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(e1 in actual)
        self.assertTrue(e2 in actual)

    def test_get_different_sized_entities_different_spot(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add(e1)
        e2 = _Entity(200, 200, 3)
        self.sut.add(e2)

        # act
        actual = self.sut.intersect_entities(pygame.Rect(50, 50, 200, 200))

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(e1 in actual)
        self.assertTrue(e2 in actual)

    def test_get_same_sized_entities_in_different_cells(self):
        # arrange
        e1 = _Entity(600, 600, 1)
        self.sut.add(e1)
        e2 = _Entity(20, 20, 1)
        self.sut.add(e2)

        # act
        actual = self.sut.intersect_entities(pygame.Rect(590, 590, 20, 20))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual(e1, actual[0])

    def test_get_entities_with_different_sizes_mass_adding(self):
        # arrange
        entities = [_Entity(60, 60, 1), _Entity(200, 200, 3)]
        self.sut.addlist(entities)

        # act
        actual = self.sut.intersect_entities(pygame.Rect(50, 50, 200, 200))

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(entities[0] in actual)
        self.assertTrue(entities[1] in actual)

    def test_element_count_with_entities(self):
        # arrange
        e1 = _Entity(10, 10, 2)
        self.sut.add(e1)
        e2 = _Entity(20, 20, 10)
        self.sut.add(e2)

        # act
        actual = len(self.sut.entities)

        # verify
        self.assertEqual(2, actual, "bin space len should be 1")

    def test_empty_element_count(self):
        # arrange

        # act
        actual = len(self.sut.entities)

        # verify
        self.assertEqual(0, actual, "empty count should be 0")

    def test_remove_entity(self):
        # arrange
        e1 = _Entity(10, 10, 10)
        self.sut.add(e1)

        # act
        self.sut.remove(e1)

        # verify
        self.assertEqual(0, len(self.sut.entities))

    def test_mass_removable_of_entities(self):
        # arrange
        entities = [_Entity(60, 60, 1), _Entity(200, 200, 3)]
        self.sut.addlist(entities)

        # act
        for entity in entities:
            self.sut.remove(entity)

        # verify
        self.assertEqual(0, len(self.sut.entities))

    def test_partial_mass_removable_of_entities(self):
        # arrange
        entities = [
            _Entity(60, 60, 1),
            _Entity(200, 200, 3),
            _Entity(0, 0, 3),
            _Entity(206, 206, 3),
        ]
        self.sut.addlist(entities)

        # act
        for ent in entities[:3]:
            self.sut.remove(ent)

        # verify
        self.assertEqual([entities[-1]], self.sut.intersect_entities(pygame.Rect(0, 0, 250, 250)))

    def test_clear(self):
        # arrange
        entities = [
            _Entity(1, 1, 10),
            _Entity(10, 100, 100),
            _Entity(1000, 1000, 1),
            _Entity(125, 1, 50),
            _Entity(1, 221, 20),
        ]
        self.sut.addlist((_e for _e in entities))
        self.assertEqual(len(entities), len(self.sut.entities),
                         "entities in binspace should be equal the entities added")

        # act
        self.sut.clear()

        # verify
        self.assertEqual(0, len(self.sut.entities), "The binspace instance should be empty after clearing.")

    def test_get_collision_pairs_with_others_same_sizes(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 1)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 1)
        entities = [e1, e2, e3, ]

        o1 = _Entity(1, 1, 1)
        o2 = _Entity(1, 1, 1)
        o3 = _Entity(1, 1, 1)
        others = [o1, o2, o3, ]

        expected_pairs = {
            (o1, e1), (o1, e2), (o1, e3),
            (o2, e1), (o2, e2), (o2, e3),
            (o3, e1), (o3, e2), (o3, e3)}

        expected, actual = self._act(entities, others, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_with_others_different_sizes(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 20)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 50)
        entities = [e1, e2, e3, ]

        o1 = _Entity(1, 1, 5)
        o2 = _Entity(1, 1, 100)
        o3 = _Entity(1, 1, 8)
        others = [o1, o2, o3, ]

        expected_pairs = {
            (o1, e1), (o1, e2), (o1, e3),
            (o2, e1), (o2, e2), (o2, e3),
            (o3, e1), (o3, e2), (o3, e3)}
        expected, actual = self._act(entities, others, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_with_self_collision_same_size(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 1)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 1)
        entities = [e1, e2, e3]

        expected_pairs = {(e1, e1), (e1, e2), (e1, e3), (e2, e2), (e3, e2), (e3, e3)}
        expected, actual = self._act(entities, entities, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_with_self_collision_different_sizes(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 20)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 50)
        entities = [e1, e2, e3]

        expected_pairs = {(e1, e1), (e1, e2), (e1, e3), (e2, e2), (e3, e2), (e3, e3)}
        expected, actual = self._act(entities, entities, expected_pairs, True)

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_no_self_collision_same_size(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 1, 1)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 1)
        entities = [e1, e2, e3]

        expected, actual = self._act(entities, entities, {(e1, e2), (e1, e3), (e2, e3)})

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_no_self_collision_different_sizes(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        e1 = _Entity(1, 1, 20)
        e2 = _Entity(1, 1, 1)
        e3 = _Entity(1, 1, 50)
        entities = [e1, e2, e3]

        expected, actual = self._act(entities, entities, {(e1, e2), (e1, e3), (e2, e3)})

        self.assertEqual(expected, actual)

    def test_get_collision_pairs_no_self_collision_very_different_sizes(self):
        # arrange
        self.maxDiff = None
        self.longMessage = True
        self.sut = mut.SpatialHash(32)
        # <rect(268, 232, 8, 38)> vs <rect(270, 208, 2, 32)>
        # e1 = _Entity(270 + 16, 208 + 16, 16)
        # e2 = _Entity(268 + 19, 232 + 19 + 36, 19)
        # <rect(781, 157, 1, 35)> vs <rect(780, 155, 5, 6)>
        # <rect(287, 235, 4, 29)> vs <rect(285, 232, 4, 13)>
        # <rect(7, 70, 3, 9)> vs <rect(5, 77, 3, 30)>
        # <rect(332, 273, 5, 48)> vs <rect(330, 273, 6, 12)>
        # <rect(321, 537, 7, 47)> vs <rect(325, 512, 8, 35)>
        e1 = _Entity(29, 29, 3)
        e2 = _Entity(190, 190, 200)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def _act(self, entities, colliders, expected, self_collision=False):
        # arrange
        self.sut.addlist(entities)

        # act
        if self_collision:
            raise NotImplementedError("no method 'collideflatlist' present")
            actual = self.sut.collideflatlist(colliders)
        else:
            actual = self.sut.collidedict(colliders)

        # verify
        expected = self._get_sorted_pairs(expected)
        actual = self._get_sorted_pairs_from_dict(actual)
        print("expect", expected)
        print("actual", actual)
        return expected, actual

    def _get_sorted_pairs(self, pairs):
        result = list(set([(a, b) if a.id < b.id else (b, a) for a, b in pairs]))
        result.sort(key=lambda t: (t[0].id, t[1].id))
        return result

    def test_add_same_entity_twice(self):
        # arrange
        e = _Entity(1, 1, 1)
        self.sut.add(e)

        # act
        self.sut.add(e)

        # verify
        self.assertEqual(1, len(self.sut.entities))

    def test_remove_entity_not_in_cells(self):
        # arrange
        e = _Entity(1, 1, 1)
        o = _Entity(1, 1, 1)
        self.sut.add(o)

        # act
        self.sut.remove(e)

        # verify
        self.assertEqual(1, len(self.sut.entities))

    def test_remove_entity_twice(self):
        # arrange
        e = _Entity(1, 1, 1)
        self.sut.add(e)
        self.sut.remove(e)

        # act / verify
        self.sut.remove(e)

    def test_entities_in_neighbour_cells_overlap(self):
        # arrange
        e1 = _Entity(31, 31, 3)
        self.sut.add(e1)
        e2 = _Entity(33, 33, 3)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def _get_sorted_pairs_from_dict(self, expected):
        # return self._get_sorted_pairs(expected)
        return self._get_sorted_pairs(((e1, e2) for e1 in expected for e2 in list(expected[e1])))

    def test_get_collision_pair_for_entities_overlapping_near_edge(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(1, 6, 5)
        e2 = _Entity(-6, -1, 5)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def test_get_collision_pair_for_big_vs_small_entity_overlapping_near_edge(self):
        # arrange
        self.maxDiff = None
        e1 = _Entity(25, 0, 30)
        e2 = _Entity(-6, -1, 5)

        expected, actual = self._act([e1], [e2], {(e1, e2)})

        self.assertEqual(expected, actual)

    def test_negative_coordinates(self):
        # arrange
        self.sut = mut.SpatialHash(32)
        e1 = _Entity(30 * 32, -3 * 32, 1)
        self.sut.add(e1)

        # act
        # actual = self.sut.intersect_entities(pygame.Rect(-32, -32, 3 * 32, 3 * 32))
        actual = self.sut.collide(_Entity(0, 0, 5))

        # verify
        expected = []
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
