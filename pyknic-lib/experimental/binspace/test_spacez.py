# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_spacez.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division
import unittest
import warnings

import pygame

import spacez
from test_spatialhash import SpatialHashTests


warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


class _Entity(object):

    _next_id = 0

    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius
        r = radius
        self.rect = pygame.Rect(x-r, y-r, 2*r, 2*r)
        self.id = _Entity._next_id
        _Entity._next_id += 1

    def __str__(self):
        return "_Entity<{0}>".format(self.id)

    def __repr__(self):
        return self.__str__()


class SpaceZTests(SpatialHashTests):

    def setUp(self):
        self.sut = spacez.Spacez(size=32)

    def test_partial_mass_removable_of_entities(self):
        # arrange
        entities = [
            _Entity(60, 60, 1),
            _Entity(200, 200, 3),
            _Entity(0, 0, 3),
            _Entity(206, 206, 3),
        ]
        self.sut.addlist(entities)

        # act
        for ent in entities[:3]:
            self.sut.remove(ent)

        # verify
        self.assertEqual({entities[-1]}, self.sut.entities_in_rect(pygame.Rect(0, 0, 250, 250)))

    def test_get_entities_with_different_sizes_mass_adding(self):
        # arrange
        entities = [_Entity(60, 60, 1), _Entity(200, 200, 3)]
        self.sut.addlist(entities)

        # act
        actual = self.sut.entities_in_rect(pygame.Rect(50, 50, 200, 200))

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(entities[0] in actual)
        self.assertTrue(entities[1] in actual)

    def test_get_different_sized_entities_different_spot(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add(e1)
        e2 = _Entity(200, 200, 3)
        self.sut.add(e2)

        # act
        actual = self.sut.entities_in_rect(pygame.Rect(50, 50, 200, 200))

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(e1 in actual)
        self.assertTrue(e2 in actual)

    def test_get_different_sized_entities_same_spot(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add(e1)
        e2 = _Entity(60, 60, 3)
        self.sut.add(e2)

        # act
        actual = self.sut.entities_in_rect(pygame.Rect(50, 50, 20, 20))

        # verify
        self.assertEqual(2, len(actual))
        self.assertTrue(e1 in actual)
        self.assertTrue(e2 in actual)

    def test_get_empty_area(self):
        # arrange
        # act
        actual = self.sut.entities_in_rect(pygame.Rect(5, 5, 10, 10))

        # verify
        self.assertEqual(0, len(actual))

    def test_get_same_sized_entities_in_different_cells(self):
        # arrange
        e1 = _Entity(600, 600, 1)
        self.sut.add(e1)
        e2 = _Entity(20, 20, 1)
        self.sut.add(e2)

        # act
        actual = self.sut.entities_in_rect(pygame.Rect(590, 590, 20, 20))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual)

    def test_get_small_entity(self):
        # arrange
        e1 = _Entity(60, 60, 1)
        self.sut.add(e1)

        # act
        actual = self.sut.entities_in_rect(pygame.Rect(50, 50, 20, 20))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual)

    def test_get_entity_in_first_cell(self):
        # arrange
        e1 = _Entity(6, 6, 5)
        self.sut.add(e1)

        # act
        actual = self.sut.entities_in_rect(pygame.Rect(5, 5, 10, 10))

        # verify
        self.assertEqual(1, len(actual))
        self.assertEqual({e1}, actual)


if __name__ == '__main__':
    unittest.main()
