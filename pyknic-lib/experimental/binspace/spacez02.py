from collections import namedtuple, defaultdict

NAME = 0
SIZE = 1
ENTITY_MAP = 2
BUCKETS = 3
CACHE = 4
FAST_REMOVE = 5


# TODO: an object-style wrapper?


_SpacezTuple = namedtuple('SpaceZ', 'name size entity_map buckets cache fast_remove')


def new(name='anonymous', size=50, fast_remove=False):
    """create a new spacez hash table
    If you add many redundant sprites then use fast_remove=True. If your sprite lists are clean of dupes then
    fast_remove=False is ideal.
    """
    # space = {NAME: name, SIZE: size, BUCKETS: defaultdict(set), CACHE: {}, FAST_REMOVE: fast_remove}
    space = _SpacezTuple(name=name, size=size,
                   entity_map=defaultdict(set) if fast_remove else set(),
                   buckets=defaultdict(set), cache={}, fast_remove=fast_remove)
    return space


def add(space, *entities):
    addlist(space, entities)


def addlist(space, entities):
    buckets = space[BUCKETS]
    entity_map = space[ENTITY_MAP]
    fast_remove = space[FAST_REMOVE]
    size = space[SIZE]
    cache = space[CACHE]
    set_add = set.add
    set_update = set.update
    for e in entities:
        if e in entity_map:
            remove(space, e)
        rect = e.rect
        x = rect[0]
        y = rect[1]
        left = x // size
        top = y // size
        right = (x + rect[2]) // size
        bottom = (y + rect[3]) // size
        key = left, top, right, bottom
        if key in cache:
            bucket_ids = cache[key]
        else:
            xr = range(left, right + 1)
            yr = range(top, bottom + 1)
            bucket_ids = tuple((x, y) for x in xr for y in yr)
            cache[key] = bucket_ids
        for key in bucket_ids:
            set_add(buckets[key], e)
        if fast_remove:
            set_update(entity_map[e], bucket_ids)
    if not fast_remove:
        set_update(entity_map, entities)


def remove(space, entity):
    buckets = space[BUCKETS]
    entity_map = space[ENTITY_MAP]
    if space[FAST_REMOVE]:
        if e in entity_map:
            for key in entity_map[e]:
                buckets[key].discard(e)
            del entity_map[e]
    else:
        for bucket in buckets.values():
            bucket.discard(e)
        entity_map.discard(e)


def entities(space):
    return tuple(space[ENTITY_MAP])


def count(space):
    return len(space[ENTITY_MAP])


def clear(space):
    space[BUCKETS].clear()
    space[ENTITY_MAP].clear()


def entities_in_rect(space, rect):
    result = set()
    buckets = space[BUCKETS]
    size = space[SIZE]
    cache = space[CACHE]
    result_add = result.add
    colliderect = rect.colliderect
    x = rect[0]
    y = rect[1]
    left = x // size
    top = y // size
    right = (x + rect[2]) // size
    bottom = (y + rect[3]) // size
    key = left, top, right, bottom
    if key in cache:
        bucket_ids = cache[key]
    else:
        xr = range(left, right + 1)
        yr = range(top, bottom + 1)
        bucket_ids = tuple((x, y) for x in xr for y in yr)
        cache[key] = bucket_ids
    for key in bucket_ids:
        if key in buckets:
            for e in buckets[key]:
                if colliderect(e.rect):
                    result_add(e)
    return result


def collidedict(space, entities):
    result = {}
    buckets = space[BUCKETS]
    size = space[SIZE]
    cache = space[CACHE]
    colls = []
    append = list.append
    for e in entities:
        rect = e.rect
        x = rect[0]
        y = rect[1]
        left = x // size
        top = y // size
        right = (x + rect[2]) // size
        bottom = (y + rect[3]) // size
        key = left, top, right, bottom
        if key in cache:
            bucket_ids = cache[key]
        else:
            xr = range(left, right + 1)
            yr = range(top, bottom + 1)
            bucket_ids = tuple((x, y) for x in xr for y in yr)
            cache[key] = bucket_ids
        e_colliderect = rect.colliderect
        for key in bucket_ids:
            if key in buckets:
                for o in buckets[key]:
                    if e_colliderect(o.rect):
                        if o in result:
                            append(result[o], e)
                        else:
                            append(colls, o)
        if colls:
            if e in colls:
                colls.remove(e)
            if colls:
                result[e] = colls
                colls = []
    return result


def collidealldict(space):
    pass


def collidepairs(space, entities):
    result = []
    buckets = space[BUCKETS]
    size = space[SIZE]
    cache = space[CACHE]
    result_append = result.append
    for e in entities:
        rect = e.rect
        x = rect[0]
        y = rect[1]
        left = x // size
        top = y // size
        right = (x + rect[2]) // size
        bottom = (y + rect[3]) // size
        key = left, top, right, bottom
        if key in cache:
            bucket_ids = cache[key]
        else:
            xr = range(left, right + 1)
            yr = range(top, bottom + 1)
            bucket_ids = tuple((x, y) for x in xr for y in yr)
            cache[key] = bucket_ids
        e_colliderect = rect.colliderect
        for key in bucket_ids:
            if key not in buckets:
                continue
            for o in buckets[key]:
                if e_colliderect(o.rect):
                    result_append((e, o))
    return result


def collideallpairs(space, others):
    pass


class Spacez(object):
    def __init__(self, name='anonymous', size=50, fast_remove=False):
        self._spacez = new(name, size, fast_remove)

    def add(self, *entities):
        self.addlist(entities)

    def addlist(self, entities):
        addlist(self._spacez, entities)

    def remove(self, entity):
        remove(self._spacez, entity)

    def get_entities(self):
        return entities(self._spacez)
    entities = property(get_entities)

    def get_count(self):
        return count(self._spacez)
    count = property(get_count)

    def clear(self):
        clear(self._spacez)

    def entities_in_rect(self, rect):
        return entities_in_rect(self._spacez, rect)

    def collidedict(self, entities):
        return collidedict(self._spacez, entities)

    def collidealldict(self):
        return collidealldict(self._spacez)

    def collidepairs(self, entities):
        return collidepairs(self._spacez, entities)

    def collideallpairs(self):
        return collideallpairs(self._spacez)


if __name__ == '__main__':
    from random import randrange
    import pygame
    from pygame.locals import *

    class Sprite(object):
        def __init__(self, rect):
            self.rect = Rect(rect)

    def main():
        pygame.init()
        screen = pygame.display.set_mode((640, 480))
        screen_rect = screen.get_rect()
        clock = pygame.time.Clock()
        max_fps = 30

        mouse_rect = Rect(0, 0, 75, 75)
        arranged_sprites = set([Sprite((x, y, 20, 20))
                               for x in range(0, screen_rect.w, 20 * 3)
                               for y in range(0, screen_rect.h, 20 * 3)])
        random_sprites = set(Sprite((randrange(0, screen_rect.w), randrange(0, screen_rect.h), 20, 20))
                             for i in range(25))
        space = new()
        addlist(space, arranged_sprites)

        display_colls = 1
        running = True

        while running:
            clock.tick(max_fps)
            for e in pygame.event.get():
                if e.type == QUIT:
                    running = False
                elif e.type == KEYDOWN:
                    if e.key == K_ESCAPE:
                        running = False
                    elif e.key == K_SPACE:
                        display_colls += 1
                        display_colls %= 2
                elif e.type == MOUSEMOTION:
                    mouse_rect.center = e.pos
            screen.fill((0, 0, 0))
            size = space[SIZE]
            for x, y in space[BUCKETS]:
                pygame.draw.rect(screen, Color('grey25'), (x * size, y * size, size, size), 1)
            if display_colls == 0:
                # entities_in_rect: mouse vs hash
                colls = entities_in_rect(space, mouse_rect)
                for e in arranged_sprites - colls:
                    pygame.draw.rect(screen, Color('green'), e.rect, 1)
                for e in colls:
                    pygame.draw.rect(screen, Color('red'), e.rect, 1)
            elif display_colls == 1:
                # collidepairs: random vs hash
                colls = collidepairs(space, random_sprites)
                for e in arranged_sprites - set([c[1] for c in colls]):
                    pygame.draw.rect(screen, Color('green'), e.rect, 1)
                for e in random_sprites - set([c[0] for c in colls]):
                    pygame.draw.rect(screen, Color('yellow'), e.rect, 2)
                for rsprite, asprite in colls:
                    pygame.draw.rect(screen, Color('orange'), rsprite.rect, 2)
                    pygame.draw.rect(screen, Color('orange'), asprite.rect, 1)
            pygame.draw.rect(screen, Color('white'), mouse_rect, 1)
            pygame.display.flip()

    main()
