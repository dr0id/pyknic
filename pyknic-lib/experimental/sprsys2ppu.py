# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

import pygame

from pyknic.mathematics import Point2, Vec2
from pyknic.pygame import spritesystem2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

SCREENSIZE = 1024, 768


def main():
    pygame.init()
    # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    screen = pygame.display.set_mode(SCREENSIZE)

    cam_position = Point2(0, 0)
    cam_100x100_ppu1 = spritesystem2.Camera(cam_position, pygame.Rect(10, 10, 100, 100))
    cam_200x200_ppu1 = spritesystem2.Camera(cam_position, pygame.Rect(120, 10, 200, 200))
    cam_100x100_ppu5 = spritesystem2.Camera(cam_position, pygame.Rect(10, 220, 100, 100))
    cam_200x200_ppu10 = spritesystem2.Camera(cam_position, pygame.Rect(120, 220, 300, 300))

    cams = [cam_100x100_ppu1, cam_200x200_ppu1, cam_100x100_ppu5, cam_200x200_ppu10]
    ppus = [1.0, 1.0, 5.0, 15.0]

    world_pos = Point2(0, 0)
    step = 5

    running = True
    while running:
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    cam_position.x += step
                elif event.key == pygame.K_LEFT:
                    cam_position.x -= step
                elif event.key == pygame.K_UP:
                    cam_position.y -= step
                elif event.key == pygame.K_DOWN:
                    cam_position.y += step

        # draw

        # sprite
        for idx, cam in enumerate(cams):
            # move cam so it points to the given world position
            cam.position = cam_position

            ppu = ppus[idx]
            screen.set_clip(cam.rect)

            # screen_pos = cam.world_to_screen(world_pos, parallax_factors=Vec2(0, 0), offset_in_pixels=Vec2(-50, -50), ppu=ppu)
            # pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(screen_pos.as_tuple(), (20 * ppu, 20 * ppu)), 0)

            # screen_pos = cam.world_to_screen(world_pos, parallax_factors=Vec2(0.5, 0.5), ppu=ppu)
            screen_pos = cam.world_to_screen(world_pos, ppu=ppu)
            pygame.draw.rect(screen, (150, 255, 255), pygame.Rect(screen_pos.as_tuple(), (20 * ppu, 20 * ppu)), 0)
            pygame.draw.rect(screen, (0, 0, 255), pygame.Rect(screen_pos.as_tuple(), (cam.world_to_screen(world_pos+Vec2(20, 20), ppu=ppu)- screen_pos).as_tuple()), 2)

            # coord axis at origin
            origin = cam.world_to_screen(Point2(0, 0), ppu=ppu).as_tuple()
            pygame.draw.line(screen, (255, 0, 0), origin, cam.world_to_screen(Point2(10, 0), ppu=ppu).as_tuple(), 2)
            pygame.draw.line(screen, (0, 255, 0), origin, cam.world_to_screen(Point2(0, 10), ppu=ppu).as_tuple(), 2)


            screen.set_clip(None)


            # viewport rect
            pygame.draw.rect(screen, (255, 255, 0), cam.rect, 1)

        img = pygame.Surface(cams[-2].rect.size)
        img.blit(screen, (0, 0), cam_100x100_ppu5.rect)
        img = pygame.transform.rotozoom(img, 0.0, ppus[-1] / ppus[-2])

        screen.blit(img, (cams[-1].rect.right + 10, cams[-1].rect.y))

        pygame.display.flip()
        screen.fill((255, 0, 255))

    pygame.quit()


if __name__ == '__main__':
    main()
