# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division
from math import sin
from random import randint as ri, choice
import pygame

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREENSIZE = SCREEN_WIDTH, SCREEN_HEIGHT


def r255():
    return ri(0, 255)


def main():
    pygame.init()
    # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    screen = pygame.display.set_mode(SCREENSIZE)
    rect = screen.get_rect()
    rect.x = rect.w // 2
    rect.y = rect.h // 2

    step = 5
    angle_step = 2
    angle = 0

    zoom = 1.0
    zoom_step = 0.1

    virtual = pygame.Surface((rect.w*2, rect.h*2)).convert()
    for i in range(100):
        pygame.draw.rect(virtual, (r255(), r255(), r255()), pygame.Rect(ri(0, rect.w*2), ri(0, rect.h*2), ri(0, 100), ri(0, 100)), 0)

    rot = pygame.transform.rotozoom(virtual, angle, zoom)

    clock = pygame.time.Clock()
    count = 0
    running = True
    while running:
        clock.tick()
        print(clock.get_fps())
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    rect.x += step
                elif event.key == pygame.K_LEFT:
                    rect.x -= step
                elif event.key == pygame.K_UP:
                    rect.y -= step
                elif event.key == pygame.K_DOWN:
                    rect.y += step
                elif event.key == pygame.K_a:
                    angle -= angle_step
                    # rot = pygame.transform.rotozoom(virtual, angle, zoom)
                elif event.key == pygame.K_q:
                    angle += angle_step
                    # rot = pygame.transform.rotozoom(virtual, angle, zoom)
                elif event.key == pygame.K_w:
                    zoom += zoom_step
                    # rot = pygame.transform.rotozoom(virtual, angle, zoom)
                elif event.key == pygame.K_s:
                    zoom -= zoom_step
                    # rot = pygame.transform.rotozoom(virtual, angle, zoom)


        # draw
        count += 1
        if count > 15:
            rot = pygame.transform.rotozoom(virtual, angle, zoom)
            # rot = pygame.transform.scale(virtual, (int(rect.w * zoom), int(rect.h * zoom)))
            count = 0
        # rot = pygame.transform.rotate(virtual, angle)
        screen.blit(rot, (0, 0), rect)

        pygame.display.flip()
        screen.fill((255, 0, 255))

    pygame.quit()


def main2():
    pygame.init()
    # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    screen = pygame.display.set_mode(SCREENSIZE)
    rect = screen.get_rect()
    # start position in world, where the cam looks at
    # rect.x = rect.w // 4
    # rect.y = rect.h // 4

    rot_scaled = screen.copy()

    step = -5
    angle_step = 2
    angle = 0

    zoom = 1.0
    zoom_step = 0.05

    virtual = pygame.Surface((rect.w*2, rect.h*2)).convert()

    tracked = None
    sprites = []
    for i in range(1000):
        spr_rect = pygame.Rect(ri(0, rect.w*4), ri(0, rect.h*4), ri(0, 100), ri(0, 100))
        spr = pygame.sprite.Sprite()
        spr.image = pygame.Surface(spr_rect.size)
        spr.image.fill((r255(), r255(), r255()))
        spr.rect = spr_rect
        spr.vel = choice([-1, 0, 1])
        if spr.vel != 0:
            tracked = spr
        sprites.append(spr)

    clock = pygame.time.Clock()
    running = True
    while running:
        dt = clock.tick()
        print(clock.get_fps())
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    rect.x += step
                elif event.key == pygame.K_LEFT:
                    rect.x -= step
                elif event.key == pygame.K_UP:
                    rect.y -= step
                elif event.key == pygame.K_DOWN:
                    rect.y += step
                elif event.key == pygame.K_a:
                    angle = change_angle(angle, -angle_step)
                elif event.key == pygame.K_q:
                    angle = change_angle(angle, angle_step)
                elif event.key == pygame.K_w:
                    virtual, zoom = change_zoom(rect, virtual, zoom, zoom_step)
                elif event.key == pygame.K_s:
                    virtual, zoom = change_zoom(rect, virtual, zoom, -zoom_step)

        # angle = change_angle(angle, 1)
        # virtual, zoom = change_zoom(rect, virtual, zoom, zoom_step * sin(angle / 6))
        # rect.center = tracked.rect.center

        # draw
        _screen = screen
        if zoom != 1.0 or angle != 0.0:
            virtual.fill((0, 0, 0))
            _screen = virtual

        # update sprites and draw them
        for spr in sprites:
            spr.rect.x += dt * 0.3 * spr.vel
            if spr.rect.x > SCREENSIZE[0] * 4:
                spr.rect.x = 0
            if spr.rect.x < 0:
                spr.rect.x = SCREENSIZE[0] * 4
            _screen.blit(spr.image, spr.rect.move(rect.topleft))

        if zoom != 1.0 or angle != 0.0:
            pygame.transform.scale(virtual, SCREENSIZE, rot_scaled)
            rot = pygame.transform.rotate(rot_scaled, angle)
            screen.blit(rot, (0, 0))


        pygame.display.flip()
        screen.fill((255, 0, 255))

    pygame.quit()


def change_angle(angle, angle_step):
    angle += angle_step
    return angle


def change_zoom(rect, virtual, zoom, zoom_step):
    if zoom > 2 * zoom_step:
        zoom += zoom_step
    zoom_x = int(rect.w * 2 / zoom)
    zoom_y = int(rect.h * 2 / zoom)
    # need to limit to a max size otherwise out of memory can happen very easily
    zoom_x = zoom_x if zoom_x < SCREEN_WIDTH * 4 else SCREEN_WIDTH * 4
    zoom_y = zoom_y if zoom_y < SCREEN_HEIGHT * 4 else SCREEN_HEIGHT * 4
    virtual = pygame.transform.scale(virtual, (zoom_x, zoom_y))
    return virtual, zoom


if __name__ == '__main__':
    main2()
