# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

import pygame

from pyknic.mathematics import Point2, Vec2
from pyknic.pygame import spritesystem2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

SCREENSIZE = 800, 600


def main():
    pygame.init()
    # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    screen = pygame.display.set_mode(SCREENSIZE)

    view_port = pygame.Rect(100, 100, 200, 200)
    cam = spritesystem2.Camera(Point2(0, 0), view_port)

    world_pos = Point2(0, 0)
    step = 5
    angle_step = 1

    running = True
    while running:
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    cam.position.x += step
                elif event.key == pygame.K_LEFT:
                    cam.position.x -= step
                elif event.key == pygame.K_UP:
                    cam.position.y -= step
                elif event.key == pygame.K_DOWN:
                    cam.position.y += step
                elif event.key == pygame.K_a:
                    cam.angle += angle_step
                elif event.key == pygame.K_q:
                    cam.angle -= angle_step

        # draw

        # sprite
        screen_pos = cam.world_to_screen(world_pos, parallax_factors=Vec2(0, 0), offset_in_pixels=Vec2(-100, -100))
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(screen_pos.as_tuple(), (20, 20)), 0)

        screen_pos = cam.world_to_screen(world_pos, parallax_factors=Vec2(0.5, 0.5))
        pygame.draw.rect(screen, (150, 255, 255), pygame.Rect(screen_pos.as_tuple(), (20, 20)), 0)

        # coord axis at origin
        origin = cam.world_to_screen(Point2(0, 0)).as_tuple()
        pygame.draw.line(screen, (255, 0, 0), origin, cam.world_to_screen(Point2(10, 0)).as_tuple(), 1)
        pygame.draw.line(screen, (0, 255, 0), origin, cam.world_to_screen(Point2(0, 10)).as_tuple(), 1)

        offset = Vec2(30, 30)
        origin = cam.world_to_screen(Point2(0, 0), offset_in_pixels=offset).as_tuple()
        pygame.draw.line(screen, (255, 255, 0), origin, cam.world_to_screen(Point2(10, 0), offset_in_pixels=offset).as_tuple(), 1)

        # viewport rect
        pygame.draw.rect(screen, (255, 255, 0), view_port, 1)

        pygame.display.flip()
        screen.fill((255, 0, 255))

    pygame.quit()


if __name__ == '__main__':
    main()
