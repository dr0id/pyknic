# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division
import random
import timeit

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


HANDLED = True
UNHANDLED = False
NEW_FIRST = 0
NEW_LAST = -1
USE_EVENT_TYPE_ORDER = None


class Signal(object):
    """
    A signal object. It is for event dispatching. It saves a list of
    observers and when fired, it calls the observers. The handler method
    has to accept the exact same number of arguments that are given to
    the fire method. Or it should make use of the args and kwargs arguments.
    The return type of a handler is important. When returning True or any value
    that evaluates to True in a if statement, the signal stops calling further
    observers. Returning False or None (as default behavior of methods without a
    return), it will call further handler methods.

    The order in which the handler methods will be called can be defined
    when the signal is instanciated. The sort order can only have one of
    these values:

     * Signal.`NEW_FIRST`
     * Signal.`NEW_LAST`


    Default is NEW_FIRST. This means that handlers added later are called before
    the older ones::

       s = Signal()
       s.add(handler1)
       s.add(handler2)
       s.fire(sender) # call order: handler2, handler1

    This is important and useful, when imitating a push behavior. If handler2
    returns a `HANDLED` then handler1 wont get be called.

    Using the `NEW_LAST` order for event handler leads to this::

       s = Signal()
       s.add(handler1)
       s.add(handler2)
       s.fire(sender) # call order: handler1, handler2

    This is useful for a draw event. A convinient way to draw is in a back to
    front manner. Adding first the background and then the other things on top
    will draw the things in the correct order.

    This Signal implementation is iteration safe. It is possible to remove
    the handler from within the handler without getting in trouble.

    :Warning:
        Any class that has a method added will be kept alive by this reference.
        To allow the class to die it has to remove the handler. See `WeakSignal`
        for a implementation using weak references.

    :Ivariables:
        name : string
            Either the string that was given or None.
        enabled : bool
            If this Signal fires or not.
        cur_event_list : list
            current list of methods to call during firing. Only used if chaining
            signals.

    :Cvariables:
        NEW_FIRST : int
            Used to define the call order. New handlers are added in front of
            the others and therefore called before the others.
        NEW_LAST : int
            Used to define the call order. New handlers are appended
            and therefore called after the others.

    :Note:
        If you get trouble with the recursion limit then try to avoid the
        shortcuts::

            sig += sig2 # use sig += sig2.fire instead
            sig -= sig2 # use sig -= sig2.fire instead

        This should double the available recursion depth due to less function
        calls done.

    """

    _ADD, _RMV, _CLR = list(range(3))


    def __init__(self, name=None, sort_order = NEW_FIRST):
        """
        Constructor.

        :Parameters:
            name : string
                Optional. A name to identify the signal easier when debugging.
            sort_order : Constant
                Either NEW_FIRST or NEW_LAST. Defines the order in
                which the handler are called. Defaults to NEW_FIRST.

        """
        # -- public -- #
        if name:
            self.name = name
        else:
            self.name = hex(id(self))
        self.enabled = True
        self.has_observers = 0
        self.fire = self._fire_normal
        # -- protected -- #
        # observers
        self._observers = []
        # sortorder
        self._sort_oder = sort_order
        # commands
        self._commands = []

    def add(self, obs):
        """
        Adds a handler to the signal.

        :Note:
            Shortcut::

                sig += obs

        :Parameters:
            obs : callable
                A handler to add, has to be callable.
        """
        self.fire = self._fire_changed
        self._commands.append((self._ADD, obs))
        self.has_observers += 1
        return self

    def remove(self, obs):
        """
        Removes a handler.

        :Note:
            Shortcut::

                sig -= obs

        :Parameters:
            obs : callable
                The handler to be removed.
        """
        self.fire = self._fire_changed
        self._commands.append((self._RMV, obs))
        self.has_observers -= 1
        self.has_observers = 0 if self.has_observers < 0 else self.has_observers
        return self

    def _fire_normal(self, *args, **kwargs):
        if self.enabled:
            for observer in self._observers:
                if observer(*args, **kwargs):
                    return HANDLED
        return UNHANDLED

    def _fire_changed(self, *args, **kwargs):
        """
        The fire method used if changes have occured, calls _fire_normal.
        """
        self._sync()
        return self.fire(*args, **kwargs)

    def fire(self, *args, **kwargs):
        """
        Fires the signal with any arguments.

        :Note:
            Shortcut::

                sig(sender, *args, **kwargs)

            Be aware of recursion, see class introduction.

        :Parameters:
            sender : object
                The sender of this event.
            args : args
                Arguments list.
            kwargs : kwargs
                Named arguments, a dict.

        :rtype: True when a handler returns `HANDLED` , else False
        """
        # its here for documentation, actual implementation see _fire_changed and _fire_normal
        pass

    def clear(self):
        """
        Removes all handlers from the signal.
        """
        self.has_observers = 0
        self.fire = self._fire_changed
        self._commands.append((self._CLR, []))

    def _sync(self):
        """
        Only used internaly. This method is for synchronizing
        the added or removed observers.
        """
        # self._changed = False
        while self._commands:
            cmd, obs = self._commands.pop(0)
            if cmd == self._ADD and obs not in self._observers:
                if self._sort_oder == NEW_FIRST:
                    self._observers.insert(0, obs) # start
                else:
                    self._observers.append(obs) # end
            elif cmd == self._RMV:
                if obs in self._observers:
                    self._observers.remove(obs)
            elif cmd == self._CLR:
                self._observers = []
                self._commands = []
        self.fire = self._fire_normal

    def __len__(self):
        """
        Returns the number of handlers. Schould not be called from within a
        handler of this event.
        """
        return self.has_observers
        # self._sync()
        # return len(self._observers)

    def __str__(self):
        return '<%s(\'%s\') %s at %s>' % (self.__class__.__name__,
                                       self.name,
                                       self._observers,
                                       hex(id(self)))

    # convinients (but slow)
    __iadd__ = add
    __isub__ = remove


class FastSignal(object):

    def __init__(self):
        self.observers = []

    def add(self, observer):
        self.observers.append(observer)

    def fire(self, *args, **kwargs):
        # make copy to avoid observer removing itself -> list changed exception
        for obs in list(self.observers):
            obs(*args, **kwargs)

    def remove(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)

class AttrSprite(object):

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self.x = 0
        self.y = 0

class SetAttrSprite(object):

    dirty = set()

    def __init__(self):
        self.x = 0
        self.y = 0

    def __setattr__(self, key, value):
        object.__setattr__(self, key, value)
        SetAttrSprite.dirty.add(self)


class PropSprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        # self.old_x = self._x
        self._x = value
        PropSprite.dirty.add(self)

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        # self.old_y = self._y
        self._y = value
        PropSprite.dirty.add(self)

    y = property(_get_y, _set_y)

class SigSprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0
        self.event_dirty = FastSignal()

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        self._x = value
        SigSprite.dirty.add(self)
        self.event_dirty.fire()

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        self._y = value
        SigSprite.dirty.add(self)
        self.event_dirty.fire()

    y = property(_get_y, _set_y)

class Sig1Sprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0
        self.children = []

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        dx = value - self._x
        self._x = value
        Sig1Sprite.dirty.add(self)
        for c in self.children:
            c.x += dx

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        dy = value - self._y
        self._y = value
        Sig1Sprite.dirty.add(self)
        for c in self.children:
            c.y += dy

    y = property(_get_y, _set_y)

class Sig2Sprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0
        self.children = []

    def set_dirty(self):
        PropSprite.dirty.add(self)

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        self.old_x = self._x
        self._x = value
        Sig2Sprite.dirty.add(self)
        for c in self.children:
            c.x += value - self.old_x

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        self.old_y = self._y
        self._y = value
        Sig2Sprite.dirty.add(self)
        for c in self.children:
            c.y += value - self.old_y

    y = property(_get_y, _set_y)

class Sig3Sprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0
        self.children = []

    def set_dirty(self):
        PropSprite.dirty.add(self)

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        self.old_x, self._x = self._x, value
        Sig3Sprite.dirty.add(self)
        for c in self.children:
            c.x += value - self.old_x

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        self.old_y, self._y = self._y, value
        Sig3Sprite.dirty.add(self)
        for c in self.children:
            c.y += value - self.old_y

    y = property(_get_y, _set_y)

class Sig4Sprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0
        self.event_dirty = Signal()

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        self._x = value
        SigSprite.dirty.add(self)
        if self.event_dirty.has_observers:
            self.event_dirty.fire()

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        self._y = value
        SigSprite.dirty.add(self)
        if self.event_dirty.has_observers:
            self.event_dirty.fire()

    y = property(_get_y, _set_y)

class Sig5Sprite(object):

    dirty = set()

    def __init__(self):
        self.old_x = 0
        self.old_y = 0
        self._x = 0
        self._y = 0
        self._rel_x = 0
        self._rel_y = 0
        self.children = []

    def _get_x(self):
        return self._x

    def _set_x(self, value):
        self._x = value
        Sig1Sprite.dirty.add(self)
        for c in self.children:
            c.x = value + c._rel_x

    x = property(_get_x, _set_x)

    def _get_y(self):
        return self._y

    def _set_y(self, value):
        self._y = value
        Sig1Sprite.dirty.add(self)
        for c in self.children:
            c.y = value + c._rel_y

    y = property(_get_y, _set_y)


class PPSprite(object):

    dirty = set()

    def __init__(self):
        self._x = 0
        self._y = 0
        self._parent = None
        self.event_pos_x = Signal()
        self.event_pos_y = Signal()

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        if self._parent is not None:
            self._parent.event_pos_x.remove(self._x_observer)
            self._parent.event_pos_y.remove(self._y_observer)
        self._parent = value
        if value is not self:
            value.event_pos_x.add(self._x_observer)
            value.event_pos_y.add(self._y_observer)

    def _x_get(self):
        return self._x

    def _x_set(self, value):
        self._x = value
        PPSprite.dirty.add(self)

    x = property(_x_get, _x_set)

    def _y_get(self):
        return self._y

    def _y_set(self, value):
        self._y = value
        PPSprite.dirty.add(self)

    y = property(_y_get, _y_set)

    def _x_observer(self, sender, value):
        self._x = value
        PPSprite.dirty.add(self)

    def _y_observer(self, sender, value):
        self._y = value
        PPSprite.dirty.add(self)


class TheSprite(object):

    dirty = set()

    def __init__(self):
        self._x = 0
        self._y = 0
        self._parent = None
        self.event_pos_x = Signal()
        self.event_pos_y = Signal()

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        if self._parent is not None:
            self._parent.event_pos_x.remove(self._x_observer)
            self._parent.event_pos_y.remove(self._y_observer)
        self._parent = value
        if value is not self:
            value.event_pos_x.add(self._x_observer)
            value.event_pos_y.add(self._y_observer)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value
        TheSprite.dirty.add(self)

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value
        TheSprite.dirty.add(self)

    def _x_observer(self, sender, value):
        self._x = value
        TheSprite.dirty.add(self)

    def _y_observer(self, sender, value):
        self._y = value
        TheSprite.dirty.add(self)




count = 10
asprites = [AttrSprite() for i in range(count)]


dirty_count = 3
indices = [random.randint(0, count-1) for i in range(dirty_count)]

def test_attr_sprites(indices, sprites):
    for idx in indices:
        spr = sprites[idx]
        spr.old_x = spr.x
        spr.old_y = spr.y
        spr.x = idx
        spr.y = idx

    return [spr for spr in asprites if spr.x != spr.old_x or spr.y != spr.old_y]

def test_prop_sprites(indices, sprites):
    for idx in indices:
        spr = sprites[idx]
        spr.x = idx
        spr.y = idx

    return PropSprite.dirty

def main():
    def wrapper(func, *args, **kwargs):
        def wrapped():
            return func(*args, **kwargs)

        return wrapped
    print('attr    ', timeit.timeit(wrapper(test_attr_sprites, indices, asprites)))
    print('property', timeit.timeit(wrapper(test_prop_sprites, indices, [PropSprite() for i in range(count)])))
    print('property2', timeit.timeit(wrapper(test_prop_sprites, indices, [PPSprite() for i in range(count)])))
    print('the sprite', timeit.timeit(wrapper(test_prop_sprites, indices, [TheSprite() for i in range(count)])))
    print('setattrsprite', timeit.timeit(wrapper(test_prop_sprites, indices, [SetAttrSprite() for i in range(count)])))
    print('sig sprites', timeit.timeit(wrapper(test_prop_sprites, indices, [SigSprite() for i in range(count)])))
    print('sig1 sprites', timeit.timeit(wrapper(test_prop_sprites, indices, [Sig1Sprite() for i in range(count)])))
    print('sig2 sprites', timeit.timeit(wrapper(test_prop_sprites, indices, [Sig2Sprite() for i in range(count)])))
    print('sig3 sprites', timeit.timeit(wrapper(test_prop_sprites, indices, [Sig3Sprite() for i in range(count)])))
    print('sig4 sprites', timeit.timeit(wrapper(test_prop_sprites, indices, [Sig4Sprite() for i in range(count)])))
    print('sig5 sprites', timeit.timeit(wrapper(test_prop_sprites, indices, [Sig5Sprite() for i in range(count)])))


if __name__ == '__main__':
    main()
