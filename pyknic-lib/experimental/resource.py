# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function, division

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class LoaderForResourceTypeAlreadyExistsError(Exception):
    pass


class Resource(object):

    def __init__(self):
        self._loaders = {}  # {res_type: loader}

    def register_loader(self, res_type, name, loader):
        if res_type in self._loaders:
            raise LoaderForResourceTypeAlreadyExistsError("loader already exists for res type " + str(res_type))
        setattr(self, name, loader)
        loader.resources = self
        self._loaders[res_type] = loader

    def un_register_loader(self, loader):
        pass

    def load(self, res_type, *args, **kwargs):
        res = self._get(res_type, *args, **kwargs)
        if res:
            return id

        loader = self._loaders.get(res_type, None)
        if loader:
            res = loader.get_resource(*args, **kwargs)
            # TODO: add to cache
            return id


    def get_resource(self, res_id):
        pass

    def _get(self, res_type, *args, **kwargs):
        pass


class AbstractLoader(object):

    def __init__(self, res_type):
        self.res_type = res_type
        self.resources = None

    def load(self, *args, **kwargs):
        pass

    def get_resource(self, *args, **kwargs):
        pass


class Loader(AbstractLoader):

    def __init__(self, res_type):
        super(Loader, self).__init__(res_type)

    def load(self, name, w, h):
        self.resources.load(self.res_type, name, w, h)

    def _load(self, name, w, h):
        # load from disk and return resource
        pass

    def get_resource(self, name, w, h):
        pass

